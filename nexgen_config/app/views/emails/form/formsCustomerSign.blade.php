<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h4>Hi {{$contactPerson}},</h4>

		<div>
			<p>Some email content here</p>
			<a href="http://staging.nexgenconfigapp.xyz/appredirect/{{$userEmail}}/{{$editId}}">http://staging.nexgenconfigapp.xyz/appredirect/{{$userEmail}}/{{$editId}}</a>
      	<p>Kind Regards</p>
		  <p>Nexgen Australia Pty Ltd</p>
		</div>
	</body>
</html>
