<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h4>Hi {{$user}}</h4>

		<div>
		<p>Thank you for the opportunity to discuss your business communication and hardware needs.</p>

		<p>Please find the attached proposal for all items as discussed. </p>

		<p>Feel free to contact me any time with any questions you have otherwise I will be in touch shortly.</p>

		<p>Kind Regards,</p>
		<p>{{$salesRepName}}</p>

		</div>
	</body>
</html>
