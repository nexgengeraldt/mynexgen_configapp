
<!DOCTYPE html>
<html ng-app="qms">


  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Configapp</title>

  

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/angular/font-awesome/css/font-awesome.css">
	<style>

    @font-face {
        font-family: 'Helvetica Neue Medium';
        font-style: normal;
        font-weight: normal;
        src: local('Helvetica Neue Medium'), url('/assets/fonts/HelveticaNeue-Medium.woff') format('woff');
    }

		
		body {
            font-family: 'Helvetica Neue Medium';
            background: #FFFFFF url(/assets/images/sitebg-home.png) center top;
            padding: 0;
            margin: 0;
		}
        h1 {
            color: #e31e25;
            font-size: 40px;
        }
        i {
            margin: 0 auto;
            display: block !important;
            color: #e31e25;
            text-align: center;
            font-size: 35px !important;
        }
        ul {
            margin-top: 20px;
        }
        a {
            color: #000000;
        }
	</style>
	
  </head>
<body >
    <div class="404-content">
        <h1 style="text-align: center;margin-top: 150px;">404 Page Not Found!</h1>
        <a href="/"><img style="text-align: center;display: block;margin: 0 auto;" src="/assets/images/logo.png" alt="" srcset="">
        <ul class="list-inline text-center mt-5"><li><i class="fa fa-arrow-circle-left"></i><p>Go back</p></li></ul></a>
    </div>
</body>

</html>
