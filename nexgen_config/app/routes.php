<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/','HomeController@index');
Route::post('/feedbacks/upload','FeedbacksController@upload');
Route::post('/schedule_goods/upload','ScheduleGoodsController@upload');
Route::post('/schedule_goods/uploads','ScheduleGoodsController@uploads');
Route::post('/proposal/upload','ProposalsController@upload');
Route::get('/category/{id}/{level}','CategoryController@show');
Route::resource('product', 'ProductController');
Route::resource('propose', 'ProposeController');
Route::resource('feedbacks', 'FeedbacksController');
Route::resource('schedule_goods','ScheduleGoodsController');
Route::resource('category', 'CategoryController');
Route::resource('rates', 'RatesController');
Route::resource('financetype', 'FinanceTypeController');
Route::resource('typet', 'TypetController');
Route::resource('company', 'CompanyController');
Route::resource('salesrep', 'SalesRepController');
Route::resource('telemarketer', 'TelemarketerController');
Route::resource('winback', 'WinBackController');
Route::resource('leadsource', 'LeadSourceController');
Route::resource('user', 'UserController');
Route::resource('config', 'ConfigController');
Route::resource('sellprice', 'SellPriceController');
Route::resource('rentalprice', 'RentalPriceController');
Route::resource('flexirent', 'FlexiRentController');
Route::resource('proposal', 'ProposalsController');
Route::resource('dashboard', 'DashboardController');
Route::resource('Jobsheets', 'JobsheetsController');
Route::get('report{id}', array('uses' => 'HomeController@showReport'));
Route::get('login', array('uses' => 'HomeController@showLogin'));
Route::post('login', array('uses' => 'HomeController@doLogin'));
Route::get('logout', array('uses' => 'HomeController@doLogout'));
Route::get('profile', array('uses' => 'HomeController@getCurrentUser'));
Route::post('/form/create-pdf', array('uses'=> 'FormController@createPdf'));
Route::post('/form/save-data', array('uses'=> 'FormController@saveData'));
Route::post('/form/show-data', array('uses'=> 'FormController@showForms'));
Route::post('/form/show-sales-rep-form-data', array('uses'=> 'FormController@showSaleRepForms'));
Route::post('/form/delete-data', array('uses'=> 'FormController@deleteForm'));
Route::post('/form/send-customer-email', array('uses'=> 'FormController@sendCustomerEmail'));

Route::post('/form/download-forms', array('uses'=> 'FormController@downloadForms'));
Route::post('/form/get-schedule-goods', array('uses'=> 'FormController@getScheduleGoods'));
Route::post('/form/get-rates', array('uses'=> 'FormController@getRates'));
Route::post('/form/view-referals',array('uses'=>'FormController@viewReferals'));
Route::post('/form/view-all',array('uses'=>'FormController@viewAll'));
Route::post('/form/to-csv',array('uses'=>'FormController@toCsv'));
Route::post('/form/send-referrals',array('uses'=>'FormController@sendReferrals'));
Route::post('frontier/qualify',array('uses'=>'SQController@getServiceQualification'));
Route::post('frontier/get-addresses',array('uses'=>'SQController@getLocationIds'));
Route::post('frontier/get-advance',array('uses'=>'SQController@getLocationId'));
Route::post('frontier/get-fnn',array('uses'=>'SQController@getServiceQualification'));
Route::post('frontier/sqCsv',array('uses'=>'SQController@to_Csv'));
Route::post('/form/show-form', array('uses'=> 'FormController@showPaycorp'));
Route::post('/get-token', array('uses'=> 'FormController@getToken'));
Route::post('/form/get-paycorp-token',array('uses'=>'FormController@getPaycorpToken'));
Route::post('/proposals/create-pdf', array('uses'=> 'ProposalsController@createPdf'));
Route::post('/proposals/show-data', array('uses'=> 'ProposalsController@showProposals'));
Route::post('/proposals/show-sales-rep-proposal-data', array('uses'=> 'ProposalsController@showSalesRepProposals'));
Route::post('/proposals/save-data',array('uses'=>'ProposalsController@saveData'));
Route::post('/proposals/get-schedule-goods',array('uses'=>'ProposalsController@getScheduleGoods'));
Route::post('/cloud/save-data', array('uses'=> 'CloudController@saveData'));
Route::post('/cloud/create-pdf', array('uses'=> 'CloudController@createPdf'));
Route::post('/cloud/get-schedule-goods', array('uses'=> 'CloudController@getScheduleGoods'));
Route::post('/cloud/show-data', array('uses'=> 'CloudController@showClouds'));
Route::post('/cloud/delete-data', array('uses'=> 'CloudController@deleteCloud'));
Route::post('/cloud/download-forms', array('uses'=> 'CloudController@downloadForms'));
Route::post('/proposals/delete-data',array('uses'=>'ProposalsController@deleteForm'));
Route::post('/proposals/download-forms', array('uses'=> 'ProposalsController@downloadForms'));
Route::post('/dashboard/check-move', array('uses'=> 'DashboardController@checkMove'));
Route::post('/dashboard/create-month-sales', array('uses'=> 'DashboardController@createMonthSales'));
Route::post('/dashboard/user-list', array('uses'=> 'DashboardController@getUserList'));
Route::post('/dashboard/chart-formdata', array('uses'=> 'DashboardController@getChartFormData'));
Route::post('/dashboard/chart-proposaldata', array('uses'=> 'DashboardController@getChartProposalData'));
Route::post('/users/your-staff', array('uses'=> 'UserController@showUsers'));
Route::post('/users/pick-staff', array('uses'=> 'UserController@pickstaff'));
Route::post('/jobsheets/search-customer', array('uses'=> 'JobsheetsController@searchCustomer'));
Route::post('/jobsheets/show', array('uses'=> 'JobsheetsController@showJobsheets'));
Route::post('/jobsheets/save-data',array('uses'=>'JobsheetsController@saveData'));
Route::post('/jobsheets/download-forms', array('uses'=> 'JobsheetsController@downloadForms'));
Route::post('/jobsheets/create-pdf', array('uses'=> 'JobsheetsController@createPdf'));
Route::post('/jobsheets/upload','JobsheetsController@upload');
Route::post('/jobsheets/uploads','JobsheetsController@uploads');
Route::post('/jobsheets/uploaded-files','JobsheetsController@getAttached');
Route::get('/api/forms', 'ApiController@index');
Route::post('/customersign/show-data', array('uses'=> 'CustomerSignController@showForms'));
Route::post('/customersign/complete', array('uses'=> 'CustomerSignController@complete'));



Route::get('appredirect/{key}/{id}', function($key, $id){
    $ciphering = "AES-128-CTR"; 
    $decryption_iv = 'nexgen123452020!'; 
    $decryption_key = "nexgen2020!"; 
    $options = 0; 
    $user_email = openssl_decrypt ($key, $ciphering, $decryption_key, $options, $decryption_iv); 
    // $user_email = = decrypt($key,'your secret key');
    // now find the user
    $user = User::where('email',$user_email)->first();
    if($user){
          Auth::login($user); // login user automatically
          Blade::setEscapedContentTags('<$', '$>');
          Blade::setContentTags('<$$', '$$>');
          $version = Config::get('app.version');
        //   return View::make('index')->with('version',$version);
          return Redirect::to('/#/customerSign/show/'.$id);
    }else {
          return "User not found!";
    }
    
    });




App::missing(function($exception)
{
    return Response::view('404', array(), 404);
});