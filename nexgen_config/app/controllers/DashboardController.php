<?php

class DashboardController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$proposals = Proposals::all();
		return Response::json($proposals);
	}
	
	  /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
    
    public function checkMove()
	{
        $proposal_id = Request::input("proposal_id");
        $data = DB::table('form_data')->orderBy('created_at', "desc")
        ->where('proposal_id', $proposal_id)
        ->count();

        if($data == 0) {
            return Response::json(array('success' => true, "message"=>'Proposal saved successfully'));

        } else {
            return Response::json(array('success' => false,'message'=> 'You already have pending application.'));
        }
    }

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function createMonthSales()
	{

		$yrNow = date("Y");
		for ($x = 1; $x <= 12; $x++) {
			$data[] = DB::table('form_data')->orderBy('updated_at', "desc")
			->where('status', '=', 'Completed')
			->whereYear('updated_at', '=', $yrNow)
			->whereMonth('updated_at', '=',$x)
			->get();
		}
		foreach($data as $key => $row) {
			$key++;
			foreach($row as $data_row) {
				$yr = substr($data_row->updated_at, 0, 4);
				$sales = DashboardMonthlySales::where('form_id', '=', $data_row->id)->first();
				if(empty($sales)) {
					$a = unserialize($data_row->form_data);
					if(isset($a['aPayment']) && $a['aPayment'] != null) {
						$amount = preg_replace('/[^A-Za-z0-9. -]/', '', $a['aPayment']);
						$amount = str_replace(' ', '', $amount);
						if($a['aTerm'] == '60' ||  $a['aTerm'] == '48' ||  $a['aTerm'] == '36' ||  $a['aTerm'] == '24') {
							$amount = $amount * $a['aTerm'];
						}
					} else {
						$amount = 0;
					}
					$s = new DashboardMonthlySales();
					$s->form_id = $data_row->id;
					$s->user_id = $data_row->user_id;
					$s->year = $yr;
					$s->amount = $amount;
					$s->updated_at = $data_row->updated_at;
					$s->save();
				}
			}
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function getUserList()
	{
		$allusers = DB::table('users')->select('name','id')->distinct()->get();
		return Response::json(array('results' => $allusers));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function getChartFormData()
	{
		$user_id = Request :: input("user_id");
		$year = Request :: input("year");
		$d = array();

		if($user_id) {
			foreach($user_id as $row) {
				$sales = DB::table('users')
				->select('users.name', 'users.id')
				->where('id', '=', $row)
				->get();

				if($sales) {
					for ($x = 1; $x <= 12; $x++) {

						$m = DB::table('dashboard_monthly_sales')
						->select('amount as total')
						->where('user_id', '=',$row)
						->whereMonth('updated_at', '=', $x)
						->whereYear('updated_at', '=', $year)
						->get();

						$sum = array();
						$sum = 0;
						foreach($m as $num) {
							$sum += $num->total;
						}
						$total_sales[] = array($sum);
					}
					foreach($total_sales as $key => $ar) {
						$f[$row][] = $ar[0];
					}
					
					$k = array_chunk($f[$row],12);
					$l = end($k);
					foreach($l as $val) {
						$g[$row][] = $val; 
					}

					foreach($sales as $key => $s) {
						$d[] = array(
							'name' => $s->name,
							'data' => $g[$row]
						);
					}
				}
			}
		}
		if(empty($f)) {
			$d[] = array(
				'name' => '',
				'data' => []
			);
		}
		$draftProposals = Proposals::where('status', '=', 'Draft')
		->whereYear('updated_at', '=', $year)
		->count();

		$completedProposals = Proposals::where('status', '=', 'Completed')
		->whereYear('updated_at', '=', $year)
		->count();

		 return Response::json(array('chart' => $d, 'user_id' => $user_id));
	}

		/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function getChartProposalData()
	{
		$year = Request :: input("year");

		$draftProposals = Proposals::where('status', '=', 'Draft')
		->whereYear('updated_at', '=', $year)
		->count();

		$completedProposals = Proposals::where('status', '=', 'Completed')
		->whereYear('updated_at', '=', $year)
		->count();

		
		$draftForms = FormData::where('status', '=', 'Draft')
		->whereYear('updated_at', '=', $year)
		->count();

		$completedForms = FormData::where('status', '=', 'Completed')
		->whereYear('updated_at', '=', $year)
		->count();

		$data = DB::table('form_data')
		->where('status', '=', 'Completed')
		->whereYear('updated_at', '=', $year)
		->get();
		$sum = array();
		$sum = 0;
		foreach($data as $row) {
			$a = unserialize($row->form_data);
			if(isset($a['aPayment']) && $a['aPayment'] != null) {
				$amount = preg_replace('/[^A-Za-z0-9-]/', '', $a['aPayment']);
				$amount = str_replace(' ', '', $amount);
				if($a['aTerm'] == '60' ||  $a['aTerm'] == '48' ||  $a['aTerm'] == '36' ||  $a['aTerm'] == '24') {
					$amount = $amount * $a['aTerm'];
				}
			} else {
				$amount = 0;
			}
			 $sum += $amount;
		}

		$total_sales =  number_format( $sum,2);


		 return Response::json(array('draftProposals' => $draftProposals, 'completedProposals' => $completedProposals,'draftForms' => $draftForms, 'completedForms' => $completedForms, 'totalSales' => $total_sales));
	}


}
