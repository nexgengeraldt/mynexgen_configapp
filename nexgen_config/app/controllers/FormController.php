<?php

class FormController extends \BaseController
{
  public function createPdf()
  {
    set_time_limit(0);
    $forms = Request :: input("htmlData");
    $userDetails = Request :: input("userDetails");
    $account = Request :: input("account");
    $fromEmail = "noreply@nexgenconfigapp.xyz";
    $fromName = "Nexgen";
    $subject = 'Welcome to Nexgen';
    $toEmail = $userDetails["email"];
    $toName = $userDetails["name"];
    $accEmail = $account['email'];
    $accName = $account['name'];
    $mailData = [
        'user' => $toName,
        'userEmail' => $toEmail,
        'salesRepName' => $accName,
        'name' => $fromName,
        'email'=> $fromEmail
    ];

    $paths = array();

    if (Auth::user()) {
        $managerEmail = explode(";",Auth::user()->manageremail);
    }

    if (is_array($forms) || is_object($forms)) {
        foreach ($forms as $form) {
          $pdf = new \Mpdf\Mpdf([
              'mode' => 'utf-8',
              'format' => 'A4',
              'orientation' => 'P',

          ]);
          $pdf->WriteHTML($form['html']);
          $data = array("pdf" => $pdf->Output("assets/files/".$form['fileName']));
          $paths[] = "assets/files/".$form['fileName'];
        }
    }

    if (strpos($form['fileName'],'draft')!==false){
      $emailTemplate = 'emails.form.draftforms';
    } else {
      $emailTemplate = 'emails.form.forms';
    }
    
    // Mail::send($emailTemplate, $mailData, function($message) use ($subject, $paths, $accEmail, $managerEmail)
    // {
    //     $message->to($accEmail);
    //     if ($accEmail!='info@somelongcompany.com.au'){
    //       $message->bcc('JamesHarb@nexgen.com.au')
    //             ->bcc('ElieAyoub@nexgen.com.au')
    //             ->bcc('developer@businesstelecom.com.au')
    //             ->bcc('QA@nexgen.com.au');
    //     }
    //     else {
    //       $message->bcc('JamesHarb@nexgen.com.au')
    //             ->bcc('ElieAyoub@nexgen.com.au')
    //             ->bcc('developer@businesstelecom.com.au');
    //     }
        
    //     foreach ($managerEmail as $manager){
    //       $message->bcc($manager);
    //     }

    //     $message->subject($subject);

    //     foreach ($paths as $path) {
    //         $message->attach($path);
    //     }
    // });

    foreach ($forms as $form) {

            if(strstr($form['fileName'], "rental2")) {
              $removedate = str_replace('font-weight:300;font-size:8pt', 'font-weight:300;font-size:8pt;color: white;visibility: hidden;', $form['html']);
              $removeimage = str_replace('img class="imported" height="36" ', 'img class="imported" height="36" style="display: none"',$removedate);
              $final = str_replace('img class="imported ng-scope"', 'img class="imported ng-scope" style="display: none"',$removeimage);
              $pdf = new \Mpdf\Mpdf([
                  'mode' => 'utf-8',
                  'format' => 'A4',
                  'orientation' => 'P',
    
              ]);
              $pdf->WriteHTML($final);
              $data = array("pdf" => $pdf->Output("assets/files/".$form['fileName']));
              $path[] = "assets/files/".$form['fileName'];
              $emailTemplate = 'emails.form.formsRental2';
              
            //   Mail::send($emailTemplate, $mailData, function($message) use ($subject, $paths, $accEmail, $managerEmail)
            // {
            //     $message->to($accEmail);
    
            //     $message->subject($subject);
    
            //     foreach ($paths as $path) {
                  
            //         if(strstr($path, "rental2") ) {
            //           $message->attach($path);
            //           break;
            //         }
            //     }
            //   });

            }

    }

    if (Mail::failures()) {
        Log::debug(Mail::failures());
        return Response::json(array("success" => "fail"));
    } else {
        // return Response::json(array("success" => "success","managerEmail"=>$managerEmail,"message"=>$message));
        return Response::json(array("success" => "success","managerEmail"=>$managerEmail));
    }
  }

    public function saveData()
    {
        $proposal_id = Request :: input("proposal_id");
        $data = Request :: input("data");
        $user = Request :: input("user");
        $status = Request :: input("type");
        $ord_num = Request :: input("ord_num");


         //No credit card info to be saved in the server.
        isset($data["aCardNumber"]) ? $data["aCardNumber"] = "" : "";
        isset($data["aCardExpiry"]) ? $data["aCardExpiry"] = "" : "";
        isset($data["aCardName"]) ? $data["aCardName"] = "" : "";
        isset($data["aCardType"]) ? $data["aCardType"] = "" : "";

        $formData = serialize($data);
        $editId = Request :: input("editId");

        $fileName = serialize(Request::input("fileName"));

        if ($editId) {
            $fd = FormData::find($editId);
        } else {
            $fd = new FormData();
            $fd->user_id = $user;
        }

        try {
            if($fd->hash_id == "") {
              $fd->hash_id = sha1(rand(1, 999999999).$editId);
            }
            // $fd->user_id = $user;
            $fd->form_data = $formData;
            $fd->status = $status;
            $fd->files = $fileName;
            // $fd->ord_num = $ord_num;
            $fd->proposal_id = $proposal_id;

            $fd->save();

            if($status == 'Completed') {
              // For Jobsheet
              $company_info = array(
                'companyName' => $data['cName'],
                'address' => $data['pIAddress'],
                'suburb' => $data['pISuburb'],
                'state' => $data['pIState'],
                'postcode' => $data['pIPostcode'],
                'dName' => $data['dName'],
                'cPosition' => $data['cPosition'],
                'cctvDisplay' => true,
              );
              $files = array(
              );
              $latestFormId = FormData::latest()->first();
              $latestFormId = $latestFormId['id'] + 1;
              $check = JobsheetData::where('form_id', '=',$latestFormId)->first();
              if($check == null) {
                $js = new JobsheetData();
                $js->jobsheet_number = 'JS-'.$ord_num;
                $js->form_id = $latestFormId;
                $js->company_name = $data['cName'];
                $js->address = $data['pIAddress'];
                $js->contact_person = $data['cContact'];
                $js->contact_email = $data['cEmail'];
                $js->status = "draft";
                $js->jobsheet_data = serialize($company_info);
                $js->files = serialize($files);
                $js->save();
              }
          
              // For dashboard statistics
              $get = DashboardMonthlySales::where('form_id', '=',$fd->id)->first();
              $a = unserialize($formData);
              if(isset($a['aPayment']) && $a['aPayment'] != null) {
                $amount = preg_replace('/[^A-Za-z0-9-]/', '', $a['aPayment']);
                $amount = str_replace(' ', '', $amount);
                if($a['aTerm'] == '60' ||  $a['aTerm'] == '48' ||  $a['aTerm'] == '36' ||  $a['aTerm'] == '24') {
                  $amount = $amount * $a['aTerm'];
                }
              } else {
                $amount = 0;
              }
              $yr = substr($fd->updated_at, 0, 4);
              if($get) {
                $s = DashboardMonthlySales::find($get->id);
              } else {
                $s = new DashboardMonthlySales();
              } 
              $s->form_id =  $fd->id;
              $s->user_id = $user;
              $s->year = $yr;
              $s->amount = $amount;
              $s->updated_at = $fd->updated_at;
              $s->save();

              //Access Log
              $accessed = User::find(Auth::user()->id);
              $accessed->action = "Completed a Application Form";
              $accessed->activity_timestamp = date('Y-m-d H:i:s');
              $accessed->save();
            } else {
                //Access Log
                $accessed = User::find(Auth::user()->id);
                $accessed->action = "Edited a Application Form";
                $accessed->activity_timestamp = date('Y-m-d H:i:s');
                $accessed->save();
            }

            return Response::json(array("success" => "success", "editId"=> $fd->id));
        } catch (Exception $ex) {
            return Response ::json(array("success" => "false", "message" => $ex->getMessage()));
        }
    }

    public function downloadForms()
    {
        $id = Request :: input("id");
        if ($id) {
           $data = FormData::find($id);
           $files = unserialize($data['files']);
           return Response::json(array("success" => "success", "files" => $files));
        }

    }
    public function toCsv(){
      $referals = Request :: input("referals");
      $headers = array (
        'Content-Type' => 'application/csv',
        'Content-Disposition' => 'attachment; filename=referals.csv',
        'Pragma' => 'no-cache'
      );
      $file = __DIR__ . '/../../../public_html/assets/files/referals.csv';
      $fp = fopen($file, 'w');
      // // $fp = fopen('php://output', 'w');
      fputcsv($fp, array('Company Name','Contact Person','Telephone Number','Refered By','Referral Date'));
      foreach ($referals as $fields) {
        for ($i=0;$i<$fields['referal_count'];$i++){
          fputcsv($fp, array(isset($fields['rBusinessName'][$i])?$fields['rBusinessName'][$i]:'',isset($fields['rContactPerson'][$i])?$fields['rContactPerson'][$i]:'',isset($fields['rPhoneNumber'][$i])?$fields['rPhoneNumber'][$i]:'',$fields['name'],$fields['created_at']));
        }  
      }
      fclose($fp);
      return Response::json(array("success"=>"success","filename"=>"referals.csv","referals"=>$referals)); 
    }

    public function sendReferrals(){
        $lp_Comments = Request :: input("lp_Comments");
        $Company = Request :: input("lp_Company");
        $ContactFirstName = Request :: input("lp_ContactFirstName");
        $Phone = Request :: input("lp_Phone");
        //$lp_UserField6 = Request :: input("Referral From Ap");
        $wl_referredby = Request :: input("wl_referredby"); 
        $wl_salesrep = Request :: input("wl_salesrep");
        $companyCount = sizeof($Company);
        $paramArray = array();
        for ($i=0;$i<$companyCount;$i++){
          $lp_Company = $Company[$i];
          $lp_Phone = $Phone[$i];
          $lp_ContactFirstName = $ContactFirstName[$i];
          if ($lp_Company!="" || $lp_Phone!="" || $lp_ContactFirstName!=""){ 
            $params = array(
              'lp_UserField6' => "Referral From Ap Nexgen",              
              'lp_Company' => $lp_Company,
              'lp_CompanyID' => '12498',
              'lp_Comments' => $lp_Comments,
              'lp_ContactFirstName' => $lp_ContactFirstName,
              'lp_Phone' => $lp_Phone,
              'lp_Password' => '4Gm18ji29',
              'wl_referredby' => $wl_referredby,
              'lp_Username' => 'NexgenTMSG',
              'wl_salesrep' => $wl_salesrep,
              'wl_leadsource' => 2                 
            );
            $paramArray[] = $params;
            $options = array(
              CURLOPT_URL => 'https://www.crmtool.net/lp_NewLead.asp', 
              CURLOPT_POST => 1,
              CURLOPT_POSTFIELDS => http_build_query($params),
              CURLOPT_RETURNTRANSFER => 1,
            );
            $ch = curl_init(); 
            curl_setopt_array ($ch, $options);  
            $output = curl_exec($ch); 
            curl_close($ch);  
            $responses[] = $output;
            $urlArray[] = $params;
          }
        }
      return Response::json(array("success"=>"success","response"=>$responses,"link"=>$urlArray,"paramArray"=>$paramArray)); 
      // var_dump($name);
    }

    public function viewReferals(){
      $id = Request :: input("id");
      if ($id) {
        $data = FormData::find($id);
        $created_date = $data['created_at'];
        $data = unserialize($data['form_data']);
        $referalArray = array (
          'cName' => $data['cName'],
          'cContact' => $data['cContact'],
          'cTel' => $data['cTel'],
          'referals'=> $data['referals'],
          'rBusinessName'=> $data['rBusinessName'],
          'rContactPerson' => $data['rContactPerson'],
          'rPhoneNumber' => $data['rPhoneNumber'],
          'created_at' => date('Y-m-d',strtotime($created_date))
        );
        return Response::json(array("success" => "success", "id" => $id , "data" => $referalArray));
      }
    }

    public function getRates(){
        $term = Request :: input("term");
        $type = Request :: input("type");

        $data = Rates::where('term', '=', $term)
                       ->where('type', '=', $type)
                       ->orderBy('effective_at', 'desc')
                       ->first(['rates']);
        // $returnData = array();
        $rates = unserialize($data['rates']);

        return Response::json(array("success"=>"success","rates"=>$rates));
    } 
    //  $getRates = Rates::all();
    //   $goodsList = array();
    //   foreach ($getRates as $sg) {
    //       $goodsList[] = array(
    //                            "term" => $sg->term,
    //                            "rate" => $sg->rate,
    //                            "type" => $sg->type,
    //                            "effective_at" => $sg->effective_at
    //                           );
        
    //   }
    //    return Response::json(array("success" => true, "goods" => $goodsList));
    // }
    public function viewAll(){  
        $data = DB::table('form_data')
                        ->select('form_data.id','user_id','form_data','name','form_data.created_at')
                        ->join('users','users.id','=','user_id')
                        ->where('form_data.form_data','like','%referals%')
                        ->where('form_data.status','=','Completed')
                        ->orderBy('created_at', 'asc')
                        ->get();
        $returnData = array();
        foreach ($data as $row){
          $processedData = unserialize($row->form_data);
          $businessNames = array();
          $contactPersons = array();
          $phoneNumbers = array();
          if(count($processedData['referals']) > 0 ){
              foreach ($processedData['rBusinessName'] as $businessName){
                if (!is_null($businessName) || $businessName!='')
                  $businessNames[] = $businessName;
              }
              foreach ($processedData['rContactPerson'] as $contactPerson){
                if (!is_null($contactPerson) || $contactPerson!='')
                  $contactPersons[] = $contactPerson;
              }
              foreach ($processedData['rPhoneNumber'] as $phoneNumber){
                if (!is_null($phoneNumber) || $phoneNumber!='')
                  $phoneNumbers[] = $phoneNumber;
              }
              if (count($businessNames)>0){
                $dataArray = array(
                  'user_id' => $row->user_id,
                  'id' => $row->id,
                  'name' => $row->name,
                  // 'status' => $row->status,
                  'created_at' => $row->created_at,
                  'referals' => array_filter($processedData['referals']),          
                  'rBusinessName' => $businessNames,
                  'rContactPerson' => $contactPersons,
                  'rPhoneNumber' => $phoneNumbers,
                  'referal_count' => count($businessNames)
                );
                $returnData[] = $dataArray;
              }
          }
        }
        return Response::json(array("success" => "success", "dbData"=>$processedData, "response" => $returnData));
    }
    

  //   public function getRates()
  //   {
  //     $term = Request :: input("term");
  //     if ($term) {
  //       $data = Rates::find($term);
  //       $data = $data['term'];
  //       $data = $data['id'];
        
  //   } return Response::json(array("success" => "success", "data" => $data));
  //          // $data = Rates::all();
  //          // $data = DB::table('rates')->orderBy('effective_at', "desc")
  //          //                                    ->get();
  //          // $rates = array();
  //          // foreach ($data as $d) {
  //          //     $item['rates'] = unserialize($d->rates);
  //          //     $item['term'] = $d->term;
  //          //     $item['effective_at'] = $d->effective_at;
  //          //     $item['name'] = $d->name;
  //          //     $item['id'] = $d->id;
  //          //     $item['type'] = $d->type;
  //          //     $rates[] = $item;
  //          //  }
  //          //  return Response::json(array("success" => "success", "Rates" => $rates));
      
  // }

    public function showSaleRepForms()
    {
        $id = Request :: input("id");
        $user_id = Request :: input("user");
        if ($id) {
           $data = FormData::find($id);
           
           $forms['data'] = unserialize($data['form_data']);
           $forms['id'] = $data['id'];
           return Response::json(array("success" => "success", "formData" => $forms));
        } else {
          $userStatus = User::find($user_id)["type"];
           if($userStatus == "manager1" || $userStatus == "manager2") {
            $getStaffsList = unserialize(User::find($user_id)["staff"]);
            foreach($getStaffsList as $key => $data) {
                if($data['status'] == '1') {
                   $staff_id[] = $data['staff_id'];
                }
            }
            // $push = array($user_id);
            //if(!empty($staff_id)) {
              // $allstaff_id = array_push($staff_id, $push);
            // } else {
            //     $staff_id[] = $user_id;
            // }
            if(empty($staff_id)) {
                $staff_id[] = 0;
            }
            $data = DB::table('form_data')->orderBy('created_at', "desc")
                ->whereIn('user_id',$staff_id)
            ->get();
           }
           
            $forms = array();
            foreach ($data as $d) {
                $item['creator_id'] = $d->user_id;
                $item['creator_name'] = User::find($d->user_id)["username"];
                //echo 'Record ID : ' . $d->id . '-' . $d->form_data . '<br />';
                $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
                 function($match) {
                   return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                 },
                 $d->form_data );
               //preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $d->form_data);
                $item['data'] = unserialize($data);
                $item['created_at'] = $d->created_at;
                $item['status'] = $d->status;
                $item['id'] = $d->id;
                if (array_key_exists('type',$item['data'])) {
                     $types = $item['data']['type'];
                     $formNameStr = "";
                     foreach ($types as $i => $tp) {
                         if ($tp == true) {
                            $formNameStr.= $i.", ";
                         }
                     }
 
                     $item['types'] = substr_replace($formNameStr,"","-2");
                }
 
                $forms[] = $item;
             }
            return Response::json(array("success" => "success", "formData" => $forms, "userStatus" => $userStatus));
        }
    }


    public function showForms()
    {
        $id = Request :: input("id");
        $user_id = Request :: input("user");
        
        if ($id) {
           $data = FormData::find($id);
           $forms['data'] = unserialize($data['form_data']);
           $forms['id'] = $data['id'];
           return Response::json(array("success" => "success", "formData" => $forms));
        } else {
          $userStatus = User::find($user_id)["type"];
          if ($userStatus == "admin") {
              $data = DB::table('form_data')->orderBy('created_at', "desc")
                                            ->get();
          } else {
                $data = DB::table('form_data')->orderBy('created_at', "desc")
                                              ->where('user_id', $user_id)
                                          ->get();
            }
            $forms = array();
            foreach ($data as $d) {
                $item['creator_id'] = $d->user_id;
                $item['creator_name'] = User::find($d->user_id)["username"];
                //echo 'Record ID : ' . $d->id . '-' . $d->form_data . '<br />';
                $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
                 function($match) {
                   return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                 },
                 $d->form_data );
               //preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $d->form_data);
                $item['data'] = unserialize($data);
                $item['created_at'] = $d->created_at;
                $item['status'] = $d->status;
                $item['id'] = $d->id;
                if (array_key_exists('type',$item['data'])) {
                     $types = $item['data']['type'];
                     $formNameStr = "";
                     foreach ($types as $i => $tp) {
                         if ($tp == true) {
                            $formNameStr.= $i.", ";
                         }
                     }
 
                     $item['types'] = substr_replace($formNameStr,"","-2");
                }
 
                $forms[] = $item;
             }
            return Response::json(array("success" => "success", "formData" => $forms, "userStatus" => $userStatus));
        }
    }

    public function deleteForm()
    {
       $id = Request :: input("id");
       $user_id = Request :: input("user");


       if ($id) {
           $data = FormData::where('created_at', '<', 2018);
           $data->delete();
           return Response::json(array("success" => true, "message" => "item Deleted"));
       } else {
           return Response::json(array("success" => false, "message" => "Some Error!"));
       }
    }

    public function getScheduleGoods()
    {
      $schedule_goods = ScheduleGoods::all();
      $goodsList = array();
      foreach ($schedule_goods as $sg) {
        if ($sg->group != 'Misc') {
          $goodsList[] = array(
                               "group" => $sg->group,
                               "item"=> $sg->group." ".$sg->item,
                               "price" => $sg->rrp,
                               "category" => $sg->category,
                               "model" => $sg->model,
                               "cpiBw" => $sg->cpiBw,
                               "cpiColour" => $sg->cpiColour,
                               'features'=>$sg->features,
                               'descriptions'=>$sg->descriptions,
                               'hardware_solutions'=>$sg->hardware_solutions,
                               'img1' => $sg->img1,
                               'img2' => $sg->img2
                              );
        }
        else {
            $goodsList[] = array(
                               "group" => $sg->group,
                               "item"=> $sg->item,
                               "price" => $sg->rrp,
                               "category" => $sg->category,
                               "model" => $sg->model,
                               "cpiBw" => $sg->cpiBw,
                               "cpiColour" => $sg->cpiColour,
                               'features'=>$sg->features,
                               'descriptions'=>$sg->descriptions,
                               'hardware_solutions'=>$sg->hardware_solutions,
                               'img1' => $sg->img1,
                               'img2' => $sg->img2
                              );
        }
      }
       return Response::json(array("success" => true, "goods" => $goodsList));
    }

    public function showPaycorp()
    {
      $config = Config::get('app.paycorp');
      /*------------------------------------------------------------------------------
      STEP1: Build ClientConfig object
      ------------------------------------------------------------------------------*/
      $ClientConfig = new ClientConfig();
      $ClientConfig->setServiceEndpoint($config['endpoint']);
      $ClientConfig->setAuthToken($config['authToken']);
      $ClientConfig->setHmacSecret($config['hmac']);
      $ClientConfig->setValidateOnly(FALSE);
      /*------------------------------------------------------------------------------
      STEP2: Build Client object
      ------------------------------------------------------------------------------*/
      $Client = new GatewayClient($ClientConfig);
      /*------------------------------------------------------------------------------
      STEP3: Build PaymentInitRequest object
      ------------------------------------------------------------------------------*/
      $initRequest = new PaymentInitRequest();
      $initRequest->setClientId($config['clientID']);
      $initRequest->setTransactionType(TransactionType::$TOKEN);
      $initRequest->setClientRef("merchant_token_reference");
      $initRequest->setComment("merchant_additional_data");
      $initRequest->setTokenize(TRUE);
      //$initRequest->setExtraData(array("ADD-KEY-1" => "ADD-VALUE-1", "ADD-KEY-2" => "ADD-VALUE-2"));
      $initRequest->setCssLocation1('https://nexgenconfigapp.xyz/assets/css/form.css');
      // sets transaction-amounts details (all amounts are in cents)
      $transactionAmount = new TransactionAmount(0,"AUD");
      $transactionAmount->setTotalAmount(0);
      $transactionAmount->setServiceFeeAmount(0);
      $transactionAmount->setPaymentAmount(0);
      $transactionAmount->setCurrency("AUD");
      $initRequest->setTransactionAmount($transactionAmount);
      // sets redirect settings
      $redirect = new PaycorpRedirect($config['redirectUrl']);
      //$redirect->setReturnUrl();
      $redirect->setReturnMethod("POST");
      $initRequest->setRedirect($redirect);

      /*------------------------------------------------------------------------------
      STEP4: Process PaymentInitRequest object
      ------------------------------------------------------------------------------*/
      $initResponse = $Client->payment()->init($initRequest);

      $paycorpResponse = array(
                                'reqId' => $initResponse->getReqid(),
                                'expireAt' => $initResponse->getExpireAt(),
                                'paymentPageUrl' => $initResponse->getPaymentPageUrl()
                                );
      return Response::json(array("success" => true, "response" => $paycorpResponse));
    }

    public function getToken(){
      Blade::setEscapedContentTags('<$', '$>');
      Blade::setContentTags('<$$', '$$>');
      $reqid = Request :: input("reqid");
      $clientRef = Request :: input("clientRef");
      $config = Config::get('app.paycorp');
      /*------------------------------------------------------------------------------
      STEP1: Build ClientConfig object
      ------------------------------------------------------------------------------*/
      $ClientConfig = new ClientConfig();
      $ClientConfig->setServiceEndpoint($config['endpoint']);
      $ClientConfig->setAuthToken($config['authToken']);
      $ClientConfig->setHmacSecret($config['hmac']);
      $ClientConfig->setValidateOnly(FALSE);

      /* ------------------------------------------------------------------------------
       STEP2: Build PaycorpClient object
       ------------------------------------------------------------------------------ */
      $client = new GatewayClient($ClientConfig);

      /* ------------------------------------------------------------------------------
       STEP3: Build PaymentCompleteRequest object
       ------------------------------------------------------------------------------ */
      $completeRequest = new PaymentCompleteRequest();
      $completeRequest->setClientId($config['clientID']);
      $completeRequest->setReqid($reqid);
      /* ------------------------------------------------------------------------------
       STEP4: Process PaymentCompleteRequest object
       ------------------------------------------------------------------------------ */
      $completeResponse = $client->payment()->complete($completeRequest);
      $creditCard = $completeResponse->getCreditCard();
      $paycorpResponse = array(
                                'holderName' => $creditCard->getHolderName(),
                                'cardNumber' => $creditCard->getNumber(),
                                'expiry' => $creditCard->getExpiry(),
                                'type' => $creditCard->getType(),
                                'token' => $completeResponse->getToken()
                                );
      Session::put('paycorpResponse',$paycorpResponse);
      return View::make('payment_complete')->with('paycorpResponse',$paycorpResponse);
      //return Response::json(array("success" => true, "response" => $paycorpResponse));
    }

    public function getPaycorpToken(){
      $creditCard = Session::get('paycorpResponse');
      $paycorpResponse = array(
        'holderName' => $creditCard['holderName'],
        'cardNumber' => $creditCard['cardNumber'],
        'expiry' => $creditCard['expiry'],
        'type' => $creditCard['type'],
        'token' => $creditCard['token']
      );
      return Response::json(array("success" => true, "response" => $paycorpResponse));
    }

    public function sendCustomerEmail()
    {
      set_time_limit(0);
      $userEmail = Request :: input("userEmail");
      $editId = Request :: input("editId");
      $data = Request :: input("data");
     
      $userDetails = Request :: input("userDetails");
      $account = Request :: input("account");


      $fromEmail = "noreply@nexgenconfigapp.xyz";
      $fromName = "Nexgen";
      $subject = 'Welcome to Nexgen';
      $toEmail = $userDetails["email"];
      $toName = $userDetails["name"];
      $accEmail = $account['email'];
      $accName = $account['name'];
      
      $cContact = $data['cContact'];
      $contactPerson = strstr($cContact, ' ', true);
      $customerEmail = $data['cEmail'];

      // ecrypt
      $ciphering = "AES-128-CTR";
      $iv_length = openssl_cipher_iv_length($ciphering);  
      $options = 0; 
      $encryption_iv = 'nexgen123452020!'; 
      $encryption_key = "nexgen2020!";
 
      $encryptionUserEmail = openssl_encrypt($userEmail, $ciphering, $encryption_key, $options, $encryption_iv); 

      $mailData = [
          'user' => $toName,
          'salesRepName' => $accName,
          'name' => $fromName,
          'contactPerson' => $contactPerson,
          'userEmail'=> $encryptionUserEmail,
          'editId'=> $editId,
          'customerEmail'=> $customerEmail,
      ];
  
      // $emailTemplate = 'emails.form.formsCustomerSign';

      // Mail::send($emailTemplate, $mailData, function($message) use ($subject, $customerEmail)
      // {
      //     $message->to($customerEmail);
  
      //     $message->subject($subject);
         
      // });
  
      if (Mail::failures()) {
          Log::debug(Mail::failures());
          return Response::json(array("success" => "fail"));
      } else {
          // return Response::json(array("success" => "success","managerEmail"=>$managerEmail,"message"=>$message));
          return Response::json(array("success" => "success"));
      }
    }
}
