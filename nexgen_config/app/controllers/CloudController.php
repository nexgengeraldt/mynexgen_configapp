<?php

class CloudController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$clouds = Cloud::all();
		return Response::json($clouds);

	}

	 public function createPdf()
    {
        ini_set('max_execution_time', 300);
        $forms = Request::input("htmlData");
        $userDetails = Request::input("userDetails");
        $account = Request::input("account");
        $fromEmail = "noreply@nexgenconfigapp.xyz";
        $fromName = "Nexgen";
        $subject = 'Welcome to Nexgen';
        $toEmail = $userDetails["email"];
        $toName = $userDetails["name"];
        $accEmail = $account['email'];
        $accName = $account['name'];
        $mailData = [
            'user' => $toName,
            'userEmail' => $toEmail,
            'name' => $fromName,
            'email'=> $fromEmail
        ];

        $paths = array();

        if (Auth::user()) {
            $managerEmail = explode(";",Auth::user()->manageremail);
        }

        if (is_array($forms) || is_object($forms)) {
            foreach ($forms as $form) {
                $mpdf = new \Mpdf\Mpdf([
                    'mode' => 'utf-8',
                    'orientation' => 'P',
                    'format' => 'A4'
                ]);
                $mpdf->packTableData = true;
                // Buffer the following html with PHP so we can store it to a variable later
                ob_start();
                // This is where your script would normally output the HTML using echo or print
                echo $form['html'];
                // Now collect the output buffer into a variable
                $html = ob_get_contents();
                ob_end_clean();
                // send the captured HTML from the output buffer to the mPDF class for processing
                $mpdf->WriteHTML($html);
                $data = array("pdf" => $mpdf->Output("assets/files/".$form['fileName']));
                $paths[] = "assets/files/".$form['fileName'];
            }
        }

        if (strpos($form['fileName'],'draft')!==false){
          $emailTemplate = 'emails.form.draftforms';
        } else {
          $emailTemplate = 'emails.form.forms';
        }
        
        // Mail::send($emailTemplate, $mailData, function($message) use ($subject, $paths, $accEmail, $managerEmail)
        // {
        //     $message->to($accEmail);
        //     if ($accEmail!='info@somelongcompany.com.au'){
        //       $message
        //             // ->cc('JamesHarb@nexgen.com.au')
        //             // ->cc('ElieAyoub@nexgen.com.au')
        //             // ->bcc('developer@businesstelecom.com.au')
        //             // ->cc('QA@nexgen.com.au')
        //             ->cc('fakeemail@email.com');
        //     }
        //     else { 
        //       $message
        //             // ->cc('JamesHarb@nexgen.com.au')
        //             // ->cc('ElieAyoub@nexgen.com.au')
        //             // ->cc('developer@businesstelecom.com.au')
        //             ->cc('fakeemail@email.com');
        //     }
            
        //     foreach ($managerEmail as $manager){
        //       $message->cc($manager);
        //     }

        //     $message->subject($subject);

        //     foreach ($paths as $path) {
        //         $message->attach($path);
        //     } 
        // });

        // if (Mail::failures()) {
        //     Log::debug(Mail::failures());
        //     return Response::json(array("success" => "fail"));
        // } else {
        //     return Response::json(array("success" => "success","managerEmail"=>$managerEmail,"message"=>$message));
        // }
    }

    public function getScheduleGoods()
    {
      $schedule_goods = ScheduleGoods::all();
      $goodsList = array();
      foreach ($schedule_goods as $sg) {
        if ($sg->group != 'Misc') {
          $goodsList[] = array(
                               "group" => $sg->group,
                               "item"=> $sg->group." ".$sg->item,
                               "price" => $sg->rrp,
                               "category" => $sg->category,
                               "model" => $sg->model,
                               "cpiBw" => $sg->cpiBw,
                               "cpiColour" => $sg->cpiColour,
                               'features'=>$sg->features,
                               'descriptions'=>$sg->descriptions,
                               'hardware_solutions'=>$sg->hardware_solutions,
                               'img1' => $sg->img1,
                               'img2' => $sg->img2
                              );
        }
        else {
            $goodsList[] = array(
                               "group" => $sg->group,
                               "item"=> $sg->item,
                               "price" => $sg->rrp,
                               "category" => $sg->category,
                               "model" => $sg->model,
                               "cpiBw" => $sg->cpiBw,
                               "cpiColour" => $sg->cpiColour,
                               'features'=>$sg->features,
                               'descriptions'=>$sg->descriptions,
                               'hardware_solutions'=>$sg->hardware_solutions,
                               'img1' => $sg->img1,
                               'img2' => $sg->img2
                              );
        }
      }
       return Response::json(array("success" => true, "goods" => $goodsList));
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function saveData()
	{
		$data = Request::input("data");
        $user = Request::input("user");
        $status = Request::input("type");

         //No credit card info to be saved in the server.
        // isset($data["aCardNumber"]) ? $data["aCardNumber"] = "" : "";
        // isset($data["aCardExpiry"]) ? $data["aCardExpiry"] = "" : "";
        // isset($data["aCardName"]) ? $data["aCardName"] = "" : "";
        // isset($data["aCardType"]) ? $data["aCardType"] = "" : "";

        $cloudData = serialize($data);
        $editId = Request::input("editId");

        $fileName = serialize(Request::input("fileName"));
        if ($editId) {
            $fd = Cloud::find($editId);
        } else {
            $fd = new Cloud();
            $fd->user_id = $user;
        }

        try {
            // $fd->user_id = $user;
            $fd->form_data = $cloudData;
            $fd->status = $status;
            $fd->files = $fileName;

            $fd->save();
            return Response::json(array("success" => "success", "editId"=> $fd->id));
        } catch (Exception $ex) {
            return Response::json(array("success" => "false", "message" => $ex->getMessage()));
        }
	}

	public function show($id)
	{
		$clouds = Cloud::find($id);
		$cloudArray = array (
            'id' 			=> $clouds->id,
            'form_data'		=> unserialize($clouds->form_data)
		);
		return Response::json($cloudArray);

	}

    public function showClouds()
    {

        $id = Request::input("id");
        $user_id = Request::input("user");
        if ($id) {
           $data = Cloud::find($id);
           $clouds['data'] = unserialize($data['form_data']);
           $clouds['id'] = $data['id'];
           return Response::json(array("success" => "success", "cloudData" => $clouds));
        } else {
            $userStatus = User::find($user_id)["type"];
            if ($userStatus == "admin") {
                $data = DB::table('clouds')->orderBy('created_at', "desc")
                                              ->get();
            } else {
                $data = DB::table('clouds')->orderBy('created_at', "desc")
                                              ->where('user_id', $user_id)
                                          ->get();
            }
            $forms = array();
            foreach ($data as $d) {
               $item['creator_id'] = $d->user_id;
               $item['creator_name'] = User::find($d->user_id)["username"];
               $item['data'] = unserialize($d->form_data);
               $item['created_at'] = $d->created_at;
               $item['status'] = $d->status;
               $item['id'] = $d->id;
               if (array_key_exists('type',$item['data'])) {
                    $types = $item['data']['type'];
                    $formNameStr = "";
                    foreach ($types as $i=>$tp) {
                        if ($tp == true) {
                           $formNameStr.= $i.", ";
                        }
                    } 

                    $item['types'] = substr_replace($formNameStr,"","-2");
               }

               $forms[] = $item;
            }
            return Response::json(array("success" => "success", "cloudData" => $forms, "userStatus" => $userStatus));
        }
    }

    public function deleteCloud()
    {
       $id = Request::input("id");
       if ($id) {
           $data = Cloud::find($id);
           $data->delete();
           return Response::json(array("success" => true, "message" => "item Deleted"));
       } else {
           return Response::json(array("success" => false, "message" => "Some Error!"));
       }
    }

    public function delete($id)
	{
		 if(Cloud::find($id)->delete())
			return Response::json(array('success' => true));
		 else
		 	return Response::json(array('success' => false));

	}

    public function downloadForms()
    {
        $id = Request::input("id");
        if ($id) {
           $data = Cloud::find($id);
           $files = unserialize($data['files']);
           return Response::json(array("success" => "success", "files" => $files));
        }

    }
        
    

}
