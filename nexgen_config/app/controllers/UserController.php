<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$users = User::all();

		function get_time_ago( $time ) {
			$time_difference = time() - $time;
		
			if( $time_difference < 1 ) { return 'less than 1 second ago'; }
			$condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
						30 * 24 * 60 * 60       =>  'month',
						24 * 60 * 60            =>  'day',
						60 * 60                 =>  'hour',
						60                      =>  'minute',
						1                       =>  'second'
			);
		
			foreach( $condition as $secs => $str )
			{
				$d = $time_difference / $secs;
		
				if( $d >= 1 )
				{
					$t = round( $d );
					return 'about ' . $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
				}
			}
		}

		foreach($users as $data) {
			$accessed = User::find($data->id);
			$accessed->time_log = get_time_ago(strtotime($accessed->activity_timestamp ));
			$accessed->save();
		}
		return Response::json($users);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		if(User::userExists($input['username'])){
			return Response::json(array('success' => false,'message' => 'The username has already been taken!'));
		}

		try{

			$c = new User();

			$c->name 			= $input['name'];
			$c->username 		= $input['username'];
			$c->password 		= Hash::make($input['password']);
			// $c->api_token 		= Str::random(60);
			$c->phone 			= $input['phone'];
			$c->email 			= $input['email'];
			$c->manageremail		= $input['manageremail'];
			$c->type 			= strtolower($input['type']);
			$c->financeonly 	= $input['financeonly'];

            $data = $input['signData'];  
            if ($data) {
                list ($type,$data) = explode(';', $data);
                list (, $data) = explode(',', $data);
                $data = base64_decode($data);
                $rand = substr(md5(microtime()),rand(0,26),5);
                $fileName = dirname($_SERVER['SCRIPT_FILENAME']).'/assets/files/UserSign/'.$c->name.'-'.$rand.'.png';

                $c->sign = 'assets/files/UserSign/'.$c->name.'-'.$rand.'.png';
                file_put_contents($fileName, $data);
            }
						
			$allusers =  User::all();
			$array = array();
			foreach($allusers as $key => $user) {
				$array[] = array( 
					'status' => 0,
					'staff_id' => $user['id'],
					'name' => $user['name'],
				);
			}
			$c->staff = serialize($array);

			$c->created_at 		= time();
			$c->updated_at 		= time();

			//update staff list of every user
			$latestUserId = User::latest()->first();
			$latestUserId = $latestUserId['id'] + 1;
			$update_list =  User::all();
			foreach($update_list as $key => $data) {
				$update =  User::find($data['id']);
				$user_list = unserialize($update['staff']);
				$push = array(
					'status' => 0,
					'staff_id' => $latestUserId,
					'name' => $c->name,
				);
				
				if(isset($user_list) && isset($push)) {
					$new = @array_push($user_list, $push);
				}
				$update->staff = serialize($user_list);
				$update->save();
			}

			if($c->save())
				return Response::json(array('success' => true,'id'=>$c->id ,"message"=>'User has been registered successfully!'));
			else
				throw new \Exception("Could not save the Users");
		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage()));
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		$user['sign_exists'] = file_exists(dirname($_SERVER['SCRIPT_FILENAME']). '/' . $user['sign']);

		return Response::json($user);
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showUsers()
	{
		$input = Input::all();
		$user =  User::find($input['user_id']);
		$allUsers = unserialize($user['staff']);


		return Response::json(array('staffs' => $allUsers));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

		/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function pickstaff()
	{
		$input = Input::all();
		$user =  User::find($input['user_id']);
		$allusers = unserialize($user['staff']);
		if($input['value'] == '1') {
			$status = '0';
		} else {
			$status = '1';
		}
		$array = array();
		foreach($allusers as $key => $user) {
			if($input['staff_id'] == $user['staff_id']) {
				$array[$key] = array( 
					'status' => $status,
					'staff_id' => $input['staff_id'],
					'name' => $user['name'],
				);
			}
		}
		$replaced =  array_replace($allusers, $array);
		try {
		$update =  User::find($input['user_id']);
		$update->staff =serialize($replaced);
		
		if($update->save())
			return Response::json(array('success' => true,'id'=>$update->id));
		else
			throw new \Exception("Couldnot save the Users");

		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage()));
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    	$c =  User::find($id);
		$input = Input::all();
		
		try{

			$c->name 			= $input['name'];
			$c->username 		= $input['username'];
			$c->phone 			= $input['phone'];
            if (isset($input['password'])) {
                $c->password 		= Hash::make($input['password']);
            }
			$c->email 			= $input['email'];
			// $c->staff 			= $input['staff'];
			$c->financeonly 	= $input['financeonly'];
			$c->manageremail	= $input['manageremail'];
			$c->type 			= strtolower($input['type']);
			$c->updated_at 		= time();

            $data = $input['signData'];  
            if ($data) {
                list ($type,$data) = explode(';', $data);
                list (, $data) = explode(',', $data);
                $data = base64_decode($data);
                $rand = substr(md5(microtime()),rand(0,26),5);
                $fileName = dirname($_SERVER['SCRIPT_FILENAME']).'/assets/files/UserSign/'.$c->name.'-'.$rand.'.png';
                
                $c->sign = 'assets/files/UserSign/'.$c->name.'-'.$rand.'.png';
                file_put_contents($fileName, $data);
            }
                      
                                
			if($c->save())
				return Response::json(array('success' => true,'id'=>$c->id));
			else
				throw new \Exception("Couldnot save the Users");

		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage()));
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	  if(User::find($id)->delete())
		return Response::json(array('success' => true));
	 else
	 	return Response::json(array('success' => false));
	}
       

}
