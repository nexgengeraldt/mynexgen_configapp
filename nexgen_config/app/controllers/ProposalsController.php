<?php

class ProposalsController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$proposals = Proposals::all();
		return Response::json($proposals);

    }

	// create pdf function here v
	public function createPdf()
    {
        set_time_limit(0);
        $forms = Request::input("htmlData");
        $orientation = Request::input("orientation");
        if($forms) {
            $formsValue ='1';
        } else {
            $formsValue = '0';
        }
        $userDetails = Request::input("userDetails");
        $account = Request::input("account");
        $fromEmail = "noreply@nexgenconfigapp.xyz";
        $fromName = "Nexgen";
        $subject = 'Welcome to Nexgen';
        $toEmail = $userDetails["email"];
        $toName = $userDetails["name"];
        $accEmail = $account['email'];
        $accName = $account['name'];
        $fileName = $userDetails["fileName"];
        
        $mailData = [
            'user' => $toName,
            'userEmail' => $toEmail,
            'salesRepName' => $accName,
            'name' => $fromName,
            'email'=> $fromEmail
        ];
        $paths = array();

        if (Auth::user()) {
            $managerEmail = explode(";",Auth::user()->manageremail);
        }
   
        //Access Log
        $accessed = User::find(Auth::user()->id);
        $accessed->action = "Completed a proposal";
		$accessed->activity_timestamp = date('Y-m-d H:i:s');
        $accessed->save();

        // We have different ways for generating pdf
     
            foreach($forms as $form) {
                $mpdf = new \Mpdf\Mpdf([
                    'mode' => 'utf-8',
                    'format' => $orientation,
                ]);
                $mpdf->packTableData = true;
                // Buffer the following html with PHP so we can store it to a variable later
                ob_start();
                // This is where your script would normally output the HTML using echo or print
                echo $form['html'];
                // Now collect the output buffer into a variable
                $html = ob_get_contents();
                ob_end_clean();
                // send the captured HTML from the output buffer to the mPDF class for processing
                $mpdf->WriteHTML($html);
                $data = array("pdf" => $mpdf->Output("assets/files/".$form['fileName']));
                $paths[] = "assets/files/".$form['fileName'];
            }

            
            if ($formsValue == '1' && strpos($form['fileName'],'draft')!==false ){
                $emailTemplate = 'emails.form.draftforms';
              } else {
                $emailTemplate = 'emails.form.proposals';
              }
              
            //   Mail::send($emailTemplate, $mailData, function($message) use ($subject, $paths, $accEmail, $managerEmail)
            //   {
            //       $message->to($accEmail);
            //       if ($accEmail!='info@somelongcompany.com.au'){
            //         $message->cc('JamesHarb@nexgen.com.au')
            //               ->cc('ElieAyoub@nexgen.com.au')
            //               ->cc('developer@businesstelecom.com.au')
            //               ->cc('QA@nexgen.com.au');
            //       }
            //       else {
            //         $message->cc('JamesHarb@nexgen.com.au')
            //               ->cc('ElieAyoub@nexgen.com.au')
            //               ->cc('developer@businesstelecom.com.au');
            //       }
                  
            //       foreach ($managerEmail as $manager){
            //         $message->cc($manager);
            //       }
      
            //       $message->subject($subject);
      
            //       foreach ($paths as $path) {
            //           $message->attach($path);
            //       }
            //   });
        
        if (!empty($forms)){
           
                $mailHost = "192.168.10.1";
                $mailUsername = "";
                $mailPassword = "";
                $setFrom = "web@nexgenconfigapp.xyz";
                $setTo = "Papercut@user.com";
                $tls = "";
            
            // Create the Transport
            $transport = (new Swift_SmtpTransport($mailHost, 25, $tls))
            ->setUsername($mailUsername)
            ->setPassword($mailPassword);
            // Create the Mailer using your created Transport
            $mailer = new Swift_Mailer($transport);
            // Create a message
            $message = (new Swift_Message('Online Quotation'))
            ->setFrom(['noreply@nexgenconfigapp.xyz' => 'Nexgen'])
            ->setTo([$setTo, $setTo => 'You'])
            ->setCc(['geraldtejero@nexgen.com.au'])
            // And optionally an alternative body
            ->addPart('<p>Here is the message itself</p> <h1>This is a sample email content!</h1>', 'text/html')
            // Optionally add any attachments
            ->attach(Swift_Attachment::fromPath('assets/files/'.$form['fileName']));
            // Send the message
            $result = $mailer->send($message);
        }



        if ($formsValue == '1'){
            if (Mail::failures()) {
                Log::debug(Mail::failures());
                return Response::json(array("success" => "fail"));
            } else {
                return Response::json(array("success" => "success","managerEmail"=>$managerEmail,"message"=>$message));
            }
        } else {
            return Response::json(array("success" => "failed"));
        }   
        // return Response::json(array("success"=>"Call to function Successful")); 
    }


	public function upload(){

		if ( !empty( $_FILES ) ) {

		    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
		    // $uploadPath = storage_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
			$uploadPathT = 'assets' . "/" . 'files' . "/" . 'logo' . "/" . $_FILES[ 'file' ][ 'name' ];

		    move_uploaded_file( $tempPath, $uploadPathT );

		    $answer = array( 'answer' => 'File transfer completed', 'FilePath' => $uploadPathT );
		    $json = json_encode( $answer );

			//Log::error($exception);
		    echo $json;

		} else {

		    echo 'No files';
		}
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function saveData()
    {
        $data = array();
        $data = Request::input("data");
        $user = Request::input("user");
        $status = Request::input("type");
        // $quot_num = Request::input("quot_num");
         //No credit card info to be saved in the server.
        // isset($data["aCardNumber"]) ? $data["aCardNumber"] = "" : "";
        // isset($data["aCardExpiry"]) ? $data["aCardExpiry"] = "" : "";
        // isset($data["aCardName"]) ? $data["aCardName"] = "" : "";
        // isset($data["aCardType"]) ? $data["aCardType"] = "" : "";

        $formData = serialize($data);
        $editId = Request::input("editId");
        $fileName = serialize(Request::input("fileName"));
        if ($editId) {
            $fd = Proposals::find($editId);

            
           
        } else {
            $fd = new Proposals();
            $fd->user_id = $user;
        }
        try {
            //$fd->user_id = $user;
            $fd->proposal_data = $formData;
            $fd->status = $status;
            $fd->files = $fileName;
            // $fd->quot_num = $quot_num;

            if($status == 'Draft') {
                //Access Log
                $accessed = User::find(Auth::user()->id);
                $accessed->action = "Edited a proposal";
                $accessed->activity_timestamp = date('Y-m-d H:i:s');
                $accessed->save();
            }

            $fd->save();
            return Response::json(array("success" => "success", "editId"=> $fd->id));
        } catch (Exception $ex) {
            return Response::json(array("success" => "false", "message" => $ex->getMessage()));
        }
    }
    
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $files = Request::input("files");
        $user = Request::input("user");

        $proposalData = serialize($files);
        $editId = Request::input("editId");

        if ($editId) {
            $fd = Proposals::find($editId);
        } else {
            $fd = new Proposals();
            $fd->user_id = $user;
        }

        try {
            // $fd->user_id = $user;
            $fd->proposal_data = $proposalData;
            $fd->save();

            return Response::json(array("success" => "success", "editId"=> $fd->id, "data"=> $fd->proposal_data));
        } catch (Exception $ex) {
            return Response::json(array("success" => "false", "message" => $ex->getMessage(),"proposalData"=>$files));
        }
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$proposals = Proposals::find($id);
		$proposalArray = array (
            'id' 				=>$proposals->id,
            'proposal_data'		=>unserialize($proposals->proposal_data)
		);
		return Response::json($proposalArray);

	}
    /**
	 * Show the form for editing the specified resource. 
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function checkQuotNum()
	{
        $generatedQuot_num = Request::input("generatedQuot_num");

        $proposal = Proposals::where('quot_num', $generatedQuot_num)->first();
       if(empty($proposal)) {
            return Response::json(array("success" => "success", "quot_num"=> $generatedQuot_num));
       } else {
            return Response::json(array("success" => "fail"));
       }
    }
	/**
	 * Show the form for editing the specified resource. 
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{	
		$files = Request::input("files");
        $companyName = Request::input("companyName");
        $businessName = Request::input("businessName");
        $contactNumber = Request::input("contactNumber");
        $emailAddress = Request::input("emailAddress");
        $schQty = Request::input("schQty");
        $schItems = Request::input("schItems");
        $schOption = Request::input("schOption");
        $hTerm = Request::input("hTerm");
        $bTerm = Request::input("bTerm");
        $nationalCall = Request::input("nationalCall");
        $localCall = Request::input("localCall");
        $callNumbers = Request::input("callNumbers");
        $directQuantity = Request::input("directQuantity");
        $diD = Request::input("diD");
        $selectedFeedbacks = Request::input("selectedFeedbacks");
        $editId = Request::input("editId");
		    $fd =  Proposals::find($id);
		// $input = Input::all();

		try{
			$fd->files					= $files;
			$fd->companyName			= $companyName;
			$fd->businessName			= $businessName;
			$fd->contactNumber			= $contactNumber;
			$fd->emailAddress			= $emailAddress;
			$fd->display				= 'asasa';
			$fd->schQty					= serialize($schQty);
			$fd->schItems				= serialize($schItems);
			$fd->schOption				= serialize($schOption);
			$fd->hTerm					= $hTerm;
			$fd->bTerm					= $bTerm;
			$fd->nationalCall			= $nationalCall;
			$fd->localCall				= $localCall;
			$fd->callNumbers			= $callNumbers;
			$fd->directQuantity			= $directQuantity;
			$fd->diD					= $diD;
			$fd->selectedFeedbacks		= serialize($selectedFeedbacks);
            
                      
                                
			if($fd->save())
				return Response::json(array('success' => true,'id'=>$fd->id));
			else
				throw new \Exception("Could not save the Feedbacks");

		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage()));
		}

	}

	// {
	// 	// $input = Input::all();
	// 	// $fd     = SalesRep::find($id);

	//  //    try{

	// 	// 	$fd->name 			= $input['name'];
	// 	// 	$fd->phone 			= $input['phone'];
	// 	// 	$fd->email 			= $input['email'];

	// 	// 	$fd->updated_at 		= time();


	// 	// 	if($fd->save())
	// 	// 		return Response::json(array('success' => true,'id'=>$fd->id,"message"=>'Salesrep saved successfully'));
	// 	// 	else
	// 	// 		throw new \Exception("Couldnot save the salesrep");

	//  //   } catch (\Exception $e){
	// 	// 	return Response::json(array('success' => false,'message'=>$e->getMessage()));
	//  //   }

	// }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function destroy($id)
	// {
	// 	 if(Proposals::find($id)->delete())
	// 		return Response::json(array('success' => true));
	// 	 else
	// 	 	return Response::json(array('success' => false));

	// }

     public function deleteForm()
    {
        $id = Request::input("id");
        $user_id = Request::input("user");
        
       if ($id) {
        //    $data = Proposals::find($id);
        //    $data->delete();
           return Response::json(array("success" => false, "message" => "Delete function disabled"));
       } else {
           return Response::json(array("success" => false, "message" => "Some Error!"));
       }
    }

    public function showSalesRepProposals()
    {
        $id = Request::input("id");
        $user_id = Request::input("user");
        $forms = array();

        if ($id) {
            $data = Proposals::find($id);
            $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
            function($match) {
            return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
            },
            $data['proposal_data'] );
            $proposal['data'] = unserialize($data);
            
            if (array_key_exists('type',$proposal['data'])) {
                $types = $proposal['data']['type'];
                $formNameStr = "";
                foreach ($types as $i => $tp) {
                    if ($tp == true) {
                        $formNameStr.= $i.", ";
                    }
                }
                $proposal['types'] = substr_replace($formNameStr,"","-2");
            }
           $proposal['id'] = $id;
           return Response::json(array("success" => "success", "proposalData" => $proposal));

        } else {
            $userStatus = User::find($user_id)["type"];
           if($userStatus == "manager1" || $userStatus == "manager2") {
                 $getStaffsList = unserialize(User::find($user_id)["staff"]);
                foreach($getStaffsList as $key => $data) {
                    if($data['status'] == '1') {
                        $staff_id[] = $data['staff_id'];
                    }
                }
                // $push = array($user_id);
                //if(!empty($staff_id)) {
                    // $allstaff_id = array_push($staff_id, $push);
                // } else {
                //     $staff_id[] = $user_id;
                // }
                if(empty($staff_id)) {
                    $staff_id[] = 0;
                }
                $data = DB::table('proposals')->orderBy('id', "desc")
                ->whereIn('user_id',$staff_id)
                ->get();
            }
            else {
                $data = DB::table('proposals')->orderBy('id', "desc")
                                              ->where('user_id', $user_id)
                                          ->get();
            }
            foreach ($data as $d) {
                $item['creator_id'] = $d->user_id;
                $item['creator_name'] = User::find($d->user_id)["username"];
                $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
                 function($match) {
                   return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                 },
                 $d->proposal_data );
                $item['data'] = unserialize($data);
                $item['created_at'] = $d->created_at;
                $item['status'] = $d->status;
                $item['id'] = $d->id;
                if (array_key_exists('type',$item['data'])) {
                     $types = $item['data']['type'];
                     $formNameStr = "";
                     foreach ($types as $i => $tp) {
                         if ($tp == true) {
                            $formNameStr.= $i.", ";
                         }
                     }
                     $item['types'] = substr_replace($formNameStr,"","-2");
                }
                $forms[] = $item;
             }
            return Response::json(array("success" => "success", "proposalData" => $forms, "userStatus" => $userStatus));
        }
    }


    public function showProposals()
    {
        $id = Request::input("id");
        $user_id = Request::input("user");
        $forms = array();

        if ($id) {
            $data = Proposals::find($id);
            $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
            function($match) {
            return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
            },
            $data['proposal_data'] );
            $proposal['data'] = unserialize($data);
            
            if (array_key_exists('type',$proposal['data'])) {
                $types = $proposal['data']['type'];
                $formNameStr = "";
                foreach ($types as $i => $tp) {
                    if ($tp == true) {
                        $formNameStr.= $i.", ";
                    }
                }
                $proposal['types'] = substr_replace($formNameStr,"","-2");
            }
           $proposal['id'] = $id;
           return Response::json(array("success" => "success", "proposalData" => $proposal));

        } else {
            $userStatus = User::find($user_id)["type"];
            if ($userStatus == "admin") {
                $data = DB::table('proposals')->orderBy('id', "desc")
                                              ->get();
            } 
            else {
                $data = DB::table('proposals')->orderBy('id', "desc")
                                              ->where('user_id', $user_id)
                                          ->get();
            }
            foreach ($data as $d) {
                $item['creator_id'] = $d->user_id;
                $item['creator_name'] = User::find($d->user_id)["username"];
                $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
                 function($match) {
                   return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                 },
                 $d->proposal_data );
                $item['data'] = unserialize($data);
                $item['created_at'] = $d->created_at;
                $item['status'] = $d->status;
                $item['id'] = $d->id;
                if (array_key_exists('type',$item['data'])) {
                     $types = $item['data']['type'];
                     $formNameStr = "";
                     foreach ($types as $i => $tp) {
                         if ($tp == true) {
                            $formNameStr.= $i.", ";
                         }
                     }
                     $item['types'] = substr_replace($formNameStr,"","-2");
                }
                $forms[] = $item;
             }
            return Response::json(array("success" => "success", "proposalData" => $forms, "userStatus" => $userStatus));
        }
    }

    public function downloadForms()
    {
        $id = Request::input("id");
        if ($id) {
           $data = Proposals::find($id);
           $files = unserialize($data['files']);
           return Response::json(array("success" => "success", "files" => $files));
        }

    }


}
