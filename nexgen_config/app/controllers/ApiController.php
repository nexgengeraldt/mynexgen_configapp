<?php

class ApiController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function index()
	{
        $id = Request :: input("id");
        // 34606252966
        header('Access-Control-Allow-Origin: *');
        header("Authorization: Gxnjox9Ghg3oe2Ag1JPqfbvmw8BGxgBILpugywDk0QJww1P2PcT8yakL8Uoo");
        header("ABN: 34606252966");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        $forms = FormData::where('hash_id', '=',$id)->first();

        if($id != 'all' && !empty($forms) && $forms->status == 'Completed') {
            $form_data = array();
            $form_data =  unserialize($forms->form_data);
            $files  =  unserialize($forms->files);

            if(!empty($form_data['type']['rental2']) || !empty($form_data['type']['rental'])) {
                $FinanceType = 'Rental';
            }else if(!empty($form_data['type']['chattle'])) {
                $FinanceType = 'Chattel Mortgage';
            }else if(!empty($form_data['type']['leasing'])) {
                $FinanceType = 'Finance Lease';
            } else {
                $FinanceType = 'Software Agreement';
            }

            $FirstPropertyValue = str_replace(" ", "", $form_data['pValue']);
            $FirstPropertyValue = preg_replace("#[^\w()/.%\-&]#", "", $FirstPropertyValue);
            $FirstMortgageBalance = str_replace(" ", "", $form_data['pMortgage']);
            $FirstMortgageBalance = preg_replace("#[^\w()/.%\-&]#", "", $FirstMortgageBalance);

            $SecondPropertyValue = str_replace(" ", "", $form_data['g2Value']);
            $SecondPropertyValue = preg_replace("#[^\w()/.%\-&]#", "", $SecondPropertyValue);
            $SecondMortgageBalance = str_replace(" ", "", $form_data['g2Mortgage']);
            $SecondMortgageBalance = preg_replace("#[^\w()/.%\-&]#", "", $SecondMortgageBalance);

            $InvoiceAmount = str_replace(" ", "", $form_data['aPayment']);
            $InvoiceAmount = preg_replace("#[^\w()/.%\-&]#", "", $InvoiceAmount);
            $InvoiceAmount = intval($InvoiceAmount);

            $LoanAmount = str_replace(" ", "", $form_data['aPayout']);
            $LoanAmount = preg_replace("#[^\w()/.%\-&]#", "", $LoanAmount);

            $ResidualValue = str_replace(" ", "", $form_data['aResidual']);
            $ResidualValue = preg_replace("#[^\w()/.%\-&]#", "", $ResidualValue);

            foreach($forms->updated_at as $key => $data)
            {
                if($key == 'date') {
                    $date = str_replace('/', '-', $data);
                    $InvoiceDate = date('Y-m-d', strtotime($date));
                }
            }
            $pBirth = date('Y-m-d', strtotime($form_data['pBirth']));
            $g2Birth = date('Y-m-d', strtotime($form_data['g2Birth']));
            
            $pExpiry = date('Y-m-d', strtotime($form_data['pExpiry']));

            
           

            if($form_data['aIndustry'] == 'AUTOMOTIVE') {
                $PrimaryIndustry = 'G';
                $IndustrySector = '391';
            } else if($form_data['aIndustry'] == 'CONSTRUCTION') {
                $PrimaryIndustry = 'E';
                $IndustrySector = '329';
            } else if($form_data['aIndustry'] == 'EDUCATION') {
                $PrimaryIndustry = 'P';
                $IndustrySector = '822';
            } else if($form_data['aIndustry'] == 'GOVERNMENT') {
                $PrimaryIndustry = 'O';
                $IndustrySector = '751';
            } else if($form_data['aIndustry'] == 'HOSPITALITY') {
                $PrimaryIndustry = 'H';
                $IndustrySector = '453';
            } else if($form_data['aIndustry'] == 'MANUFACTURING') {
                $PrimaryIndustry = 'C';
                $IndustrySector = '259';
            } else if($form_data['aIndustry'] == 'NGO') {
                $PrimaryIndustry = 'O';
                $IndustrySector = '755';
            } else if($form_data['aIndustry'] == 'PROFESSIONAL OFFICE') {
                $PrimaryIndustry = 'M';
                $IndustrySector = '699';
            } else if($form_data['aIndustry'] == 'RETAIL') {
                $PrimaryIndustry = 'G';
                $IndustrySector = '432';
            } else if($form_data['aIndustry'] == 'SERVICE') {
                $PrimaryIndustry = 'S';
                $IndustrySector = '953';
            } else if($form_data['aIndustry'] == 'WHOLESALE') {
                $PrimaryIndustry = 'F';
                $IndustrySector = '380';
            } else if($form_data['aIndustry'] == 'MEDICAL') {
                $PrimaryIndustry = 'Q';
                $IndustrySector = '851';
            } else if($form_data['aIndustry'] == 'OTHER') {
                $PrimaryIndustry = 'S';
                $IndustrySector = '949';
            }

            $pieces = explode(" ", $form_data['cContact']);
            if(isset($pieces[2])) {
                $SurName = $pieces[2];
                $GivenName = $pieces[0]." ".$pieces[1];
            } else {
                if(isset($pieces[1])) {
                    $SurName = $pieces[1];
                } else {
                    $SurName = '';
                }
                $GivenName = $pieces[0];
            }

            $Mobile = str_replace("-", '', $form_data['cMobile']);

            $AreaCode = substr($form_data['cTel'], 1, 2);  
            $Telephone = str_replace("-", '', $form_data['cTel']);
            $Telephone = substr($Telephone, 5, 13);  

            $RefTelephone = str_replace("-", '', $form_data['tTel']);
            $RefTelephone = substr($RefTelephone, 5, 13);  



            if(strpos($form_data['cName'], 'Pty Ltd') !== false || strpos($form_data['cName'], 'PTY LIMITED') !== false || strpos($form_data['cName'], 'pty ltd') !== false || strpos($form_data['cName'], 'PTY. LIMITED') !== false || strpos($form_data['cName'], 'PTY. LTD.') !== false || strpos($form_data['cName'], 'PTY LTD') !== false || strpos($form_data['cName'], 'PROPRIETARY LIMITED') !== false){
                $EntityType =  "P/L";
            } else if(strpos($form_data['cName'], 'Incorporated') !== false ){
                $EntityType =  "INC";
            } else if(strpos($form_data['cName'], 'Partnership') !== false ){
                $EntityType =  "PTNR";
            } else if(strpos($form_data['cName'], 'Corporation') !== false ){
                $EntityType =  "CORP";
            } else if(strpos($form_data['cName'], 'Business') !== false ){
                $EntityType =  "BUS";
            } else if(strpos($form_data['cName'], 'Trust') !== false || strpos($form_data['cName'], 'TRUST') !== false){
                $EntityType =  "TRST";
            } else {
                $EntityType =  "Registered Body";
            }

            if(!(isset($form_data['cNemp']))) {
                $NoEmployees = 0;
            } else {
                $NoEmployees = $form_data['cNemp'];
            }
            
            //Contacts
            $Contacts = [ 
                'Contacts' => [
                        'Title'      => 'Unknown',
                        'GivenName'  => $GivenName,
                        'SurName'    => $SurName,
                        'Mobile'     => $Mobile,
                        'Telephone'  => $Telephone,
                        'AreaCode'   => $AreaCode,
                        'Email'      => $form_data['cEmail'],
                 
            ]];
            $Contacts = array_filter($Contacts['Contacts']); 

            //CommercialEntities
            $CommercialEntities = [
                'CommercialEntities' => array([
                    'LegalName'                => (isset($form_data['cName']) ?  $form_data['cName'] : ''),
                    'ABN'                      => (isset($form_data['cAbn']) ?  $form_data['cAbn'] : ''),
                    'EntityType'               => $EntityType,
                    'PrimaryIndustry'          => (isset($PrimaryIndustry) ?  $PrimaryIndustry : ''),
                    'PrinciplePlaceofBusiness' =>   [
                                                    'Suburb'             => (isset($form_data['cSuburb']) ?  $form_data['cSuburb'] : ''),
                                                    'State'              => (isset($form_data['cState']) ?  $form_data['cState'] : ''),
                                                    'Postcode'           => (isset($form_data['cPostCode']) ?  strval($form_data['cPostCode']) : ''),
                                                    // 'Longitude'          => '',
                                                    'UnformattedAddress' => (isset($form_data['cAddress']) ?  $form_data['cAddress'].', '.$form_data['cSuburb'].', '.$form_data['cState'].' '.$form_data['cPostCode'] : ''),
                                                    // 'Property'           => '',
                                                    // 'UnitNumber'         => '',
                                                    // 'StreetNumber'       => '',
                                                    // 'StreetName'         => '',
                                                    // 'StreetType'         => '',
                                                    // 'Latitude'           => '',
                    ],
                    'Type'                     => 'Primary',
                    'ACN'                      => '999999999',
                    'TradingName'              => (isset($form_data['cTrading']) ?  $form_data['cTrading'] : ''),
                    'NoEmployees'              => $NoEmployees,
                    // 'YearsEstablished'                => $form_data['aYear'],
                    'IndustrySector'                  => (isset($IndustrySector) ?  $IndustrySector : ''),
                    'BusinessCategory'                => 'SME ($1m+ Revenue | $0.5m to $5m Capital)',
                    // 'Revenue'                         => '',
                    // 'LandLineFlag'                    => '',
                    'Website'                         => (isset($form_data['cWebsite']) ?  $form_data['cWebsite'] : ''),
                    // 'OperateatCommercialAddressFlag'  => '',
                    // 'ComEquifaxScoreID'               => '',
                    // 'ComillionScore'           => '',
                    // 'ComillionScoreID'         => '',
                    // 'EquifaxRawReturn'         => '',
                    // 'EquifaxRawReturnType'     => '',
                    // 'illionRawReturn'          => '',
                    // 'illionRawReturnType'      => '',
                    'AreaCode'                 => (isset($AreaCode) ?  $AreaCode : ''),
                    'PhoneNumber'              => (isset($Telephone) ?  $Telephone : ''),
            ])];
            $CommercialEntities = array_filter($CommercialEntities['CommercialEntities'][0]); 
            
            // First Guarator
            $firstGuarantorpieces = explode(" ", $form_data['pName']);
            if(isset($firstGuarantorpieces[2])) {
                $firstGuarantorSurName = $firstGuarantorpieces[2];
                $firstGuarantorGivenName = $firstGuarantorpieces[0]." ".$firstGuarantorpieces[1];
            } else {
                if(isset($firstGuarantorpieces[1])) {
                    $firstGuarantorSurName = $firstGuarantorpieces[1];
                } else {
                    $firstGuarantorSurName = '';
                }
                $firstGuarantorGivenName = $firstGuarantorpieces[0];
            }

            $FirstHomeAddress = [
                'Suburb'             => (isset($form_data['pSuburb']) ?  $form_data['pSuburb'] : ''),
                'State'              => (isset($form_data['pState']) ?  $form_data['pState'] : ''),
                'Postcode'           => (isset($form_data['pPostcode']) ?  strval($form_data['pPostcode']) : ''),
                // 'StreetName'         => (isset($form_data['pAddress']) ?  $form_data['pAddress'] : ''),
                // 'StreetNumber'       => (isset($form_data['pAddress']) ?  $form_data['pAddress'] : ''),
                // 'StreetType'         => (isset($form_data['pAddress']) ?  $form_data['pAddress'] : ''),
                'UnformattedAddress' => (isset($form_data['pAddress']) ? $form_data['pAddress'].', '.$form_data['pSuburb'].', '.$form_data['pState'].' '.$form_data['pPostcode'] : '')];
            $FirstHomeAddress = array_filter($FirstHomeAddress); 

            $FirstAddressofPropertyOwned = [
                'Suburb'             => (isset($form_data['pSuburb']) ?  $form_data['pSuburb'] : ''),
                'State'              => (isset($form_data['pState']) ?  $form_data['pState'] : ''),
                'Postcode'           => (isset($form_data['pPostcode']) ?  strval($form_data['pPostcode']) : ''),
                // 'StreetName'         => (isset($form_data['pAddress']) ?  $form_data['pAddress'] : ''),
                // 'StreetNumber'       => (isset($form_data['pAddress']) ?  $form_data['pAddress'] : ''),
                // 'StreetType'         => (isset($form_data['pAddress']) ?  $form_data['pAddress'] : ''),
                'UnformattedAddress' => (isset($form_data['pAddress']) ? $form_data['pAddress'].', '.$form_data['pSuburb'].', '.$form_data['pState'].' '.$form_data['pPostcode'] : ''),
                // 'Property'           => '',
                // 'UnitNumber'         => '',
                // 'Latitude'           => '',
                // 'Longitude'          => '',
            ];
            $FirstAddressofPropertyOwned = array_filter($FirstAddressofPropertyOwned); 

            if(isset($form_data['pName'])) {
                $firstGuarantor = [
                    'FirstName'                  => (isset($firstGuarantorGivenName) ?  $firstGuarantorGivenName : ''),
                    'SurName'                    => (isset($firstGuarantorSurName) ?  $firstGuarantorSurName : ''),
                    'DoB'                        => (isset($pBirth) ? $pBirth  : ''),
                    'PrivacyConsent'             => false,
                    'DriverLicenseNo'            => (isset($form_data['pLicense']) ?  $form_data['pLicense'] : ''),
                    'DriverLicenseExpiryDt'      => (isset($pExpiry) ?  $form_data['pExpiry'] : ''),
                    'DriverLicenseState'         => (isset($form_data['pLicState']) ?  $form_data['pLicState'] : $form_data['pState']),
                    'HomeAddress'                => $FirstHomeAddress,
                    'PropertyOwnerFlag'          => (isset($form_data['pOwned']) ?  $form_data['pOwned'] : ''),
                    'Role'                       => (isset($form_data['cPosition']) ?  $form_data['cPosition'] : ''),
                    // 'Gender'                     => '',
                    // 'NumberofDependants'         => '',
                    'PropertyValue'              => (isset($FirstPropertyValue) && isset($form_data['pOwned']) == true ?  $FirstPropertyValue : ''),
                    'MortgageBalance'            => (isset($FirstMortgageBalance) ?  $FirstMortgageBalance : ''),
                    'AddressofPropertyOwned'     => $FirstAddressofPropertyOwned,
                    'Title'                      => 'Unknown',
                    // 'TitleSearchRaw'             => '',
                    // 'TitleSearchType'            => '',
                    // 'EquifaxScore'               => '',
                    // 'EquifaxScoreID'             => '',
                    // 'EquifaxRaw'                 => '',
                    // 'illionScore'                => '',
                    // 'illionScoreID'              => '',
                    // 'illionRaw'                  => '',
                ];
            }

            // Second Guarator
            $secondGuarantorpieces = explode(" ", $form_data['g2Name']);
            if(isset($secondGuarantorpieces[2])) {
                $secondGuarantorSurName = $secondGuarantorpieces[2];
                $secondGuarantorGivenName = $secondGuarantorpieces[0]." ".$secondGuarantorpieces[1];
            } else {
                if(isset($secondGuarantorpieces[1])) {
                    $secondGuarantorSurName = $secondGuarantorpieces[1];
                } else {
                    $secondGuarantorSurName = '';
                }
                $secondGuarantorGivenName = $secondGuarantorpieces[0];
            }

            $SecondHomeAddress = [
                'Suburb'             => (isset($form_data['g2Suburb']) ?  $form_data['g2Suburb'] : ''),
                'State'              => (isset($form_data['g2State']) ?  $form_data['g2State'] : ''),
                'Postcode'           => (isset($form_data['g2Postcode']) ?  strval($form_data['g2Postcode']) : ''),
                'Longitude'          => '',
                'StreetName'         => (isset($form_data['g2Address']) ?  $form_data['g2Address'] : ''),
                'StreetNumber'       => (isset($form_data['g2Address']) ?  $form_data['g2Address'] : ''),
                'StreetType'         => (isset($form_data['g2Address']) ?  $form_data['g2Address'] : ''),
                'UnformattedAddress' => (isset($form_data['g2Address']) ? $form_data['g2Address'].', '.$form_data['g2Suburb'].', '.$form_data['g2State'].' '.$form_data['g2Postcode']  : ''),
                // 'Property'           => '',
                // 'UnitNumber'         => '',
                // 'Latitude'           => '',
                ];
            $SecondHomeAddress = array_filter($SecondHomeAddress); 

            $SecondAddressofPropertyOwned = [
                'Suburb'                 => (isset($form_data['g2Suburb']) ?  $form_data['g2Suburb'] : ''),
                'State'                  => (isset($form_data['g2State']) ?  $form_data['g2State'] : ''),
                'Postcode'               => (isset($form_data['g2Postcode']) ?  strval($form_data['g2Postcode']) : ''),
                'StreetName'             => (isset($form_data['g2Address']) ?  $form_data['g2Address'] : ''),
                'StreetNumber'           => (isset($form_data['g2Address']) ?  $form_data['g2Address'] : ''),
                'StreetType'             => (isset($form_data['g2Address']) ?  $form_data['g2Address'] : ''),
                'UnformattedAddress'     => (isset($form_data['g2Address']) ? $form_data['g2Address'].', '.$form_data['g2Suburb'].', '.$form_data['g2State'].' '.$form_data['g2Postcode']  : ''),
                // 'Property'               => '',
                // 'UnitNumber'             => '',
                // 'Latitude'               => '',
                // 'Longitude'              => '',
            ];
            $SecondAddressofPropertyOwned = array_filter($SecondAddressofPropertyOwned); 

            if(isset($form_data['g2Name'])) {
                $secondGuarantor = [
                    'FirstName'                  => (isset($secondGuarantorGivenName) ? $secondGuarantorGivenName : ''),
                    'SurName'                    => (isset($secondGuarantorSurName) ?  $secondGuarantorSurName : ''),
                    'DoB'                        => (isset($g2Birth) ?  $g2Birth : ''),
                    'PrivacyConsent'             => false,
                    'DriverLicenseNo'            => (isset($form_data['g2License']) ?  $form_data['g2License'] : ''),
                    'DriverLicenseExpiryDt'      => (isset($form_data['g2Expiry']) ?  $form_data['g2Expiry'] : ''),
                    'DriverLicenseState'         => (isset($form_data['g2State']) ?  $form_data['g2State'] : ''),
                    'HomeAddress'                => $SecondHomeAddress,
                    'PropertyOwnerFlag'          => (isset($form_data['g2Owned']) ?  $form_data['g2Owned'] : ''),
                    'Role'                       => (isset($form_data['cSPosition']) &&  strlen($form_data['cSPosition']) < 15 ?  $form_data['cSPosition'] : ''),
                    // 'Gender'                     => '',
                    // 'NumberofDependants'         => '',
                    'PropertyValue'              => (isset($SecondPropertyValue) && isset($form_data['g2Owned']) == true ?  $SecondPropertyValue : ''),
                    'MortgageBalance'            => (isset($SecondMortgageBalance) ?  $SecondMortgageBalance : ''),
                    'AddressofPropertyOwned'     => $SecondAddressofPropertyOwned,
                    'Title'                      => 'Unknown',
                    // 'TitleSearchRaw'             => '',
                    // 'TitleSearchType'            => '',
                    // 'EquifaxScore'               => '',
                    // 'EquifaxScoreID'             => '',
                    // 'EquifaxRaw'                 => '',
                    // 'illionScore'                => '',
                    // 'illionScoreID'              => '',
                    // 'illionRaw'                  => '',
                ];
            }
           
            // FILTER ARRAYS IF EMPTY
            $firstGuarantor = array_filter($firstGuarantor);
            if(isset($secondGuarantor)) {
                $secondGuarantor = array_filter($secondGuarantor);
            }

            //Invoice 
            $DeliveryAddress = array(
                'Suburb'             => (isset($form_data['pISuburb']) ?  $form_data['pISuburb'] : ''),
                'State'              => (isset($form_data['pIState']) ?  $form_data['pIState'] : ''),
                'Postcode'           => (isset($form_data['pIPostcode']) ?  $form_data['pIPostcode'] : ''),
                'UnformattedAddress' => (isset($form_data['pIAddress']) ?  $form_data['pIAddress'] : ''),
                'Property'           => '',
                'UnitNumber'         => '',
                'StreetNumber'       => (isset($form_data['pIAddress']) ?  $form_data['pIAddress'] : ''),
                'StreetName'         => (isset($form_data['pIAddress']) ?  $form_data['pIAddress'] : ''),
                'StreetType'         => (isset($form_data['pIAddress']) ?  $form_data['pIAddress'] : ''),
                'Latitude'           => '',
                'Longitude'          => '',
            );
            $DeliveryAddress = array_filter($DeliveryAddress);
            $SupplierAddress = array(
                'Suburb'             => 'Sydney',
                'State'              => 'NSW',
                'Postcode'           => '2000',
                // 'StreetName'         => '',
                // 'StreetNumber'       => '',
                // 'StreetType'         => '',
                // 'Suburb'             => '',
                'UnformattedAddress' => 'Level 2/ 71 York Street, Sydney, NSW 2000',
                // 'Property'           => '',
                // 'UnitNumber'         => '',
                // 'Latitude'           => '',
                // 'Longitude'          => '',
            );
            $SupplierAddress = array_filter($SupplierAddress);
            $Invoice = [
                'Invoice'       => array([
                    'InvoiceNo'     => $forms->id,   
                    'InvoiceAmount' => (isset($InvoiceAmount) ?  $InvoiceAmount : ''),
                    'SupplierABN'   => '',
                    'InvoiceDate'   => $InvoiceDate,
                    'DeliveryAddress' => $DeliveryAddress,  
                    'SupplierACN'      => '',
                    'SupplierAddress'  => $SupplierAddress,
                    'InvoiceDescription'     => ''
            ])];
            $Invoice = array_filter($Invoice['Invoice'][0]); 
            
            //PricingDetails
            $PricingDetails = array(
                'LoanTerm'       => (isset($form_data['aBillingTerm']) ?  intval($form_data['aBillingTerm']) : ''),
                'LoanAmount'     => (isset($LoanAmount) ?  $LoanAmount : ''),
                'PaymentPeriod'  => '',
                'InterestRate'   => '',
                'ResidualValue'  => (isset($ResidualValue) ?  $ResidualValue : ''),
                'AssetCondition' => 'New',
                'PrivateSale'    => '',
                'SaleRentBack'   => '',
            );
            $PricingDetails = array_filter($PricingDetails);

            //Reference
            $Reference = array([
                'CompanyName'   => (isset($form_data['aTradeRef']) ?  $form_data['aTradeRef'] : ''),
                'Telephone'     => (isset($RefTelephone) ?  $RefTelephone : ''),
                'ContactPerson' => (isset($form_data['tContact']) ?  $form_data['tContact'] : ''),
                'Type'          => 'Accountant',
            ]);
            $Reference = array_filter($Reference[0]);

            //BankDetails
            $BranchAddress = array(
                'UnformattedAddress' => '',                 
                        'Property'           => '',                 
                        'UnitNumber'         => '',                 
                        'StreetNumber'       => '',                 
                        'StreetName'         => '',                 
                        'StreetType'         => '',                 
                        'Suburb'             => '',                 
                        'State'              => '',                 
                        'Postcode'           => '',                 
                        'Latitude'           => '',                 
                        'Longitude'          => '',                 
                    );
            $BranchAddress = array_filter($BranchAddress);
            $BankDetails =  array(
                                'BSB'                    => '',
                                'AccountNumber'          => (isset($form_data['aAccDetails']) ?  $form_data['aAccDetails'] : ''),
                                'AccountName'            => (isset($form_data['aAccName']) ?  $form_data['aAccName'] : ''),
                                'AuthorityofDirectDebit' => false,
                                'BranchName'             => (isset($form_data['aFinBranch']) ?  $form_data['aFinBranch'] : ''),
                                'BranchAddress'          => $BranchAddress
                );
            $BankDetails = array_filter($BankDetails);

            // Files
            foreach($files as $key => $row) {
                $array_files[] = array(
                    'DocumentID'   => '',
                    'DocumentName' => $row,
                    'DocumentType' => 'pdf',
                    'DocumentRaw'  => '',
                    'DocumentLink' => 'https://nexgenconfigapp.xyz/assets/files/'.$row,
                );
             $array_allfiles[] = array_filter($array_files[$key]);
            }

            //InsuranceDetails
            $InsuranceDetails = array(
                'InsurerName'   => (isset($form_data['aInsurer']) ?  $form_data['aInsurer'] : ''),
                'PolicyNumber'  => (isset($form_data['aPolicy']) && strlen($form_data['aPolicy']) > 1 ?  $form_data['aPolicy'] : 'N/A'),
                'AmountInsured' => '',
                // 'InsurerEmail'  => $form_data['iEmail'],
                'InsurancePremimum'  => '',
                'InsurerPhoneNumber' => '',
            );
            $InsuranceDetails = array_filter($InsuranceDetails);

            $user_id = $forms->user_id;
            $salesPerson = User::find($user_id);

            $output = array();  
            $output = [
                'AppInfo'       => [
                                        'BrokerAppID'     => $id,
                                        // 'IntroducerGroup' => '',
                                        // 'GrowAppID'       => '',
                                        'SalesPerson'     => $salesPerson->name,
                                        // 'Financier'       => '',
                                        'FinanceType'     => (isset($FinanceType) ?  $FinanceType : 'Software Agreement'),
                                        'AssetCategory'   => '103',
                                        'AssetType'       => '15',
                                        'Disclosed'       => false,

                                    ],
                'Contacts'      => $Contacts,
                'CommercialEntities'=> [$CommercialEntities],
                'Individuals'       => 
                                        ( $secondGuarantorGivenName != null ?  array($firstGuarantor, $secondGuarantor) : array($firstGuarantor)),
                                    
               'Invoice'            => [$Invoice],
                'PricingDetails'    => $PricingDetails,                   
                'Reference'         => [$Reference],    
                'BankDetails'       => $BankDetails,
                'DocumentDetails'   => (isset($array_allfiles) ?  $array_allfiles : ''),
                'InsuranceDetails'  => $InsuranceDetails
            ];


            // FILTER ARRAY IF EMPTY
            $output = array_filter($output);

            
        } else if ($id == 'all') {
            $output = FormData::where('status', '!=',  '')->get();
        } else{
            $output = 'error: invalid id or application is not completed!';
        }

      return Response::json($output);  
    }
}
