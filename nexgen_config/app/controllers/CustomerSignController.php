<?php

class CustomerSignController extends \BaseController
{

 	
	public function index()
	{	
    Blade::setEscapedContentTags('<$', '$>');
		Blade::setContentTags('<$$', '$$>');
    $version = Config::get('app.version');
  
    View::make('login')->with('version',$version);
	}
  public function showForms()
	{
    $id = Request :: input("id");
        $user_id = Request :: input("user");
        if ($id) {
           $data = FormData::where('id', '=',$id)->first();
           
           $forms['data'] = unserialize($data['form_data']);
           $forms['id'] = $data['id'];
           return Response::json(array("success" => "success", "formData" => $forms));
        } else {
          $userStatus = User::find($user_id)["type"];
          if ($userStatus == "admin") {
              $data = DB::table('form_data')->orderBy('created_at', "desc")
                                            ->get();
          } else {
                $data = DB::table('form_data')->orderBy('created_at', "desc")
                                              ->where('user_id', $user_id)
                                          ->get();
            }
            $forms = array();
            foreach ($data as $d) {
                $item['creator_id'] = $d->user_id;
                $item['creator_name'] = User::find($d->user_id)["username"];
                //echo 'Record ID : ' . $d->id . '-' . $d->form_data . '<br />';
                $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
                 function($match) {
                   return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                 },
                 $d->form_data );
               //preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $d->form_data);
                $item['data'] = unserialize($data);
                $item['created_at'] = $d->created_at;
                $item['status'] = $d->status;
                $item['id'] = $d->id;
                if (array_key_exists('type',$item['data'])) {
                     $types = $item['data']['type'];
                     $formNameStr = "";
                     foreach ($types as $i => $tp) {
                         if ($tp == true) {
                            $formNameStr.= $i.", ";
                         }
                     }
 
                     $item['types'] = substr_replace($formNameStr,"","-2");
                }
 
                $forms[] = $item;
             }
            return Response::json(array("success" => "success", "formData" => $forms, "userStatus" => $userStatus));
        }
  }

  public function complete()
  {	
    Auth::logout();
    return Response::json(array("success" => "success"));

  }

  public function done()
  {	

  }
}