<?php

class JobsheetsController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$jobsheets = Proposals::all();
		return Response::json($jobsheets);
    }
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function searchCustomer()
	{
        $companyName = Request :: input("companyName");
        $jobsheets = DB::table('jobsheet_data')->orderBy('form_id', "asc")
        ->where('company_name', 'like', '%' . $companyName . '%')
        ->get();
        $counts = array();
        foreach ($jobsheets as $key => $item) {
            if (!isset($counts[$item->form_id])) {
                $counts[$item->form_id][0]= 0;
            }
            $counts[$item->form_id][] = [
                'jobsheet_id' => $item->id
            ];
            $counts[$item->form_id][0]++;
        }
        foreach($jobsheets as $key => $list) {
            $jobsheet_list[$list->id] = $list;
        }
        $data_results = DB::table('jobsheet_data')->orderBy('form_id', "asc")
        ->where('company_name', 'like', '%' . $companyName . '%')
        ->groupBy('form_id')
        ->get();
        if($data_results) {
            return Response::json(array('results' => $data_results, 'counts' => $counts, 'jobsheet_list' => $jobsheet_list, 'message' => ''));

        } else {
            return Response::json(array('results' => $data_results, 'counts' => $counts, 'message' => 'No results found!'));
        }
    }
    /**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

    public function showJobsheets()
    {
        $id = Request::input("id");
        if ($id) {
            $data = Jobsheets::find($id);
           $jobsheetStatus = $data->status;
           $jobsheetType = $data->type;
           $jobsheetUpdatedAt = $data->updated_at;

            $jobsheet['jobsheet_number'] = $data['jobsheet_number'];
            // $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
            // function($match) {
            // return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
            // },
            // $data['jobsheet_data']);
            // $jobsheet['data'] = unserialize($data);



            $data_files = preg_replace_callback ('!s:(\d+):"(.*?)";!',
            function($match) {
            return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
            },
            $data['files']);
            $jobsheet['files'] = unserialize($data_files);


            $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
            function($match) {
            return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
            },
            $data['jobsheet_data']);
            $jobsheet['data'] = unserialize($data);
         




           $jobsheet['id'] = $id;
           return Response::json(array("success" => "success", "jobsheetData" => $jobsheet, "jobsheetType" => $jobsheetType, "jobsheetStatus" => $jobsheetStatus, 'jobsheetUpdatedAt' => $jobsheetUpdatedAt ));
        }

    }
    	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function saveData()
    {

        $data = array();
        $data = Request::input("data");
        $id = Request::input("id");
        $user_id = Request::input("user_id");
        $js_num = Request::input("js_num");
        $status = Request::input("status");
        $type = Request::input("type");
        $action = Request::input("action");
        $jobsheetData = serialize($data);
        $editId = Request::input("editId");
        if ($editId) {
            $js = Jobsheets::find($editId);
            $js->jobsheet_data = $jobsheetData;
            $js->user_id = $user_id;

        } else {
            $rep = Jobsheets::find($id);
            $form_id = $rep->form_id;
            $company_name = $rep->company_name;
            $address = $rep->address;
            if($action == 'new') {
                $jobsheet_data = array(
                    'companyName' =>  $company_name,
                );
            } else {
                $jobsheet_data = unserialize($rep->jobsheet_data);
            }
            $files = array();
            $js = new Jobsheets();
            $js->form_id = $form_id;
            $js->company_name = $company_name;
            $js->address = $address;
            $js->jobsheet_number = $js_num;
            $js->type =  $type;
            $js->status =  $status;
            $js->user_id = $user_id;
            $js->jobsheet_data = serialize($jobsheet_data); 
            $js->files = serialize($files);
        }
        try {
            $js->status = $status;

            $js->save();
            return Response::json(array("success" => "success", "editId"=> $js->id,  "message" => 'Changes saved successfully.'));
        } catch (Exception $ex) {
            return Response::json(array("success" => "false", "message" => $ex->getMessage()));
        }
    }
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function createPdf()
    {
        set_time_limit(0);
        $forms = Request::input("htmlData");
        $js_id = Request::input("js_id");

        if($forms) {
            $formsValue ='1';
   
        } else {
            $formsValue = '0';
        }
        $userDetails = Request::input("userDetails");
        $account = Request::input("account");
        $fromEmail = "noreply@nexgenconfigapp.xyz";
        $fromName = "Nexgen";
        $subject = 'Welcome to Nexgen';

        $get_info = Jobsheets::find($js_id);
        $data = $get_info->jobsheet_data;
        $toName = $get_info->contact_person;
        $toEmail = $get_info->contact_email;
        $files = $get_info->files;
        $attachments = unserialize($files);
        $accEmail = $account['email'];
        $accName = $account['name'];
        


        $mailData = [
            'user' => $toName,
            'userEmail' => $toEmail,
            'technician' => $accName,
            'name' => $fromName,
            'email'=> $fromEmail
        ];
        $paths = array();

        $attachment = array_push($paths, $attachments);

        if (Auth::user()) {
            $managerEmail = explode(";",Auth::user()->manageremail);
        }
 
        foreach($forms as $form) {
            $mpdf = new \Mpdf\Mpdf([
                'mode' => 'utf-8',
                'format' => 'A4',
                'orientation' => 'P',

            ]);
            $mpdf->packTableData = true;
            // Buffer the following html with PHP so we can store it to a variable later
            ob_start();
            // This is where your script would normally output the HTML using echo or print
            echo $form['html'][0];
            // Now collect the output buffer into a variable
            $html = ob_get_contents();
            ob_end_clean();
            // send the captured HTML from the output buffer to the mPDF class for processing
            $mpdf->WriteHTML($html);
            $data = array("pdf" => $mpdf->Output("assets/files/".$form['fileName']));
            $paths[] = "assets/files/".$form['fileName'];
        }

        
        if ($formsValue == '1' && strpos($form['fileName'],'draft')!==false ){
            $emailTemplate = 'emails.form.draftforms';
            } else {
            $emailTemplate = 'emails.form.proposals';
            }
            
        // Mail::send($emailTemplate, $mailData, function($message) use ($subject, $paths, $accEmail, $managerEmail)
        // {
        //     $message->to($accEmail);
        //     if ($accEmail!='info@somelongcompany.com.au'){
        //         $message->bcc('JamesHarb@nexgen.com.au')
        //             ->bcc('ElieAyoub@nexgen.com.au')
        //             ->bcc('developer@businesstelecom.com.au')
        //             ->bcc('QA@nexgen.com.au');
        //     }
        //     else {
        //         $message->bcc('JamesHarb@nexgen.com.au')
        //             ->bcc('ElieAyoub@nexgen.com.au')
        //             ->bcc('developer@businesstelecom.com.au');
        //     }
            
        //     foreach ($managerEmail as $manager){
        //         $message->bcc($manager);
        //     }

        //     $message->subject($subject);

        //     foreach ($paths as $path) {
        //         $message->attach($path);
        //     }
        // });
    

        if ($formsValue == '1'){
            if (Mail::failures()) {
                Log::debug(Mail::failures());
                return Response::json(array("success" => "fail"));
            } else {
                return Response::json(array("success" => "success","managerEmail"=>$managerEmail));
            }
        } else {
            return Response::json(array("success" => "failed"));
        }   
        // return Response::json(array("success"=>"Call to function Successful")); 
    }
       	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

    public function upload(){

		if ( !empty( $_FILES ) ) {

		    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
		    // $uploadPath = storage_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
			$uploadPathT = 'assets' . "/" . 'files' . "/" . 'uploads' . "/" . $_FILES[ 'file' ][ 'name' ];

		    move_uploaded_file( $tempPath, $uploadPathT );

		    $answer = array( 'answer' => 'File transfer completed', 'FilePath' => $uploadPathT );
		    $json = json_encode( $answer );

			//Log::error($exception);
		    echo $json;

		} else {

		    echo 'No files';
		}
	}

	public function uploads(){

        $js_id = Request::input("js_id");

        $js = Jobsheets::find($js_id);
        $file = $js->files;
        $files = unserialize($file);
        $new = array_push($files, $_FILES[ 'file' ][ 'name' ]);
        $files = serialize($files);
        $js->files = $files;
        $js->save();

		if ( !empty( $_FILES ) ) {

		    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
		    // $uploadPath = storage_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
			$uploadPathT = 'assets' . "/" . 'files' . "/" . 'uploads' . "/" . $_FILES[ 'file' ][ 'name' ];

		    move_uploaded_file( $tempPath, $uploadPathT );

		    $answer = array( 'answer' => 'File transfer completed', 'FilePath' => $uploadPathT );
		    $json = json_encode( $answer );

			//Log::error($exception);
		    echo $json;

		} else {

		    echo 'No files';
        }
        
       
    }
         	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

    public function getAttached(){
        $js_id = Request::input("js_id");

        $js = Jobsheets::find($js_id);
        $file = $js->files;

        $data = preg_replace_callback ('!s:(\d+):"(.*?)";!',
        function($match) {
          return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
        },
        $js->files );

        $allAttaced = unserialize($data);
        if ($allAttaced ) {
            return Response::json(array("success" => "success", "allAtached" => $allAttaced));
        } else {
            return Response::json(array("success" => "failed"));
        }
    }
}
