
var Qms = null; //'summernote',
(function() {
    Qms = angular.module('qms', [
        'ui.router', 'ngResource', 'angularFileUpload', 'ui.bootstrap',"ngTouch", "angucomplete-alt","ngTableToCsv", 
        'tld.csvDownload',"ui.bootstrap",'datatables', 'ngSanitize', 'ui.select', 'datatables.buttons',"ngDialog",
        'textAngular', 'localytics.directives','ngMap','google.places','ui.tinymce','ngFileUpload', 'ui.filters'
    ]);
})();

Qms.controller('DefaultCtrl', function($scope, Scopes, $rootScope, $http, FlashMessage,$locale) {
    $scope.flashMessage = FlashMessage;
    $scope.currentYear = new Date().getFullYear();
    $scope.currentMonth = new Date().getMonth() + 1;
    $scope.months = $locale.DATETIME_FORMATS.MONTH;
    $rootScope.version = _version;
    //alert($scope.currentYear + '-' + $scope.months);
    $http.get("profile").success(function(response) {
        $scope.profile = response;
        $rootScope.profile = response;
        $rootScope.sign = response.sign;
        Scopes.store('DefaultCtrl', $scope);
    });
})

Qms.controller('ServiceQualifyCtrl',function($scope, $rootScope,$state,FlashMessage,NgMap,GeoCoder,FrontierQualification,ngDialog,$filter,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder , $http){
    $scope.vm = this;
    $scope.advancedShown = false;
    $scope.xmlResponse = "";
    $scope.address = {};
    $scope.fnn = {};
    $scope.autoCompleteOptions = {
        componentRestrictions : {country: 'au'},
        types : 'geocode'
    }
    $scope.locationId = "";

    $scope.sort = {       
            sortingOrder : 'locationId',
            reverse : false
        };
    
    $scope.gap = 10;
    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 5;
    $scope.pagedItems = [];
    $scope.currentPage = 0;

    NgMap.getMap().then(function(evtMap) {
        //console.log(evtMap);
        $scope.vm.map = evtMap;
        var ll = evtMap.markers[0].position;
        GeoCoder.geocode({'latLng': ll}).then(function(result){
            //console.log(result);
            $scope.deleteMarkers();
            $scope.vm.positions.push({lat:ll.lat(),lng:ll.lng()});
            $scope.locationReference = result[0];
        });
    });

    $scope.getCurrentLocation = function(){
        var position;
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(
                function(position){
                    position = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                }
            );
        } else {
            position = {lat:-33.867926,lng:151.205392};
        }
        if (angular.isUndefined(position)){
            position = {lat:-33.867926,lng:151.205392};   
        }
        //console.log(position);
        return position;
    }

    $scope.vm.positions = [{
                            lat: $scope.getCurrentLocation().lat,
                            lng: $scope.getCurrentLocation().lng
                        }];

    $scope.loadAddress = function(){
        var address = $('#locationReference').val();
        GeoCoder.geocode({'address': address}).then(function(result){
            $scope.locationReference = result[0];
            $('#locationReference').val($scope.locationReference.formatted_address);
        });
    }

    $scope.deleteMarkers = function(){
        $scope.vm.positions = [];
    }

    $scope.placeChanged = function(){
        $scope.vm.place = this.getPlace();
        var location = $scope.vm.place.geometry.location();
        $scope.deleteMarkers();
        $scope.vm.positions.push({lat : location.lat(),lng: location.lng()});
    }

    $scope.addMarker = function(event){
        //nsole.log(event);
        var ll = event.latLng;
        GeoCoder.geocode({'latLng': ll}).then(function(result){
            //console.log(result);
            $scope.deleteMarkers();
            $scope.vm.positions.push({lat:ll.lat(),lng:ll.lng()});
            $scope.locationReference = result[0];
            $('#locationReference').val($scope.locationReference.formatted_address);
        });
    }

    $scope.showMarkers = function(){
        for(var key in $scope.vm.markers){
            vm.map.markers[key].setMap($scope.vm.map);
        }
    }

    $scope.searchAddress = function(){
        // $scope.clearMarkers();
        var location = $scope.locationReference.geometry.location;
        $scope.deleteMarkers();
        //console.log(location);
        $scope.vm.positions.push({lat : location.lat(),lng: location.lng()});
    }

    $scope.toggleAdvanced = function(){
        $scope.advancedShown = !$scope.advancedShown;
    }

    $scope.sqCsv = function() {
        $scope.sqCsv = !$scope.sqCsv;
        FrontierQualification.sq_Csv({addresses:$scope.addresses}, function(response) {
            // console.log($scope.addresses);
            if(response.success){
                $scope.csvFile = response.filename;
                $('#sqCSV').click();
            }
        });

    }
    
    $scope.response = null;    
    $scope.locationId = {};
    
    $scope.qualifyAddress = function(){
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        console.log($scope.locationReference);
        var address = {
            'streetNumber' : $scope.locationReference.address_components[0].short_name,
            'streetName' : $scope.locationReference.address_components[1].long_name,
            'suburb' : $scope.locationReference.address_components[2].short_name,
            'state' : $scope.locationReference.address_components[0].short_name,
            'postcode' : $scope.locationReference.address_components[4].short_name
        };

        FrontierQualification.get_addresses(address, function(data) {
        $scope.loading = false;        
            $scope.addresses = data.response;
            // console.log($scope.items);
            if (data.success){
                $scope.template = "templates/catalog/frontier/sq_address.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ServiceQualifyCtrl',
                     width: 1080,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                 });
                $scope.vms = {};
                $scope.vms.dtInstance = {};   
                // $scope.vms.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
                $scope.vms.dtOptions = DTOptionsBuilder.newOptions()
                                  .withOption('paging', true)
                                  .withOption('searching', true)
                                  .withOption('info', true)
                                  .withOption('lengthMenu',[5,10,15,20,50,100])
            }
        });
    }
    $scope.qualifyServiceNumber = function(){
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        FrontierQualification.qualify({'serviceNumber' : $scope.fnn.serviceNumber, 'serviceProvider' : ''}, function(data){
            $scope.loading = false;
            $scope.items = data.response;
            if (data.success){
                $scope.template = "templates/catalog/frontier/sq_response.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ServiceQualifyCtrl',
                     width: 1080,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                 });
                $scope.vms = {};
                $scope.vms.dtInstance = {};   
                // $scope.vms.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
                $scope.vms.dtOptions = DTOptionsBuilder.newOptions()
                    .withOption('paging', true)
                    .withOption('searching', true)
                    .withOption('info', true)
                    .withOption('lengthMenu',[5,10,15,20,50,100])
            }
        });

    }


    $scope.qualifyAdvance = function(){
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        // console.log($scope.address);
        var advance = {
            'streetNumber' : $scope.address.streetNumber,
            'streetName' : $scope.address.streetName,
            'suburb' : $scope.address.suburb,
            'state' : $scope.address.state,
            'postcode' : $scope.address.postcode
        };

        FrontierQualification.get_advance(advance, function(data) {
            $scope.loading = false;
            console.log(advance);
            $scope.addresses = data.response;
            if (data.success){
                $scope.template = "templates/catalog/frontier/sq_address.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ServiceQualifyCtrl',
                     width: 1080,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                     // preCloseCallback : function(value) {
                     //     var signData = $('img.imported').attr('src');
                     //     if ((!signData) && (!$rootScope.isDraftMode)) {
                     //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                     //             return true;
                     //         }
                     //         return false;
                     //         $timeout(angular.noop());
                     //     }
                     // }
                 });
                $scope.vms = {};
                $scope.vms.dtInstance = {};   
                // $scope.vms.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
                $scope.vms.dtOptions = DTOptionsBuilder.newOptions()
                                  .withOption('paging', true)
                                  .withOption('searching', true)
                                  .withOption('info', true)
                                  .withOption('lengthMenu',[5,10,15,20,50,100])
            }
        });
    }

    $scope.showClicked = function(item){
        // alert('Clicked');
        //console.log(item);
        $scope.location  = item;
    };

    $scope.qualifyLocationId = function(){
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        if ($scope.location!=undefined){
            $scope.msg = ' ';
            //console.log($scope.location);
            // $scope.location = {
            //     'locationId' : '5554545454',
            //     'serviceProvider' : 'nbn'
            // }    
        // console.log($scope.location);
            $scope.vma = {};
            $scope.vma.dtInstance = {};   
            // $scope.vma.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vma.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100])

            FrontierQualification.qualify($scope.location, function(data) {
                $scope.loading = false;
                $scope.items = data.response;
                if (data.success){
                    $scope.template = "templates/catalog/frontier/sq_response.html";
                    ngDialog.open({
                         template: $scope.template,
                         plan: true,
                         controller : 'ServiceQualifyCtrl',
                         width: 1080,
                         scope: $scope,
                         className: 'ngdialog-theme-default',
                         showClose: true,
                         closeByEscape: true,
                         closeByDocument: true,
                         sableAnimation: false
                         // preCloseCallback : function(value) {
                         //     var signData = $('img.imported').attr('src');
                         //     if ((!signData) && (!$rootScope.isDraftMode)) {
                         //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                         //             return true;
                         //         }
                         //         return false;
                         //         $timeout(angular.noop());
                         //     }
                         // }

                     });

                }

                    
            });
             
            
        }

        else{       
            $scope.loading = false;   
            alert('Please choose One Row!');
            // $scope.msg = 'Please choose One Row!';
        }
        
    }
});

Qms.controller('RatesCtrl',function($scope, $location, $state,
$stateParams, $filter, Form, $location,Scopes, ScheduleGoods, $rootScope, FlashMessage,
$http, ngDialog,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder,$timeout,
$interval,Category,Upload, $timeout, Rates){
  
  //console.log($stateParams.id);
  if ($stateParams.id) {
    Rates.get({
                id: $stateParams.id
                }, function(data) {
                  $scope.rateForm = {};
                  $scope.rateForm.id = data.id;
                  $scope.rateForm.name = data.name;
                  $scope.rateForm.term = data.term;
                  $scope.rateForm.type = data.type;
                  $scope.rateForm.effective_at = new Date(data.effective_at); 
                  $scope.rateForm.rate = [];
                  angular.forEach(data.rates,function (item,index){
                    $scope.rateForm.rate.push({
                        id : item.id,
                        rate : item.rate,
                        lowerLimit : item.type,
                        lowerLimit : item.lowerLimit,
                        upperLimit : item.upperLimit
                      })
                  })
             });
  } else {
    $scope.rateForm={};
    $scope.rateForm.name ='';
    $scope.rateForm.term ='';
    $scope.rateForm.type ='';
    $scope.rateForm.rate = [{id: '1', rate:'', lowerLimit:'',  upperLimit:''}];
    $scope.rateForm.effective_at = new Date();
  }

  $scope.addRates = function() {
    var newItemNo = $scope.rateForm.rate.length+1;
    $scope.rateForm.rate.push({'id':+newItemNo});
  };

  $scope.removeRates = function(index) {
    $scope.rateForm.rate.splice(index,1);
  };

  $scope.saveRate = function() {
   if (angular.isDefined($scope.rateForm.id)) {
            Rates.update({
                id: $scope.rateForm.id
            }, $scope.rateForm, function(data) {
                FlashMessage.setMessage(data);
                if (data.success)
                    $location.path("/rates");
            });
            console.log($scope.rateForm);
        }
    else{
      Rates.save($scope.rateForm, 
                  function(data) {
                      $scope.editId = data.editId;
                       if (data.success)
                      $location.path("/rates");
                  });    
        }
    }
})

Qms.controller('RatesListCtrl',function($scope,$stateParams,$rootScope,$filter,$location,$http,$state,ScheduleGoods,FileUploader,FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder, Rates){
    $scope.vmrates = {};
            $scope.vmrates.dtInstance = {};   
            $scope.vmrates.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('order', [[8, 'desc']])
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500]);
    $scope.rateForm = Rates.query();

    $scope.edit = function(row_id) {
           $state.go('edit_rates', {
              id: row_id
            });
           console.log($scope.rateForm);
    }
    $scope.delete = function(row_id) {
        Rates.delete({
            id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.rateForm = Rates.query();

        });
    }

});


Qms.controller('ScheduleGoodsCtrl',function($scope, $location, $state,
$stateParams, $filter, Form, Scopes, ScheduleGoods, $rootScope, FlashMessage,
$http, ngDialog,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder,$timeout,
$interval,Category,FileUploader, $timeout){
   
    $scope.categories = Category.query();

    Form.getScheduleGoods(function(data) {
        $scope.scheduleGoodsList = data.goods;
        $scope.scheduleGoodsList.unshift({
            item: ""
        });
    });
     
    if ($stateParams.id) {
        $scope.sg_form = ScheduleGoods.get({
            id: $stateParams.id
        }, function(data) {

        });
    }
    else {
        $scope.sg_form ={};
        $scope.sg_form.descriptions = '';
        $scope.sg_form.features = '';
        $scope.sg_form.hardware_solutions = '';
        $scope.sg_form.img1 = '';
        $scope.sg_form.img2 = '';
        $scope.sg_form.item = '';
        $scope.sg_form.rrp = 0;
        $scope.sg_form.model = 0;
    }


    var uploaders = $scope.uploaders = new FileUploader({
        url: '/schedule_goods/upload',
        formData: [{
            name: null
        }]
    });

    var dataPath;

    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploaders.onSuccessItem = function(fileItem, response, status, headers) {
        dataPath = response.FilePath;
        $scope.sg_form.img2 = dataPath;
    };

    var uploader = $scope.uploader = new FileUploader({
        url: '/schedule_goods/upload',
        formData: [{
            name: null
        }]
    });

    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        dataPath = response.FilePath;
        $scope.sg_form.img1 = dataPath;
    };

    $scope.save = function() {
          if (!$("#sg_form").parsley('validate')) return false;
          console.log($scope.sg_form);
          if (angular.isDefined($scope.sg_form.id)) {
              console.log($scope.sg_form);
              ScheduleGoods.update({
                  id: $scope.sg_form.id
              }, $scope.sg_form, function(data) {
                  FlashMessage.setMessage(data);
                  if (data.success)
                      $location.path("/schedule_goods");
              });
          } else {
              ScheduleGoods.save($scope.sg_form, function(data) {
                  FlashMessage.setMessage(data);
                  if (data.success)
                      $location.path("/schedule_goods");
              });
          }
    }

  $scope.tinymceOptions = {
    selector: "textarea",
    theme: "modern",
    paste_data_images: true,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons "
  };
  
})

Qms.controller('ScheduleGoodsListCtrl',function($scope,$filter,$http,$state,ScheduleGoods,FileUploader,FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder, Category){
    $scope.vmsc = {};
            $scope.vmsc.dtInstance = {};   
            $scope.vmsc.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('order', [[5, 'desc']])
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500]);

    $scope.sched_goods = ScheduleGoods.query();
    $scope.categories = Category.query();
    $scope.edit = function(row_id) {
        var x=window.confirm("Do you want to Modify this? d2 ba")
        if (x)
        $state.go('edit_sg', {
            id: row_id
        });
    }
    $scope.delete = function(row_id) 
    {
        var x=window.confirm("Do you want to Delete this?")
        if (x)
        ScheduleGoods.delete({
            id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.sched_goods = ScheduleGoods.query();

        });
    }
});



Qms.controller('ProductListCtrl', function($scope, $filter, $http, $state, Product, FileUploader, FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
    
    $scope.vmp = {};
            $scope.vmp.dtInstance = {};   
            // $scope.vmp.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vmp.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.products = Product.query();

    $scope.edit = function(row_id) {
        $state.go('edit_product', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Product.delete({
            product_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.products = Product.query();

        });
    }
});

Qms.controller('ProductCtrl', function($scope, $http, $state, $location, $stateParams, Product, Category, FlashMessage, FileUploader) {
    var uploader = $scope.uploader = new FileUploader({
        url: 'product/upload',
        formData: [{
            name: null
        }]
    });

    var dataPath;

    console.log(dataPath);
    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        dataPath = response.FilePath
    };

    if ($stateParams.id)
        $scope.product = Product.get({
            product_id: $stateParams.id
        });
    else
        $scope.product = {};

    
    $scope.products = Product.query();


    $scope.save = function() {

        if (!$("#products_form").parsley('validate'))
            return;

        if (angular.isDefined($scope.product.id)) {
            $scope.product.image = dataPath;
            Product.update({
                product_id: $scope.product.id
            }, $scope.product, function(data) {
                FlashMessage.setMessage(data);

                if (data.success)
                    $location.path("/products");
            })
        } else {
            Product.save($scope.product, function(data) {
                FlashMessage.setMessage(data);

                if (data.success)
                    $location.path("/products");

            })

        }
    }

});



Qms.controller('CategoryListCtrl', function($scope, $http, $state, $location, $stateParams, Category, FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
    $scope.vmc = {};
            $scope.vmc.dtInstance = {};   
            // $scope.vmc.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vmc.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.categories = Category.query();
    $scope.edit = function(row_id) {
        $state.go('edit_category', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Category.delete({
            category_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.categories = Category.query();
        });
    }

});


Qms.controller('CategoryCtrl', function($scope, $http, $state, $location, $stateParams, Category, FinanceType, Typet, FlashMessage) {
    $scope.categories = Category.query();

    if ($stateParams.id)
        $scope.category = Category.get({
            category_id: $stateParams.id
        });
    else
        $scope.category = {};

    $scope.financetypes = FinanceType.query();
    $scope.typets = Typet.query();

    $scope.save = function() {
        if (!$("#cat_form").parsley('validate')) return false;

        if (angular.isDefined($scope.category.id)) {
            Category.update({
                category_id: $scope.category.id
            }, $scope.category, function(data) {
                FlashMessage.setMessage(data);

                if (data.success)
                    $location.path("/categories");
            });

        } else {


            Category.save($scope.category, function(data) {
                FlashMessage.setMessage(data);
                if (data.success)
                    $location.path("/categories");
            });
        }
    }

});

Qms.controller('UserListCtrl', function($scope, $http, $state, $location, $stateParams, User, FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
    $scope.vmu = {};
            $scope.vmu.dtInstance = {};   
            $scope.vmu.dtOptions = DTOptionsBuilder.newOptions()
                                .withOption('order', [[ 0, 'desc' ]])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.pages = 0;
    $scope.itemsPerPage = 10;
    $scope.count = 0;
    $scope.status = "";

    $scope.sortType = 'created_at';
    $scope.reverse = false;

    $scope.users = User.query();
    
    $scope.edit = function(row_id) {
        $state.go('edit_user', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        User.delete({
            user_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.users = User.query();

        });
        
    }
});


Qms.controller('UserCtrl', function($scope, $http, $state, $location, $stateParams, User, FlashMessage, Scopes, $rootScope, Users) {
    $scope.users = User.query();
    $scope.noSign = true;
    // $scope.allUsers = $scope.users;
        
    $scope.pickstaff = function(staff_id){
        $scope.staff_id = staff_id;
        var value = document.getElementById("boxstaff-"+staff_id).value;
        if(value == "1") {
            document.getElementById("boxstaff-"+staff_id).value='0'; 
        } else {
            document.getElementById("boxstaff-"+staff_id).value='1'; 
        }
        console.log(value);
        var pickUser  = Users.pickstaff({
             user_id: $scope.user.id,
            staff_id: $scope.staff_id,
            value: value
        }, function(data) {
        
        });

    console.log($scope.staff_id);

    }  
    

    if ($stateParams.id) {
        $scope.user = User.get({
            user_id: $stateParams.id
        }, function(data) {
            console.log(data);
          if (data.sign && data.sign_exists) {
              $scope.noSign = false;
          }
        });
        $scope.allUsers = Users.showUsers({
            user_id: $stateParams.id
        }, function(data) {
        
        });
        console.log($scope.user);
    
    }
    else {
        $scope.user = {};
    }

    $scope.profile = [];
    $scope.readonly = true;
    
    $http.get("profile").success(function(response) {
            $scope.profile = response;
            if (response.type == 'admin') {
                $scope.readonly = false;
            }
        if ($state.current.url == "/user/settings") {
            $state.go('edit_user', {
            id: $scope.profile.id
        });
     

    }
        });
    $scope.$watch('profile');
    $scope.$watch('readonly');
    $scope.$watch('user');


    $('#userSign').jSignature({color:"#000000",lineWidth:1,
                                   width :350, height :70,
                                   cssclass : "signature-canvas",
                                  "decor-color": 'transparent'
                                 });

    $scope.clearSg = function() {
        $scope.noSign = true;
        $("#userSign").jSignature("reset");
    }


    $scope.save = function() {
        var pInstance = $("#user_form").parsley();

        if ( !pInstance.validate(true) ) {
            return false;
        }

        //get the signature

        if ($("#userSign").jSignature('getData', 'native').length != 0) {
            var signData = $("#userSign").jSignature('getData');
            $scope.user.signData = signData;
        } else {
            $scope.user.signData = "";
        }

        $scope.user.financeonly = $('input[name=financeonly]').prop('checked');

        if (angular.isDefined($scope.user.id)) {
            User.update({
                user_id: $scope.user.id
            }, $scope.user, function(data) {

                FlashMessage.setMessage(data);
                if (data.success && !$scope.readonly) {
                    $location.path("/users");
                    $http.get("profile").success(function(response) {
                        $rootScope.sign = response.sign;
                    });
                } else {
                   $(window).scrollTop(0);
                }
            });

        } else {

            User.save($scope.user, function(data) {
                FlashMessage.setMessage(data);
                if (data.success) {
                    $location.path("/users");
                    $http.get("profile").success(function(response) {
                        $rootScope.sign = response.sign;
                    });
                }
            })
        }
    }
});




Qms.controller('CompanyListCtrl', function($scope, $http, $state, $location, $stateParams, Company, FlashMessage) {

    $scope.companies = Company.query();


    $scope.edit = function(row_id) {
        $state.go('edit_company', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Company.delete({
            company_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.companies = Company.query();

        });
    }

});


Qms.controller('CompanyCtrl', function($scope, $http, $state, $location, $stateParams, Company, FlashMessage) {
    $scope.companies = Company.query();

    if ($stateParams.id)
        $scope.company = Company.get({
            company_id: $stateParams.id
        });
    else
        $scope.company = {};


    $scope.save = function() {
        if (!$("#company_form").parsley('validate')) return;

        if (angular.isDefined($scope.company.id)) {

            Company.update({
                company_id: $scope.company.id
            }, $scope.company, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/companies");
            })

        } else {
            Company.save($scope.company, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/companies");
            })

        }
    }

});


Qms.controller('SalesRepListCtrl', function($scope, $http, $state, $location, $stateParams, Salesrep, FlashMessage) {
    $scope.salesreps = Salesrep.query();


    $scope.edit = function(row_id) {
        $state.go('edit_salesrep', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Salesrep.delete({
            salesrep_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.salesreps = Salesrep.query();


        });
    }

});


Qms.controller('SalesRepCtrl', function($scope, $http, $state, $location, $stateParams, Salesrep, FlashMessage) {

    $scope.salesreps = Salesrep.query();

    if ($stateParams.id)
        $scope.salesrep = Salesrep.get({
            salesrep_id: $stateParams.id
        });
    else
        $scope.salesrep = {};


    $scope.save = function() {

        if (!$("#salesrep_form").parsley('validate')) return;

        if (angular.isDefined($scope.salesrep.id)) {

            Salesrep.update({
                salesrep_id: $scope.salesrep.id
            }, $scope.salesrep, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/salesreps");
            })

        } else {
            Salesrep.save($scope.salesrep, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/salesreps");
            })

        }


    }
});




Qms.controller('TelemarketerListCtrl', function($scope, $http, $state, $location, $stateParams, Telemarketer, FlashMessage) {

    $scope.telemarketers = Telemarketer.query();

    $scope.edit = function(row_id) {
        $state.go('edit_telemarketer', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Telemarketer.delete({
            telemarketer_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.telemarketers = Telemarketer.query();

        });
    }
});


Qms.controller('TelemarketerCtrl', function($scope, $http, $state, $location, $stateParams, Telemarketer, FlashMessage) {
    $scope.telemarketers = Telemarketer.query();

    if ($stateParams.id)
        $scope.telemarketer = Telemarketer.get({
            telemarketer_id: $stateParams.id
        });
    else
        $scope.telemarketer = {};


    $scope.save = function() {

        if (!$("#telemarketer_form").parsley('validate')) return;

        if (angular.isDefined($scope.telemarketer.id)) {

            Telemarketer.update({
                telemarketer_id: $scope.telemarketer.id
            }, $scope.telemarketer, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/telemarketers");
            })

        } else {
            Telemarketer.save($scope.telemarketer, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/s");
            })

        }


    }
});


Qms.controller('WinBackListCtrl', function($scope, $http, $state, $location, $stateParams, Winback, FlashMessage) {

    $scope.winbacks = Winback.query();

    $scope.edit = function(row_id) {
        $state.go('edit_winback', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Winback.delete({
            winback_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.winbacks = Winback.query();

        });
    }
});


Qms.controller('WinBackCtrl', function($scope, $http, $state, $location, $stateParams, Winback, FlashMessage) {
    $scope.winbacks = Winback.query();

    if ($stateParams.id)
        $scope.winback = Winback.get({
            winback_id: $stateParams.id
        });
    else
        $scope.winback = {};


    $scope.save = function() {

        if (!$("#winback_form").parsley('validate')) return;

        if (angular.isDefined($scope.winback.id)) {

            Winback.update({
                winback_id: $scope.winback.id
            }, $scope.winback, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/winbacks");
            })

        } else {
            Winback.save($scope.winback, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/winbacks");
            })

        }


    }

});



Qms.controller('LeadSourceListCtrl', function($scope, $http, $state, $location, $stateParams, Leadsource, FlashMessage) {
    $scope.leadsources = Leadsource.query();

    $scope.edit = function(row_id) {
        $state.go('edit_leadsource', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Leadsource.delete({
            leadsource_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true) {
                $scope.leadsources = Leadsource.query();

            }
        });
    }
});


Qms.controller('LeadSourceCtrl', function($scope, $http, $state, $location, $stateParams, Leadsource, FlashMessage) {
    $scope.leadsources = Leadsource.query();

    if ($stateParams.id)
        $scope.leadsource = Leadsource.get({
            leadsource_id: $stateParams.id
        });
    else
        $scope.leadsource = {};


    $scope.save = function() {

        if (!$("#leadsource_form").parsley('validate')) return;

        if (angular.isDefined($scope.leadsource.id)) {

            Leadsource.update({
                leadsource_id: $scope.leadsource.id
            }, $scope.leadsource, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/leadsources");
            })

        } else {
            Leadsource.save($scope.leadsource, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/leadsources");
            })

        }

    }

});




Qms.controller('ConfigCtrl', function($scope, $http, $state, $location, $stateParams, Config, FlashMessage) {
    $scope.config = Config.get();



    $scope.save = function() {

        if (!$("#config_form").parsley('validate')) return;

        //console.log($scope.config);
        Config.save($scope.config, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/config");
        })


    }

});



Qms.controller('FinanceRatesCtrl', function($scope, $http, $state, $location, $stateParams, SellPrice, RentalPrice, FlashMessage) {
    $scope.sellprice = SellPrice.get();
    $scope.rentalprice = RentalPrice.get();


    $scope.saveSellPrice = function() {

        SellPrice.save($scope.sellprice, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/finance_rates");
        })

    }

    $scope.saveRentalPrice = function() {

        RentalPrice.save($scope.rentalprice, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/finance_rates");
        })

    }

});




Qms.controller('FlexiRentCtrl', function($scope, $http, $state, $location, $stateParams, FlexiRent, FlashMessage) {
    $scope.fr = FlexiRent.query(function(fr) {

    });


    $scope.remRow = function(i, type) {
        $scope.fr["fr_" + type].splice(i, 1);
    }

    $scope.addRow = function(type) {
        //console.log(type);
        $scope.fr["fr_" + type].push({});
    }


    $scope.saveFREGST = function() {

        FlexiRent.save($scope.fr, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/flexi_rent");
        })

    }

    $scope.saveRentalPrice = function() {

        RentalPrice.save($scope.rentalprice, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/flexi_rent");
        })

    }

});

Qms.controller('ProductViewCtrl', function($scope, Products, $stateParams){
    var items = [];
        $scope.products = Products.query({id :$stateParams.id}, function() {
           var itemObj = $scope.products.items;
           $.each(itemObj, function(){
               var obj = $(this)[0];

               var item = [obj.name,obj.category,obj.description,obj.qty,obj.rrp_tax,obj.sell_price,obj.tax];

               items.push(item);

           });



        });

       function exportToCsv(filename, rows) {
        var processRow = function (row) {
            var finalVal = '';
            for (var j = 0; j < row.length; j++) {
                var innerValue = row[j] == null ? '' : row[j].toString();
                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                };
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }
            return finalVal + '\n';

        };
        var header ='Name,Category,Description,Quantity,Tax,RRP Tax,Sell Price\n';
        var csvFile = header;
        for (var i = 0; i < rows.length; i++) {
            csvFile += processRow(rows[i]);
        }

        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style = "visibility:hidden";
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }
    $("#downloadCsv").on("click", function() {
           exportToCsv("proposal-"+$stateParams.id+".csv",items);
    });

});

Qms.controller('FeedbacksListCtrl',function($scope, $rootScope,$filter,$http,$state,FileUploader,FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder, Feedbacks){
   $scope.feedbackForm = Feedbacks.query();

   $scope.edit = function(row_id) {
        $state.go('edit_feedbacks', {
            id: row_id
        });
    }
    $scope.delete = function(row_id) {
        Feedbacks.delete({
            id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.feedbackForm = Feedbacks.query();

        });
    }
   $scope.vmfb = {};
            $scope.vmfb.dtInstance = {};   
            $scope.vmfb.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('order', [[4, 'desc']])
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500]);
});


Qms.controller('FeedbacksCtrl',function($scope,$filter, $rootScope,$stateParams,FileUploader,$location,$http,$state,FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder, Feedbacks){
  
  $scope.feedbackForm = Feedbacks.query();

  var uploader = $scope.uploader = new FileUploader({
        url: 'feedbacks/upload',
        formData: [{
            name: null
        }]
    });

    var dataPath;
    // console.log(dataPath);
    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        dataPath = response.FilePath
    };
  if ($stateParams.id) {
        $scope.feedbackForm = Feedbacks.get({
            id: $stateParams.id,
            logo : $scope.feedbackForm.logo
        }, function(data) {

        });
    }
  $scope.savefb = function() {
        if (!$("#feedbackForm").parsley('validate')) return false;
        // console.log(dataPath);
        if (angular.isDefined($scope.feedbackForm.id)) {
          // $scope.feedbackForm.logo = dataPath;
          if(angular.isDefined(dataPath)){
            $scope.feedbackForm.logo = dataPath;
              Feedbacks.update({
                id: $scope.feedbackForm.id,
                logo : $scope.feedbackForm.logo
              }, $scope.feedbackForm, function(data) {
                  FlashMessage.setMessage(data);
                  if (data.success)
                      $location.path("/feedbacks");
              });
          }
          else{
            
             Feedbacks.update({
                id: $scope.feedbackForm.id,
                logo : $scope.feedbackForm.logo
              }, $scope.feedbackForm, function(data) {
                  FlashMessage.setMessage(data);
                  if (data.success)
                      $location.path("/feedbacks");
              });
          }
        } else {
            $scope.feedbackForm.logo = dataPath;
            Feedbacks.save({fb:$scope.feedbackForm.fb,
                            logo:$scope.feedbackForm.logo}, function(data) {
                FlashMessage.setMessage(data);
                if (data.success)
                    $location.path("/feedbacks");
            });
        }
    }


   $scope.tinymceOption = {
    selector: "textarea",
    theme: "modern",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons "
  };
});


Qms.controller('ProposalCtrl',function($scope,$stateParams,$rootScope,$interval,$window,$anchorScroll,$location,$timeout,$http,Scopes,Form,Rates,FlashMessage,ngDialog,ScheduleGoods,FileUploader,ProposalActions) {
    Form.getScheduleGoods(function(data) {
        $scope.scheduleGoodsList = data.goods;
        $scope.scheduleGoodsList.unshift({
            item: ""
        });
    });

    var dateObj = new Date();
    var dd = dateObj.getDate();
    var mm = dateObj.getMonth()+1;
    var yy = dateObj.getFullYear();
    var today = mm+"/"+dd+"/"+yy;
    var minYY = yy - 18; //(Applicant have to be atleast 18);
    var birthMinDate = mm+"/"+dd+"/"+minYY;
    
    $(function() {
        $('body').on('focus',".dateIssued", function(){
        $(this).datetimepicker({
            format : "DD/MM/YYYY",
            defaultDate : today
        });
        });
      }); 

    $scope.pickDate = function() {
        $('.datePayment').datetimepicker({
            format : "DD/MM/YYYY hh:mm:ss.ss",
            defaultDate : today
        });
    }

    $scope.pics = [
      {
      "title": "Proposal",
      "link": "",
      "media": {"m":"assets/images/proposal/thumbnails/proposal.png"},
      "date_taken": "2018-11-20T19:12:44-08:00",
      "description": "Proposal Document",
      "published": "2018-11-21",
      "author": "",
      "author_id": "",
      "tags": "",
      "actualWidth": "567px",
      "actualHeight": "440px"
     },
     {
      "title": "Quotation",
      "link": "",
      "media": {"m":"assets/images/proposal/thumbnails/quotation.png"},
      "date_taken": "2018-11-20T19:12:44-08:00",
      "description": "Proposal Document",
      "published": "2018-11-21",
      "author": "",
      "author_id": "",
      "tags": "",
      "actualWidth": "567px",
      "actualHeight": "440px"
     }
    ];
    $scope.scheduleCount = 0;
    $scope.locationCount = 0;
    $scope.accountLocation = 0;
    $scope.addTelCount = 0;
    $scope.addMobPortCount = 0;
    $scope.htmlData = {};
    $scope.discountArr = {};
    $scope.companyLocations = [];
    $rootScope.draftMode = false;

    $scope.locationAvailableTemps = [
        'nbf_cap',
        'nbf_std',
        'voice_comp',
        'voice_solution_untimed',
        'voice_essentials_cap',
        'cloud_connect_standard',
        'cloud_connect_cap',
        'voice_solution_standard',
        'data_adsl',
        'ip_midband',
        'ip_midbandunli',
        'nbnUnlimited',
        'nbn',
        'mobile_mega',
        'mobile_ut',
        'mobile_wireless',
        '1300',
        '1300_discounted',
        'fibre',
        'ethernet',
        'telstraUntimed',
        'telstra4gSuper',
        'telstraWirelessSuper',
        'telstraWireless',
        'printer'
    ];

    $scope.items = {
        'nbf_cap'               : [0],
        'nbf_std'               : [0],
        'voice_comp'            : [0],
        'voice_solution_untimed'  : [0],
        'voice_essentials_cap'    : [0],
        'cloud_connect_standard'  : [0],
        'cloud_connect_cap'       : [0],
        'voice_solution_standard' : [0],
        'data_adsl'               : [0],
        'ip_midband'              : [0],
        'ip_midbandunli'          : [0],
        'nbn'                     : [0],
        'nbnUnlimited'            : [0],
        'mobile_mega'             : [0],
        'mobile_ut'               : [0],
        'mobile_wireless'         : [0],
        '1300'                    : [0],
        '1300_discounted'         : [0],
        'fibre'                   : [0],
        'ethernet'                : [0],
        'telstraUntimed'          : [0],
        'telstra4gSuper'          : [0],
        'telstraWirelessSuper'    : [0],
        'telstraWireless'         : [0],
        'printer'                 : [0],
        'proposal'                : [0],
        'quotation'               : [0],
        'landscape'               : [0],
        'cloud'                   : [0],
        'schedule_of_goods'       : [0],
        'provider'                : [0],
        'mrc'                     : [0],
        'usage'                   : [0],
        'otherMrc'                : [0],
    };

    $scope.recurring_services = [
        {'description' : 'Monitoring Centre connect 24 hours','price' : 59.00},
        {'description' : "Monitoring Centre connect 24 hours (Including NBN/3G Wireless Unit)",'price' : 89}
    ];

    $scope.ethernet_plans = [
        {'description' : 'Up to 10Mbps/10Mbps* Unlimited','price':499},
        {'description' : 'Up to 20Mbps/20Mbps* Unlimited','price':599},
    ];

    $scope.nbnUnlimited_plans = [
        {'description' : 'NBN Standard 25','inclusion' : 'Download speeds Between 5Mbps and 25Mbps*<br/>Upload speeds Between 1Mbps and 5Mbps*','price':159},
        {'description' : 'NBN Fast 50','inclusion' : 'Download speeds Between 12Mbps and 50Mbps*<br/>Upload speeds Between 1Mbps and 20Mbps*','price':179},
        {'description' : 'NBN WIRELESS 75','inclusion' : 'Download speeds Between 12Mbps and 75Mbps*<br/>Upload speeds Between 1Mbps and 10Mbps*','price':179},
        {'description' : 'NBN Ultra fast 100','inclusion' : 'Download speeds Between 12Mbps and 100Mbps*<br/>Upload speeds Between 1Mbps and 40Mbps*','price':189}
    ];

    $scope.mobile_mega_plans = [
        {'description' : 'Cap 39','inclusions' : '$250 inc. calls 500MB','price':39},
        {'description' : 'Cap 49','inclusions' : '$2500 inc. calls 2GB','price':49},
        {'description' : 'Cap 59','inclusions' : '$2500 inc. calls 3GB','price':59},
    ];

    $scope.data_bolt_on_telstra4gSuperStandard_plans = [
        {'description' : 'Data Bolt-on 1GB','memory': '1GB','price':9.89, 'plan_group' : 'Telstra 4G Super Standard', 'status': 1, 'itemIndex':0},
        {'description' : 'Data Bolt-on 5GB','memory': '5GB','price':31.99,'plan_group' : 'Telstra 4G Super Standard', 'status': 1, 'itemIndex':1}
    ];
    $scope.data_bolt_on_telstra4gSuper_plans = [
        {'description' : 'Data Bolt-on 1GB','memory': '1GB','price':9.89, 'plan_group' : 'Telstra 4G Super', 'status': 1,  'itemIndex':0},
        {'description' : 'Data Bolt-on 5GB','memory': '5GB','price':31.99,'plan_group' : 'Telstra 4G Super', 'status': 1, 'itemIndex':1}
    ];
    $scope.data_bolt_on_telstra4gSuperMax_plans = [
        {'description' : 'Data Bolt-on 1GB','memory': '1GB','price':9.89, 'plan_group' : 'Telstra 4G Super Max', 'status': 1,  'itemIndex':0},
        {'description' : 'Data Bolt-on 5GB','memory': '5GB','price':31.99,'plan_group' : 'Telstra 4G Super Max', 'status': 1, 'itemIndex':1}
    ];
    $scope.data_bolt_on_telstra4gSuperExtreme_plans = [
        {'description' : 'Data Bolt-on 1GB','memory': '1GB','price':9.89, 'plan_group' : 'Telstra 4G Super Extreme', 'status': 1,  'itemIndex':0},
        {'description' : 'Data Bolt-on 5GB','memory': '5GB','price':31.99,'plan_group' : 'Telstra 4G Super Extreme', 'status': 1, 'itemIndex':1}
    ];
    
    $scope.telstra4gSuper_rate_plans = [
        //  {'plan' : 'Select a Mobile Plan','inclusions':''},
        {'group':'Telstra 4G Super','plan':'Telstra 4G Super Standard','inclusions':'– Unlimited National Calls, 4GB Data, Unlimited standard SMS and MMS, plus 100 Mins International calls', 'status': 1, 'price':49,'itemIndex':0},
        {'group':'Telstra 4G Super','plan':'Telstra 4G Super','inclusions':'– Unlimited National Calls, 30GB Data, Unlimited standard SMS and MMS, plus 300 Mins International calls', 'status': 1, 'price':69,'itemIndex':1},
        {'group':'Telstra 4G Super','plan':'Telstra 4G Super Max','inclusions':'– Unlimited National Calls, 40GB Data, Unlimited standard SMS and MMS, plus 300 Mins International calls', 'status': 1, 'price':85,'itemIndex':2},
        {'group':'Telstra 4G Super','plan':'Telstra 4G Super Extreme','inclusions':'– Unlimited National Calls, 75GB Data, Unlimited standard SMS and MMS, plus 300 Mins International calls', 'status': 1, 'price':99,'itemIndex':3},
    ];

    // $scope.telstra_rate_plans = [
    //     {'plan' : 'Select a Mobile Plan','inclusions':''},
    //     {'group':'Telstra 4G','plan':'Telstra 4G Lite','inclusions':'$1000 Calls 1GB Data','price':49,'itemIndex':0},
    //     {'group':'Telstra 4G','plan':'Telstra 4G Business Executive','inclusions':'Unlimited Calls 3GB Data','price':69,'itemIndex':1},
    //     {'group':'Telstra 4G','plan':'Telstra 4G Business Professional','inclusions':'Unlimited Calls 10GB Data','price':79,'itemIndex':2},
    //     {'group':'Telstra 4G','plan':'Telstra 4G Business Maximum','inclusions':'Unlimited Calls 20GB Data','price':89,'itemIndex':3}
    // ];

    $scope.untimed_rate_plans = [
        
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 49','inclusions':'Includes 700MB','price':49,'itemIndex':0},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 59','inclusions':'Includes 2GB','price':59,'itemIndex':1},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 79','inclusions':'Includes 4GB and 300 INTERNATIONAL MINS','price':79,'itemIndex':2},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 89','inclusions':'Includes 6GB and 300 INTERNATIONAL MINS','price':89,'itemIndex':3}
    ];
    // $scope.mobile_mega_rate_plans = [
    //     {'plan' : 'Select a Mobile Plan','inclusions':''},
    //     {'group':'Mobile Mega','plan':'Mobile Mega Cap 39','inclusions':'$250 inc. calls 500Mb','price':39,'itemIndex':0},
    //     {'group':'Mobile Mega','plan':'Mobile Mega Cap 49','inclusions':'$2500 inc. calls 2GB','price':49,'itemIndex':1},
    //     {'group':'Mobile Mega','plan':'Mobile Mega Cap 59','inclusions':'$2500 inc. calls 3GB','price':59,'itemIndex':2}
    // ];

    $scope.configMobPort = [
        {'confAccHolder':'','confMobNo':'','confProvider':'','confAccNo':'','confPatPlan':$scope.telstra4gSuper_rate_plans[0],'boltOn':{}, 'status': 1}
    ];

    $scope.adsl_plans = [
        {'description' : 'ADSL 2 + LITE','details' : '(Unlimited Lite (no static IP))','price':79},
        {'description' : 'ADSL 2 + Unlimited','details' : 'Business Grade (ON NET)','price':109},
        {'description' : 'ADSL 2 + 100GB','details' : 'Business Grade (OFF NET)','price':109},
        {'description' : 'ADSL 2 + 200GB','details' : 'Business Grade (OFF NET)','price':129},
        {'description' : 'ADSL 2 + 300GB','details' : 'Business Grade (OFF NET)','price':139},
        {'description' : 'ADSL 2 + 500GB','details' : 'Business Grade (OFF NET)','price':149},
        {'description' : 'ADSL 2 + 1TB','details' : 'Business Grade (OFF NET)','price':169},
    ];

    // $scope.telstraUntimed_plans = [
    //     {'description' : 'Lite','inclusions' : 'INCLUDES $1000 National Calls,INCLUDES 1GB,UNLIMITED NATIONAL SMS,$0.69 PER MMS','price':11},
    //     {'description' : 'Business Executive','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 3GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,100 INTERNATIONAL MINS*','price':69},
    //     {'description' : 'Business Professional','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 10GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':79},
    //     {'description' : 'Business Maximum','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 20GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':89},
    // ];


    $scope.telstraUntimed_plans = [
        {'description' : 'Lite','inclusions' : 'INCLUDES $1000 National Calls,INCLUDES 1GB,UNLIMITED NATIONAL SMS,$0.69 PER MMS','price':49},
        {'description' : 'Business Executive','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 3GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,100 INTERNATIONAL MINS*','price':69},
        {'description' : 'Business Professional','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 10GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':79},
        {'description' : 'Business Maximum','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 20GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':89},
    ];

    $scope.telstra4gSuper_plans = [
        {'description' : 'Broadband Wireless Super Mid 10GB','price':49},
        {'description' : 'Broadband Wireless Super Large 30GB','price':79},
        {'description' : 'Broadband Wireless Super Extra Large 50GB','price':99}
    ];

    $scope.telstraWirelessSuper_plans = [
        {'description' : 'Broadband Wireless Super Mid 10GB','price':49},
        {'description' : 'Broadband Wireless Super Large 30GB','price':79},
        {'description' : 'Broadband Wireless Super Extra Large 50GB','price':99}
    ];
    $scope.telstraWireless_plans = [
        {'description' : 'Broadband Wireless 3GB','price':43},
        {'description' : 'Broadband Wireless 6GB','price':64},
        {'description' : 'Broadband Wireless 10GB','price':93}
    ];

    $scope.ip_midbandunli_plans = [
        {'description' : '10Mbps/10Mbps*','price':549},
        {'description' : '20Mbps/20Mbps*','price':699},
        {'description' : '40Mbps/40Mbps*','price':999}
    ];

    $scope.ip_midband_plans = [
        {'description' : 'None','price':0},
        {'description' : '6Mbps/6Mbps*','price':319},
        {'description' : '8Mbps/8Mbps*','price':349},
        {'description' : '18Mbps/18Mbps*','price':399},
        {'description' : '20Mbps/20Mbps*','price':449},
        {'description' : '30Mbps/30Mbps*','price':799},
        {'description' : '40Mbps/40Mbps*','price':849}
    ];

    $scope.professional_install = [
        {'description' : 'ASDL 2+ 20/1Mbps','price':399},
        {'description' : 'ASDL 2+ 20/3 to 40Mbps Midband','price':599},
    ];

    $scope.download_plans = [
        {'description' : 'None','price':0},
        {'description' : '50 GB','price':19},
        {'description' : '100 GB','price':35},
        {'description' : '200 GB','price':69},
        {'description' : '300 GB','price':99},
        {'description' : '500 GB','price':149},
        {'description' : '1000 GB','price':289}
    ];

    $scope.mobile_4G_Untimed_Calls = [
        {'description' : '4G Untimed 49','inclusions' : 'Includes 700MB','price':49},
        {'description' : '4G Untimed 59','inclusions' : 'Includes 2GB','price':59},
        {'description' : '4G Untimed 79','inclusions' : 'Includes 4GB,300 INTERNATIONAL MINS*','price':79},
        {'description' : '4G Untimed 89','inclusions' : 'Includes 6GB,300 INTERNATIONAL MINS*','price':89}
    ];

    $scope.wireless_cap_plans = [
        {'description' : 'Wireless 15 - 500MB','price':15},
        {'description' : 'Wireless 19 - 1GB','price':19},
        {'description' : 'Wireless 29 - 2GB','price':29},
        {'description' : 'Wireless 39 - 3GB','price':39},  
        {'description' : 'Wireless 44 - 4GB','price':44},
        {'description' : 'Wireless 49 - 6GB','price':49},
        {'description' : 'Wireless 59 - 8GB','price':59},
        {'description' : 'Wireless 119 - 12GB','price':119}
    ];

    $scope.fibre_unli_plans = [
        {'description' : '10Mbps / 10Mbps Unlimited - 36 month term','price':599},
        {'description' : '20Mbps / 20Mbps Unlimited - 36 month term','price':699},
        {'description' : '100Mbps / 100Mbps Unlimited - 36 month term','price':1199},
        {'description' : '400Mbps / 400Mbps Unlimited - 36 month term','price':758},
        {'description' : '500Mbps / 500Mbps Unlimited - 36 month term','price':1599},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 24 month term','price':1899},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 36 month term','price':1699},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 48 month term','price':1299},
    ];

    $scope.addCompanyLocation = function(form_type) {
        console.log($scope.items[form_type].length);
        if ($scope.items[form_type].length!=0){
            $scope.locationCount = $scope.items[form_type].length-1;
        }
        $scope.locationCount++;
        if (form_type=='voice_solution_standard'){
            $scope.proposalData.voiceSolutionChannel[$scope.locationCount] = 0;
            $scope.proposalData.voiceSolutionDID[$scope.locationCount] = 14.95;
            $scope.proposalData.bFSolutionDiscount[$scope.locationCount] = 0;
            $scope.proposalData.bFSolutionCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_solution_untimed'){
            $scope.proposalData.voiceUntimedChannel[$scope.locationCount] = 3;
            $scope.proposalData.voiceUntimedDID[$scope.locationCount] = 14.95;
            $scope.proposalData.bFUntimedDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_comp'){
            $scope.proposalData.voiceCompAnalouge[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompLineHunt[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompBri[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompPri[$scope.locationCount] = 309.85;
            $scope.proposalData.voiceCompDID[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompBriDis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompPriDis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompCallDis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_std'){
            $scope.proposalData.voiceStdChannel[$scope.locationCount] = 5;
            $scope.proposalData.voiceStdLineHunt[$scope.locationCount] = 5;
            $scope.proposalData.voiceStdDID[$scope.locationCount] = 0;
            $scope.proposalData.bFStdDiscount[$scope.locationCount] = 0;
            $scope.proposalData.bFCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_cap'){
            $scope.proposalData.voiceStd[$scope.locationCount] = 495;
            $scope.proposalData.dID[$scope.locationCount] = 9.99;
            $scope.proposalData.bFCapDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_essentials_cap'){
            $scope.proposalData.ipMidbandPlansVoice[$scope.locationCount] = $scope.ip_midband_plans[0];
            $scope.proposalData.ipMidbandDownloadVoice[$scope.locationCount] = $scope.download_plans[0];
            $scope.proposalData.voiceEssentialChannel[$scope.locationCount] = 4;
            $scope.proposalData.voiceEssentialDID[$scope.locationCount] = 0;
            $scope.proposalData.bECapDiscount[$scope.locationCount] = 0;
            $scope.proposalData.ipMidbandDis[$scope.locationCount] = 0;
            // $scope.proposalData.ipMidbandDownloadVoice[$scope.locationCount] = 0;            
        } 
        if (form_type=='cloud_connect_standard'){
            $scope.proposalData.cloudConnectStandardChannel[$scope.locationCount] = 58;
            $scope.proposalData.cloudConnectStandardFaxQty[$scope.locationCount] = 0;
            $scope.proposalData.cloudConnectStandardDID[$scope.locationCount] = 0;
            $scope.proposalData.cloudConnectStandardAnalouge[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectStandarduConeApp[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectStandardAddCallQueue[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectStandardAddAutoAttndnt[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectStandardAddVoicemail[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectStandardCallRecording[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectStandardCallRecordingPremium[$scope.locationCount] = "";
            $scope.proposalData.cCStandardAnalogueDis[$scope.locationCount] = "";
            $scope.proposalData.cCStandardCallRecordingDiscount[$scope.locationCount] = "";
            $scope.proposalData.cCStandardCallRecordingPremiumDiscount[$scope.locationCount] = "";
            $scope.proposalData.cCStandardCallDiscount[$scope.locationCount] = "";
        }
        if (form_type=='cloud_connect_cap'){
            $scope.proposalData.cloudConnectCapChannel[$scope.locationCount] = 78;
            $scope.proposalData.cloudConnectCapFaxQty[$scope.locationCount] = 0;
            $scope.proposalData.cloudConnectCapDID[$scope.locationCount] = 0;
            $scope.proposalData.cloudConnectCapAnalouge[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectCapuConeApp[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectCapRepClientApp[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectCapAddCallQueue[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectCapAddAutoAttndnt[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectCapAddVoicemail[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectCapCallRecording[$scope.locationCount] = "";
            $scope.proposalData.cloudConnectCapCallRecordingPremium[$scope.locationCount] = "";
            $scope.proposalData.cCCapAnalogueDis[$scope.locationCount] = "";
            $scope.proposalData.cCCapCallRecordingDiscount[$scope.locationCount] = "";
            $scope.proposalData.cCCapCallRecordingPremiumDiscount[$scope.locationCount] = "";
        }
        if (form_type=='data_adsl'){
            // $scope.proposalData.adsl2Plans[$scope.locationCount] = 79;
            $scope.proposalData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
            $scope.proposalData.adsl2Dis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDSL[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDSLDis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midband'){
            $scope.proposalData.ipMidbandDownload[$scope.locationCount] = $scope.download_plans[1];
            $scope.proposalData.ipMidbandPlans[$scope.locationCount] = $scope.ip_midband_plans[1];
            $scope.proposalData.ipMidbandDis[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midbandunli'){
            $scope.proposalData.ipMidbandUnliPlans[$scope.locationCount] = $scope.ip_midbandunli_plans[0];
            $scope.proposalData.ipMidbandUnliProf[$scope.locationCount] = $scope.professional_install[0];
            $scope.proposalData.ipMidbandUnliDis[$scope.locationCount] = 0;
        }
        if (form_type=='1300_discounted'){
            $scope.proposalData.rate131300Dis.qt1800[$scope.locationCount] = 0;
            $scope.proposalData.rate131300Dis.qt1300[$scope.locationCount] = 0;
            $scope.proposalData.rateDis131300.discount[$scope.locationCount] = 0;
        }
        if (form_type=='1300'){
            $scope.proposalData.rate131300.qt1800[$scope.locationCount] = 0;
            $scope.proposalData.rate131300.qt1300[$scope.locationCount] = 0;
            $scope.proposalData.rate131300.qt13[$scope.locationCount] = 0;
        }
        if (form_type=='mobile_wireless'){
            $scope.proposalData.mobileWirelessPlans[$scope.locationCount] = $scope.wireless_cap_plans[0];
            // $scope.proposalData.mobileWirelessPlans[$scope.locationCount] = 15;
            $scope.proposalData.mobileWirelessDis[$scope.locationCount] = 0;
        }        
        if (form_type=='fibre'){
            $scope.proposalData.fibreUtPlans[$scope.locationCount] = $scope.fibre_unli_plans[0];
            $scope.proposalData.fibreDis[$scope.locationCount] = 0;
        }
        // if (form_type=='adsl'){
        //     $scope.proposalData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
        // }
        if (form_type=='ethernet'){
            $scope.proposalData.ethernetPlans[$scope.locationCount] = $scope.ethernet_plans[0];
            $scope.proposalData.ethernetDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbnUnlimited'){
            $scope.proposalData.UnlimitedPlans[$scope.locationCount] = $scope.nbnUnlimited_plans[0];
            $scope.proposalData.nbnUnlimitedDis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDisNBNUnli[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeNBNUnli[$scope.locationCount] =0;
        }
        if (form_type=='telstra4gSuper'){
            $scope.proposalData.telstra4gSuperPlans[$scope.locationCount] = $scope.telstra4gSuper_plans[0];
        }
        if (form_type=='telstraUntimed'){
            $scope.proposalData.telstraUntimedPlans[$scope.locationCount] = $scope.telstraUntimed_plans[0];
            $scope.proposalData.mobileUtDis[$scope.locationCount] = 0;

        }
        if (form_type=='telstraWirelessSuper'){
            $scope.proposalData.telstraWirelessSuperPlans[$scope.locationCount] = $scope.telstraWirelessSuper_plans[0];
            $scope.proposalData.telstraWirelessSuperDis[$scope.locationCount] = 0;
        }
        if (form_type=='telstraWireless'){
            $scope.proposalData.telstraWirelessPlans[$scope.locationCount] = $scope.telstraWireless_plans[0];
            $scope.proposalData.telstraWirelessDis[$scope.locationCount] = 0;
        }
        $scope.items[form_type].push($scope.locationCount);
    };



    
    $scope.reset = function(){
        $("a[href$='#next']").click(function(){
            $("a[href$='#finish']").css("pointer-events", "fill");
            $(".nav#side-menu li a").css("pointer-events", "none");
            setTimeout(function() {
                $(".nav#side-menu li a").css("pointer-events", "fill");
            }, 4000);
        });
        $("a[href$='#finish']").click(function(){
            $("a[href$='#finish']").css("pointer-events", "none");
        });
        $scope.proposalData={};
        $scope.proposalData.tradingName = "";
        $scope.proposalData.contactPerson = "";
        $scope.proposalData.businessName = "";
        $scope.proposalData.emailAddress = "";
        $scope.proposalData.schQty = [];
        $scope.proposalData.schAddQty = {};
        $scope.proposalData.untimed = [];
        $scope.proposalData.untimed[0] = 0;
        $scope.proposalData.megaRate = [];
        $scope.proposalData.telstra = [];
        $scope.proposalData.term = 60;
        $scope.proposalData.billingType = 'rental';
        $scope.schItemCount = 0;  
        $scope.schAddItemCount = 0;
        $scope.schItemsProvider = 0;
        $scope.schItemsMrc = 0;
        $scope.schItemsUsage = 0;
        $scope.schItemsOtherMrc = 0;
        $scope.proposalData.totalNoUsers = 0;
        $scope.proposalData.proposalType = "";

        $scope.proposalData.analysis = {};
        $scope.proposalData.analysis.NumRow = 0;
        $scope.proposalData.analysis.schItemsProvider = [];
        $scope.proposalData.analysis.schItemsProvider[0] = [{count:$scope.schItemsProvider, status:1}];
        $scope.proposalData.analysis.provider = [];
        $scope.proposalData.analysis.provider[0] = "N/A";
        $scope.proposalData.analysis.account = [];
        $scope.proposalData.analysis.account[0] = "";
        $scope.proposalData.analysis.dateIssued = [];
        $scope.proposalData.analysis.dateIssued[0] = today;
        $scope.proposalData.analysis.date = "";

        $scope.proposalData.analysis.schItemsMrc = [];
        $scope.proposalData.analysis.schItemsMrc[0] = [{count:$scope.schItemsMrc, status:1}];
        $scope.proposalData.analysis.schItemsMrcLandlines = [];
        $scope.proposalData.analysis.schItemsMrcLandlines[0] = [{count:$scope.schItemsMrc, status:1}];
        $scope.proposalData.analysis.schItemsMrcInternet = [];
        $scope.proposalData.analysis.schItemsMrcInternet[0] = [{count:$scope.schItemsMrc, status:1}];
        $scope.proposalData.analysis.schItemsMrcMobile = [];
        $scope.proposalData.analysis.schItemsMrcMobile[0] = [{count:$scope.schItemsMrc, status:1}];
        $scope.proposalData.analysis.schItemsMrcInbound = [];
        $scope.proposalData.analysis.schItemsMrcInbound[0] = [{count:$scope.schItemsMrc, status:1}];
        $scope.proposalData.analysis.landlinesQty = [];
        $scope.proposalData.analysis.landlinesQty[0] = "1";
        $scope.proposalData.analysis.internetQty = [];
        $scope.proposalData.analysis.internetQty[0] = "1";
        $scope.proposalData.analysis.mobileQty = [];
        $scope.proposalData.analysis.mobileQty[0] = "1";
        $scope.proposalData.analysis.inboundQty = [];
        $scope.proposalData.analysis.inboundQty[0] = "1";
        $scope.proposalData.analysis.landlinesType = [];
        $scope.proposalData.analysis.landlinesType[0] = "N/A";
        $scope.proposalData.analysis.internetType = [];
        $scope.proposalData.analysis.internetType[0] = "N/A";
        $scope.proposalData.analysis.mobileType = [];
        $scope.proposalData.analysis.mobileType[0] = "N/A";
        $scope.proposalData.analysis.inboundType = [];
        $scope.proposalData.analysis.inboundType[0] = "N/A";
        $scope.proposalData.analysis.landlinesCost = [];
        $scope.proposalData.analysis.landlinesCost[0] = "";
        $scope.proposalData.analysis.internetCost = [];
        $scope.proposalData.analysis.internetCost[0] = "";
        $scope.proposalData.analysis.mobileCost = [];
        $scope.proposalData.analysis.mobileCost[0] = "";
        $scope.proposalData.analysis.inboundCost = [];
        $scope.proposalData.analysis.inboundCost[0] = "";

        $scope.proposalData.analysis.schItemsUsage = [];
        $scope.proposalData.analysis.schItemsUsage[0] = [{count:$scope.schItemsUsage, status:1}];
        $scope.proposalData.analysis.schItemsUsageLocal = [];
        $scope.proposalData.analysis.schItemsUsageLocal[0] = [{count:$scope.schItemsUsage, status:1}];
        $scope.proposalData.analysis.schItemsUsageNational = [];
        $scope.proposalData.analysis.schItemsUsageNational[0] = [{count:$scope.schItemsUsage, status:1}];
        $scope.proposalData.analysis.schItemsUsageCallstoMobile = [];
        $scope.proposalData.analysis.schItemsUsageCallstoMobile[0] = [{count:$scope.schItemsUsage, status:1}];
        $scope.proposalData.analysis.schItemsUsageInternational = [];
        $scope.proposalData.analysis.schItemsUsageInternational[0] = [{count:$scope.schItemsUsage, status:1}];
        $scope.proposalData.analysis.schItemsUsageCallsto13 = [];
        $scope.proposalData.analysis.schItemsUsageCallsto13[0] = [{count:$scope.schItemsUsage, status:1}];
        $scope.proposalData.analysis.schItemsUsageRoaming = [];
        $scope.proposalData.analysis.schItemsUsageRoaming[0] = [{count:$scope.schItemsUsage, status:1}];
        $scope.proposalData.analysis.localCost = [];
        $scope.proposalData.analysis.localCost[0] = "";
        $scope.proposalData.analysis.nationalCost = [];
        $scope.proposalData.analysis.nationalCost[0] = "";
        $scope.proposalData.analysis.callsToMobileCost = [];
        $scope.proposalData.analysis.callsToMobileCost[0] = "";
        $scope.proposalData.analysis.internationalCost = [];
        $scope.proposalData.analysis.internationalCost[0] = "";
        $scope.proposalData.analysis.callsTo13Cost = [];
        $scope.proposalData.analysis.callsTo13Cost[0] = "";
        $scope.proposalData.analysis.roamingCost = [];
        $scope.proposalData.analysis.roamingCost[0] = "";
 
        $scope.proposalData.analysis.schItemsOtherMrc = [];
        $scope.proposalData.analysis.schItemsOtherMrc[0] = [{count:$scope.schItemsOtherMrc, status:1}];
        $scope.proposalData.analysis.schItemsOtherMrcMisc = [];
        $scope.proposalData.analysis.schItemsOtherMrcMisc[0] = [{count:$scope.schItemsOtherMrc, status:1}];
        $scope.proposalData.analysis.schItemsOtherMrcRental = [];
        $scope.proposalData.analysis.schItemsOtherMrcRental[0] = [{count:$scope.schItemsOtherMrc, status:1}];
        $scope.proposalData.analysis.miscType = [];
        $scope.proposalData.analysis.miscCost = [];
        $scope.proposalData.analysis.miscCost[0] = "";
        $scope.proposalData.analysis.rentalType = [];
        $scope.proposalData.analysis.rentalCost = [];
        $scope.proposalData.analysis.rentalCost[0] = "";
        $scope.proposalData.analysis.totalCost = 0;

        // $scope.proposalData.schItems = [{count:$scope.schItemCount, status:1}];
        $scope.proposalData.schOption = [];
        $scope.proposalData.schOption[0] = [];

        $scope.proposalData.schItems = [];
        $scope.proposalData.schItems[0] = [{count:$scope.schItemCount, status:1}];
        $scope.proposalData.schOptions = [];
        $scope.proposalData.schOptions[0] = [{count:$scope.schItemCount}];

        $scope.proposalData.schAddItems = [];
        $scope.proposalData.schAddItems[0] = [{count:$scope.schItemCount, status:1}];
        $scope.proposalData.schAddOption = [];
        $scope.proposalData.schAddOption[0] = [];
        $scope.proposalData.schAddOptions = [];
        $scope.proposalData.schAddOptions[0] = [{count:$scope.schAddItemCount}];
        $scope.proposalData.dID = [];
        $scope.proposalData.dID[0] = 0;
        $scope.proposalData.voiceStd = [];
        $scope.proposalData.voiceStd[0] = 0;
        $scope.proposalData.voiceStdDID = [];
        $scope.proposalData.voiceStdDID[0] = 0;
        $scope.proposalData.voiceStdChannel = [];
        $scope.proposalData.voiceStdChannel[0] = 0;
        $scope.proposalData.voiceStdLineHunt = [];
        $scope.proposalData.voiceStdLineHunt[0] = 0;
        $scope.proposalData.bFCapDiscount = [];
        $scope.proposalData.bFCapDiscount[0] = 0;
        $scope.proposalData.bFStdDiscount = [];
        $scope.proposalData.bFStdDiscount[0] = 0;
        $scope.proposalData.bFCallDiscount = [];
        $scope.proposalData.bFCallDiscount[0] = 0;
        $scope.proposalData.voiceUntimedChannel = [];
        $scope.proposalData.voiceUntimedChannel[0] = 0;
        $scope.proposalData.voiceUntimedDID = [];
        $scope.proposalData.voiceUntimedDID[0] = 14.95;
        $scope.proposalData.bFUntimedDiscount = [];
        $scope.proposalData.bFUntimedDiscount[0] = 0;
        $scope.proposalData.voiceCompDID = [];
        $scope.proposalData.voiceCompDID[0] = 0;    
        $scope.proposalData.voiceSolutionChannel = [];
        $scope.proposalData.voiceSolutionChannel[0] = 0;  
        $scope.proposalData.voiceSolutionLineHunt = [];
        $scope.proposalData.voiceSolutionLineHunt[0] = 0;  
        $scope.proposalData.voiceFcusStdLineHunt = [];
        $scope.proposalData.voiceFcusStdLineHunt[0] = 0;  
        $scope.proposalData.voiceSolutionDID = [];
        $scope.proposalData.voiceSolutionDID[0] = 14.95;  
        $scope.proposalData.bFSolutionDiscount = [];
        $scope.proposalData.bFSolutionDiscount[0] = 0;  
        $scope.proposalData.bFSolutionCallDiscount = [];
        $scope.proposalData.bFSolutionCallDiscount[0] = 0;  
        $scope.proposalData.cloudConnectStandardChannel = [];
        // $scope.proposalData.cloudConnectStandardChannel[0] = 58;  
        $scope.proposalData.cloudConnectStandardLineHunt = [];
        // $scope.proposalData.cloudConnectStandardLineHunt[0] = 10.9;  
        $scope.proposalData.cloudConnectStandardFaxQty = [];
        $scope.proposalData.cloudConnectStandardFaxQty[0] = 0;
        $scope.proposalData.cloudConnectStandardDID = [];
        $scope.proposalData.cloudConnectStandardDID[0] = 0; 
        $scope.proposalData.cloudConnectStandardAnalouge = [];
        $scope.proposalData.cloudConnectStandardAnalouge[0] = ""; 
        $scope.proposalData.cloudConnectStandarduConeApp = [];
        $scope.proposalData.cloudConnectStandarduConeApp[0] =  ""; 
        $scope.proposalData.cloudConnectStandardRepClientApp = [];
        $scope.proposalData.cloudConnectStandardRepClientApp[0] = ""; 
        $scope.proposalData.cloudConnectStandardAddCallQueue = [];
        $scope.proposalData.cloudConnectStandardAddCallQueue[0] =  ""; 
        $scope.proposalData.cloudConnectStandardAddAutoAttndnt = [];
        $scope.proposalData.cloudConnectStandardAddAutoAttndnt[0] =  ""; 
        $scope.proposalData.cloudConnectStandardAddVoicemail = [];
        $scope.proposalData.cloudConnectStandardAddVoicemail[0] =  ""; 
        $scope.proposalData.cloudConnectStandardCallRecording = [];
        $scope.proposalData.cloudConnectStandardCallRecording[0] =  ""; 
        $scope.proposalData.cloudConnectStandardCallRecordingPremium = [];
        $scope.proposalData.cloudConnectStandardCallRecordingPremium[0] =  ""; 
        $scope.proposalData.cCStandardAnalogueDis = [];
        $scope.proposalData.cCStandardAnalogueDis[0] =  ""; 
        $scope.proposalData.cCStandardCallRecordingDiscount = [];
        $scope.proposalData.cCStandardCallRecordingDiscount[0] =  ""; 
        $scope.proposalData.cCStandardCallRecordingPremiumDiscount = [];
        $scope.proposalData.cCStandardCallRecordingPremiumDiscount[0] =  ""; 
        $scope.proposalData.cCStandardDiscount = [];
        $scope.proposalData.cCStandardDiscount[0] =  "";  
        $scope.proposalData.cCStandardCallDiscount = [];
        $scope.proposalData.cCStandardCallDiscount[0] =  "";  
        $scope.proposalData.cloudConnectCapChannel = [];
        $scope.proposalData.cloudConnectCapFaxQty = [];
        $scope.proposalData.cloudConnectCapFaxQty[0] = "";
        $scope.proposalData.cloudConnectCapDID = [];
        $scope.proposalData.cloudConnectCapDID[0] = ""; 
        $scope.proposalData.cloudConnectCapAnalouge = [];
        $scope.proposalData.cloudConnectCapAnalouge[0] = ""; 
        $scope.proposalData.cloudConnectCapuConeApp = [];
        $scope.proposalData.cloudConnectCapuConeApp[0] = ""; 
        $scope.proposalData.cloudConnectCapRepClientApp = [];
        $scope.proposalData.cloudConnectCapRepClientApp[0] = ""; 
        $scope.proposalData.cloudConnectCapAddCallQueue = [];
        $scope.proposalData.cloudConnectCapAddCallQueue[0] = ""; 
        $scope.proposalData.cloudConnectCapAddAutoAttndnt = [];
        $scope.proposalData.cloudConnectCapAddAutoAttndnt[0] = ""; 
        $scope.proposalData.cloudConnectCapAddVoicemail = [];
        $scope.proposalData.cloudConnectCapAddVoicemail[0] = ""; 
        $scope.proposalData.cloudConnectCapCallRecording = [];
        $scope.proposalData.cloudConnectCapCallRecording[0] = ""; 
        $scope.proposalData.cloudConnectCapCallRecordingPremium = [];
        $scope.proposalData.cloudConnectCapCallRecordingPremium[0] = ""; 
        $scope.proposalData.cCCapAnalogueDis = [];
        $scope.proposalData.cCCapAnalogueDis[0] = 0; 
        $scope.proposalData.cCCapiscount = [];
        $scope.proposalData.cCCapiscount[0] = 0; 
        $scope.proposalData.cCCapCallRecordingDiscount = [];
        $scope.proposalData.cCCapCallRecordingDiscount[0] = 0; 
        $scope.proposalData.cCCapCallRecordingPremiumDiscount = [];
        $scope.proposalData.cCCapCallRecordingPremiumDiscount[0] = 0; 
        $scope.proposalData.cCCapDiscount = [];
        $scope.proposalData.cCCapDiscount[0] = 0;  
        $scope.proposalData.voiceCompAnalougeDisNBNUnli = [];
        $scope.proposalData.voiceCompAnalougeDisNBNUnli[0] = 0;
        $scope.proposalData.voiceEssentialChannel = [];
        $scope.proposalData.voiceEssentialChannel[0] = 0; 
        $scope.proposalData.voiceEssentialLineHunt = [];
        $scope.proposalData.voiceEssentialLineHunt[0] = 0; 
        $scope.proposalData.voiceEssentialDID = [];
        $scope.proposalData.voiceEssentialDID[0] = 0;  
        $scope.proposalData.bECapDiscount = [];
        $scope.proposalData.bECapDiscount[0] = 0; 
        $scope.proposalData.ipMidbandDis = [];
        $scope.proposalData.ipMidbandDis[0] = 0; 
        $scope.proposalData.voiceCompAnalougeDis = [];
        $scope.proposalData.voiceCompAnalougeDis[0] = 0;
        $scope.proposalData.voiceCompBriDis = [];
        $scope.proposalData.voiceCompBriDis[0] = 0;
        $scope.proposalData.voiceCompPri = [];
        $scope.proposalData.voiceCompPri[0] = 0;
        $scope.proposalData.voiceCompPriDis = [];
        $scope.proposalData.voiceCompPriDis[0] = 0;
        $scope.proposalData.voiceCompCallDis = [];
        $scope.proposalData.voiceCompCallDis[0] = 0;
        $scope.proposalData.voiceCompAnalougeDisDSL = [];
        $scope.proposalData.voiceCompAnalougeDisDSL[0] = 0;
        $scope.proposalData.UnlimitedPlans = [];
        $scope.proposalData.UnlimitedPlans[0] = 0;
        $scope.proposalData.mobileWirelessDis = [];
        $scope.proposalData.mobileWirelessDis[0] = 0;
        $scope.proposalData.nbnUnlimitedDis = [];
        $scope.proposalData.nbnUnlimitedDis[0] = 0;
        $scope.proposalData.voiceCompAnalougeNBNUnli = [];
        $scope.proposalData.voiceCompAnalougeNBNUnli[0] = 0;
        $scope.proposalData.ipMidbandUnliDis = [];
        $scope.proposalData.ipMidbandUnliDis[0] = 0;
        $scope.proposalData.ethernetDis = [];
        $scope.proposalData.ethernetDis[0] = 0;
        $scope.proposalData.fibreDis = [];
        $scope.proposalData.fibreDis[0] = 0;
        $scope.proposalData.telstra4gSuper = [];
        $scope.proposalData.telstra4gSuperQty = [];
        $scope.proposalData.telstra4gSuperPrice = [];
        $scope.proposalData.telstra4gSuperPrice[0] = 0;
        $scope.proposalData.telstra4gSuperPrice[1] = 0;
        $scope.proposalData.telstra4gSuperPrice[2] = 0;
        $scope.proposalData.telstra4gSuperPrice[3] = 0;
        $scope.proposalData.telstra4gSuperBoltOn1GBQty = [];
        $scope.proposalData.telstra4gSuperBoltOn1GBQty[0] = 0;
        $scope.proposalData.telstra4gSuperBoltOn1GBQty[1] = 0;
        $scope.proposalData.telstra4gSuperBoltOn1GBQty[2] = 0;
        $scope.proposalData.telstra4gSuperBoltOn1GBQty[3] = 0;
        $scope.proposalData.telstra4gSuperBoltOn5GBQty = [];
        $scope.proposalData.telstra4gSuperBoltOn5GBQty[0] = 0;
        $scope.proposalData.telstra4gSuperBoltOn5GBQty[1] = 0;
        $scope.proposalData.telstra4gSuperBoltOn5GBQty[2] = 0;
        $scope.proposalData.telstra4gSuperBoltOn5GBQty[3] = 0;
        $scope.proposalData.telstra4gSuperBoltOn = [];
        $scope.proposalData.telstra4gSuperBoltOn1GBPrice = [];
        $scope.proposalData.telstra4gSuperBoltOn1GBPrice[0] = [];
        $scope.proposalData.telstra4gSuperBoltOn1GBPrice[1] = [];
        $scope.proposalData.telstra4gSuperBoltOn1GBPrice[2] = [];
        $scope.proposalData.telstra4gSuperBoltOn1GBPrice[3] = [];
        $scope.proposalData.telstra4gSuperBoltOn5GBPrice = [];
        $scope.proposalData.telstra4gSuperBoltOn5GBPrice[0] = [];
        $scope.proposalData.telstra4gSuperBoltOn5GBPrice[1] = [];
        $scope.proposalData.telstra4gSuperBoltOn5GBPrice[2] = [];
        $scope.proposalData.telstra4gSuperBoltOn5GBPrice[3] = [];
        $scope.proposalData.telstra4gSuperDisQty = [];
        $scope.proposalData.telstra4gSuperDisQty[0] = 0;
        $scope.proposalData.telstra4gSuperDisQty[1] = 0;
        $scope.proposalData.telstra4gSuperDisQty[2] = 0;
        $scope.proposalData.telstra4gSuperDisQty[3] = 0;
        $scope.proposalData.telstra4gSuperStandardDis = [];
        $scope.proposalData.telstra4gSuperDis = [];
        $scope.proposalData.telstra4gSuperMaxDis = [];
        $scope.proposalData.telstra4gSuperExtremeDis = [];
        $scope.proposalData.telstra4gSuperDisPrice = [];
        $scope.proposalData.telstra4gSuperDisPrice[0] = 0;
        $scope.proposalData.mobile4GUntimedDisPrice = 0;
        
        $scope.proposalData.boltOn4gSuper = [];
        $scope.proposalData.telstra4gSuperStandardBoltOn = [];
        $scope.proposalData.telstra4gSuperBoltOn = [];
        $scope.proposalData.telstra4gSuperMaxBoltOn = [];
        $scope.proposalData.telstra4gSuperExtremeBoltOn = [];
        $scope.proposalData.telstraUntimedDis = 0;
        $scope.proposalData.mobileUtDis = 0;
        $scope.proposalData.mobileCapDis = 0;
        $scope.proposalData.telstraWirelessSuperDis = [];
        $scope.proposalData.telstraWirelessSuperDis[0] = 0;
        $scope.proposalData.telstraWirelessDis = [];
        $scope.proposalData.telstraWirelessDis[0] = 0;
        $scope.proposalData.rate131300 = {};
        $scope.proposalData.rate131300.qt1800 = [];
        $scope.proposalData.rate131300.qt1800[0] = 0;
        $scope.proposalData.rate131300.qt1300 = [];
        $scope.proposalData.rate131300.qt1300[0] = 0;
        $scope.proposalData.rate131300.qt13 = [];
        $scope.proposalData.rate131300.qt13[0] = 0;
        $scope.proposalData.rate131300Dis = {};
        $scope.proposalData.rate131300Dis.qt1800 = [];
        $scope.proposalData.rate131300Dis.qt1800[0] = 0;
        $scope.proposalData.rate131300Dis.qt1300 = [];
        $scope.proposalData.rate131300Dis.qt1300[0] = 0;
        $scope.proposalData.rateDis131300 = {};
        $scope.proposalData.rateDis131300.discount = [];
        $scope.proposalData.rateDis131300.discount[0] = 0;
        $scope.proposalData.siteType = {};
        $scope.proposalData.leaser = 0;
        $scope.proposalData.fa = 0;
        $scope.proposalData.siteType = "Existing Site";
        $scope.proposalData.adsl2Dis = [];
        $scope.proposalData.adsl2Dis[0] = 0;
        $scope.proposalData.voiceCompAnalougeDSLDis = [];
        $scope.proposalData.voiceCompAnalougeDSLDis[0] = 0;
        $scope.proposalData.comLocation = {};
        $scope.proposalData.comLocation = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstra4gSuper'          : [],
            'telstraUntimed'          : [],
            'telstraWirelessSuper'    : [],
            'telstraWireless'         : [],
            'printer'                 : []
        };
        $scope.proposalData.voiceFaxToEmail = {};
        $scope.proposalData.voiceFaxQty = {};
        $scope.proposalData.voiceMobility = {};
        $scope.proposalData.voiceMobQty = {};

        $scope.proposalData.voiceStdFaxToEmail = {};
        $scope.proposalData.voiceStdFaxQty = {};
        $scope.proposalData.voiceStdMobility = {};
        $scope.proposalData.voiceStdMobQty = {};

        $scope.proposalData.voiceCompAnalouge = {};
        $scope.proposalData.voiceCompLineHunt = {};
        $scope.proposalData.voiceCompBri = {};
        $scope.proposalData.voiceCompPri = {};
        $scope.proposalData.voiceCompFaxToEmail = {};
        $scope.proposalData.voiceCompFaxQty = {};
        $scope.proposalData.voiceCompMobility = {};
        $scope.proposalData.voiceCompMobQty = {};
        // $scope.proposalData.voiceCompDID = {};
        // $scope.proposalData.voiceCompAnalougeDis = {};
        // $scope.proposalData.voiceCompBriDis = {};
        // $scope.proposalData.voiceCompPriDis = {};
        //$scope.proposalData.voiceCompPri = {};

        $scope.proposalData.voiceUntimedFaxToEmail = {};
        $scope.proposalData.voiceUntimedFaxQty = {};
        $scope.proposalData.voiceUntimedMobility = {};
        $scope.proposalData.voiceUntimedMobQty = {};

        $scope.proposalData.voiceSolutionFaxToEmail = {};
        $scope.proposalData.voiceSolutionFaxQty = {};
        $scope.proposalData.voiceSolutionMobility = {};
        $scope.proposalData.voiceSolutionMobQty = {};

        $scope.proposalData.ipMidbandDownload = {
            0:$scope.download_plans[1]
        };

        $scope.proposalData.alarmPlans = $scope.recurring_services[0];

        //console.log($scope.proposalData.alarmPlans);

        $scope.proposalData.transactionDetails = "Commercial Premises";
        $scope.proposalData.hardwareInstalled = "MPM (multi path monitoring device)";
        $scope.proposalData.monitoredServices = "Burglary";
        $scope.proposalData.panelType = "NESS";

        $scope.proposalData.adsl2Plans = {
            0:$scope.adsl_plans[0]
        }

        $scope.proposalData.telstra4gSuperPlans = {
            0:$scope.telstra4gSuper_plans[0]
        }

        $scope.proposalData.telstraUntimedPlans = {
            0:$scope.telstraUntimed_plans[0]
        }

        $scope.proposalData.telstraWirelessSuperPlans = {
            0:$scope.telstraWirelessSuper_plans[0]
        }
        $scope.proposalData.telstraWirelessPlans = {
            0:$scope.telstraWireless_plans[0]
        }
        $scope.proposalData.ethernetPlans = {
            0:$scope.ethernet_plans[0]
        }

        $scope.proposalData.UnlimitedPlans = {
            0:$scope.nbnUnlimited_plans[0]
        }

        $scope.proposalData.ipMidbandPlans = {
            0:$scope.ip_midband_plans[1]
        };

        $scope.proposalData.ipMidbandUnliPlans = {
            0:$scope.ip_midbandunli_plans[0]
        };

        $scope.proposalData.ipMidbandUnliProf = {
            0:$scope.professional_install[0]
        };

        $scope.proposalData.ipMidbandDownloadVoice = {
            0:$scope.download_plans[0]
        };

        $scope.proposalData.ipMidbandPlansVoice = {
            0:$scope.ip_midband_plans[0]
        };

        $scope.proposalData.mobileUtPlans = {
            0:$scope.mobile_4G_Untimed_Calls[0]
        };

        $scope.proposalData.fibreUtPlans = {
            0:$scope.fibre_unli_plans[0]
        };

        $scope.proposalData.mobileWirelessPlans = {
            0:$scope.wireless_cap_plans[0]
        };

        $scope.proposalData.voiceCompAnalougeDSL = {
            0:0
        };

        $scope.proposalData.voiceCompAnalougeNBNMonthly = {
            0:0
        };

        $scope.proposalData.voiceCompAnalougeNBNUnli = {
            0:0
        };

        $scope.proposalData.voiceCompAnalouge = {
            0:0
        };
        $scope.proposalData.voiceCompLineHunt = {
            0:0
        };
        $scope.proposalData.voiceCompBri = {
            0:0
        };

        $scope.proposalData.qt1300new = {};
        $scope.proposalData.qt800new = {};
        $scope.proposalData.qt13new = {};
        console.log($scope.proposalData);
    }

   if ($rootScope.viewProposalData)    {

        $scope.proposalData = $rootScope.viewProposalData.data;

        // console.log($scope.proposalData.schItems[0]);
        if($scope.proposalData.schItems.length != 1) {
            $scope.proposalData.schItems = [
                $scope.proposalData.schItems
            ];

            $scope.proposalData.analysis.provider = [
                $scope.proposalData.analysis.provider 
            ];
            $scope.proposalData.analysis.account = [
                $scope.proposalData.analysis.account 
            ];
            $scope.proposalData.analysis.dateIssued = [
                $scope.proposalData.analysis.dateIssued 
            ];


            $scope.proposalData.schQty = [
                $scope.proposalData.schQty
            ];
    
            $scope.proposalData.schAddQty = [
                $scope.proposalData.schAddQty
            ];
            $scope.proposalData.schAddItems = [
                $scope.proposalData.schAddItems
            ];
            
            $scope.proposalData.schAddOptionQty =  [
                $scope.proposalData.schAddOptionQty
            ];
    
            $scope.proposalData.schAddOption =  [
                $scope.proposalData.schAddOption
            ]; 
        }
           
        if(angular.isDefined($scope.proposalData.voiceStd)) {
            for (var index=0;index<$scope.proposalData.voiceStd.length;index++){
                $scope.locationCount = $scope.items['nbf_cap'].length-1;
                $scope.items['nbf_cap'][index] = index;
            }
        }
        if($scope.proposalData.voiceStdChannel) {
            for (var index=0;index<$scope.proposalData.voiceStdChannel.length;index++){
                $scope.locationCount = $scope.items['nbf_std'].length-1;
                $scope.items['nbf_std'][index] = index;
            } 
        }
        if($scope.proposalData.voiceUntimedChannel) {
            for (var index=0;index<$scope.proposalData.voiceUntimedChannel.length;index++){
                $scope.locationCount = $scope.items['voice_solution_untimed'].length-1;
                $scope.items['voice_solution_untimed'][index] = index;
            } 
        }
        if($scope.proposalData.voiceSolutionChannel) {
            for (var index=0;index<$scope.proposalData.voiceSolutionChannel.length;index++){
                $scope.locationCount = $scope.items['voice_solution_standard'].length-1;
                $scope.items['voice_solution_standard'][index] = index;
            } 
        }
        if($scope.proposalData.voiceEssentialChannel) {
            for (var index=0;index<$scope.proposalData.voiceEssentialChannel.length;index++){
                $scope.locationCount = $scope.items['voice_essentials_cap'].length-1;
                $scope.items['voice_essentials_cap'][index] = index;
            } 
        }
        if($scope.proposalData.cloudConnectStandardChannel) {
            for (var index=0;index<$scope.proposalData.cloudConnectStandardChannel.length;index++){
                $scope.locationCount = $scope.items['cloud_connect_standard'].length-1;
                $scope.items['cloud_connect_standard'][index] = index;
            } 
        }
        if($scope.proposalData.cloudConnectCapChannel) {
            for (var index=0;index<$scope.proposalData.cloudConnectCapChannel.length;index++){
                $scope.locationCount = $scope.items['cloud_connect_cap'].length-1;
                $scope.items['cloud_connect_cap'][index] = index;
            } 
        }
        if($scope.proposalData.voiceCompAnalouge) {
            for (var index=0;index<$scope.proposalData.voiceCompAnalouge.length;index++){
                $scope.locationCount = $scope.items['voice_comp'].length-1;
                $scope.items['voice_comp'][index] = index;
            } 
        }
        if($scope.proposalData.adsl2Plans) {
            for (var index=0;index<$scope.proposalData.adsl2Plans.length;index++){
                $scope.locationCount = $scope.items['data_adsl'].length-1;
                $scope.items['data_adsl'][index] = index;
            }
        }
        if($scope.proposalData.UnlimitedPlans) {
            for (var index=0;index<$scope.proposalData.UnlimitedPlans.length;index++){
                $scope.locationCount = $scope.items['nbnUnlimited'].length-1;
                $scope.items['nbnUnlimited'][index] = index;
            }
        }
        if($scope.proposalData.ethernetPlans) {
            for (var index=0;index<$scope.proposalData.ethernetPlans.length;index++){
                $scope.locationCount = $scope.items['ethernet'].length-1;
                $scope.items['ethernet'][index] = index;
            }
        }
        if($scope.proposalData.fibreUtPlans) {
            for (var index=0;index<$scope.proposalData.fibreUtPlans.length;index++){
                $scope.locationCount = $scope.items['fibre'].length-1;
                $scope.items['fibre'][index] = index;
            }
        }
        if($scope.proposalData.telstra4gSuperPlans) {
            for (var index=0;index<$scope.proposalData.telstra4gSuperPlans.length;index++){
                $scope.locationCount = $scope.items['telstra4gSuper'].length-1;
                $scope.items['telstra4gSuper'][index] = index;
            }
        }
        if($scope.proposalData.telstraWirelessSuperPlans) {
            for (var index=0;index<$scope.proposalData.telstraWirelessSuperPlans.length;index++){
                $scope.locationCount = $scope.items['telstraWirelessSuper'].length-1;
                $scope.items['telstraWirelessSuper'][index] = index;
            }
        }
        if($scope.proposalData.telstraWirelessPlans) {
            for (var index=0;index<$scope.proposalData.telstraWirelessPlans.length;index++){
                $scope.locationCount = $scope.items['telstraWireless'].length-1;
                $scope.items['telstraWireless'][index] = index;
            }
        }
        if($scope.proposalData.ipMidbandUnliPlans) {
            for (var index=0;index<$scope.proposalData.ipMidbandUnliPlans.length;index++){
                $scope.locationCount = $scope.items['ip_midbandunli'].length-1;    
                $scope.items['ip_midbandunli'][index] = index;
            }
        }
        if($scope.proposalData.ipMidbandPlans) {
            for (var index=0;index<$scope.proposalData.ipMidbandPlans.length;index++){
                $scope.locationCount = $scope.items['ip_midband'].length-1;
                $scope.items['ip_midband'][index] = index;
            }
        }

        if($scope.proposalData.telstraUntimedPlans) {
            for (var index=0;index<$scope.proposalData.telstraUntimedPlans.length;index++){
                $scope.locationCount = $scope.items['telstraUntimed'].length-1;
                $scope.items['telstraUntimed'][index] = index;
            }
        }
        // for (var index=0;index<$scope.proposalData.comLocation['mobileUntimed'].length;index++){
        //     $scope.locationCount = $scope.items['mobileUntimed'].length-1;
        //     $scope.items['mobileUntimed'][index] = index;
        //     }

        $scope.editId = $rootScope.viewProposalData.id;
         if (angular.isDefined($scope.proposalData.schedule)) {
            angular.forEach($scope.proposalData.schedule, function(item, count) {
                if (count == 0) {
                  $(".qunatity").val(item.qty);
                  $(".desc").val(item.desc);
                } else {
                  var wrapped = '<div class="row">\n\
                                            <div class="col-md-2 col-sm-2">\n\
                                                <button class="btn btn-primary" id="schedule-'+count+'">\n\
                                                   <i class="fa fa-times"></i> \n\
                                                </button></div>\n\
                                            <div class="col-md-10 schedule-goods">\n\
                                                <div class="col-md-3 col-sm-3">\n\
                                                    <div class="form-group">\n\
                                                        <div class="col-xs-4">\n\
                                                            <label class="control-label">Quantity</label>\n\
                                                        </div>\n\
                                                        <div class ="col-xs-8">\n\
                                                            <input type="number" value='+item.qty+' class="qunatity form-control">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="col-md-9 col-sm-9">\n\
                                                    <div class="form-group">\n\
                                                        <div class="col-xs-3 text-right">\n\
                                                                <label class="control-label">Description</label>\n\
                                                        </div>\n\
                                                        <div class ="col-xs-9">\n\
                                                            <input type="text"  value='+item.desc+' class ="desc form-control">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                </div>';
                    $(".rentalGoods").append(wrapped);
                    $("#schedule-"+count).click(function() {
                        $(this).parent().parent().remove();
                    });
                    $scope.scheduleCount = count;
                }
            });
        }
        if (angular.isDefined($scope.proposalData.cState)) {
            $scope.proposalData.cState = $scope.proposalData.cState;
        }
        if (angular.isDefined($scope.proposalData.type)) {
            if ($scope.proposalData.type.configApp) {
                if (angular.isDefined($scope.proposalData.configAppTelNo)) {
                    angular.forEach($scope.proposalData.configAppTelNo, function(item,count) {
                      $scope.addTel();
                      $timeout(function() {
                          $("#telItemConfigApp-"+count).val(item.tel);
                      });
                    });
                }

                if (angular.isDefined($scope.proposalData.configMobPort)) {
                    $timeout(function() {
                        $scope.configMobPort = $scope.proposalData.configMobPort;
                    });
                }
                if ($scope.proposalData.config) {
                   if ($scope.proposalData.config.pstn) {
                       $scope.proposalData.config.pstn = true;
                   } else {
                        $scope.proposalData.config.pstn = false;
                   }
                   if ($scope.proposalData.config.lineHunt) {
                       $scope.proposalData.config.lineHunt = true;
                   }
                   if ($scope.proposalData.config.ISDN) {
                       $scope.proposalData.config.ISDN = true;
                   } else {
                        $scope.proposalData.config.pstn = false;
                   }
                   if ($scope.proposalData.config.indial) {
                       $scope.proposalData.config.indial = true;
                   }
                   if ($scope.proposalData.config.port ) {
                       $scope.proposalData.config.port = true;
                   }
                }
            }


        }

        $rootScope.viewFormData = null;
    }


    $('.mob').on("keyup", function()
    {
        var val = this.value.replace(/[^0-9]/ig, '');
        var newVal = '';
        /*if (val.length >10) {
            val = val.substr(0,10);
        }*/
        if (val.length >4) {
            newVal +=val.substr(0,4)+'-';
            val = val.substr(4);
        }
        while (val.length > 3)   {
            newVal += val.substr(0, 3) +'-';
            val = val.substr(3);

        }
        newVal += val;
        this.value = newVal;
    });


    /** FOR DISPLAY IN FRONT END**/
     $scope.reset();
    console.log($rootScope.viewProposalData);
    if (angular.isDefined($rootScope.viewProposalData)) {
        $scope.editId = $rootScope.viewProposalData.id;
        $scope.quot_num = $rootScope.viewProposalData.quot_num;
        $scope.proposalData = $rootScope.viewProposalData.data;
    }

    console.log($scope.editId,$scope.quot_num);


    $scope.tinymceOptions = {
      selector: "textarea",
      theme: "modern",
      paste_data_images: true,
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      toolbar2: "print preview media | forecolor backcolor emoticons "
    };

    $scope.removeOptionsBlock = function(form_type, index) {
        console.log($scope.items[form_type]);

        if (form_type == 'nbf_cap') {
            delete $scope.proposalData.voiceStd[index];
            delete $scope.proposalData.voiceStdLineHunt[index];
            delete $scope.proposalData.voiceFaxToEmail[index];
            delete $scope.proposalData.voiceFaxQty[index];
            delete $scope.proposalData.dID[index];
            delete $scope.proposalData.bFCapDiscount[index];
            $("#voiceCapOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'nbf_std') {
            delete $scope.proposalData.voiceStdChannel[index];
            delete $scope.proposalData.voiceFcusStdLineHunt[index];
            delete $scope.proposalData.voiceStdFaxQty[index];
            delete $scope.proposalData.voiceStdDID[index];
            delete $scope.proposalData.bFStdDiscount[index];
            delete $scope.proposalData.bFCallDiscount[index];
            $("#focusStandardOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'voice_comp') {
            delete $scope.proposalData.voiceCompAnalouge[index];
            delete $scope.proposalData.voiceCompLineHunt[index];
            delete $scope.proposalData.voiceCompBri[index];
            delete $scope.proposalData.voiceCompPri[index];
            delete $scope.proposalData.voiceCompFaxQty[index];
            delete $scope.proposalData.voiceCompFaxQty[index];
            delete $scope.proposalData.voiceCompAnalougeDis[index];
            delete $scope.proposalData.voiceCompBriDis[index];
            delete $scope.proposalData.voiceCompPriDis[index];
            delete $scope.proposalData.voiceCompCallDis[index];
            $("#completeOfficeOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'voice_solution_untimed') {
            delete $scope.proposalData.voiceUntimedChannel[index];
            delete $scope.proposalData.voiceUntimedLineHunt[index];
            delete $scope.proposalData.voiceUntimedFaxQty[index];
            delete $scope.proposalData.voiceUntimedDID[index];
            delete $scope.proposalData.bFUntimedDiscount[index];
            $("#voiceUtOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'voice_solution_standard') {
            delete $scope.proposalData.voiceSolutionChannel[index];
            delete $scope.proposalData.voiceSolutionLineHunt[index];
            delete $scope.proposalData.voiceSolutionFaxQty[index];
            delete $scope.proposalData.voiceSolutionDID[index];
            delete $scope.proposalData.bFSolutionDiscount[index];
            delete $scope.proposalData.bFSolutionCallDiscount[index];
            $("#voiceSolutionOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'voice_essentials_cap') {
            delete $scope.proposalData.voiceEssentialChannel[index];
            delete $scope.proposalData.voiceEssentialLineHunt[index];
            delete $scope.proposalData.voiceEssentialDID[index];
            delete $scope.proposalData.bECapDiscount[index];
            delete $scope.proposalData.ipMidbandPlansVoice[index];
            delete $scope.proposalData.ipMidbandDownloadVoice[index];
            delete $scope.proposalData.ipMidbandDis[index];
            $("#voiceEssentialOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'cloud_connect_standard') {
            delete $scope.proposalData.cloudConnectStandardChannel[index];
            delete $scope.proposalData.cloudConnectStandardFaxQty[index];
            delete $scope.proposalData.cloudConnectStandardDID[index];
            delete $scope.proposalData.cloudConnectStandardAnalouge[index];
            delete $scope.proposalData.cloudConnectStandarduConeApp[index];
            delete $scope.proposalData.cloudConnectStandardAddCallQueue[index];
            delete $scope.proposalData.cloudConnectStandardRepClientApp[index];
            delete $scope.proposalData.cloudConnectStandardAddAutoAttndnt[index];
            delete $scope.proposalData.cloudConnectStandardAddVoicemail[index];
            delete $scope.proposalData.cloudConnectStandardCallRecording[index];
            delete $scope.proposalData.cloudConnectStandardCallRecordingPremium[index];
            delete $scope.proposalData.cCStandardAnalogueDis[index];
            delete $scope.proposalData.cCStandardCallRecordingDiscount[index];
            delete $scope.proposalData.cCStandardCallRecordingPremiumDiscount[index];
            delete $scope.proposalData.cCStandardDiscount[index];
            delete $scope.proposalData.cCStandardCallDiscount[index];
            $("#cloudConnectStandardOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'cloud_connect_cap') {
            delete $scope.proposalData.cloudConnectCapChannel[index];
            delete $scope.proposalData.cloudConnectCapFaxQty[index];
            delete $scope.proposalData.cloudConnectCapDID[index];
            delete $scope.proposalData.cloudConnectCapAnalouge[index];
            delete $scope.proposalData.cloudConnectCapuConeApp[index];
            delete $scope.proposalData.cloudConnectCapAddCallQueue[index];
            delete $scope.proposalData.cloudConnectCapRepClientApp[index];
            delete $scope.proposalData.cloudConnectCapAddAutoAttndnt[index];
            delete $scope.proposalData.cloudConnectCapAddVoicemail[index];
            delete $scope.proposalData.cloudConnectCapCallRecording[index];
            delete $scope.proposalData.cloudConnectCapCallRecordingPremium[index];
            delete $scope.proposalData.cCCapAnalogueDis[index];
            delete $scope.proposalData.cCCapCallRecordingDiscount[index];
            delete $scope.proposalData.cCCapCallRecordingPremiumDiscount[index];
            delete $scope.proposalData.cCCapiscount[index];
            $("#cloudConnectCapOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'data_adsl') {
            delete $scope.proposalData.adsl2Plans[index];
            delete $scope.proposalData.adsl2Dis[index];
            delete $scope.proposalData.voiceCompAnalougeDSL[index];
            delete $scope.proposalData.voiceCompAnalougeDSLDis[index];
            delete $scope.proposalData.voiceCompAnalougeDisDSL[index];
            $("#adsl2Option .row-"+index).css('display', 'none');
        }
        if (form_type == 'nbnUnlimited') {
            delete $scope.proposalData.UnlimitedPlans[index];
            delete $scope.proposalData.nbnUnlimitedDis[index];
            delete $scope.proposalData.voiceCompAnalougeNBNUnli[index];
            delete $scope.proposalData.voiceCompAnalougeDisNBNUnli[index];
            $("#nbnUnlimitedPlansOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'ip_midband') {
            delete $scope.proposalData.ipMidbandPlans[index];
            delete $scope.proposalData.ipMidbandDownload[index];
            delete $scope.proposalData.ipMidbandDis[index];
            $("#ipMidbandOption .row-"+index).css('display', 'none');
        }
        if (form_type == 'ip_midbandunli') {
            delete $scope.proposalData.ipMidbandUnliPlans[index];
            delete $scope.proposalData.ipMidbandUnliDis[index];
            $("#ipMidbandUnliOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'ethernet') {
            delete $scope.proposalData.ethernetPlans[index];
            delete $scope.proposalData.ethernetDis[index];
            $("#ethernetOption .row-"+index).css('display', 'none');
        } 
        if (form_type == 'fibre') {
            delete $scope.proposalData.fibreUtPlans[index];
            delete $scope.proposalData.fibreDis[index];
            $("#fibreOption .row-"+index).css('display', 'none');
        }
        if (form_type == 'telstra4gSuper') {
            $scope.proposalData.telstra4gSuper[index].plan = "";
            $scope.proposalData.telstra4gSuper[index].price = "";
            $scope.proposalData.telstra4gSuper[index].group = "";
            $scope.proposalData.telstra4gSuper[index].inclusions = "";
            $scope.proposalData.telstra4gSuper[index].status = 0;
            $scope.proposalData.telstra4gSuper[index].itemIndex = "";
     
            if($scope.proposalData.telstra4gSuperStandardBoltOn[index]) {
                $scope.proposalData.telstra4gSuperStandardBoltOn[index].description = "";
                $scope.proposalData.telstra4gSuperStandardBoltOn[index].itemIndex = "";
                $scope.proposalData.telstra4gSuperStandardBoltOn[index].memory = null;
                $scope.proposalData.telstra4gSuperStandardBoltOn[index].plan_group = "";
                $scope.proposalData.telstra4gSuperStandardBoltOn[index].price = "";
            }
            if($scope.proposalData.telstra4gSuperBoltOn[index]) {
                $scope.proposalData.telstra4gSuperBoltOn[index].description = "";
                $scope.proposalData.telstra4gSuperBoltOn[index].itemIndex = "";
                $scope.proposalData.telstra4gSuperBoltOn[index].memory = null;
                $scope.proposalData.telstra4gSuperBoltOn[index].plan_group = "";
                $scope.proposalData.telstra4gSuperBoltOn[index].price = "";
            }
            if($scope.proposalData.telstra4gSuperMaxBoltOn[index]) {
                $scope.proposalData.telstra4gSuperMaxBoltOn[index].description = "";
                $scope.proposalData.telstra4gSuperMaxBoltOn[index].itemIndex = "";
                $scope.proposalData.telstra4gSuperMaxBoltOn[index].memory = null;
                $scope.proposalData.telstra4gSuperMaxBoltOn[index].plan_group = "";
                $scope.proposalData.telstra4gSuperMaxBoltOn[index].price = "";
            }
            if($scope.proposalData.telstra4gSuperExtremeBoltOn[index]) {
                $scope.proposalData.telstra4gSuperExtremeBoltOn[index].description = "";
                $scope.proposalData.telstra4gSuperExtremeBoltOn[index].itemIndex = "";
                $scope.proposalData.telstra4gSuperExtremeBoltOn[index].memory = null;
                $scope.proposalData.telstra4gSuperExtremeBoltOn[index].plan_group = "";
                $scope.proposalData.telstra4gSuperExtremeBoltOn[index].price = "";
            }

            if($scope.proposalData.telstra4gSuperStandardDis[index]) {
                $scope.proposalData.telstra4gSuperStandardDis[index] = 0;
            }
            if($scope.proposalData.telstra4gSuperDis[index]) {
                $scope.proposalData.telstra4gSuperDis[index] = 0;
            }
            if($scope.proposalData.telstra4gSuperMaxDis[index]) {
                $scope.proposalData.telstra4gSuperMaxDis[index] = 0;
            }
            if($scope.proposalData.telstra4gSuperExtremeDis[index]) {
                $scope.proposalData.telstra4gSuperExtremeDis[index] = 0;
            }

            $("#telstra4gSuperOption .row-"+index).css('display', 'none');
        }
        if (form_type == 'telstraWirelessSuper') {
            delete $scope.proposalData.telstraWirelessSuperPlans[index];
            delete $scope.proposalData.telstraWirelessSuperDis[index];
            $("#telstraWirelessSuperOption .row-"+index).css('display', 'none');
        }
        if (form_type == 'telstraWireless') {
            delete $scope.proposalData.telstraWirelessPlans[index];
            delete $scope.proposalData.telstraWirelessDis[index];
            $("#telstraWirelessOption .row-"+index).css('display', 'none');
        }
        if (form_type == 'telstraUntimed') {
            delete $scope.proposalData.untimed[index];
            delete $scope.proposalData.mobileUtDis[index];
            $("#mobileUtOption .row-"+index).css('display', 'none');
        }
       
        delete $scope.items[form_type][index];

    };

    $scope.setValue = function() {
        $scope.proposalData.tradingName = 'Sample Long Name Company';
        $scope.proposalData.contactPerson = 'Arthur Conan Doyle';
        $scope.proposalData.currentHardware = 'currentHardware to Show';
        $scope.proposalData.currentServices = 'currentServices to Show';
        $scope.proposalData.abn = '01234567891';
        $scope.proposalData.suburb = 'suburb to Show';
        $scope.proposalData.address = 'Address to Show';
        $scope.proposalData.emailAddress = 'emailtocontact@gmail.com';
        $scope.proposalData.phoneNumber = '(02) 4342-1232';
        $scope.proposalData.mobileNumber = '4532-533-267';
        // $scope.quot_num = "";
    }

    $scope.array = [];
    $scope.array_ = angular.copy($scope.array);

    var uploader = $scope.uploader = new FileUploader({
        url: 'proposal/upload'
    });

    var dataPaths;
    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        dataPaths = response.FilePath
        $scope.proposalData.files = dataPaths; 
    };

    if ($stateParams.id) {
        Proposals.get({
            id: $stateParams.id
        }, function(data) {
            $scope.proposalData = data;
            $scope.array = data.selectedFeedbacks;
            $scope.editId= data.id;
            // alert($scope.array);
        });
    } 
    // $.each ($rootScope.rates,function(index,value){
    //         subrate = (rental_per_month/value.rate)*1000; 
    //         //rates.push(subrate);
    //         if($scope.checkRange(subrate,value.lowerLimit,value.upperLimit)){
    //           financeAmt = subrate;
    //         }
    //         console.log
    //     });
    $scope.proposalData.fa = 0;
    $scope.getCurrentDate = function(){
        var currentDate = new Date();
        var month =  new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";   
        return month[currentDate.getMonth()]+"-"+currentDate.getDate()+"-"+currentDate.getFullYear();
    }

    
    $scope.dollarSign = function(val){
        var strCurrency = '';
        if (angular.isDefined(val))
            strCurrency = '$' + val;
        //alert(strCurrency);
        return strCurrency;
    }
    
    $scope.computeTotalCost = function(){
        $scope.proposalData.analysis.totalCost = 0;
        angular.forEach($scope.proposalData.analysis.landlinesCost, function(value, key) {
            console.log(key);
            console.log(value);
        if($scope.proposalData.analysis.landlinesCost[key]!="" && $scope.proposalData.analysis.schItemsMrcLandlines[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        });
        angular.forEach($scope.proposalData.analysis.internetCost, function(value, key) {
            if($scope.proposalData.analysis.internetCost[key]!="" && $scope.proposalData.analysis.schItemsMrcInternet[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        });
        angular.forEach($scope.proposalData.analysis.mobileCost, function(value, key) {
            if($scope.proposalData.analysis.mobileCost[key]!="" && $scope.proposalData.analysis.schItemsMrcMobile[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })
        angular.forEach($scope.proposalData.analysis.inboundCost, function(value, key) {
            if($scope.proposalData.analysis.inboundCost[key]!="" && $scope.proposalData.analysis.schItemsMrcInbound[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })
        
        angular.forEach($scope.proposalData.analysis.localCost, function(value, key) {
            if($scope.proposalData.analysis.localCost[key]!="" && $scope.proposalData.analysis.schItemsUsageLocal[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })
        angular.forEach($scope.proposalData.analysis.nationalCost, function(value, key) {
            if($scope.proposalData.analysis.nationalCost[key]!="" && $scope.proposalData.analysis.schItemsUsageNational[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })
        angular.forEach($scope.proposalData.analysis.callsToMobileCost, function(value, key) {
            if($scope.proposalData.analysis.callsToMobileCost[key]!="" && $scope.proposalData.analysis.schItemsUsageCallstoMobile[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })
        angular.forEach($scope.proposalData.analysis.internationalCost, function(value, key) {
            if($scope.proposalData.analysis.internationalCost[key]!="" && $scope.proposalData.analysis.schItemsUsageInternational[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })
        angular.forEach($scope.proposalData.analysis.callsTo13Cost, function(value, key) {
            if($scope.proposalData.analysis.callsTo13Cost[key]!="" && $scope.proposalData.analysis.schItemsUsageCallsto13[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })
        angular.forEach($scope.proposalData.analysis.roamingCost, function(value, key) {
            if($scope.proposalData.analysis.roamingCost[key]!="" && $scope.proposalData.analysis.schItemsUsageRoaming[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })

        angular.forEach($scope.proposalData.analysis.miscCost, function(value, key) {
            if($scope.proposalData.analysis.miscCost[key]!="" && $scope.proposalData.analysis.schItemsOtherMrcMisc[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })
        angular.forEach($scope.proposalData.analysis.rentalCost, function(value, key) {
            if($scope.proposalData.analysis.rentalCost[key]!="" && $scope.proposalData.analysis.schItemsOtherMrcRental[0][key].status == 1){
                if(value) {
                $scope.proposalData.analysis.totalCost += (1 * value); 
                }
            }
        })
    } 

    $scope.computeFinanceAmmount = function(){
      $scope.proposalData.fa = 0;
      console.log($scope.proposalData.schOption);
      angular.forEach($scope.proposalData.schOption, function(value, key) {
          // $scope.proposalData.fa = parseInt($scope.proposalData.fa.replace(/[^\d.]/g,''));
      if($scope.proposalData.schOption[key]!="" && $scope.proposalData.schItems[0][key].status !="0"){
                if(value.price) {
                     $scope.proposalData.fa += ($scope.proposalData.schQty[0][key] * value.price); 
                }
            }
      });
      // $scope.proposalData.fa = $scope.dollarSign($scope.proposalData.fa);
      // $scope.proposalData.fa = parseInt($scope.proposalData.fa.replace(/[^\d.]/g,''));
    }  

    $scope.LineHunt = function(type, index){

        console.log(type);

        if(type == 'nbf_cap') {
            angular.forEach($scope.proposalData.voiceStd, function(value, key) {
                if (value == 495){
                    $scope.proposalData.voiceStdLineHunt[key] = '27.25';
                } else if (value == 594) {
                    $scope.proposalData.voiceStdLineHunt[key] = '32.7';
                } else if (value == 792) {
                    $scope.proposalData.voiceStdLineHunt[key] = '43.6';
                } else if (value == 990) {
                    $scope.proposalData.voiceStdLineHunt[key] = '54.5';
                } else if (value == 1188) {
                    $scope.proposalData.voiceStdLineHunt[key] = '65.4';
                } else if (value == 1106) {
                    $scope.proposalData.voiceStdLineHunt[key] = '76.3';
                } else if (value == 1264) {
                    $scope.proposalData.voiceStdLineHunt[key] = '87.2';
                } else if (value == 1422) {
                    $scope.proposalData.voiceStdLineHunt[key] = '98.1';
                } else if (value == 1580) {
                    $scope.proposalData.voiceStdLineHunt[key] = '109';
                } else if (value == 1975) {
                    $scope.proposalData.voiceStdLineHunt[key] = '136.25';
                }
            });
        }
        if(type == 'nbf_std') {
            angular.forEach($scope.proposalData.voiceStdChannel, function(value, key) {
                if (value == 199){
                    $scope.proposalData.voiceFcusStdLineHunt[key] = '27.25';
                } else if (value == 219) {
                    $scope.proposalData.voiceFcusStdLineHunt[key] = '32.7';
                } else if (value == 249) {
                    $scope.proposalData.voiceFcusStdLineHunt[key] = '54.5';
                } else if (value == 299) {
                    $scope.proposalData.voiceFcusStdLineHunt[key] = '109';
                } else if (value == 349) {
                    $scope.proposalData.voiceFcusStdLineHunt[key] = '163.5';
                }
            });
        }
        if(type == 'completeOffice') {
            angular.forEach($scope.proposalData.voiceCompAnalouge, function(value, key) {
                if (value == 43.95){
                    $scope.proposalData.voiceCompLineHunt[key] = '6.50';
                } else if (value == 87.9) {
                    $scope.proposalData.voiceCompLineHunt[key] = '13';
                } else if (value == 131.85) {
                    $scope.proposalData.voiceCompLineHunt[key] = '19.5';
                } else if (value == 175.8) {
                    $scope.proposalData.voiceCompLineHunt[key] = '26';
                } else if (value == 219.75) {
                    $scope.proposalData.voiceCompLineHunt[key] = '32.5';
                } else if (value == 263.7) {
                    $scope.proposalData.voiceCompLineHunt[key] = '39';
                } else if (value == 307.65) {
                    $scope.proposalData.voiceCompLineHunt[key] = '45.5';
                } else if (value == 351.6) {
                    $scope.proposalData.voiceCompLineHunt[key] = '52';
                } else if (value == 395.55) {
                    $scope.proposalData.voiceCompLineHunt[key] = '58.5';
                } else if (value == 439.5) {
                    $scope.proposalData.voiceCompLineHunt[key] = '65';
                }
            });
        }
        
        if(type == 'voice_solution_untimed') {
            $scope.proposalData.voiceUntimedLineHunt = [];
            $scope.proposalData.voiceUntimedLineHunt[0] = 0;
            angular.forEach($scope.proposalData.voiceUntimedChannel, function(value, key) {
                console.log($scope.proposalData.voiceUntimedLineHunt);
                if (value == 199){
                    $scope.proposalData.voiceUntimedLineHunt[key] = '16.35';
                } else if (value == 259) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '21.8';
                } else if (value == 319) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '27.25';
                } else if (value == 379) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '32.7';
                } else if (value == 439) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '38.15';
                } else if (value == 499) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '43.6';
                } else if (value == 559) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '49.05';
                } else if (value == 619) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '54.5';
                } else if (value == 679) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '59.95';
                } else if (value == 739) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '65.4';
                } else if (value == 799) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '70.85';
                } else if (value == 859) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '76.3';
                } else if (value == 919) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '81.75';
                } else if (value == 979) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '87.2';
                } else if (value == 1039) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '92.65';
                } else if (value == 1099) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '98.1';
                } else if (value == 1159) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '103.55';
                } else if (value == 1219) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '109';
                } else if (value == 1279) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '114.45';
                } else if (value == 1339) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '119.9';
                } else if (value == 1399) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '125.35';
                } else if (value == 1459) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '130.8';
                } else if (value == 1519) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '136.25';
                } else if (value == 1579) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '141.7';
                } else if (value == 1639) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '147.15';
                } else if (value == 1699) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '152.6';
                } else if (value == 1759) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '158.05';
                } else if (value == 1819) {
                    $scope.proposalData.voiceUntimedLineHunt[key] = '163.5';
                }
            });
        }

        if(type == 'voice_solution_standard') {
            angular.forEach($scope.proposalData.voiceSolutionChannel, function(value, key) {
                if (value == 149){
                    $scope.proposalData.voiceSolutionLineHunt[key] = '16.35';
                } else if (value == 159) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '21.8';
                } else if (value == 169) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '27.25';
                } else if (value == 179) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '32.7';
                } else if (value == 189) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '38.15';
                } else if (value == 199) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '43.6';
                } else if (value == 209) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '49.05';
                } else if (value == 219) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '54.5';
                } else if (value == 229) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '59.95';
                } else if (value == 239) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '65.4';
                } else if (value == 249) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '70.85';
                } else if (value == 259) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '76.3';
                } else if (value == 269) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '81.75';
                } else if (value == 279) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '87.2';
                } else if (value == 289) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '92.65';
                } else if (value == 299) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '98.1';
                } else if (value == 309) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '103.55';
                } else if (value == 319) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '109';
                } else if (value == 329) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '114.45';
                } else if (value == 339) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '119.9';
                } else if (value == 349) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '125.35';
                } else if (value == 359) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '130.8';
                } else if (value == 369) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '136.25';
                } else if (value == 379) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '141.7';
                } else if (value == 389) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '147.15';
                } else if (value == 399) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '152.6';
                } else if (value == 409) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '158.05';
                } else if (value == 419) {
                    $scope.proposalData.voiceSolutionLineHunt[key] = '163.5';
                }
            });
        }

      

        
        if(type == 'voice_essentials_cap') {
            angular.forEach($scope.proposalData.voiceEssentialChannel, function(value, key) {
                if (value == 196){
                    $scope.proposalData.voiceEssentialLineHunt[key] = '15.8';
                } else if (value == 294) {
                    $scope.proposalData.voiceEssentialLineHunt[key] = '23.7';
                } else if (value == 392) {
                    $scope.proposalData.voiceEssentialLineHunt[key] = '31.6';
                } else if (value == 490) {
                    $scope.proposalData.voiceEssentialLineHunt[key] = '39.5';
                } else if (value == 588) {
                    $scope.proposalData.voiceEssentialLineHunt[key] = '47.4';
                } else if (value == 686) {
                    $scope.proposalData.voiceEssentialLineHunt[key] = '55.3';
                } else if (value == 784) {
                    $scope.proposalData.voiceEssentialLineHunt[key] = '63.2';
                } else if (value == 882) {
                    $scope.proposalData.voiceEssentialLineHunt[key] = '71.1';
                } else if (value == 980) {
                    $scope.proposalData.voiceEssentialLineHunt[key] = '79';
                }
            });
        }
    }  

    $scope.counter = function(){
         $scope.proposalData.term = $('#term').val();
         $scope.proposalData.billingType = $('#type').val();
         // $scope.proposalData.fa = parseInt($scope.proposalData.fa);
         var rates = Form.getRates({term:$scope.proposalData.term,
                                    type:$scope.proposalData.billingType
                                    }, function(data) {
            if (data.success) {
                $scope.rates = data.rates;
            }
            //console.log($scope.rates);
            if($scope.proposalData.billingType == 'rental'){
                // $scope.proposalData.fa = parseInt($scope.proposalData.fa);
              angular.forEach($scope.rates, function(value, key){
                       //console.log('Rental:' + key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate + $scope.proposalData.fa);
                       if($scope.checkRange($scope.proposalData.fa,value.lowerLimit,value.upperLimit)){
                          $scope.proposalData.leaser = ($scope.proposalData.fa/1000)*value.rate;
                          $scope.proposalData.leaser = parseInt($scope.proposalData.leaser);
                         // $scope.leaser = $scope.dollarSign($scope.leaser);
                     }
                 });
            }
            else if($scope.proposalData.billingType == 'leasing'){
              angular.forEach($scope.rates, function(value, key){
                       //console.log(key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate + $scope.proposalData.fa);
                       if($scope.checkRange($scope.proposalData.fa,value.lowerLimit,value.upperLimit)){
                          // $scope.proposalData.fa = parseInt($scope.proposalData.fa.replace(/[^\d.]/g,''));
                          $scope.proposalData.leaser = ($scope.proposalData.fa/1000)*value.rate;
                          $scope.proposalData.leaser = parseInt($scope.proposalData.leaser);
                         // $scope.leaser = $scope.dollarSign($scope.leaser);
                     }
                 });
            } else {
              $scope.proposalData.leaser = $scope.proposalData.fa;
            }
        });
            
  }
   $scope.checkRange = function(val,lower_limit,upper_limit){
        //alert(val + '-' + lower_limit + '-' + upper_limit);
        if (upper_limit==0){
            if (val>=lower_limit){
                return true;
            } else {
                return false;
            }
        } else {
            if (val >= lower_limit && val <= upper_limit){
                return true;
            } else {
                return false;
            }
        }
    }
    
    $scope.calculateDiscount = function(currentVal,plan, planOptionId, completeOptions,optionId) {
        var payment = 0;
        var capTotalVal =0;
        //console.log(planOptionId);
        if (planOptionId != '') {
            var planStr = $("#"+planOptionId+" option:selected").text();
            var planVal =parseInt(planStr.match(/\$(\d+)/)[1]);
        }
        currentVal = parseInt(currentVal);
        //alert(currentVal);
        if(angular.isDefined($scope.proposalData.leaser)) {
            payment = parseInt($scope.proposalData.leaser);
        }
        //alert(completeOptions);
        var c_options = angular.isDefined(completeOptions)?completeOptions:0;
        //alert(c_options);
        $(".main-disc").each(function() {
            capTotalVal += parseInt($(this).val());
        });
        console.log("Current Val : " + currentVal + " | Cap Total Val : " + capTotalVal + " | Plan Value : " + planVal);
        if (plan == 'voice-cap' || plan == 'data-plan' || plan == 'mobile-plan' ) {
            if (currentVal > planVal) {
                alert("Discount cannot exceed monthly payment");
                //$('#' + optionId).val('');
            } else {
                if(capTotalVal  == currentVal) {
                    if (currentVal> planVal/2) {
                        var disc = planVal/2;
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                } else {
                    var remaining = payment-(capTotalVal-currentVal);
                    if (remaining<0) {
                        remaining = 0;
                    }

                    var disc = remaining + planVal*30/100;
                    if (currentVal > disc) {
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                }
            }
        } else if (plan == 'standard-plan') {
            if (currentVal>100) {
                alert("Maximum Discount should not go over $100");
               // $('#' + optionId).val('');
            }
        }  else if (plan == 'complete-bri') {
            if (currentVal>50*c_options) {
                alert("Maximum Discount should not go over $"+50*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-pri') {
            if (currentVal>100*c_options) {
                alert("Maximum Discount should not go over $"+100*c_options);
               // $('#' + optionId).val('');
            }
        } else if (plan == 'complete-analog') {
            if (currentVal>(10*c_options)) {
                alert("Maximum Discount should not go over $" + 10*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-call' || plan == '1300-plan' || plan == 'standard-call') {
            if (currentVal> payment) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            }
        } else if (plan == 'unlimited-plan') {
            if (currentVal> planVal) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            } else {
                if (currentVal> planVal/2) {
                    var disc = planVal/2;
                    alert("Maximum Discount should not go over $"+disc);
                   // $('#' + optionId).val('');
                }
            }
        }
    }

    $scope.prepDiscounts = function(payment) {
      payment = payment.replace(/^\D+|\,+/g, '');
       $scope.EqPayment = payment;
      }

    $scope.showOptions = function(form) {
        // alert($scope.proposalData.type.voiceCap);
        $(".optionBox").hide();
        $("#"+form+"Option").show();
        $(".form-label label").css('color','black');
        $("#"+form+"Label label").css('color','green');
        $scope.locationCount = 0;

        if (form=='cloudConnectStandard'){
            $scope.processcloudRateCard();
        }

        if (form=='cloudConnectCap'){
            $scope.processcloudRateCard();
        }
    }

    $scope.processcloudRateCard = function(){
        var cloudphones = [];
        //console.log($scope.items['schedule_of_goods']);
        angular.forEach($scope.items['schedule_of_goods'],function(item,index){
            cloudphones[index] = [];
            //console.log($scope.formData.schItems[index]);
           angular.forEach($scope.proposalData.schItems[index],function(schItem){
            console.log(schItem);
                if (!angular.isUndefined($scope.proposalData.schOption[index])){
                    if (!angular.isUndefined($scope.proposalData.schOption[index])){
                            if (($scope.proposalData.schOption[index].group.toUpperCase().indexOf('CISCO')!==-1)){
                                var cpiBwAmt = $scope.proposalData.schQty[index] * $scope.proposalData.schOption[index].cpiBw;
                                if (cpiBwAmt==0){
                                    cpiBwAmt = 1.0;
                                }
                                var cpiColourAmt = $scope.proposalData.schQty[index] * $scope.proposalData.schOption[index].cpiColour;
                                if (cpiColourAmt==0){
                                    cpiColourAmt = 10.0;
                                }
                                var printer = {
                                    "model" : $scope.proposalData.schOption[index].model,
                                    "group" : $scope.proposalData.schOption[index].group,
                                    "item"  : $scope.proposalData.schOption[index].item,
                                    "qty"  : $scope.proposalData.schQty[index],
                                    "cpiBwAmt"  : cpiBwAmt.toFixed(1),
                                    "cpiColourAmt"  : cpiColourAmt.toFixed(1)
                                }
                                cloudphones[index].push(printer);
                                console.log(cloudphones[index]);
                            }
                    }
                }
             });
        });

        // console.log(cloudphones[0]);

        angular.forEach(cloudphones[0], function(cloudphone,index){
            if (cloudphone.length>0){
                cloudphones[0].splice(index,1);
            }
            console.log($scope.proposalData.cloudConnectStandardChannel[0]);
            if(cloudphone.item == 'CISCO IP Phone 7821' || cloudphone.item == 'CISCO IP Phone 7861' || cloudphone.item == 'CISCO IP Phone 8841 Executive' || cloudphone.item == 'CISCO IP Phone 8851 - Bluetooth' || cloudphone.item == 'CISCO IP Phone 8861 - Wifi Enabled' || cloudphone.item == 'CISCO IP Phone 8845 HD Video conf Handset' || cloudphone.item == 'CISCO Headset 521 Wired Single' || cloudphone.item == 'CISCO Headset 522 Wired Dual' || cloudphone.item == 'CISCO 561 Headset Wireless Single') {
                if(index==0) {
                    if($scope.proposalData.cloudConnectStandardChannel[0] == undefined || $scope.proposalData.cloudConnectCapChannel[0] == undefined) {
                        $scope.proposalData.cloudConnectStandardChannel[0] = cloudphone.qty[0];
                        $scope.proposalData.cloudConnectCapChannel[0] = cloudphone.qty[0];
                        var totalCloudStand = parseFloat(cloudphone.qty[0]) * 29;
                        $scope.proposalData.cloudConnectStandardChannel[0] = totalCloudStand;
    
                        var totalCloudCap = parseFloat(cloudphone.qty[0]) * 39;
                        $scope.proposalData.cloudConnectCapChannel[0] = totalCloudCap;
                    }
                }
            }
        });

    }

    $scope.quotationNumberDate = function(){
        var currentDate = new Date();   
        return ("0" + (currentDate.getMonth() + 1)).slice(-2)+""+("0" + currentDate.getDate()).slice(-2);
    }
    $scope.tradingQuotName = function(){
      var name = $scope.ngDialogData.proposalData.tradingName.substring(0,3).toUpperCase();
      return name;
    }

    $scope.saveAsDrafted = function(proposalData) {
      var userProfile =  Scopes.get('DefaultCtrl').profile;

      //for issued date 
      angular.forEach($scope.proposalData.analysis.dateIssued,function(value,key){
          $scope.proposalData.analysis.dateIssued[key] =  $("#dateIssued-"+key).val();
      });
      
      
        if (!$scope.editId) {
            $scope.editId = "";
        }

        // $(".reAssign").each(function() {
        //     var id = $(this).prop("id");
        //     proposalData[id] = $(this).val();  
        // });

        // if(angular.isUndefined($scope.quot_num)){
        //   $scope.quot_num = $scope.getQuotationNumber();
        //   console.log($scope.quot_num);
        // };
        ProposalActions.save({ data : $scope.proposalData,
                        user : userProfile.id,
                        editId : $scope.editId,
                        quot_num : $scope.quot_num,
                        type:"Draft"}, function(data) {
                          if (data.success) {
                            $scope.editId = data.editId;
                            // console.log($scope.editId);
                          }
                        }
        );
        $scope.processcloudRateCard();
    }
    
    
    $scope.telephone = function(item){
        if(item.category === 'Telephone System'){
            return item;
        }
    };
    $scope.printer = function(item){
        if(item.category === 'MULTI-FUNCTION PRINTERS'){
            return item;
        }
    };
    $scope.camera = function(item){
        if(item.category === 'Camera'){
            return item;
        }
    };

    $scope.roundVal = function(val) {
        return val.toFixed(2);
    }

    $scope.downloadPdf = function() {
        console.log($scope.htmlData);
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var formHtmlData = new Array();
        var fileNameArr = new Array();
        //$interval.cancel(saveDraft);
        // $("#loading").show();
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;

        angular.forEach($scope.htmlData, function(value,key) {
            var nametail = new Date().getTime();
            var rand = Math.floor((Math.random()*1000)+10);
            var str = $scope.proposalData.tradingName;
            str = str.replace(/\s+/g, '_');
            str = str.replace(/[\/\\#,+$~%.'":*?<>{}]/g, '_');
            var fileName = str+rand+nametail+".pdf";
            $scope.proposalData.rand_num = fileName;
            fileNameArr.push(fileName);
            $("#downloadPdfBn").prop('disabled',true);
            formHtmlData.push({html:value, fileName : fileName});
        });
        // if(angular.isUndefined($scope.quot_num)){
        //   $scope.quot_num = $scope.getQuotationNumber();
        // }
        var pdf = ProposalActions.createPdf({
            htmlData :formHtmlData,
            orientation : $scope.proposalData.orientation, 
            userDetails : { name : $scope.proposalData.contactPerson,
                            email :$scope.proposalData.emailAddress,
                            fileName : $scope.proposalData.rand_num,

                           },
            account: {name: userProfile.name, email: userProfile.email}
          },
            function(data) {
                console.log(data);
                if (data.success == "failed") {
                    $scope.loading = false;
                    FlashMessage.setMessage({
                    success:false,
                    message:"Please choose a proposal to generate pdf."}
                    );
                    alert("Please choose a proposal to generate pdf.");
                } else {
                    if($scope.editId) {
                        var editId = $scope.editId;
                        $rootScope.editId = null;
                    }
                    ProposalActions.save({ data : $scope.proposalData,
                        user : userProfile.id,
                        editId : editId,
                        fileName : fileNameArr,
                        note: "finish",
                        quot_num : $scope.quot_num,
                        type:"Completed"}, function(data) {
                        if (data.success) {
                            $scope.proposalData = {};
                            // $("#loading").hide();
                            $scope.loading = false;
                            FlashMessage.setMessage({
                            success:true,
                            message:"Proposal successully created! You will receive an email shortly"}
                            );
                            alert("Proposal successully created! You will receive an email shortly.");
                            $window.location.reload();
                        }
                    });
                }
                
        });
    }
    
    $scope.processCreditCard = function (){
      
    }

    $scope.getCurrentDateMin = function(){
        var currentDate = new Date();   
        return ("0" + currentDate.getDate()).slice(-2)+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getFullYear();
    }

    // $scope.tradingQuotName = function(){
    //   var name = $scope.ngDialogData.proposalData.tradingName.substring(0,3).toUpperCase();
    //   return name;
    // }

    $scope.signDocument = function(templateName, formData) {
        $scope.count = 0;
        console.log(templateName);
        $scope.proposalData.proposalType = templateName;
        if(templateName == 'landscape') {
            $scope.proposalData.orientation = 'A4-L';
            var size = 1150;
        } else {
            $scope.proposalData.orientation = 'A4';
            var size = 900;
        }
        angular.forEach( $scope.proposalData.type, function(value, key) {
            if (value == true) {
                $scope.count = $scope.count + 1;
            }
        });
        $scope.proposalData.footerCount = $scope.count;
        console.log($scope.proposalData.footerCount);
        console.log($scope.items);
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var datum = $scope.proposalData;
        datum.quot_num = $scope.quot_num;
        datum.tradingName = datum.tradingName;
        console.log($scope.proposalData.schOption);
        var datePrepared = new Date();
        datum.datePrepared = datePrepared.toDateString();

        var current_datetime = new Date()
        var formatted_date = current_datetime.getDate() + "/" + (current_datetime.getMonth() + 1).toString().padStart(2, "0") + "/" + current_datetime.getFullYear()
        datum.formatted_date = formatted_date;
        datum.preparedBy = userProfile.name;
        datum.preparedEmail = userProfile.email;
        datum.preparePhone = userProfile.phone;
        $scope.template = "templates/proposal/"+templateName+".html?t=" + _version;        
        $rootScope.formData = formData;
        // console.log($rootScope.formData)
        console.log(datum);
        
        //Validate cloud plan
        // var check = $rootScope.viewProposalData.types.includes("cloudConnectStandard", "cloudConnectCap");
        // console.log(check);

        angular.forEach($scope.proposalData.type, function(value, key) {
            if((key == 'cloudConnectStandard' || key == 'cloudConnectCap') && templateName != 'cloud') {
                if(value == true) {
                    alert("You have selected a Cloud plan. Please choose only the Cloud Proposal");
                }
            }
            
            // if(templateName == 'cloud') {
            //     if(check == false ) {
            //         alert("Please select a Cloud plan!");
            //     }
            // }
        });


        ngDialog.open({
            data: angular.toJson({
                proposalData: datum
            }),
            template: $scope.template,
            plan: true,
            controller : 'ProposalCtrl',
            width: size,
            height: 600,
            scope: $scope,
            className: 'ngdialog-theme-default',
            preCloseCallback : function(value) {
                var allSigned = true;

                $(".signatureImg").each(function() {
                    console.log($(this).attr('id'),$(this).children("img").length);
                    if ($(this).children("img").length == 0) {
                        allSigned = false;
                        return;
                    }
                });

                if (!allSigned) {
                   $('#isSigned').val('F');
                   if (confirm("You have not signed all the fields.Do you want to exit?")) {
                       return true;
                   }
                   return false;
                    $timeout(angular.noop());
                } else {
                    $('#isSigned').val('T');
                    // var button = $('#formData').find('a[href="#finish"]');
                    // button.show();
                    //alert($('#isSigned').val());                  // In case of forms with location fields.

                    // if(templateName == "proposal"){
                    //     $scope.htmlData.quotaion = "";
                    // } else if (templateName == "proposal"){
                    //     $scope.htmlData.proposal = "";
                    // }
                  
    
                    if ($scope.items[templateName].length > 1) {
                        $scope.htmlData[templateName] = [];
                        angular.forEach($scope.items[templateName], function(item, index) {
                            var clone = $('.ngdialog-content').clone();
                            clone.find('[class*="preview"]').each(function(divIndex,data){
                                //console.log(divIndex,data,index);  
                                if (index!=divIndex)  {
                                    clone.find('.preview-' + divIndex).remove();
                                    // console.log(clone.html());
                                    console.log($scope.items[templateName]);
                                }                        
                            });
                            //clone.find('.preview-' + index).remove();
                            var tableHtml = clone.html();
                            $scope.htmlData[templateName].push(tableHtml);
                        });
                    } else {
                        $scope.htmlData[templateName] = $(".ngdialog-content").html();

                    }

                    $scope[templateName] = true;
                    $timeout(angular.noop());
                }

            }
            });
            console.log($scope.proposalData.type);
         console.log($scope.htmlData);
    }

    // $scope.getQuotationNumber = function(){
    //     var currentDate = new Date();  
    //     // if (angular.isUndefined($scope.quot_num)){
    //       $scope.quot_num = ("0" + (currentDate.getMonth() + 1)).slice(-2)+""+("0" + currentDate.getDate()).slice(-2) + $scope.proposalData.tradingName.substring(0,3).toUpperCase() + "-" + Math.floor(Math.random() * 300); 
    //     // }

    //     var pdf = ProposalActions.checkQuotNum({
    //         generatedQuot_num :$scope.quot_num,
    //       },
    //         function(data) {
    //             if (data.success == "fail") {
    //                 $scope.quot_num = null;
    //                 $scope.getQuotationNumber();
    //             }
    //     });
    //     return $scope.quot_num;
    // }

    $scope.showProposal = function() {
      var userProfile =  Scopes.get('DefaultCtrl').profile;
      var datum = $scope.proposalData;
      datum.tradingName = datum.tradingName.toUpperCase();
      // console.log($scope.proposalData.schOption);
      var datePrepared = new Date();
      datum.datePrepared = datePrepared.toDateString();
      datum.preparedBy = userProfile.name;
      datum.preparedEmail = userProfile.email;
      datum.preparePhone = userProfile.phone;
      console.log(datum);
      $scope.template = "templates/proposal/proposal.html";
      ngDialog.open({
            data: angular.toJson({
              proposalData: datum
          }),
           template: $scope.template,
           plan: true,
           controller : 'ProposalCtrl',
           width: '22cm',
           className: 'ngdialog-theme-default',
           showClose: true,
           closeByEscape: true,
           closeByDocument: true
       });
    }

     $scope.showQuotation = function() {
      var userProfile =  Scopes.get('DefaultCtrl').profile;
      var datum = $scope.proposalData;
      datum.tradingName = datum.tradingName.toUpperCase();
      console.log($scope.proposalData.schOption);
      var datePrepared = new Date();
      datum.datePrepared = datePrepared.toDateString();
      datum.preparedBy = userProfile.name;
      datum.preparedEmail = userProfile.email;
      datum.preparePhone = userProfile.phone;
      // datum.totalMonthly 
      // datum.totalSog
      console.log(datum);
      $scope.template = "templates/proposal/quotation.php";
      ngDialog.open({
            data: angular.toJson({
              proposalData: datum
          }),
           template: $scope.template,
           plan: true,
           controller : 'ProposalCtrl',
           width: '22cm',
           // className: 'ngdialog-theme-default',
           showClose: true,
           closeByEscape: true,
           closeByDocument: true
       });
    }

    $scope.showNecsl = function() {
      var userProfile =  Scopes.get('DefaultCtrl').profile;
      var datum = $scope.proposalData;
      datum.tradingName = datum.tradingName.toUpperCase();
      console.log($scope.proposalData.schOption);
      var datePrepared = new Date();
      datum.datePrepared = datePrepared.toDateString();
      datum.preparedBy = userProfile.name;
      datum.preparedEmail = userProfile.email;
      datum.preparePhone = userProfile.phone;
      // datum.totalMonthly 
      // datum.totalSog
      console.log(datum);
      $scope.template = "templates/proposal/necsl.html";
      ngDialog.open({
            data: angular.toJson({
              proposalData: datum
          }),
           template: $scope.template,
           plan: true,
           controller : 'ProposalCtrl',
           width: '22cm',
           className: 'ngdialog-theme-default',
           showClose: true,
           closeByEscape: true,
           closeByDocument: true
       });
    }
     $scope.showNecsv = function() {
      var userProfile =  Scopes.get('DefaultCtrl').profile;
      var datum = $scope.proposalData;
      datum.tradingName = datum.tradingName.toUpperCase();
      console.log($scope.proposalData.schOption);
      var datePrepared = new Date();
      datum.datePrepared = datePrepared.toDateString();
      datum.preparedBy = userProfile.name;
      datum.preparedEmail = userProfile.email;
      datum.preparePhone = userProfile.phone;
      // datum.totalMonthly 
      // datum.totalSog
      console.log(datum);
      $scope.template = "templates/proposal/necsv.html";
      ngDialog.open({
            data: angular.toJson({
              proposalData: datum
          }),
           template: $scope.template,
           plan: true,
           controller : 'ProposalCtrl',
           width: '22cm',
           className: 'ngdialog-theme-default',
           showClose: true,
           closeByEscape: true,
           closeByDocument: true
       });
    }
     $scope.showPanasonic = function() {
      var userProfile =  Scopes.get('DefaultCtrl').profile;
      var datum = $scope.proposalData;
      datum.tradingName = datum.tradingName.toUpperCase();
      console.log($scope.proposalData.schOption);
      var datePrepared = new Date();
      datum.datePrepared = datePrepared.toDateString();
      datum.preparedBy = userProfile.name;
      datum.preparedEmail = userProfile.email;
      datum.preparePhone = userProfile.phone;
      // datum.totalMonthly 
      // datum.totalSog
      console.log(datum);
      $scope.template = "templates/proposal/panasonic.html";
      ngDialog.open({
            data: angular.toJson({
              proposalData: datum
          }),
           template: $scope.template,
           plan: true,
           controller : 'ProposalCtrl',
           width: '22cm',
           className: 'ngdialog-theme-default',
           showClose: true,
           closeByEscape: true,
           closeByDocument: true
       });
    }
    $scope.showOmniview = function() {
      var userProfile =  Scopes.get('DefaultCtrl').profile;
      var datum = $scope.proposalData;
      datum.tradingName = datum.tradingName.toUpperCase();
      console.log($scope.proposalData.schOption);
      var datePrepared = new Date();
      datum.datePrepared = datePrepared.toDateString();
      datum.preparedBy = userProfile.name;
      datum.preparedEmail = userProfile.email;
      datum.preparePhone = userProfile.phone;
      // datum.totalMonthly 
      // datum.totalSog
      console.log(datum);
      $scope.template = "templates/proposal/omniview.html";
      ngDialog.open({
            data: angular.toJson({
              proposalData: datum
          }),
           template: $scope.template,
           plan: true,
           controller : 'ProposalCtrl',
           width: '22cm',
           className: 'ngdialog-theme-default',
           showClose: true,
           closeByEscape: true,
           closeByDocument: true
       });
    }
    $scope.show = function() {
      var userProfile =  Scopes.get('DefaultCtrl').profile;
      var datum = $scope.proposalData;
      datum.tradingName = datum.tradingName.toUpperCase();
      console.log($scope.proposalData.schOption);
      var datePrepared = new Date();
      datum.datePrepared = datePrepared.toDateString();
      datum.preparedBy = userProfile.name;
      datum.preparedEmail = userProfile.email;
      datum.preparePhone = userProfile.phone;
      // datum.totalMonthly 
      // datum.totalSog
      console.log(datum);
      $scope.template = "templates/proposal/oki.html";
      ngDialog.open({
            data: angular.toJson({
              proposalData: datum
          }),
           template: $scope.template,
           plan: true,
           controller : 'ProposalCtrl',
           width: '22cm',
           className: 'ngdialog-theme-default',
           showClose: true,
           closeByEscape: true,
           closeByDocument: true
       });
    }
    $scope.sign = function(id) {
        console.log("puamsokd");
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({
                color:"#000000",lineWidth:1,
                    width :490, height :98,
                    cssclass : "signature-canvas",
                   "decor-color": 'transparent'
                });
            clicked = true;
        }
    }

    $scope.sched_goods = ScheduleGoods.query();
    $scope.true = 'true';
    
    $scope.prepDiscounts = function(fa) {
      fa = fa.replace(/^\D+|\,+/g, '');
       $scope.EqPayment = fa;
    }

    $scope.dollarSign = function(val){
        var strCurrency = '';
        if (angular.isDefined(val))
            strCurrency = '$' + val.toFixed(2);
        //alert(strCurrency);
        return strCurrency;
    }

    $scope.generatePdf = function(proposalData) {
        console.log(proposalData);
    };

    $scope.getTotal = function(templateName){
        $scope.proposalData.sum=0;
        $scope.proposalData.sumCloud=0;
        $scope.proposalData.dis=0;
        $scope.proposalData.disCloud=0;
        $scope.proposalData.totalNoUsers=0;
        $scope.proposalData.callDis=0;
        $scope.proposalData.getTotalServices=0;
        $scope.proposalData.landlineNumRow = 0;
        // NBF CAP
      if (angular.isDefined($scope.proposalData.type)){
          var domain = window.location.hostname;
           
        if($scope.proposalData.type.voiceCap != true && $scope.proposalData.type.voiceUt != true && (templateName == 'proposal' || templateName == 'quotation') && (domain == '192.168.1.7' || domain == 'nexgen_configapp.test' || domain == 'staging.nexgenconfigapp.xyz' || domain == 'nexgenconfigapp.xyz')) {
            $scope.proposalData.landlineNumRow+= 5;
            console.log('pasok');
        }
        if($scope.proposalData.type.voiceCap == true){
          $scope.proposalData.landlineNumRow++;
          angular.forEach($scope.proposalData.voiceStd,function(value,key){
              if(value>0) {
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
              }
          });
          angular.forEach($scope.proposalData.voiceStdLineHunt,function(value,key){
            if(value>0) {
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
            }
        });
          angular.forEach($scope.proposalData.voiceFaxQty,function(value,key){  
            if (parseFloat(value)>0 && $scope.proposalData.voiceFaxToEmail[key] == true){
                $scope.proposalData.sum+=parseFloat(value * 9.95);
                $scope.proposalData.landlineNumRow++;
            }
          });
          angular.forEach($scope.proposalData.dID,function(value,key){
            if(value>0) {
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
            }
          });
          angular.forEach($scope.proposalData.bFCapDiscount,function(value,key){
                 if (parseFloat(value)>0){
                  $scope.proposalData.dis+=parseFloat(value);
                  $scope.proposalData.landlineNumRow++;
                }
          });
        }
        // Focus Standard
        if($scope.proposalData.type.focusStandard == true){
            $scope.proposalData.landlineNumRow++;
          angular.forEach($scope.proposalData.voiceStdChannel,function(value,key){
            if(value>0) {
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
            }
          });
          angular.forEach($scope.proposalData.voiceFcusStdLineHunt,function(value,key){
            if(value>0) {
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
            }
        });
          angular.forEach($scope.proposalData.voiceStdDID,function(value,key){
            if(value>0) {
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
            }
          });
          angular.forEach($scope.proposalData.voiceStdFaxQty,function(value,key){
                if (parseFloat(value)>0 && $scope.proposalData.voiceStdFaxToEmail[key]==true){
                  $scope.proposalData.sum+=parseFloat(value * 9.95);
                  $scope.proposalData.landlineNumRow++;
                }
          });
          angular.forEach($scope.proposalData.bFCallDiscount,function(value,key){
            if(value>0) {
                $scope.proposalData.callDis+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
            }
          });
          angular.forEach($scope.proposalData.bFStdDiscount,function(value,key){
            if(value>0) {
                $scope.proposalData.dis+=parseFloat(value);
                $scope.proposalData.callDisAdded = 1;
            }
          });
        }
        // Complete Office
        if($scope.proposalData.type.completeOffice == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.voiceCompAnalouge,function(value,key){
                console.log(value);
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompLineHunt,function(value,key){
                console.log(value);
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompBri,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 109.85);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompPri,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value); 
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompDID,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompFaxQty,function(value,key){
                if(value>0 && $scope.proposalData.voiceCompFaxToEmail[key]==true) {
                    $scope.proposalData.sum+=parseFloat(value * 9.95);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompAnalougeDis,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompBriDis,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompPriDis,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value); 
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompCallDis,function(value,key){
                if(value>0) {
                    $scope.proposalData.callDis+=parseFloat(value);
                    $scope.proposalData.callDisAdded = 1;
                }
            });
        }
        // SOLUTION CAP
        if($scope.proposalData.type.voiceUt == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.voiceUntimedChannel,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceUntimedLineHunt,function(value,key){
                    if(value>0) {
                        $scope.proposalData.sum+=parseFloat(value);
                        $scope.proposalData.landlineNumRow++;
                    }
                });
            angular.forEach($scope.proposalData.voiceUntimedFaxQty,function(value,key){
                if(value>0 && $scope.proposalData.voiceUntimedFaxToEmail[key]==true) {
                    $scope.proposalData.sum+=parseFloat(value * 9.95);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceUntimedDID,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.bFUntimedDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }
        // Solution Standard 
        if($scope.proposalData.type.voiceSolution == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.voiceSolutionChannel,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceSolutionLineHunt,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceSolutionDID,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceSolutionFaxQty,function(value,key){
                if(value>0 && $scope.proposalData.voiceSolutionFaxToEmail[key]==true) {
                    $scope.proposalData.sum+=parseFloat(value * 9.95);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.bFSolutionDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.bFSolutionCallDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.callDis+=parseFloat(value);
                    $scope.proposalData.callDisAdded = 1;
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }
        // Essential Cap
        if($scope.proposalData.type.voiceEssential == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.voiceEssentialChannel,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceEssentialLineHunt,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
                });
            angular.forEach($scope.proposalData.voiceEssentialDID,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.bECapDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.ipMidbandPlansVoice,function(value,key){
                if(value.price>0) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.ipMidbandDownloadVoice,function(value,key){
                if(value.price>0) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.ipMidbandDis,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }

        //Cloud Connect Standard
        if($scope.proposalData.type.cloudConnectStandard == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.cloudConnectStandardChannel,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value);
                }
            });
        
            angular.forEach($scope.proposalData.cloudConnectStandardDID,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value);

                }
            });
            angular.forEach($scope.proposalData.cloudConnectStandardFaxQty,function(value,key){
                if(value>0 && $scope.proposalData.cloudConnectStandardFaxToEmail[key]==true) {
                    $scope.proposalData.sum+=parseFloat(value * 9.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 9.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectStandardAnalouge,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(43.95 * value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(43.95 * value);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectStandarduConeApp,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 14.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 14.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectStandardAddCallQueue,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 29.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 29.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectStandardRepClientApp,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 29.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 29.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectStandardAddAutoAttndnt,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 29.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 29.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectStandardAddVoicemail,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 9.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 9.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectStandardCallRecording,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 29.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 29.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectStandardCallRecordingPremium,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 49.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 49.95);
                }
            });
            angular.forEach($scope.proposalData.cCStandardAnalogueDis,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.disCloud+=parseFloat(value);
                }
            });
            angular.forEach($scope.proposalData.cCStandardCallRecordingDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.disCloud+=parseFloat(value);
                }
            });
            angular.forEach($scope.proposalData.cCStandardCallRecordingPremiumDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.disCloud+=parseFloat(value);
                }
            });
            angular.forEach($scope.proposalData.cCStandardDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.disCloud+=parseFloat(value);
                }
            });
            angular.forEach($scope.proposalData.cCStandardCallDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.callDis+=parseFloat(value);
                    $scope.proposalData.callDisAdded = 1;
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }

         //Cloud Connect Cap
         if($scope.proposalData.type.cloudConnectCap == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.cloudConnectCapChannel,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value);
                }
            });
    
            angular.forEach($scope.proposalData.cloudConnectCapDID,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectCapFaxQty,function(value,key){
                if(value>0 && $scope.proposalData.cloudConnectCapFaxToEmail[key]==true) {
                    $scope.proposalData.sum+=parseFloat(value * 9.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 9.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectCapAnalouge,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(43.95 * value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(43.95 * value);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectCapuConeApp,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 14.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 14.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectCapAddCallQueue,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 29.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 29.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectCapRepClientApp,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 29.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 29.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectCapAddAutoAttndnt,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 29.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 29.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectCapAddVoicemail,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 9.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 9.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectCapCallRecording,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 29.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 29.95);
                }
            });
            angular.forEach($scope.proposalData.cloudConnectCapCallRecordingPremium,function(value,key){
                if(value>0) {
                    $scope.proposalData.sum+=parseFloat(value * 49.95);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.sumCloud+=parseFloat(value * 49.95);
                }
            });
            angular.forEach($scope.proposalData.cCCapAnalogueDis,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.disCloud+=parseFloat(value);
                }
            });
            angular.forEach($scope.proposalData.cCCapCallRecordingDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.disCloud+=parseFloat(value);
                }
            });
            angular.forEach($scope.proposalData.cCCapCallRecordingPremiumDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.disCloud+=parseFloat(value);
                }
            });
            angular.forEach($scope.proposalData.cCCapDiscount,function(value,key){
                if(value>0) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.disCloud+=parseFloat(value);
                }
            });
     
        }

        // ADSL
        if($scope.proposalData.type.adsl2 == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.adsl2Plans,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;

                }
            });
            angular.forEach($scope.proposalData.adsl2Dis,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompAnalougeDSL,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value * 43.95);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompAnalougeDSLDis,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompAnalougeDisDSL,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }
        // NBN UNLIMITED
        if($scope.proposalData.type.nbnUnlimitedPlans == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.UnlimitedPlans,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompAnalougeNBNUnli,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value * 39.95);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.nbnUnlimitedDis,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.voiceCompAnalougeDisNBNUnli,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
        } 
        // Midband Monthly
        if($scope.proposalData.type.ipMidband == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.ipMidbandPlans,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.ipMidbandDownload,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.ipMidbandDis,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }
        // Midband Unlimited
        if($scope.proposalData.type.ipMidbandUnli == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.ipMidbandUnliPlans,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.ipMidbandUnliDis,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }
        // Ehternet Unlimited(EFM)
        if($scope.proposalData.type.ethernet == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.ethernetPlans,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.ethernetDis,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }
        // Fibre
        if($scope.proposalData.type.fibre == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.fibreUtPlans,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.fibreDis,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }
        // Telstra 4G Super
        if($scope.proposalData.type.telstra4gSuper == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.telstra4gSuper,function(value,key){
              if(value != '') {
                  if( value.price == 49){
                    value.price = 49;
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.telstra4gSuperQty[0] = $scope.proposalData.telstra4gSuper.reduce(function (n, product) {
                        return n + (product.plan == 'Telstra 4G Super Standard');
                    }, 0);
                    $scope.proposalData.telstra4gSuperPrice[0] =  $scope.proposalData.telstra4gSuperQty[0] * 49;
                   
                  }
                  if( value.price == 69){
                    value.price = 69;
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.telstra4gSuperQty[1] = $scope.proposalData.telstra4gSuper.reduce(function (n, product) {
                        return n + (product.plan == 'Telstra 4G Super');
                    }, 0);
                    $scope.proposalData.telstra4gSuperPrice[1] =  $scope.proposalData.telstra4gSuperQty[1] * 69;
                  }
                  if( value.price == 85){
                    value.price = 85;
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.telstra4gSuperQty[2] = $scope.proposalData.telstra4gSuper.reduce(function (n, product) {
                        return n + (product.plan == 'Telstra 4G Super Max');
                    }, 0);
                    $scope.proposalData.telstra4gSuperPrice[2] =  $scope.proposalData.telstra4gSuperQty[2] * 85;
                  }
                  if( value.price == 99){
                    value.price = 99;
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.telstra4gSuperQty[3] = $scope.proposalData.telstra4gSuper.reduce(function (n, product) {
                        return n + (product.plan == 'Telstra 4G Super Extreme');
                    }, 0);
                    $scope.proposalData.telstra4gSuperPrice[3] =  $scope.proposalData.telstra4gSuperQty[3] * 99;
                  }
              }
            });
            angular.forEach($scope.proposalData.telstra4gSuperStandardBoltOn,function(value,key){
                if(value != null) {
                    if(value.plan_group == 'Telstra 4G Super Standard') {
                        $scope.proposalData.telstra4gSuperBoltOn1GBQty[0] = $scope.proposalData.telstra4gSuperStandardBoltOn.reduce(function (n, product) {
                            return n + (product != null && product.memory == '1GB');
                        }, 0);
                        $scope.proposalData.telstra4gSuperBoltOn5GBQty[0] = $scope.proposalData.telstra4gSuperStandardBoltOn.reduce(function (n, product) {
                            return n + (product != null && product.memory == '5GB');
                        }, 0);

                        if( value.price == 9.89){
                            value.price = 9.89;
                            $scope.proposalData.sum+=parseFloat( value.price);
                            $scope.proposalData.telstra4gSuperBoltOn1GBPrice[0] = $scope.proposalData.telstra4gSuperBoltOn1GBQty[0] * 9.89;
                        }
                        if( value.price == 31.99){
                            value.price = 31.99;
                            $scope.proposalData.sum+=parseFloat( value.price);
                            $scope.proposalData.telstra4gSuperBoltOn5GBPrice[0] = $scope.proposalData.telstra4gSuperBoltOn5GBQty[0] * 31.99;
                        }
                        $scope.proposalData.boltOn4gSuper[key] = value.price;
                    }
                }
            });
            angular.forEach($scope.proposalData.telstra4gSuperBoltOn,function(value,key){
                if(value != null) {
                    if(value.memory != null && value.plan_group == 'Telstra 4G Super') {
                    console.log(value.memory);
                        $scope.proposalData.telstra4gSuperBoltOn1GBQty[1] = $scope.proposalData.telstra4gSuperBoltOn.reduce(function (n, product) {
                            return n + (product != null && product.memory == '1GB');
                        }, 0);
                        $scope.proposalData.telstra4gSuperBoltOn5GBQty[1] = $scope.proposalData.telstra4gSuperBoltOn.reduce(function (n, product) {
                            return n + (product != null && product.memory == '5GB');
                        }, 0);

                        if( value.price == 9.89){
                            value.price = 9.89;
                            $scope.proposalData.sum+=parseFloat( value.price);
                            $scope.proposalData.telstra4gSuperBoltOn1GBPrice[1] = $scope.proposalData.telstra4gSuperBoltOn1GBQty[1] * 9.89;
                        }
                        if( value.price == 31.99){
                            value.price = 31.99;
                            $scope.proposalData.sum+=parseFloat( value.price);
                            $scope.proposalData.telstra4gSuperBoltOn5GBPrice[1] = $scope.proposalData.telstra4gSuperBoltOn5GBQty[1] * 31.99;
                        }
                        $scope.proposalData.boltOn4gSuper[key] = value.price;
                    }
                }
            });
            angular.forEach($scope.proposalData.telstra4gSuperMaxBoltOn,function(value,key){
                if(value != null) {
                    if(value.plan_group == 'Telstra 4G Super Max') {
                        $scope.proposalData.telstra4gSuperBoltOn1GBQty[2] = $scope.proposalData.telstra4gSuperMaxBoltOn.reduce(function (n, product) {
                            return n + (product != null && product.memory == '1GB');
                        }, 0);
                        $scope.proposalData.telstra4gSuperBoltOn5GBQty[2] = $scope.proposalData.telstra4gSuperMaxBoltOn.reduce(function (n, product) {
                            return n + (product != null && product.memory == '5GB');
                        }, 0);

                        if( value.price == 9.89){
                            value.price = 9.89;
                            $scope.proposalData.sum+=parseFloat( value.price);
                            $scope.proposalData.telstra4gSuperBoltOn1GBPrice[2] = $scope.proposalData.telstra4gSuperBoltOn1GBQty[2] * 9.89;
                        }
                        if( value.price == 31.99){
                            value.price = 31.99;
                            $scope.proposalData.sum+=parseFloat( value.price);
                            $scope.proposalData.telstra4gSuperBoltOn5GBPrice[2] = $scope.proposalData.telstra4gSuperBoltOn5GBQty[2] * 31.99;
                        }
                        $scope.proposalData.boltOn4gSuper[key] = value.price;
                    }
                }
            });
            angular.forEach($scope.proposalData.telstra4gSuperExtremeBoltOn,function(value,key){
                if(value != null) {
                    if(value.plan_group == 'Telstra 4G Super Extreme') {
                        $scope.proposalData.telstra4gSuperBoltOn1GBQty[3] = $scope.proposalData.telstra4gSuperExtremeBoltOn.reduce(function (n, product) {
                            return n + (product != null && product.memory == '1GB');
                        }, 0);
                        $scope.proposalData.telstra4gSuperBoltOn5GBQty[3] = $scope.proposalData.telstra4gSuperExtremeBoltOn.reduce(function (n, product) {
                            return n + (product != null && product.memory == '5GB');
                        }, 0);

                        if( value.price == 9.89){
                            value.price = 9.89;
                            $scope.proposalData.sum+=parseFloat( value.price);
                            $scope.proposalData.telstra4gSuperBoltOn1GBPrice[3] = $scope.proposalData.telstra4gSuperBoltOn1GBQty[3] * 9.89;
                        }
                        if( value.price == 31.99){
                            value.price = 31.99;
                            $scope.proposalData.sum+=parseFloat( value.price);
                            $scope.proposalData.telstra4gSuperBoltOn5GBPrice[3] = $scope.proposalData.telstra4gSuperBoltOn5GBQty[3] * 31.99;
                        }
                        $scope.proposalData.boltOn4gSuper[key] = value.price;
                    }
                }
            });
            angular.forEach($scope.proposalData.telstra4gSuperStandardDis,function(value,key){
                if(value != null) {
                    $scope.proposalData.dis+=parseFloat(value);
                }
                $scope.proposalData.telstra4gSuperDisQty[0] = $scope.proposalData.telstra4gSuperStandardDis.reduce(function (n, product) {
                    return n + (product != null && product != 0);
                }, 0);
                $scope.proposalData.telstra4gSuperDisPrice[0] = $scope.proposalData.telstra4gSuperStandardDis.reduce((discount, a) => discount + a,0); 
            
            });
            angular.forEach($scope.proposalData.telstra4gSuperDis,function(value,key){
                if(value != null) {
                    $scope.proposalData.dis+=parseFloat(value);
                }
                $scope.proposalData.telstra4gSuperDisQty[1] = $scope.proposalData.telstra4gSuperDis.reduce(function (n, product) {
                    return n + (product != null && product != 0);
                }, 0);

                $scope.proposalData.telstra4gSuperDisPrice[1] = $scope.proposalData.telstra4gSuperDis.reduce((discount, a) => discount + a,0); 
            });
            angular.forEach($scope.proposalData.telstra4gSuperMaxDis,function(value,key){
                if(value != null) {
                    $scope.proposalData.dis+=parseFloat(value);
                }
                $scope.proposalData.telstra4gSuperDisQty[2] = $scope.proposalData.telstra4gSuperMaxDis.reduce(function (n, product) {
                    return n + (product != null && product != 0);
                }, 0);

                $scope.proposalData.telstra4gSuperDisPrice[2] = $scope.proposalData.telstra4gSuperMaxDis.reduce((discount, a) => discount + a,0); 
            });
            angular.forEach($scope.proposalData.telstra4gSuperExtremeDis,function(value,key){
                if(value != null) {
                    $scope.proposalData.dis+=parseFloat(value);
                }
                $scope.proposalData.telstra4gSuperDisQty[3] = $scope.proposalData.telstra4gSuperExtremeDis.reduce(function (n, product) {
                    return n + (product != null && product != 0);
                }, 0);
                $scope.proposalData.telstra4gSuperDisPrice[3] = $scope.proposalData.telstra4gSuperExtremeDis.reduce((discount, a) => discount + a,0); 

            });

            var triger = true;
            if(triger) {
                if($scope.proposalData.telstra4gSuperQty[0] != null) {
                    $scope.proposalData.landlineNumRow+=parseInt(2);
                    if($scope.proposalData.telstra4gSuperBoltOn1GBPrice[0] > 0 || $scope.proposalData.telstra4gSuperBoltOn5GBPrice[0] > 0) {
                        $scope.proposalData.landlineNumRow++;
                    }
                    if($scope.proposalData.telstra4gSuperDisQty[0]> 0) {
                        $scope.proposalData.landlineNumRow++;
                    }
                }
                if($scope.proposalData.telstra4gSuperQty[1] != null) {
                    $scope.proposalData.landlineNumRow+=parseInt(2);      
                    if($scope.proposalData.telstra4gSuperBoltOn1GBPrice[1] > 0 || $scope.proposalData.telstra4gSuperBoltOn5GBPrice[1] > 0) {
                        $scope.proposalData.landlineNumRow++;
                    }
                    if($scope.proposalData.telstra4gSuperDisQty[1]> 0) {
                        $scope.proposalData.landlineNumRow++;
                    }   
                }
                if($scope.proposalData.telstra4gSuperQty[2] != null) {
                    $scope.proposalData.landlineNumRow+=parseInt(2);    
                    if($scope.proposalData.telstra4gSuperBoltOn1GBPrice[2] > 0 || $scope.proposalData.telstra4gSuperBoltOn5GBPrice[2] > 0) {
                        $scope.proposalData.landlineNumRow++;
                    }
                    if($scope.proposalData.telstra4gSuperDisQty[2]> 0) {
                        $scope.proposalData.landlineNumRow++;
                    }         
                }
                if($scope.proposalData.telstra4gSuperQty[3] != null) {
                    $scope.proposalData.landlineNumRow+=parseInt(2);    
                    if($scope.proposalData.telstra4gSuperBoltOn1GBPrice[3] > 0 || $scope.proposalData.telstra4gSuperBoltOn5GBPrice[3] > 0) {
                        $scope.proposalData.landlineNumRow++;
                    }
                    if($scope.proposalData.telstra4gSuperDisQty[3]> 0) {
                        $scope.proposalData.landlineNumRow++;
                    }        
                }
                triger = false;
            }
        }
     
        // Telstra 4G
        if($scope.proposalData.type.telstraUntimed == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.telstra,function(value,key){
                if(value) {
                    if(value == 49){
                    value = 49;
                    $scope.proposalData.sum+=parseFloat(value);
                        $scope.proposalData.landlineNumRow++;
                    }
                    if(value == 69){
                    value = 69;
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    }
                    if(value == 79){
                    value = 79;
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    }
                    if(value == 89){
                    value = 89;
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    }
                }
          });
          angular.forEach($scope.proposalData.boltOn,function(value,key){
            if(value) {
                if(value == 9.89){
                  value = 9.89;
                  $scope.proposalData.sum+=parseFloat(value);
                  $scope.proposalData.landlineNumRow++;
                }
                if(value == 31.99){
                  value = 31.99;
                  $scope.proposalData.sum+=parseFloat(value);
                  $scope.proposalData.landlineNumRow++;
                }
            }
          });
          $scope.proposalData.dis+=parseFloat($scope.proposalData.telstraUntimedDis);
        }
        // Telstra Wireless Super
        if($scope.proposalData.type.telstraWirelessSuper == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.telstraWirelessSuperPlans,function(value,key){
              if(value) {
                  $scope.proposalData.sum+=parseFloat(value.price);
                  $scope.proposalData.landlineNumRow++;
              }
            });
            angular.forEach($scope.proposalData.telstraWirelessSuperDis,function(value,key){
              if(value) {
                  $scope.proposalData.dis+=parseFloat(value);
                  $scope.proposalData.landlineNumRow++;
              }
            });
          }
        // Telstra Wireless
        if($scope.proposalData.type.telstraWireless == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.telstraWirelessPlans,function(value,key){
                if(value) {
                    $scope.proposalData.sum+=parseFloat(value.price);
                    $scope.proposalData.landlineNumRow++;
                }
            });
            angular.forEach($scope.proposalData.telstraWirelessDis,function(value,key){
                if(value) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                }
            });
        }
        // Mega Cap
        if($scope.proposalData.type.mobileCap == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.megaRate,function(value,key){
                if(value) {
                    if(value == 39){
                    value = 39;
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    }
                    if(value == 49){
                    value = 49;
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    }
                    if(value == 59){
                    value = 59;
                    $scope.proposalData.sum+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    }
                }
            });
            $scope.proposalData.dis+=parseFloat($scope.proposalData.mobileCapDis);
        } 
        // Mega Untimed
        if($scope.proposalData.type.mobileUt == true){
            $scope.proposalData.landlineNumRow++;
            angular.forEach($scope.proposalData.untimed,function(value,key){
                if(value != null) {
                    if(value.price == 49){
                        value = 49;
                        $scope.proposalData.sum+=parseFloat(value);
                        $scope.proposalData.landlineNumRow++;
                    }
                    if(value.price == 59){
                        value = 59;
                        $scope.proposalData.sum+=parseFloat(value);
                        $scope.proposalData.landlineNumRow++;
                    }
                    if(value.price == 79){
                        value = 79;
                        $scope.proposalData.sum+=parseFloat(value);
                        $scope.proposalData.landlineNumRow++;
                    }
                    if(value.price == 89){
                        value = 89;
                        $scope.proposalData.sum+=parseFloat(value);
                        $scope.proposalData.landlineNumRow++;
                    }
                }
            });
            $scope.proposalData.mobile4GUntimedDisPrice = 0;
            angular.forEach($scope.proposalData.mobileUtDis,function(value,key){
                console.log(value);
                if(value != null) {
                    $scope.proposalData.dis+=parseFloat(value);
                    $scope.proposalData.landlineNumRow++;
                    $scope.proposalData.mobile4GUntimedDisPrice+=parseFloat(value);
                }
            });
        }  

        // 1300/1800 Plan Rate
        if($scope.proposalData.type.rate131300 == true){
            // 1800 Rate
            angular.forEach($scope.proposalData.rate131300.qt1800,function(value,key){
                $scope.proposalData.rate1800MonthlyPlanFee = 0;
                if (parseFloat(value)>0){
                    $scope.proposalData.rate1800MonthlyPlanFee += (19.99 * value); 
                    $scope.proposalData.sum+=parseFloat($scope.proposalData.rate1800MonthlyPlanFee);
                    $scope.proposalData.landlineNumRow+=parseInt(2);
                }
            });
            // 1300 Rate
            angular.forEach($scope.proposalData.rate131300.qt1300,function(value,key){
                $scope.proposalData.rate1300MonthlyPlanFee = 0;
                if (parseFloat(value)>0){
                    $scope.proposalData.rate1300MonthlyPlanFee += (19.99 * value); 
                    $scope.proposalData.sum+=parseFloat($scope.proposalData.rate1300MonthlyPlanFee);
                    $scope.proposalData.landlineNumRow+=parseInt(2);

                }
            });
             // 13 Rate
             angular.forEach($scope.proposalData.rate131300.qt13,function(value,key){
                $scope.proposalData.rate13MonthlyPlanFee = 0;
                if (parseFloat(value)>0){
                    $scope.proposalData.rate13MonthlyPlanFee += (19.99 * value); 
                    $scope.proposalData.sum+=parseFloat($scope.proposalData.rate13MonthlyPlanFee);
                    $scope.proposalData.landlineNumRow+=parseInt(2);
                }
            });
        } 
        // 1300/1800 Discount Plan Rate
        if($scope.proposalData.type.rateDis131300 == true){
            // 1800 Rate
            angular.forEach($scope.proposalData.rate131300Dis.qt1800,function(value,key){
                $scope.proposalData.rate1800DisMonthlyPlanFee = 0;
                if (parseFloat(value)>0){
                    $scope.proposalData.rate1800DisMonthlyPlanFee += (19.99 * value); 
                    $scope.proposalData.sum+=parseFloat($scope.proposalData.rate1800DisMonthlyPlanFee);
                    $scope.proposalData.landlineNumRow+=parseInt(2);
                }
            });
            // 1300 Rate
            angular.forEach($scope.proposalData.rate131300Dis.qt1300,function(value,key){
                $scope.proposalData.rate1300DisMonthlyPlanFee = 0;
                if (parseFloat(value)>0){
                    $scope.proposalData.rate1300DisMonthlyPlanFee += (19.99 * value); 
                    $scope.proposalData.sum+=parseFloat($scope.proposalData.rate1300DisMonthlyPlanFee);
                    $scope.proposalData.landlineNumRow+=parseInt(2);
                }
            });
             // Discount
             angular.forEach($scope.proposalData.rateDis131300.discount,function(value,key){
                if (value>0){
                    $scope.proposalData.landlineNumRow++;
                }
            });
        } 
    
      }

        //    for call discount to add 1 row 
        if($scope.proposalData.callDisAdded == 1) {
            $scope.proposalData.landlineNumRow++;
        }
        $scope.proposalData.dis = $scope.proposalData.dis.toFixed(2);
        $scope.proposalData.callDis = $scope.proposalData.callDis.toFixed(2);

        $scope.proposalData.totalDis = parseFloat($scope.proposalData.dis) + parseFloat($scope.proposalData.callDis);

        $scope.proposalData.subSum = $scope.proposalData.sum - $scope.proposalData.dis;

        $scope.proposalData.subSum = $scope.proposalData.subSum.toFixed(2);

        console.log( $scope.proposalData.billingType);
        if ($scope.proposalData.billingType == 'outright') {
            $scope.proposalData.totalSum = parseFloat($scope.proposalData.subSum);
        } else {
            $scope.proposalData.totalSum = parseFloat($scope.proposalData.subSum) + parseFloat($scope.proposalData.leaser);
        }
        $scope.proposalData.totalSum = $scope.proposalData.totalSum.toFixed(2);
        // Get total row of free text equipment
        var totalAddEquiptRow = $scope.proposalData.schAddItems[0].reduce(function (n, product) {
            return n + (product.status > 0);
        }, 0);
        //   For terms and condition to make it responsive
        var totalEquiptRow = $scope.proposalData.schItems[0].reduce(function (n, product) {
            return n + (product.status > 0);
        }, 0);
        
        $scope.proposalData.totalNumEquiptRow = parseInt(totalEquiptRow) + parseInt(totalAddEquiptRow);
        $scope.proposalData.getTotalServices = parseInt($scope.proposalData.landlineNumRow) + parseInt(totalEquiptRow) + parseInt(totalAddEquiptRow);

        // Hero Price for Cloud Proposal
        angular.forEach( $scope.proposalData.type, function(value, key) {
            console.log(key);
           if(key == 'cloudConnectStandard') {
                if(value == true) {
                    angular.forEach($scope.proposalData.cloudConnectStandardChannel,function(value,key){
                        if(value>0) {
                            var Users = parseInt(value) / 29;
                        }
                        $scope.proposalData.totalNoUsers+=parseFloat(Users);
                    });
                }
           }
           if(key == 'cloudConnectCap') {
                if(value == true) {
                    angular.forEach($scope.proposalData.cloudConnectCapChannel,function(value,key){
                        if(value>0) {
                            var Users = parseInt(value) / 39;
                        }
                        $scope.proposalData.totalNoUsers+=parseFloat(Users);
                
                    });
                }
            }
        });
        if( $scope.proposalData.billingType=='outright') {
            $scope.proposalData.totalCloudSum = parseFloat($scope.proposalData.sumCloud) - parseFloat($scope.proposalData.disCloud);
            var totalPrice = parseInt($scope.proposalData.totalCloudSum)  /  $scope.proposalData.totalNoUsers;
        } else {
            $scope.proposalData.totalCloudSum = parseFloat($scope.proposalData.leaser) + parseFloat($scope.proposalData.sumCloud) - parseFloat($scope.proposalData.disCloud);
            var totalPrice = parseInt($scope.proposalData.totalCloudSum)  /  $scope.proposalData.totalNoUsers;
        }
        $scope.proposalData.heroPrice  = Math.round(totalPrice);
    };






    $scope.addRentalAddGoodsOption =  function(index) {

    //   console.log(index);
    //     $scope.schItemCount =+parseInt($scope.proposalData.schAddItems[0].length);
    //     console.log($scope.schItemCount);
    //     $scope.proposalData.schAddItems[index].push({count:$scope.schItemCount+1, status:1});
    //     $scope.proposalData.schAddOptions.push({count:$scope.schItemCount+1});
    //     console.log($scope.proposalData.schItems.length);
console.log(index);

        $scope.schItemCount++;
        $scope.proposalData.schAddItems[index].push({count:$scope.proposalData.schAddItems[index].length, status:1});
        $scope.proposalData.schAddOptions[index].push({count:$scope.proposalData.schAddOptions[index].length});
    };

    $scope.removeRentalGoodAddOption = function(index, count) {
        $scope.proposalData.schAddQty[index][count] = 0;
        $scope.proposalData.schAddItems[index][count].status = 0;
        // $scope.proposalData.schOption[count] = "";
        // if(!angular.isUndefined($scope.proposalData.schOption[count].price)) {
        //     $scope.proposalData.schOption[count].price = 0;
        // }
    };


    $scope.issuedDate = function(count) {
        console.log(count);
    }
    $scope.addProviderOption =  function(index) {
        console.log(index);
        $scope.schItemCount++;
        $scope.proposalData.analysis.schItemsProvider[0].push({count:$scope.proposalData.analysis.schItemsProvider[index].length, status:1});
        $scope.proposalData.analysis.provider.push();
        $scope.proposalData.analysis.account.push();
        $scope.proposalData.analysis.dateIssued.push(today);
    };
    $scope.addMrcOption =  function(index, value) {
        console.log(value);
        $scope.schItemCount++;
        // $scope.proposalData.analysis.schItemsMrc[0].push({count:$scope.proposalData.analysis.schItemsMrc[index].length, status:1});

        if(value == 'landlines') {
            $scope.proposalData.analysis.schItemsMrcLandlines[0].push({count:$scope.proposalData.analysis.schItemsMrcLandlines[index].length, status:1});
            $scope.proposalData.analysis.landlinesQty.push(1);
            $scope.proposalData.analysis.landlinesType.push('N/A');
            $scope.proposalData.analysis.landlinesCost.push(0);
        }
        if(value == 'internet') {
            $scope.proposalData.analysis.schItemsMrcInternet[0].push({count:$scope.proposalData.analysis.schItemsMrcInternet[index].length, status:1});
            $scope.proposalData.analysis.internetQty.push(1);
            $scope.proposalData.analysis.internetType.push('N/A');
            $scope.proposalData.analysis.internetCost.push(0);
        }
        if(value == 'mobile') {
            $scope.proposalData.analysis.schItemsMrcMobile[0].push({count:$scope.proposalData.analysis.schItemsMrcMobile[index].length, status:1});
            $scope.proposalData.analysis.mobileQty.push(1);
            $scope.proposalData.analysis.mobileType.push('N/A');
            $scope.proposalData.analysis.mobileCost.push(0);
        }
        if(value == 'inbound') {
            $scope.proposalData.analysis.schItemsMrcInbound[0].push({count:$scope.proposalData.analysis.schItemsMrcInbound[index].length, status:1});
            $scope.proposalData.analysis.inboundQty.push(1);
            $scope.proposalData.analysis.inboundType.push('N/A');
            $scope.proposalData.analysis.inboundCost.push(0);
        }
        $scope.proposalData.analysis.NumRow++;
        console.log($scope.proposalData.analysis);

    };
    $scope.removeAnalysisOption = function(index, count, row) {
        console.log(index);
        console.log(count);
        console.log(row);
        if(row == "provider") {
            $scope.proposalData.analysis.schItemsProvider[0][count].status = 0;
        } if(row == "landlines") {
            $scope.proposalData.analysis.schItemsMrcLandlines[0][count].status = 0;
        }
        if(row == "internet") {
            $scope.proposalData.analysis.schItemsMrcInternet[0][count].status = 0;
        }
        if(row == "mobile") {
            $scope.proposalData.analysis.schItemsMrcMobile[0][count].status = 0;
        }
        if(row == "inbound") {
            $scope.proposalData.analysis.schItemsMrcInbound[0][count].status = 0;
        }
        if(row == "local") {
            $scope.proposalData.analysis.schItemsUsageLocal[0][count].status = 0;
        }
        if(row == "national") {
            $scope.proposalData.analysis.schItemsUsageNational[0][count].status = 0;
        }
        if(row == "callstomobile") {
            $scope.proposalData.analysis.schItemsUsageCallstoMobile[0][count].status = 0;
        }
        if(row == "international") {
            $scope.proposalData.analysis.schItemsUsageInternational[0][count].status = 0;
        }
        if(row == "callsto13") {
            $scope.proposalData.analysis.schItemsUsageCallsto13[0][count].status = 0;
        }
        if(row == "roaming") {
            $scope.proposalData.analysis.schItemsUsageRoaming[0][count].status = 0;
        }
         if (row == "misc") {
            $scope.proposalData.analysis.schItemsOtherMrcMisc[0][count].status = 0;
        }
        if (row == "rental") {
            $scope.proposalData.analysis.schItemsOtherMrcRental[0][count].status = 0;
        }

        $scope.proposalData.analysis.NumRow--;
        console.log($scope.proposalData.analysis);

    };

    $scope.addUsageOption =  function(index, value) {
        console.log(value);
        $scope.schItemCount++;
        if(value == 'local') {
            $scope.proposalData.analysis.schItemsUsageLocal[0].push({count:$scope.proposalData.analysis.schItemsUsageLocal[index].length, status:1});
            $scope.proposalData.analysis.localCost.push(0);
        }
        if(value == 'national') {
            $scope.proposalData.analysis.schItemsUsageNational[0].push({count:$scope.proposalData.analysis.schItemsUsageNational[index].length, status:1});
            $scope.proposalData.analysis.nationalCost.push(0);
        }
        if(value == 'callstomobile') {
            $scope.proposalData.analysis.schItemsUsageCallstoMobile[0].push({count:$scope.proposalData.analysis.schItemsUsageCallstoMobile[index].length, status:1});
            $scope.proposalData.analysis.callsToMobileCost.push(0);
        }
        if(value == 'international') {
            $scope.proposalData.analysis.schItemsUsageInternational[0].push({count:$scope.proposalData.analysis.schItemsUsageInternational[index].length, status:1});
            $scope.proposalData.analysis.internationalCost.push(0);
        }
        if(value == 'callsto13') {
            $scope.proposalData.analysis.schItemsUsageCallsto13[0].push({count:$scope.proposalData.analysis.schItemsUsageCallsto13[index].length, status:1});
            $scope.proposalData.analysis.callsTo13Cost.push(0);
        }
        if(value == 'roaming') {
            $scope.proposalData.analysis.schItemsUsageRoaming[0].push({count:$scope.proposalData.analysis.schItemsUsageRoaming[index].length, status:1});
            $scope.proposalData.analysis.roamingCost.push(0);
        }
        $scope.proposalData.analysis.NumRow++;

    };
    $scope.addOtherMrcOption =  function(index, value) {
        console.log(index);
        $scope.schItemCount++;
        if(value == 'misc') {
            $scope.proposalData.analysis.schItemsOtherMrcMisc[0].push({count:$scope.proposalData.analysis.schItemsOtherMrcMisc[index].length, status:1});
            $scope.proposalData.analysis.miscType.push();
            $scope.proposalData.analysis.miscCost.push(0);
        }
        if(value == 'rental') {
            $scope.proposalData.analysis.schItemsOtherMrcRental[0].push({count:$scope.proposalData.analysis.schItemsOtherMrcRental[index].length, status:1});
            $scope.proposalData.analysis.rentalType.push('N/A');
            $scope.proposalData.analysis.rentalCost.push(0);
        }
        $scope.proposalData.analysis.NumRow++;
    };
    $scope.addRentalGoodOption =  function(index) {
        $scope.schItemCount++;
        $scope.proposalData.schItems[index].push({count:$scope.proposalData.schItems[index].length, status:1});
        $scope.proposalData.schOptions.push({count:$scope.proposalData.schItems[index].length});
    };

    $scope.removeRentalGoodOption = function(index, count) {
        $scope.proposalData.schQty[index][count] = 0;
        $scope.proposalData.schItems[index][count].status = 0;
        $scope.proposalData.schOption[count] = "";
        if(!angular.isUndefined($scope.proposalData.schOption[count].price)) {
            $scope.proposalData.schOption[count].price = 0;
        }
        $("#product-list-"+count).remove();
    };

    $scope.transformTelFields  = function() {
        $('.tel').on("keyup", function()
        {
                var val = this.value.replace(/[^0-9]/ig, '');
                if (val.length >10) {
                    val = val.substr(0,10);
                }
                var newVal = '';
                if (val.length >2) {
                  newVal +='('+val.substr(0,2)+') ';
                  val = val.substr(2);
                }
                while (val.length > 4)   {
                  newVal += val.substr(0, 4) +'-';
                  val = val.substr(4);
                }
                newVal += val;
                this.value = newVal;
        });
        $('.telRange').on("keyup",function(){
            var val = this.value.replace(/[^0-9]/ig, '');
            if (val.length>4) {
                val = val.substr(0,4);
            }
            this.value = val;            
        });
    }

    $scope.transformTelFields();
    
    $scope.changeColTransparent = function(val) {
        
        console.log(val);
        if ((val.item == '_') || (val.plan=='_')) {
            $('.col-sel-desc .chosen-container').addClass('transparent');
        } else {
            $('.col-sel-desc .chosen-container').removeClass('transparent');
        }

        $scope.telstraPlans = [];
        $scope.mobileMegaPlans = [];
        $scope.mobile4GUntimedPlans = [];
        //console.log($scope.configMobPort);
        if (angular.isDefined(val.plan)){
            angular.forEach($scope.configMobPort,function(item,index){
                //console.log(item);
                if (proposalData.telstra=="Telstra 4G"){
                    if (angular.isUndefined($scope.telstraPlans[proposalData.telstra.itemIndex]))
                        $scope.telstraPlans[proposalData.telstra.itemIndex]=1;
                    else
                        $scope.telstraPlans[proposalData.telstra.itemIndex]++;
                } else if(proposalData.megaRate=="Mobile Mega"){
                    if (angular.isUndefined($scope.mobileMegaPlans[proposalData.telstra.itemIndex]))
                        $scope.mobileMegaPlans[proposalData.telstra.itemIndex]=1;
                    else
                        $scope.mobileMegaPlans[val.itemIndex]++;
                    proposalData.boltOn = {};
                } else if(proposalData.untimed=="Mobile 4G Untimed"){
                    if (angular.isUndefined($scope.mobile4GUntimedPlans[proposalData.telstra.itemIndex]))
                        $scope.mobile4GUntimedPlans[proposalData.telstra.itemIndex]=1;
                    else
                        $scope.mobile4GUntimedPlans[proposalData.telstra.itemIndex]++;
                    // proposalData.boltOn = {};
                }
            });
        }    
    }

    $scope.disabledPlanField = function(val, index) {
        $('#telstra4gSuperPlan-'+ index).attr('disabled', 'disabled');
    }
    

    $scope.isSigned = function(form) {
        //alert(form,formName);
        if ($scope[form]) {
            return true;
        } else {
            if (form == false) {
                if ( angular.isDefined($scope.htmlData[form])) {
                    delete $scope.htmlData[form];
                }
                    $scope[form] = false;
                }

            return false;
        }
    }
    
    // $scope.processBoltOnCount = function(){
    //     $scope.boltOnPlans = [];
    //     angular.forEach($scope.configMobPort,function(item,index){
    //         //console.log(item.boltOn.itemIndex);
    //         if (angular.isDefined(proposalData.boltOn.itemIndex)){
    //             if (angular.isUndefined($scope.boltOnPlans[proposalData.boltOn.itemIndex])){
    //                 $scope.boltOnPlans[proposalData.boltOn.itemIndex]=1;    
    //             } else {
    //                 $scope.boltOnPlans[proposalData.boltOn.itemIndex]++;
    //             }
    //         }
    //     });
    // }

    $scope.sampshow = true;
    $scope.sampbind = $location.absUrl();
    $scope.cars = {
      car1 : "Ford",
      car2 : "Fiat",
      car3 : "Volvo",
    }

});

Qms.controller('ViewSalesRepProposalListCtrl', function($stateParams,$scope,$rootScope, Scopes, $filter, $http, $state,ProposalActions, Proposals, FlashMessage, $window, $timeout,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder,ngDialog,Dashboard){
    
    $scope.vmf = {};
            $scope.vmf.dtInstance = {};   
            $scope.vmf.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [3, 'desc'])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.vmr = {};
            $scope.vmr.dtInstance = {};   
            //$scope.vmr.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vmr.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [[6, 'desc']])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.pages = 0;
    $scope.itemsPerPage = 10;
    $scope.count = 0;
    $scope.status = "";

    $scope.sortType = 'creator_name';
    $scope.reverse = false;

    $http.get("profile").success(function(response) {
        $scope.user = response;
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;   
    var profile =  ProposalActions.showSalesRepProposals({user:$scope.user.id}, function(data) {
            $scope.loading = false;
            $scope.proposals = data.proposalData;
            $scope.userStatus = 'all';
            if (data.userStatus == 'user') {
                $scope.userStatus = 5;
            }
            console.log(data);
        });
    });

    $scope.filterForms = function(status) {
       $scope.status = status;
    }

 
    $scope.edit = function(id) {
        
        var x=window.confirm("Do you want to Modify this?")
          if (x)
          var data = ProposalActions.show({id : id}, function(data) {
            console.log(data);
              angular.forEach(data.proposalData,function(name,index){
                  //console.log(name.comLocation['schedule_of_goods'].length);
                  if (index==data){
                      //alert(index);
                      angular.forEach(name,function(val,index){
                          if (index=='voiceEssentialChannel'){
                              //alert(angular.isArray(name));
                              if (!angular.isArray(name)){
                                  var key = val;
                                  delete val;
                                  name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
                              }
                          }
                          if (index=='voiceEssentialDID'){
                              //alert(angular.isArray(name));
                              if (!angular.isArray(val)){
                                  var key = val;
                                  delete val;
                                  name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
                              }
                          }
                      });
                  } 
              })
              $rootScope.viewProposalData = data.proposalData;
              console.log($rootScope.viewProposalData);
              $state.go('proposal');
          });
      }
    $scope.downloadForms = function(id) {
    var proposal = ProposalActions.downloadForms({id:id}, function(data) {
        if(data.success) {
        angular.forEach(data.files, function(name, index){
            window.open("assets/files/"+name,'_blank');
        });
        }
    });
    }

    $scope.delete = function(id) {
        var x=window.confirm("Deleting Sales Rep quation is disabled")
    }
});

Qms.controller('ProposalListCtrl', function($stateParams,$scope,$rootScope, Scopes, $filter, $http, $state,ProposalActions, Proposals, FlashMessage, $window, $timeout,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder,ngDialog,Dashboard){
    
    $scope.vmf = {};
            $scope.vmf.dtInstance = {};   
            $scope.vmf.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [3, 'desc'])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.vmr = {};
            $scope.vmr.dtInstance = {};   
            //$scope.vmr.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vmr.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [[6, 'desc']])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.pages = 0;
    $scope.itemsPerPage = 10;
    $scope.count = 0;
    $scope.status = "";

    $scope.sortType = 'creator_name';
    $scope.reverse = false;

    $http.get("profile").success(function(response) {
        $scope.user = response;
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;   
    var profile =  ProposalActions.show({user:$scope.user.id}, function(data) {
            $scope.loading = false;
            $scope.proposals = data.proposalData;
            $scope.userStatus = 'all';
            if (data.userStatus == 'user') {
                $scope.userStatus = 5;
            }
            console.log(data);
        });
    });

    $scope.filterForms = function(status) {
       $scope.status = status;
    }

    $scope.edit = function(id) {
        
      var x=window.confirm("Do you want to Modify this?")
        if (x)
        var data = ProposalActions.show({id : id}, function(data) {
          console.log(data);
            angular.forEach(data.proposalData,function(name,index){
                //console.log(name.comLocation['schedule_of_goods'].length);
                if (index==data){
                    //alert(index);
                    angular.forEach(name,function(val,index){
                        if (index=='voiceEssentialChannel'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(name)){
                                var key = val;
                                delete val;
                                name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
                            }
                        }
                        if (index=='voiceEssentialDID'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(val)){
                                var key = val;
                                delete val;
                                name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
                            }
                        }
                    });
                } 
            })
            $rootScope.viewProposalData = data.proposalData;
            console.log($rootScope.viewProposalData);
            $state.go('proposal');
        });
    }

    $scope.create = function(id) {
      var x=window.confirm("Do you want to Modify this?")
        if (x)
        var data = ProposalActions.show({id : id}, function(data) {
          console.log(data);
            angular.forEach(data.proposalData,function(name,index){
                //console.log(name.comLocation['schedule_of_goods'].length);
                if (index==data){
                    //alert(index);
                    angular.forEach(name,function(val,index){
                        if (index=='voiceEssentialChannel'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(name)){
                                var key = val;
                                delete val;
                                name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
                            }
                        }
                        if (index=='voiceEssentialDID'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(val)){
                                var key = val;
                                delete val;
                                name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
                            }
                        }
                    });
                } 
            })
            $rootScope.viewProposalData = data.proposalData;
            console.log($rootScope.viewProposalData);
            $state.go('forms');
        });
    }

    $scope.move = function(id) {
        Dashboard.checkMove({ proposal_id : id,
         }, function(data) {
              if (data.success == true) {
                $state.go('forms', {id: id });
              } else {
                $scope.loading = false;
                alert("You already have pending application for this proposal.");
              }
            }
        );
    }

    $scope.downloadForms = function(id) {
      var proposal = ProposalActions.downloadForms({id:id}, function(data) {
        if(data.success) {
          angular.forEach(data.files, function(name, index){
            window.open("assets/files/"+name,'_blank');
          });
        }
      });
    }

    $scope.convertToCSV = function (objArray){
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        return str;
    }

    $scope.to_csv = function() {
        $scope.csvButton = !$scope.csvButton;
        var forms = Form.to_csv({referals:$scope.referals}, function(response) {
            if(response.success){
                $scope.csvFile = response.filename;
                $('#referalCSV').click();
            }
        });

    }

    $scope.closedialog = function(){
        $scope.closeThisDialog();
    }

    $scope.view_referals = function(id) {
        var forms = Form.view_referals({id:id}, function(data) {
           if (data.success) {
               // console.log(data.data);
               $scope.data = data.data;
               $scope.template = "templates/forms/referals.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ViewFormsCtrl',
                     width: 900,
                     height: 500,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                     // preCloseCallback : function(value) {
                     //     var signData = $('img.imported').attr('src');
                     //     if ((!signData) && (!$rootScope.isDraftMode)) {
                     //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                     //             return true;
                     //         }
                     //         return false;
                     //         $timeout(angular.noop());
                     //     }
                     // }
                 });
           }
        });
    }

    $scope.convertToCSV = function (objArray){
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        return str;
    }

    $scope.to_csv = function() {
        $scope.csvButton = !$scope.csvButton;
        var forms = Form.to_csv({referals:$scope.referals}, function(response) {
            if(response.success){
                $scope.csvFile = response.filename;
                $('#referalCSV').click();
            }
        });

    }
    $scope.view_all = function() {
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;         
        var forms = Form.view_all(function(data) {
            $scope.loading = false;         
            if (data.success) {
                $scope.loading = false; 
                $scope.referals = data.response;
                // console.log($scope.referals);
                $scope.template = "templates/forms/view-all.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ViewFormsCtrl',
                     width: 1080,
                     height: 500,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                     // preCloseCallback : function(value) {
                     //     var signData = $('img.imported').attr('src');
                     //     if ((!signData) && (!$rootScope.isDraftMode)) {
                     //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                     //             return true;
                     //         }
                     //         return false;
                     //         $timeout(angular.noop());
                     //     }
                     // }
                 });
            }
        });
    }

    $scope.delete = function(id) {
        $('<div></div>').appendTo('body')
        .html('<div><h6>Are you really sure to delete this item?</h6></div>')
        .dialog({
            modal: true,
            title: 'You want to delete this item?',
            zIndex: 10000,
            autoOpen: true,
            width: '315px',
            resizable: false,
            buttons: {
                Yes: function () {
                    var data = ProposalActions.delete({id : id}, function(data) {
                        if (data.success) {
                
                            $http.get("profile").success(function(response) {
                                $scope.user = response;
                                $('body').append('<div class="loading"></div>');
                                $scope.loading = true;   
                            var del =    ProposalActions.show({user:$scope.user.id}, function(data) {
                                    $scope.loading = false;
                                    $scope.proposals = data.proposalData;
                                    $scope.userStatus = 'all';
                                    if (data.userStatus == 'user') {
                                        $scope.userStatus = 5;
                                    }
                                    console.log(data);
                                });
                                    FlashMessage.setMessage(data);
                            });
                        }
                    });
                    $(this).dialog("close");
                },
                No: function () {
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });

    }

});


Qms.controller("FormDataCtrl", function($state,$scope,$sce,$stateParams,Proposals, $rootScope, $interval, $window, $anchorScroll, $location, $timeout, $http, Scopes, Form, Rates, FlashMessage, ngDialog, ProposalMove) {

    $scope.scheduleCount = 0;
    $scope.locationCount = 0;
    $scope.accountLocation = 0;
    $scope.addTelCount = 0;
    $scope.addMobPortCount = 0;
    $scope.htmlData = {};
    $scope.discountArr = {};
    $scope.companyLocations = [];
    $rootScope.draftMode = false;

    $scope.locationAvailableTemps = [
        'nbf_cap',
        'nbf_std',
        'voice_comp',
        'voice_solution_untimed',
        'voice_essentials_cap',
        'cloud_connect_standard',
        'cloud_connect_cap',
        'voice_solution_standard',
        'data_adsl',
        'ip_midband',
        'ip_midbandunli',
        'nbnUnlimited',
        'nbn',
        'mobile_mega',
        'mobile_ut',
        'mobile_wireless',
        '1300',
        '1300_discounted',
        'fibre',
        'ethernet',
        'telstraUntimed',
        'telstraWirelessSuper',
        'telstraWireless',
        'printer',
        'schedule_of_goods',
        'summary'
    ];

    $scope.items = {
        'nbf_cap'               : [0],
        'nbf_std'               : [0],
        'voice_comp'            : [0],
        'voice_solution_untimed'  : [0],
        'voice_essentials_cap'    : [0],
        'cloud_connect_standard'  : [0],
        'cloud_connect_cap'       : [0],
        'voice_solution_standard' : [0],
        'data_adsl'               : [0],
        'ip_midband'              : [0],
        'ip_midbandunli'          : [0],
        'nbn'                     : [0],
        'nbnUnlimited'            : [0],
        'mobile_mega'             : [0],
        'mobile_ut'               : [0],
        'mobile_wireless'         : [0],
        '1300'                    : [0],
        '1300_discounted'         : [0],
        'fibre'                   : [0],
        'ethernet'                : [0],
        'telstraUntimed'          : [0],
        'telstraWirelessSuper'    : [0],
        'telstraWireless'         : [0],
        'printer'                 : [0],
        'schedule_of_goods'       : [0],
        'summary'                 : [0],
        'proposal'                : [0],
        'quotation'               : [0],
        'landscape'               : [0],
        'cloud'                   : [0],
    };

    $scope.recurring_services = [
        {'description' : 'Monitoring Centre connect 24 hours','price' : 59.00},
        {'description' : "Monitoring Centre connect 24 hours (Including NBN/3G Wireless Unit)",'price' : 89}
    ];

    $scope.ethernet_plans = [
        {'description' : 'Up to 10Mbps/10Mbps* Unlimited','price':499},
        {'description' : 'Up to 20Mbps/20Mbps* Unlimited','price':599},
    ];

    $scope.nbnUnlimited_plans = [
        {'description' : 'NBN Standard 25','inclusion' : 'Download speeds Between 5Mbps and 25Mbps*<br/>Upload speeds Between 1Mbps and 5Mbps*','price':159},
        {'description' : 'NBN Fast 50','inclusion' : 'Download speeds Between 12Mbps and 50Mbps*<br/>Upload speeds Between 1Mbps and 20Mbps*','price':179},
        {'description' : 'NBN WIRELESS 75','inclusion' : 'Download speeds Between 12Mbps and 75Mbps*<br/>Upload speeds Between 1Mbps and 10Mbps*','price':179},
        {'description' : 'NBN Ultra fast 100','inclusion' : 'Download speeds Between 12Mbps and 100Mbps*<br/>Upload speeds Between 1Mbps and 40Mbps*','price':189},
        {'description' : 'Existing NBN', 'inclusion': 'Use Existing NBN', 'price': 0}
    ];

    $scope.mobile_mega_plans = [
        {'description' : 'Cap 39','inclusions' : '$250 inc. calls 500MB','price':39},
        {'description' : 'Cap 49','inclusions' : '$2500 inc. calls 2GB','price':49},
        {'description' : 'Cap 59','inclusions' : '$2500 inc. calls 3GB','price':59},
    ];
    $scope.data_bolt_on_plans = [
        {'description' : 'Data Bolt-on 1GB','memory': '1GB','price':9.89,'itemIndex':0},
        {'description' : 'Data Bolt-on 5GB','memory': '5GB','price':31.99,'itemIndex':1}
    ];
    
    $scope.mobile_rate_plans = [
        {'plan' : 'Select a Mobile Plan','inclusions':''},
        {'group':'Telstra 4G Super','plan':'Telstra 4G Super Standard','inclusions':'– Unlimited National Calls, 4GB Data, Unlimited standard SMS and MMS, plus 100 Mins International calls','price':49,'itemIndex':0},
        {'group':'Telstra 4G Super','plan':'Telstra 4G Super','inclusions':'– Unlimited National Calls, 30GB Data, Unlimited standard SMS and MMS, plus 300 Mins International calls','price':69,'itemIndex':1},
        {'group':'Telstra 4G Super','plan':'Telstra 4G Super Max','inclusions':'– Unlimited National Calls, 40GB Data, Unlimited standard SMS and MMS, plus 300 Mins International calls','price':85,'itemIndex':2},
        {'group':'Telstra 4G Super','plan':'Telstra 4G Super Extreme','inclusions':'– Unlimited National Calls, 75GB Data, Unlimited standard SMS and MMS, plus 300 Mins International calls','price':99,'itemIndex':3},

        // {'group':'Telstra 4G','plan':'Telstra 4G Lite','inclusions':'$1000 National Calls 1GB Data','price':49,'itemIndex':0},
        // {'group':'Telstra 4G','plan':'Telstra 4G Business Executive','inclusions':'Unlimited Calls 3GB Data','price':69,'itemIndex':1},
        // {'group':'Telstra 4G','plan':'Telstra 4G Business Professional','inclusions':'Unlimited Calls 10GB Data','price':79,'itemIndex':2},
        // {'group':'Telstra 4G','plan':'Telstra 4G Business Maximum','inclusions':'Unlimited Calls 20GB Data','price':89,'itemIndex':3},
        // {'group':'Mobile Mega','plan':'Mobile Mega Cap 39','inclusions':'$250 inc. calls 500Mb','price':39,'itemIndex':0},
        // {'group':'Mobile Mega','plan':'Mobile Mega Cap 49','inclusions':'$2500 inc. calls 2GB','price':49,'itemIndex':1},
        // {'group':'Mobile Mega','plan':'Mobile Mega Cap 59','inclusions':'$2500 inc. calls 3GB','price':59,'itemIndex':2},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 49','inclusions':'Includes 700MB','price':49,'itemIndex':0},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 59','inclusions':'Includes 2GB','price':59,'itemIndex':1},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 79','inclusions':'Includes 4GB and 300 INTERNATIONAL MINS','price':79,'itemIndex':2},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 89','inclusions':'Includes 6GB and 300 INTERNATIONAL MINS','price':89,'itemIndex':3}
    ];

    $scope.configMobPort = [
        {'confAccHolder':'','confMobNo':'','confProvider':'','confAccNo':'','confPatPlan':$scope.mobile_rate_plans[0],'boltOn':{}, 'status': "1"}
    ];

    $scope.adsl_plans = [
        {'description' : 'ADSL 2 + LITE','details' : '(Unlimited Lite (no static IP))','price':79},
        {'description' : 'ADSL 2 + Unlimited','details' : 'Business Grade (ON NET)','price':109},
        {'description' : 'ADSL 2 + 100GB','details' : 'Business Grade (OFF NET)','price':109},
        {'description' : 'ADSL 2 + 200GB','details' : 'Business Grade (OFF NET)','price':129},
        {'description' : 'ADSL 2 + 300GB','details' : 'Business Grade (OFF NET)','price':139},
        {'description' : 'ADSL 2 + 500GB','details' : 'Business Grade (OFF NET)','price':149},
        {'description' : 'ADSL 2 + 1TB','details' : 'Business Grade (OFF NET)','price':169},
    ];
    $scope.telstra4gSuper_plans = [
        {'description' : 'Super Standard','inclusions' : 'UNLIMITED NATIONAL CALLS**, INCLUDES 4GB, UNLIMITED NATIONAL SMS, UNLIMITED NATIONAL STANDARD MMS*, 100 INTERNATIONAL MINS*','price':49},
        {'description' : 'Super','inclusions' : 'UNLIMITED NATIONAL CALLS**, INCLUDES 30GB, UNLIMITED NATIONAL SMS, UNLIMITED NATIONAL STANDARD MMS*, 300 INTERNATIONAL MINS*','price':69},
        {'description' : 'Super Max','inclusions' : 'UNLIMITED NATIONAL CALLS**, INCLUDES 40GB, UNLIMITED NATIONAL SMS, UNLIMITED NATIONAL STANDARD MMS*, 300 INTERNATIONAL MINS*','price':85},
        {'description' : 'Super Extreme','inclusions' : 'UNLIMITED NATIONAL CALLS**, INCLUDES 75GB, UNLIMITED NATIONAL SMS, UNLIMITED NATIONAL STANDARD MMS*, 300 INTERNATIONAL MINS*','price':99},
    ];

    $scope.telstraUntimed_plans = [
        {'description' : 'Lite','inclusions' : 'INCLUDES $1000 National Calls,INCLUDES 1GB,UNLIMITED NATIONAL SMS,$0.69 PER MMS','price':49},
        {'description' : 'Business Executive','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 3GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,100 INTERNATIONAL MINS*','price':69},
        {'description' : 'Business Professional','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 10GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':79},
        {'description' : 'Business Maximum','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 20GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':89},
    ];


    $scope.telstraWirelessSuper_plans = [
        {'description' : 'Broadband Wireless Super Mid 10GB','price':49},
        {'description' : 'Broadband Wireless Super Large 30GB','price':79},
        {'description' : 'Broadband Wireless Super Extra Large 50GB','price':99}
    ];

    $scope.telstraWireless_plans = [
        {'description' : 'Broadband Wireless 3GB','price':43},
        {'description' : 'Broadband Wireless 6GB','price':64},
        {'description' : 'Broadband Wireless 10GB','price':93}
    ];

    $scope.ip_midbandunli_plans = [
        {'description' : '10Mbps/10Mbps*','price':549},
        {'description' : '20Mbps/20Mbps*','price':699},
        {'description' : '40Mbps/40Mbps*','price':999}
    ];

    $scope.ip_midband_plans = [
        {'description' : 'None','price':0},
        {'description' : '6Mbps/6Mbps*','price':319},
        {'description' : '8Mbps/8Mbps*','price':349},
        {'description' : '18Mbps/18Mbps*','price':399},
        {'description' : '20Mbps/20Mbps*','price':449},
        {'description' : '30Mbps/30Mbps*','price':799},
        {'description' : '40Mbps/40Mbps*','price':849}
    ];

    $scope.professional_install = [
        {'description' : 'ASDL 2+ 20/1Mbps','price':399},
        {'description' : 'ASDL 2+ 20/3 to 40Mbps Midband','price':599},
    ];

    $scope.download_plans = [
        {'description' : 'None','price':0},
        {'description' : '50 GB','price':19},
        {'description' : '100 GB','price':35},
        {'description' : '200 GB','price':69},
        {'description' : '300 GB','price':99},
        {'description' : '500 GB','price':149},
        {'description' : '1000 GB','price':289}
    ];

    $scope.mobile_4G_Untimed_Calls = [
        {'description' : '4G Untimed 49','inclusions' : 'Includes 700MB','price':49},
        {'description' : '4G Untimed 59','inclusions' : 'Includes 2GB','price':59},
        {'description' : '4G Untimed 79','inclusions' : 'Includes 4GB,300 INTERNATIONAL MINS*','price':79},
        {'description' : '4G Untimed 89','inclusions' : 'Includes 6GB,300 INTERNATIONAL MINS*','price':89}
    ];

    $scope.wireless_cap_plans = [
        {'description' : 'Wireless 15 - 500MB','price':15},
        {'description' : 'Wireless 19 - 1GB','price':19},
        {'description' : 'Wireless 29 - 2GB','price':29},
        {'description' : 'Wireless 39 - 3GB','price':39},  
        {'description' : 'Wireless 44 - 4GB','price':44},
        {'description' : 'Wireless 49 - 6GB','price':49},
        {'description' : 'Wireless 59 - 8GB','price':59},
        {'description' : 'Wireless 119 - 12GB','price':119}
    ];

    $scope.fibre_unli_plans = [
        {'description' : '10Mbps / 10Mbps Unlimited - 36 month term','price':599},
        {'description' : '20Mbps / 20Mbps Unlimited - 36 month term','price':699},
        {'description' : '100Mbps / 100Mbps Unlimited - 36 month term','price':1199},
        {'description' : '400Mbps / 400Mbps Unlimited - 36 month term','price':758},
        {'description' : '500Mbps / 500Mbps Unlimited - 36 month term','price':1599},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 24 month term','price':1899},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 36 month term','price':1699},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 48 month term','price':1299},
    ];

    $http.get("profile").success(function(response) {
        $scope.profile = response;
        $rootScope.profile = response;
    });
    
    $scope.dollarSign = function(val){
        var strCurrency = '';
        if (angular.isDefined(val))
            strCurrency = '$' + val;
        //alert(strCurrency);
        return strCurrency;
    }

    $scope.isEmptyArray = function(arr) {
        return angular.equals([], arr);
    }

    $scope.reset = function() {
        $scope.proposalData = {};
        $scope.formData = {};
        $scope.formData.fibreTerm = [];
        // $scope.formData.assessment = [];
        // $scope.formData.assessment.q1 = 0;
        // $scope.formData.assessment.q2 = 0;
        // $scope.formData.assessment.q1drop1 = 10;
        // $scope.formData.assessment.q2drop1 = 1;
        $scope.formData.fibreTerm[0] = 36;
        $scope.formData.schItems = [];
        $scope.formData.schItems[0] = [{count:1, status: 1}];
        $scope.schItemCount = [];
        $scope.schItemCount[0] = 1;
        $scope.formData.aBillingTerm = 60;
        $scope.formData.aTerm = 60;
        $scope.formData.maintenanceAgreementCost = 0;
        $scope.formData.schAddItems = [];
        $scope.formData.schAddItems[0] = [{count:1, status:1}];
        
        $scope.schAddItemCount = [];
        $scope.schAddItemCount[0] = 1;
        $scope.formData.cPosition = "";
        $scope.formData.cSPosition = "";
        $scope.formData.aIdenQuestion = "";
        $scope.formData.referals = [0,1,2,3,4];
        $scope.formData.rBusinessName = [null,null,null,null,null];
        $scope.formData.rContactPerson = [null,null,null,null,null];
        $scope.formData.rPhoneNumber = [null,null,null,null,null];
        $scope.referalCount=5;
        $scope.formData.dID = [];
        $scope.formData.dID[0] = 10;
        $scope.formData.voiceStd = [];
        $scope.formData.voiceStd[0] = 5;
        $scope.formData.voiceStdDID = [];
        $scope.formData.voiceStdDID[0] = 10;
        $scope.formData.voiceStdChannel = [];
        $scope.formData.voiceStdChannel[0] = 5;
        $scope.formData.bFCapDiscount = [];
        $scope.formData.bFCapDiscount[0] = 0;
        $scope.formData.bFStdDiscount = [];
        $scope.formData.bFStdDiscount[0] = 0;
        $scope.formData.bFCallDiscount = [];
        $scope.formData.bFCallDiscount[0] = 0;
        $scope.formData.voiceUntimedChannel = [];
        $scope.formData.voiceUntimedChannel[0] = 3;
        $scope.formData.voiceUntimedLineHunt = [];
        $scope.formData.voiceUntimedLineHunt[0] = 3;
        $scope.formData.voiceUntimedDID = [];
        $scope.formData.voiceUntimedDID[0] = 10;
        $scope.formData.bFUntimedDiscount = [];
        $scope.formData.bFUntimedDiscount[0] = 0;
        $scope.formData.cloudConnectStandardChannel = [];
        $scope.formData.cloudConnectStandardChannel[0] = 2;
        $scope.formData.cloudConnectStandardDID = [];
        $scope.formData.cloudConnectStandardDID[0] = 0; 
        $scope.formData.cloudConnectStandardDIDQty = [];
        $scope.formData.cloudConnectStandardDIDQty[0] = 0; 
        $scope.formData.cloudConnectStandardFaxQty = [];
        $scope.formData.cloudConnectStandardFaxQty[0] = 0; 
        $scope.formData.cloudConnectStandardFaxToEmail = [];
        $scope.formData.cloudConnectStandardFaxToEmail[0] = 0; 
        $scope.formData.cloudConnectStandardCallRecording = []; 
        $scope.formData.cloudConnectStandardCallRecording[0] = 0; 
        $scope.formData.cloudConnectStandardCallRecordingPremium = []; 
        $scope.formData.cloudConnectStandardCallRecordingPremium[0] = 0; 
        $scope.formData.cloudConnectCapChannel = [];
        $scope.formData.cloudConnectCapChannel[0] = 2;
        $scope.formData.cloudConnectCapDID = [];
        $scope.formData.cloudConnectCapDID[0] = 0; 
        $scope.formData.cloudConnectCapDIDQty = [];
        $scope.formData.cloudConnectCapDIDQty[0] = 0; 
        $scope.formData.cloudConnectCapFaxQty = [];
        $scope.formData.cloudConnectCapFaxQty[0] = 0; 
        $scope.formData.cloudConnectCapFaxToEmail = [];
        $scope.formData.cloudConnectCapFaxToEmail[0] = 0; 
        $scope.formData.cloudConnectCapCallRecording = []; 
        $scope.formData.cloudConnectCapCallRecording[0] = 0; 
        $scope.formData.cloudConnectCapCallRecordingPremium = []; 
        $scope.formData.cloudConnectCapCallRecordingPremium[0] = 0; 
        $scope.formData.voiceCompDID = [];
        $scope.formData.voiceCompDID[0] = 0;    
        $scope.formData.voiceSolutionChannel = [];
        $scope.formData.voiceSolutionChannel[0] = 3;  
        $scope.formData.voiceSolutionLineHunt = [];
        $scope.formData.voiceSolutionLineHunt[0] = 3;  
        $scope.formData.voiceSolutionDID = [];
        $scope.formData.voiceSolutionDID[0] = 10;  
        $scope.formData.bFSolutionDiscount = [];
        $scope.formData.bFSolutionDiscount[0] = 0;  
        $scope.formData.bFSolutionCallDiscount = [];
        $scope.formData.bFSolutionCallDiscount[0] = 0;  
        $scope.formData.voiceCompAnalougeDisNBNUnli = [];
        $scope.formData.voiceCompAnalougeDisNBNUnli[0] = 0;
        $scope.formData.voiceEssentialChannel = [];
        $scope.formData.voiceEssentialChannel[0] = 4; 
        $scope.formData.voiceEssentialDID = [];
        $scope.formData.voiceEssentialDID[0] = 10;  
        $scope.formData.bECapDiscount = [];
        $scope.formData.bECapDiscount[0] = 0; 
        $scope.formData.ipMidbandDis = [];
        $scope.formData.ipMidbandDis[0] = 0; 
        $scope.formData.voiceCompAnalougeDis = [];
        $scope.formData.voiceCompAnalougeDis[0] = 0;
        $scope.formData.voiceCompBriDis = [];
        $scope.formData.voiceCompBriDis[0] = 0;
        $scope.formData.voiceCompPri = [];
        $scope.formData.voiceCompPri[0] = 0;
        $scope.formData.voiceCompPriDis = [];
        $scope.formData.voiceCompPriDis[0] = 0;
        $scope.formData.voiceCompCallDis = [];
        $scope.formData.voiceCompCallDis[0] = 0;
        $scope.formData.voiceCompAnalougeDisDSL = [];
        $scope.formData.voiceCompAnalougeDisDSL[0] = 0;
        $scope.formData.UnlimitedPlans = [];
        $scope.formData.UnlimitedPlans[0] = 159;
        $scope.formData.mobileWirelessDis = [];
        $scope.formData.mobileWirelessDis[0] = 0;
        $scope.formData.nbnUnlimitedDis = [];
        $scope.formData.nbnUnlimitedDis[0] = 0;
        $scope.formData.voiceCompAnalougeNBNUnli = [];
        $scope.formData.voiceCompAnalougeNBNUnli[0] = 0;
        $scope.formData.ipMidbandUnliDis = [];
        $scope.formData.ipMidbandUnliDis[0] = 0;
        $scope.formData.ethernetDis = [];
        $scope.formData.ethernetDis[0] = 0;
        $scope.formData.fibreDis = [];
        $scope.formData.fibreDis[0] = 0;
        $scope.formData.telstraUntimedDis = 0;
        $scope.formData.mobileUtDis = 0;
        $scope.formData.mobileCapDis = 0;
        $scope.formData.telstraWirelessSuperDis = [];
        $scope.formData.telstraWirelessSuperDis[0] = 0;
        $scope.formData.telstraWirelessDis = [];
        $scope.formData.telstraWirelessDis[0] = 0;
        $scope.formData.rate131300 = {};
        $scope.formData.rate131300.qt1800 = [];
        $scope.formData.rate131300.qt1800[0] = 0;
        $scope.formData.rate131300.qt1300 = [];
        $scope.formData.rate131300.qt1300[0] = 0;
        $scope.formData.rate131300.qt13 = [];
        $scope.formData.rate131300.qt13[0] = 0;
        $scope.formData.rate131300Dis = {};
        $scope.formData.rate131300Dis.qt1800 = [];
        $scope.formData.rate131300Dis.qt1800[0] = 0;
        $scope.formData.rate131300Dis.qt1300 = [];
        $scope.formData.rate131300Dis.qt1300[0] = 0;
        $scope.formData.rateDis131300 = {};
        $scope.formData.rateDis131300.discount = [];
        $scope.formData.rateDis131300.discount[0] = 0;
        $scope.formData.siteType = {};
        $scope.formData.siteType = "Existing Site";
        $scope.formData.adsl2Dis = [];
        $scope.formData.adsl2Dis[0] = 0;
        $scope.formData.accounts = [];
        $scope.formData.aPaymentMethod = 'Debit Card';
        $scope.formData.aCardNumber = "";
        $scope.formData.aCardName = "";
        $scope.formData.aCardExpiry = "";
        $scope.formData.aCardType = "";
        $scope.formData.aCardToken = "";
        $scope.formData.guarantorRequired = true;
        $scope.formData.customersign = false;
        

        $scope.formData.accounts = [
            {'configCarrier':'','configAccno':''}
        ];

        $scope.formData.comLocation = {};
        $scope.formData.comLocation = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'cloud_connect_standard'  : [],
            'cloud_connect_cap'       : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWirelessSuper'    : [],
            'telstraWireless'         : [],
            'printer'                 : [],
            'schedule_of_goods'       : []
        };

        $scope.formData.comSuburb = {};
        $scope.formData.comSuburb = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'cloud_connect_standard'  : [],
            'cloud_connect_cap'       : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWirelessSuper'    : [],
            'telstraWireless'         : [],
            'printer'                 : [],
            'schedule_of_goods'       : []
        };

        $scope.formData.comZipCode = {};
        $scope.formData.comZipCode = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'cloud_connect_standard'  : [],
            'cloud_connect_cap'       : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWirelessSuper'    : [],
            'telstraWireless'         : [],
            'printer'                 : [],
            'schedule_of_goods'       : []       
        };

        $scope.formData.voiceFaxToEmail = {};
        $scope.formData.voiceFaxQty = {};
        $scope.formData.voiceMobility = {};
        $scope.formData.voiceMobQty = {};

        $scope.formData.voiceStdFaxToEmail = {};
        $scope.formData.voiceStdFaxQty = {};
        $scope.formData.voiceStdMobility = {};
        $scope.formData.voiceStdMobQty = {};

        $scope.formData.cloudConnectStandardFaxQty = {};
        $scope.formData.cloudConnectStandardMobility = {};
        $scope.formData.cloudConnectStandardMobQty = {};
        $scope.formData.cloudConnectStandardFaxToEmail = {};
        $scope.formData.cloudConnectStandardAnalouge = {};
        $scope.formData.cloudConnectStandarduConeApp = {};
        $scope.formData.cloudConnectStandardAddAutoAttndnt = {};
        $scope.formData.cloudConnectStandardAddCallQueue = {};
        $scope.formData.cloudConnectStandardAddVoicemail = {};
        $scope.formData.cloudConnectStandardRepClientApp = {};
        $scope.formData.cloudConnectStandardCallRecording = {};
        $scope.formData.cloudConnectStandardCallRecordingPremium = {};

        $scope.formData.cloudConnectCapFaxQty = {};
        $scope.formData.cloudConnectCapMobility = {};
        $scope.formData.cloudConnectCapMobQty = {};
        $scope.formData.cloudConnectCapFaxToEmail = {};
        $scope.formData.cloudConnectCapAnalouge = {};
        $scope.formData.cloudConnectCapuConeApp = {};
        $scope.formData.cloudConnectCapAddAutoAttndnt = {};
        $scope.formData.cloudConnectCapAddCallQueue = {};
        $scope.formData.cloudConnectCapAddVoicemail = {};
        $scope.formData.cloudConnectCapRepClientApp = {};
        $scope.formData.cloudConnectCapCallRecording = {};
        $scope.formData.cloudConnectCapCallRecordingPremium = {};

        $scope.formData.voiceCompAnalouge = {};
        $scope.formData.voiceCompBri = {};
        $scope.formData.voiceCompFaxToEmail = {};
        $scope.formData.voiceCompFaxQty = {};
        $scope.formData.voiceCompMobility = {};
        $scope.formData.voiceCompMobQty = {};
        // $scope.formData.voiceCompDID = {};
        // $scope.formData.voiceCompAnalougeDis = {};
        // $scope.formData.voiceCompBriDis = {};
        // $scope.formData.voiceCompPriDis = {};
        //$scope.formData.voiceCompPri = {};

        $scope.formData.voiceUntimedFaxToEmail = {};
        $scope.formData.voiceUntimedFaxQty = {};
        $scope.formData.voiceUntimedMobility = {};
        $scope.formData.voiceUntimedMobQty = {};

        $scope.formData.voiceSolutionFaxToEmail = {};
        $scope.formData.voiceSolutionFaxQty = {};
        $scope.formData.voiceSolutionMobility = {};
        $scope.formData.voiceSolutionMobQty = {};

        $scope.formData.ipMidbandDownload = {
            0:$scope.download_plans[1]
        };

        $scope.formData.alarmPlans = $scope.recurring_services[0];

        //console.log($scope.formData.alarmPlans);

        $scope.formData.transactionDetails = "Commercial Premises";
        $scope.formData.hardwareInstalled = "MPM (multi path monitoring device)";
        $scope.formData.monitoredServices = "Burglary";
        $scope.formData.panelType = "NESS";

        $scope.formData.adsl2Plans = {
            0:$scope.adsl_plans[0]
        }

        $scope.formData.telstra4gSuperPlans = {
            0:$scope.telstra4gSuper_plans[0]
        }
        
        $scope.formData.telstraUntimedPlans = {
            0:$scope.telstraUntimed_plans[0]
        }

        $scope.formData.telstraWirelessSuperPlans = {
            0:$scope.telstraWirelessSuper_plans[0]
        }

        $scope.formData.telstraWirelessPlans = {
            0:$scope.telstraWireless_plans[0]
        }

        $scope.formData.ethernetPlans = {
            0:$scope.ethernet_plans[0]
        }

        $scope.formData.UnlimitedPlans = {
            0:$scope.nbnUnlimited_plans[0]
        }

        $scope.formData.ipMidbandPlans = {
            0:$scope.ip_midband_plans[1]
        };

        $scope.formData.ipMidbandUnliPlans = {
            0:$scope.ip_midbandunli_plans[0]
        };

        $scope.formData.ipMidbandUnliProf = {
            0:$scope.professional_install[0]
        };

        $scope.formData.ipMidbandDownloadVoice = {
            0:$scope.download_plans[0]
        };

        $scope.formData.ipMidbandPlansVoice = {
            0:$scope.ip_midband_plans[0]
        };

        $scope.formData.mobileUtPlans = {
            0:$scope.mobile_4G_Untimed_Calls[0]
        };

        $scope.formData.fibreUtPlans = {
            0:$scope.fibre_unli_plans[0]
        };

        $scope.formData.mobileWirelessPlans = {
            0:$scope.wireless_cap_plans[0]
        };

        $scope.formData.voiceCompAnalougeDSL = {
            0:0
        };

        $scope.formData.voiceCompAnalougeNBNMonthly = {
            0:0
        };

        $scope.formData.voiceCompAnalougeNBNUnli = {
            0:0
        };

        $scope.formData.voiceCompAnalouge = {
            0:0
        };

        $scope.formData.voiceCompBri = {
            0:0
        };

        for (var index=0;index<$scope.formData.comLocation['schedule_of_goods'].length;index++){
            $scope.formData.itemNo[index] = {};
            $scope.formData.serialNo[index] = {};
            $scope.formData.itemAddNo[index] = {};
            $scope.formData.serialAddNo[index] = {};
        }

        $scope.formData.qt1300new = {};
        $scope.formData.qt800new = {};
        $scope.formData.qt13new = {};
        $scope.formData.aCardExpiry = '';
        $scope.formData.rates = [];
        $scope.rates = [];
        $scope.formData.orderNumber = '';
        $scope.formData.pdfDocuments = '';
        $scope.formData.telstraSuperPlans = [];
        $scope.formData.mobile4GUntimedPlans = [];
      
    };

    $scope.reset();

    console.log($state.params);
    if($state.params) {
        var data = ProposalMove.movetoForm({id : $state.params.id}, function(data) {
            console.log(data);
              angular.forEach(data.proposal_data,function(name,index){
                  //console.log(name.comLocation['schedule_of_goods'].length);
                  if (index==data){
                      //alert(index);
                      angular.forEach(name,function(val,index){
                          if (index=='voiceEssentialChannel'){
                              //alert(angular.isArray(name));
                              if (!angular.isArray(name)){
                                  var key = val;
                                  delete val;
                                  name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
                              }
                          }
                          if (index=='voiceEssentialDID'){
                              //alert(angular.isArray(name));
                              if (!angular.isArray(val)){
                                  var key = val;
                                  delete val;
                                  name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
                              }
                          }
                      });
                  } 
              })
              $rootScope.viewProposalData = data.proposal_data;

            if (angular.isDefined($rootScope.viewProposalData)) {

                $scope.editId = $rootScope.viewProposalData.id;
                $scope.formData.proposal_id = data.id;
                $scope.formData.cName = $rootScope.viewProposalData.tradingName;
                $scope.formData.cAbn = $scope.viewProposalData.abn;
                $scope.formData.cTrading = $scope.viewProposalData.tradingName;
                $scope.formData.cAddress = $scope.viewProposalData.address;    
                $scope.formData.cSuburb = $scope.viewProposalData.suburb;        
                $scope.formData.cContact = $scope.viewProposalData.contactPerson;        
                $scope.formData.cEmail = $scope.viewProposalData.emailAddress;        
                   
                $scope.formData.schItems = $scope.viewProposalData.schItems;     
                $scope.formData.schQty = {};
                // console.log($scope.viewProposalData.schQty);
                $scope.formData.schQty = $scope.viewProposalData.schQty;
                $scope.formData.schOption = [];
                $scope.formData.schOption[0] = $scope.viewProposalData.schOption;

                // console.log( $scope.viewProposalData.schAddQty);
                $scope.formData.schAddItems = $scope.viewProposalData.schAddItems;     
                $scope.formData.schAddOptionQty = {};
                $scope.formData.schAddOptionQty = $scope.viewProposalData.schAddQty;
                $scope.formData.schAddOption = [];
                $scope.formData.schAddOption = $scope.viewProposalData.schAddOption;      

                $scope.formData.newMobPort = [];
                $scope.formData.telstraSuperPlans = [];
                $scope.formData.mobile4GUntimedPlans = [];

                $scope.formData.aBillingTerm  = $scope.viewProposalData.aBillingTerm;
                $scope.formData.aResidual  = $scope.viewProposalData.aResidual;

                console.log($scope.formData.configMobPort);
                if($scope.formData.configMobPort == undefined) {
                    $scope.formData.configMobPort = [];
                   
                    $scope.formData.newMobPort[0] = {
                        'confAccHolder':'','confMobNo':'','confProvider':'','confAccNo':'','confPatPlan':$scope.mobile_rate_plans[0],'boltOn':{}, 'status': "1"
                    };
                   
                    $scope.configMobPort = [];
                    $scope.configMobPort = $scope.formData.newMobPort;
                }
                console.log($scope.formData.configMobPort)

                if( $scope.viewProposalData.billingType == 'outright') {
                    $scope.formData.aTerm = "Outright";
                } else if( $scope.viewProposalData.billingType == 'leasing') {
                    $scope.formData.aTerm = $scope.viewProposalData.term;
                    $scope.formData.aBillingTerm = "";
                } else {
                    $scope.formData.aTerm = $scope.viewProposalData.term;
                }
                $scope.formData.aPayment = $scope.viewProposalData.leaser;

                // Services
                $scope.formData.type = {
                    'voiceCap'               : $scope.viewProposalData.type.voiceCap,
                    'focusStandard'          : $scope.viewProposalData.type.focusStandard,
                    'completeOffice'         : $scope.viewProposalData.type.completeOffice,
                    'voiceUt'                : $scope.viewProposalData.type.voiceUt,
                    'voiceSolution'          : $scope.viewProposalData.type.voiceSolution,
                    'voiceEssential'         : $scope.viewProposalData.type.voiceEssential,
                    'cloudConnectStandard'   : $scope.viewProposalData.type.cloudConnectStandard,
                    'adsl2'                  : $scope.viewProposalData.type.adsl2,
                    'nbnUnlimitedPlans'      : $scope.viewProposalData.type.nbnUnlimitedPlans,
                    'ipMidband'              : $scope.viewProposalData.type.ipMidband,
                    'ipMidbandUnli'          : $scope.viewProposalData.type.ipMidbandUnli,
                    'ethernet'               : $scope.viewProposalData.type.ethernet,
                    'fibre'                  : $scope.viewProposalData.type.fibre,
                    'telstra4gSuper'         : $scope.viewProposalData.type.telstra4gSuper,
                    'telstraWirelessSuper'   : $scope.viewProposalData.type.telstraWirelessSuper,
                    'telstraWireless'        : $scope.viewProposalData.type.telstraWireless,
                    'mobileUt'               : $scope.viewProposalData.type.mobileUt,
                    'rate131300'             : $scope.viewProposalData.type.rate131300,
                    'rateDis131300'          : $scope.viewProposalData.type.rateDis131300,
                };

                // Focus Cap
                $scope.formData.voiceStd = $scope.viewProposalData.voiceStd;
                    // change value
                for(i = 0; i < $scope.formData.voiceStd.length; i++) {
                    if($scope.formData.voiceStd[i] == "495") {
                        $scope.formData.voiceStd[i] = "5";
                    }
                    if($scope.formData.voiceStd[i] == "594") {
                        $scope.formData.voiceStd[i] = "6";
                    }
                    if($scope.formData.voiceStd[i] == "792") {
                        $scope.formData.voiceStd[i] = "8";
                    }
                    if($scope.formData.voiceStd[i] == "990") {
                        $scope.formData.voiceStd[i] = "10";
                    }
                    if($scope.formData.voiceStd[i] == "1188") {
                        $scope.formData.voiceStd[i] = "12";
                    }
                    if($scope.formData.voiceStd[i] == "1106") {
                        $scope.formData.voiceStd[i] = "14";
                    }
                    if($scope.formData.voiceStd[i] == "1264") {
                        $scope.formData.voiceStd[i] = "16";
                    }
                    if($scope.formData.voiceStd[i] == "1422") {
                        $scope.formData.voiceStd[i] = "18";
                    }
                    if($scope.formData.voiceStd[i] == "1580") {
                        $scope.formData.voiceStd[i] = "20";
                    }
                    if($scope.formData.voiceStd[i] == "1975") {
                        $scope.formData.voiceStd[i] = "25";
                    }
                }
                $scope.formData.voiceStdChannel = $scope.viewProposalData.voiceStdChannel;
                $scope.formData.voiceStdDID = $scope.viewProposalData.voiceStdDID;
                $scope.formData.voiceStdFaxQty = $scope.viewProposalData.voiceStdFaxQty;
                $scope.formData.voiceFaxToEmail = $scope.viewProposalData.voiceFaxToEmail;
                $scope.formData.voiceStdMobQty = $scope.viewProposalData.voiceStdMobQty;
                $scope.formData.voiceFaxQty = $scope.viewProposalData.voiceFaxQty;
                $scope.formData.dID =  $scope.viewProposalData.dID;
                for(i = 0; i < $scope.formData.dID.length; i++) {
                    if($scope.formData.dID[i] == "9.99") {
                        $scope.formData.dID[i] = "10";
                    }
                    if($scope.formData.dID[i] == "15.99") {
                        $scope.formData.dID[i] = "20";
                    }
                    if($scope.formData.dID[i] == "19.99") {
                        $scope.formData.dID[i] = "50";
                    }
                    if($scope.formData.dID[i] == "25.99") {
                        $scope.formData.dID[i] = "100";
                    }
                }
                $scope.formData.bFCapDiscount = $scope.viewProposalData.bFCapDiscount;
                //focusStandard
                $scope.formData.voiceStdChannel = $scope.viewProposalData.voiceStdChannel;
                for(i = 0; i < $scope.formData.voiceStdChannel.length; i++) {
                    if($scope.formData.voiceStdChannel[i] == "199") {
                        $scope.formData.voiceStdChannel[i] = "5";
                    }
                    if($scope.formData.voiceStdChannel[i] == "219") {
                        $scope.formData.voiceStdChannel[i] = "6";
                    }
                    if($scope.formData.voiceStdChannel[i] == "249") {
                        $scope.formData.voiceStdChannel[i] = "10";
                    }
                    if($scope.formData.voiceStdChannel[i] == "299") {
                        $scope.formData.voiceStdChannel[i] = "20";
                    }
                    if($scope.formData.voiceStdChannel[i] == "349") {
                        $scope.formData.voiceStdChannel[i] = "30";
                    }
                }
                $scope.formData.voiceStdFaxToEmail = $scope.viewProposalData.voiceStdFaxToEmail;
                $scope.formData.voiceStdFaxQty = $scope.viewProposalData.voiceStdFaxQty;
                $scope.formData.voiceStdDID = $scope.viewProposalData.voiceStdDID;
                for(i = 0; i < $scope.formData.voiceStdDID.length; i++) {
                    if($scope.formData.voiceStdDID[i] == "9.99") {
                        $scope.formData.voiceStdDID[i] = "10";
                    }
                    if($scope.formData.voiceStdDID[i] == "15.99") {
                        $scope.formData.voiceStdDID[i] = "20";
                    }
                    if($scope.formData.voiceStdDID[i] == "19.99") {
                        $scope.formData.voiceStdDID[i] = "50";
                    }
                    if($scope.formData.voiceStdDID[i] == "25.99") {
                        $scope.formData.voiceStdDID[i] = "100";
                    }
                }
                $scope.formData.bFStdDiscount = $scope.viewProposalData.bFStdDiscount;
                $scope.formData.bFCallDiscount = $scope.viewProposalData.bFCallDiscount;
                // Complete Office
                $scope.formData.voiceCompAnalouge = $scope.viewProposalData.voiceCompAnalouge;
                for(i = 0; i < $scope.formData.voiceCompAnalouge.length; i++) {
                    if($scope.formData.voiceCompAnalouge[i] == "39.95") {
                        $scope.formData.voiceCompAnalouge[i] = "1";
                    }
                    if($scope.formData.voiceCompAnalouge[i] == "79.9") {
                        $scope.formData.voiceCompAnalouge[i] = "2";
                    }
                    if($scope.formData.voiceCompAnalouge[i] == "119.85") {
                        $scope.formData.voiceCompAnalouge[i] = "3";
                    }
                    if($scope.formData.voiceCompAnalouge[i] == "159.8") {
                        $scope.formData.voiceCompAnalouge[i] = "4";
                    }
                    if($scope.formData.voiceCompAnalouge[i] == "199.75") {
                        $scope.formData.voiceCompAnalouge[i] = "5";
                    }
                    if($scope.formData.voiceCompAnalouge[i] == "239.7") {
                        $scope.formData.voiceCompAnalouge[i] = "6";
                    }
                    if($scope.formData.voiceCompAnalouge[i] == "279.65") {
                        $scope.formData.voiceCompAnalouge[i] = "7";
                    }
                    if($scope.formData.voiceCompAnalouge[i] == "319.6") {
                        $scope.formData.voiceCompAnalouge[i] = "8";
                    }
                    if($scope.formData.voiceCompAnalouge[i] == "359.55") {
                        $scope.formData.voiceCompAnalouge[i] = "9";
                    }
                    if($scope.formData.voiceCompAnalouge[i] == "399.5") {
                        $scope.formData.voiceCompAnalouge[i] = "10";
                    }
                }
                $scope.formData.voiceCompBri = $scope.viewProposalData.voiceCompBri;
                for(i = 0; i < $scope.formData.voiceCompBri.length; i++) {
                    if($scope.formData.voiceCompBri[i] == "99.85") {
                        $scope.formData.voiceCompBri[i] = "1";
                    }
                    if($scope.formData.voiceCompBri[i] == "199.7") {
                        $scope.formData.voiceCompBri[i] = "2";
                    }
                    if($scope.formData.voiceCompBri[i] == "299.55") {
                        $scope.formData.voiceCompBri[i] = "3";
                    }
                    if($scope.formData.voiceCompBri[i] == "399.4") {
                        $scope.formData.voiceCompBri[i] = "4";
                    }
                    if($scope.formData.voiceCompBri[i] == "499.25") {
                        $scope.formData.voiceCompBri[i] = "5";
                    }
                    if($scope.formData.voiceCompBri[i] == "599.1") {
                        $scope.formData.voiceCompBri[i] = "6";
                    }
                    if($scope.formData.voiceCompBri[i] == "698.95") {
                        $scope.formData.voiceCompBri[i] = "7";
                    }
                    if($scope.formData.voiceCompBri[i] == "798.8") {
                        $scope.formData.voiceCompBri[i] = "8";
                    }
                    if($scope.formData.voiceCompBri[i] == "989.65") {
                        $scope.formData.voiceCompBri[i] = "9";
                    }
                    if($scope.formData.voiceCompBri[i] == "998.5") {
                        $scope.formData.voiceCompBri[i] = "10";
                    }
                }
                $scope.formData.voiceCompPri = $scope.viewProposalData.voiceCompPri;
                for(i = 0; i < $scope.formData.voiceCompPri.length; i++) {
                    if($scope.formData.voiceCompPri[i] == "0") {
                        $scope.formData.voiceCompPri[i] = "0";
                    }
                    if($scope.formData.voiceCompPri[i] == "299.85") {
                        $scope.formData.voiceCompPri[i] = "10";
                    }
                    if($scope.formData.voiceCompPri[i] == "499.85") {
                        $scope.formData.voiceCompPri[i] = "20";
                    }
                    if($scope.formData.voiceCompPri[i] == "699.85") {
                        $scope.formData.voiceCompPri[i] = "30";
                    }
                }
                $scope.formData.voiceCompFaxToEmail = $scope.viewProposalData.voiceCompFaxToEmail;
                $scope.formData.voiceCompFaxQty = $scope.viewProposalData.voiceCompFaxQty;
                $scope.formData.voiceCompDID = $scope.viewProposalData.voiceCompDID;
                for(i = 0; i < $scope.formData.voiceCompDID.length; i++) {
                    if($scope.formData.voiceCompDID[i] == "0") {
                        $scope.formData.voiceCompDID[i] = "0";
                    }
                    if($scope.formData.voiceCompDID[i] == "49.95") {
                        $scope.formData.voiceCompDID[i] = "100";
                    }
                }
                $scope.formData.voiceCompAnalougeDis = $scope.viewProposalData.voiceCompAnalougeDis;
                $scope.formData.voiceCompBriDis = $scope.viewProposalData.voiceCompBriDis;
                $scope.formData.voiceCompPriDis = $scope.viewProposalData.voiceCompPriDis;
                $scope.formData.voiceCompCallDis = $scope.viewProposalData.voiceCompCallDis;
                // Solution Cap
                $scope.formData.voiceUntimedChannel = $scope.viewProposalData.voiceUntimedChannel;
                for(i = 0; i < $scope.formData.voiceUntimedChannel.length; i++) {
                    if($scope.formData.voiceUntimedChannel[i] == "199") {
                        $scope.formData.voiceUntimedChannel[i] = "3";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "259") {
                        $scope.formData.voiceUntimedChannel[i] = "4";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "319") {
                        $scope.formData.voiceUntimedChannel[i] = "5";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "379") {
                        $scope.formData.voiceUntimedChannel[i] = "6";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "439") {
                        $scope.formData.voiceUntimedChannel[i] = "7";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "499") {
                        $scope.formData.voiceUntimedChannel[i] = "8";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "559") {
                        $scope.formData.voiceUntimedChannel[i] = "9";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "619") {
                        $scope.formData.voiceUntimedChannel[i] = "10";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "679") {
                        $scope.formData.voiceUntimedChannel[i] = "11";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "739") {
                        $scope.formData.voiceUntimedChannel[i] = "12";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "799") {
                        $scope.formData.voiceUntimedChannel[i] = "13";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "859") {
                        $scope.formData.voiceUntimedChannel[i] = "14";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "919") {
                        $scope.formData.voiceUntimedChannel[i] = "15";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "979") {
                        $scope.formData.voiceUntimedChannel[i] = "16";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1039") {
                        $scope.formData.voiceUntimedChannel[i] = "17";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1099") {
                        $scope.formData.voiceUntimedChannel[i] = "18";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1159") {
                        $scope.formData.voiceUntimedChannel[i] = "19";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1219") {
                        $scope.formData.voiceUntimedChannel[i] = "20";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1279") {
                        $scope.formData.voiceUntimedChannel[i] = "21";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1339") {
                        $scope.formData.voiceUntimedChannel[i] = "22";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1399") {
                        $scope.formData.voiceUntimedChannel[i] = "23";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1459") {
                        $scope.formData.voiceUntimedChannel[i] = "24";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1519") {
                        $scope.formData.voiceUntimedChannel[i] = "25";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1579") {
                        $scope.formData.voiceUntimedChannel[i] = "26";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1639") {
                        $scope.formData.voiceUntimedChannel[i] = "27";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1699") {
                        $scope.formData.voiceUntimedChannel[i] = "28";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1759") {
                        $scope.formData.voiceUntimedChannel[i] = "29";
                    }
                    if($scope.formData.voiceUntimedChannel[i] == "1819") {
                        $scope.formData.voiceUntimedChannel[i] = "30";
                    }
                }
                $scope.formData.voiceUntimedFaxToEmail = $scope.viewProposalData.voiceUntimedFaxToEmail;
                $scope.formData.voiceUntimedFaxQty = $scope.viewProposalData.voiceUntimedFaxQty;
                $scope.formData.voiceUntimedDID = $scope.viewProposalData.voiceUntimedDID;
                for(i = 0; i < $scope.formData.voiceUntimedDID.length; i++) {
                    if($scope.formData.voiceUntimedDID[i] == "14.95") {
                        $scope.formData.voiceUntimedDID[i] = "10";
                    }
                    if($scope.formData.voiceUntimedDID[i] == "24.95") {
                        $scope.formData.voiceUntimedDID[i] = "50";
                    }
                    if($scope.formData.voiceUntimedDID[i] == "34.95") {
                        $scope.formData.voiceUntimedDID[i] = "100";
                    }
                }
                $scope.formData.bFUntimedDiscount = $scope.viewProposalData.bFUntimedDiscount;

                 // Cloud Connect Standard
                 $scope.formData.cloudConnectStandardChannel = $scope.viewProposalData.cloudConnectStandardChannel;
                 for(i = 0; i < $scope.formData.cloudConnectStandardChannel.length; i++) {
                     if($scope.formData.cloudConnectStandardChannel[i] == "199") {
                         $scope.formData.cloudConnectStandardChannel[i] = "3";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "259") {
                         $scope.formData.cloudConnectStandardChannel[i] = "4";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "319") {
                         $scope.formData.cloudConnectStandardChannel[i] = "5";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "379") {
                         $scope.formData.cloudConnectStandardChannel[i] = "6";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "439") {
                         $scope.formData.cloudConnectStandardChannel[i] = "7";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "499") {
                         $scope.formData.cloudConnectStandardChannel[i] = "8";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "559") {
                         $scope.formData.cloudConnectStandardChannel[i] = "9";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "619") {
                         $scope.formData.cloudConnectStandardChannel[i] = "10";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "679") {
                         $scope.formData.cloudConnectStandardChannel[i] = "11";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "739") {
                         $scope.formData.cloudConnectStandardChannel[i] = "12";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "799") {
                         $scope.formData.cloudConnectStandardChannel[i] = "13";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "859") {
                         $scope.formData.cloudConnectStandardChannel[i] = "14";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "919") {
                         $scope.formData.cloudConnectStandardChannel[i] = "15";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "979") {
                         $scope.formData.cloudConnectStandardChannel[i] = "16";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1039") {
                         $scope.formData.cloudConnectStandardChannel[i] = "17";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1099") {
                         $scope.formData.cloudConnectStandardChannel[i] = "18";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1159") {
                         $scope.formData.cloudConnectStandardChannel[i] = "19";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1219") {
                         $scope.formData.cloudConnectStandardChannel[i] = "20";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1279") {
                         $scope.formData.cloudConnectStandardChannel[i] = "21";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1339") {
                         $scope.formData.cloudConnectStandardChannel[i] = "22";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1399") {
                         $scope.formData.cloudConnectStandardChannel[i] = "23";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1459") {
                         $scope.formData.cloudConnectStandardChannel[i] = "24";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1519") {
                         $scope.formData.cloudConnectStandardChannel[i] = "25";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1579") {
                         $scope.formData.cloudConnectStandardChannel[i] = "26";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1639") {
                         $scope.formData.cloudConnectStandardChannel[i] = "27";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1699") {
                         $scope.formData.cloudConnectStandardChannel[i] = "28";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1759") {
                         $scope.formData.cloudConnectStandardChannel[i] = "29";
                     }
                     if($scope.formData.cloudConnectStandardChannel[i] == "1819") {
                         $scope.formData.cloudConnectStandardChannel[i] = "30";
                     }
                 }
                 $scope.formData.cloudConnectStandardDID = $scope.viewProposalData.cloudConnectStandardDID;
                 for(i = 0; i < $scope.formData.cloudConnectStandardDID.length; i++) {
                     if($scope.formData.cloudConnectStandardDID[i] == "14.95") {
                         $scope.formData.cloudConnectStandardDID[i] = "10";
                     }
                     if($scope.formData.cloudConnectStandardDID[i] == "24.95") {
                         $scope.formData.cloudConnectStandardDID[i] = "50";
                     }
                     if($scope.formData.cloudConnectStandardDID[i] == "34.95") {
                         $scope.formData.cloudConnectStandardDID[i] = "100";
                     }
                     if($scope.formData.cloudConnectStandardDID[i] == "44.95") {
                        $scope.formData.cloudConnectStandardDID[i] = "150";
                    }
                    if($scope.formData.cloudConnectStandardDID[i] == "54.95") {
                        $scope.formData.cloudConnectStandardDID[i] = "200";
                    }
                 }

                   // Cloud Connect Cap
                   $scope.formData.cloudConnectCapChannel = $scope.viewProposalData.cloudConnectCapChannel;
                   for(i = 0; i < $scope.formData.cloudConnectCapChannel.length; i++) {
                       if($scope.formData.cloudConnectCapChannel[i] == "199") {
                           $scope.formData.cloudConnectCapChannel[i] = "3";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "199") {
                        $scope.formData.cloudConnectCapChannel[i] = "3";
                    }
                       if($scope.formData.cloudConnectCapChannel[i] == "259") {
                           $scope.formData.cloudConnectCapChannel[i] = "4";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "319") {
                           $scope.formData.cloudConnectCapChannel[i] = "5";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "379") {
                           $scope.formData.cloudConnectCapChannel[i] = "6";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "439") {
                           $scope.formData.cloudConnectCapChannel[i] = "7";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "499") {
                           $scope.formData.cloudConnectCapChannel[i] = "8";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "559") {
                           $scope.formData.cloudConnectCapChannel[i] = "9";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "619") {
                           $scope.formData.cloudConnectCapChannel[i] = "10";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "679") {
                           $scope.formData.cloudConnectCapChannel[i] = "11";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "739") {
                           $scope.formData.cloudConnectCapChannel[i] = "12";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "799") {
                           $scope.formData.cloudConnectCapChannel[i] = "13";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "859") {
                           $scope.formData.cloudConnectCapChannel[i] = "14";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "919") {
                           $scope.formData.cloudConnectCapChannel[i] = "15";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "979") {
                           $scope.formData.cloudConnectCapChannel[i] = "16";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1039") {
                           $scope.formData.cloudConnectCapChannel[i] = "17";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1099") {
                           $scope.formData.cloudConnectCapChannel[i] = "18";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1159") {
                           $scope.formData.cloudConnectCapChannel[i] = "19";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1219") {
                           $scope.formData.cloudConnectCapChannel[i] = "20";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1279") {
                           $scope.formData.cloudConnectCapChannel[i] = "21";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1339") {
                           $scope.formData.cloudConnectCapChannel[i] = "22";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1399") {
                           $scope.formData.cloudConnectCapChannel[i] = "23";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1459") {
                           $scope.formData.cloudConnectCapChannel[i] = "24";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1519") {
                           $scope.formData.cloudConnectCapChannel[i] = "25";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1579") {
                           $scope.formData.cloudConnectCapChannel[i] = "26";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1639") {
                           $scope.formData.cloudConnectCapChannel[i] = "27";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1699") {
                           $scope.formData.cloudConnectCapChannel[i] = "28";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1759") {
                           $scope.formData.cloudConnectCapChannel[i] = "29";
                       }
                       if($scope.formData.cloudConnectCapChannel[i] == "1819") {
                           $scope.formData.cloudConnectCapChannel[i] = "30";
                       }
                   }
                   $scope.formData.cloudConnectCapDID = $scope.viewProposalData.cloudConnectCapDID;
                   for(i = 0; i < $scope.formData.cloudConnectCapDID.length; i++) {
                       if($scope.formData.cloudConnectCapDID[i] == "14.95") {
                           $scope.formData.cloudConnectCapDID[i] = "10";
                       }
                       if($scope.formData.cloudConnectCapDID[i] == "24.95") {
                           $scope.formData.cloudConnectCapDID[i] = "50";
                       }
                       if($scope.formData.cloudConnectCapDID[i] == "34.95") {
                           $scope.formData.cloudConnectCapDID[i] = "100";
                       }
                       if($scope.formData.cloudConnectCapDID[i] == "44.95") {
                        $scope.formData.cloudConnectCapDID[i] = "150";
                    }
                    if($scope.formData.cloudConnectCapDID[i] == "54.95") {
                        $scope.formData.cloudConnectCapDID[i] = "200";
                    }
                   }
                // Solution Standard
                $scope.formData.voiceSolutionChannel = $scope.viewProposalData.voiceSolutionChannel;
                for(i = 0; i < $scope.formData.voiceSolutionChannel.length; i++) {
                    if($scope.formData.voiceSolutionChannel[i] == "149") {
                        $scope.formData.voiceSolutionChannel[i] = "3";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "159") {
                        $scope.formData.voiceSolutionChannel[i] = "4";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "169") {
                        $scope.formData.voiceSolutionChannel[i] = "5";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "179") {
                        $scope.formData.voiceSolutionChannel[i] = "6";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "189") {
                        $scope.formData.voiceSolutionChannel[i] = "7";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "199") {
                        $scope.formData.voiceSolutionChannel[i] = "8";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "209") {
                        $scope.formData.voiceSolutionChannel[i] = "9";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "219") {
                        $scope.formData.voiceSolutionChannel[i] = "10";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "229") {
                        $scope.formData.voiceSolutionChannel[i] = "11";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "239") {
                        $scope.formData.voiceSolutionChannel[i] = "12";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "249") {
                        $scope.formData.voiceSolutionChannel[i] = "13";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "259") {
                        $scope.formData.voiceSolutionChannel[i] = "14";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "269") {
                        $scope.formData.voiceSolutionChannel[i] = "15";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "279") {
                        $scope.formData.voiceSolutionChannel[i] = "16";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "289") {
                        $scope.formData.voiceSolutionChannel[i] = "17";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "299") {
                        $scope.formData.voiceSolutionChannel[i] = "18";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "309") {
                        $scope.formData.voiceSolutionChannel[i] = "19";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "319") {
                        $scope.formData.voiceSolutionChannel[i] = "20";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "329") {
                        $scope.formData.voiceSolutionChannel[i] = "21";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "339") {
                        $scope.formData.voiceSolutionChannel[i] = "22";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "349") {
                        $scope.formData.voiceSolutionChannel[i] = "23";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "359") {
                        $scope.formData.voiceSolutionChannel[i] = "24";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "369") {
                        $scope.formData.voiceSolutionChannel[i] = "25";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "379") {
                        $scope.formData.voiceSolutionChannel[i] = "26";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "389") {
                        $scope.formData.voiceSolutionChannel[i] = "27";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "399") {
                        $scope.formData.voiceSolutionChannel[i] = "28";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "409") {
                        $scope.formData.voiceSolutionChannel[i] = "29";
                    }
                    if($scope.formData.voiceSolutionChannel[i] == "419") {
                        $scope.formData.voiceSolutionChannel[i] = "30";
                    }
                }
                $scope.formData.voiceSolutionFaxToEmail = $scope.viewProposalData.voiceSolutionFaxToEmail;
                $scope.formData.voiceSolutionFaxQty = $scope.viewProposalData.voiceSolutionFaxQty;
                $scope.formData.voiceSolutionDID = $scope.viewProposalData.voiceSolutionDID;
                for(i = 0; i < $scope.formData.voiceSolutionDID.length; i++) {
                    if($scope.formData.voiceSolutionDID[i] == "14.95") {
                        $scope.formData.voiceSolutionDID[i] = "10";
                    }
                    if($scope.formData.voiceSolutionDID[i] == "24.95") {
                        $scope.formData.voiceSolutionDID[i] = "50";
                    }
                    if($scope.formData.voiceSolutionDID[i] == "34.95") {
                        $scope.formData.voiceSolutionDID[i] = "100";
                    }
                }
                $scope.formData.bFSolutionDiscount = $scope.viewProposalData.bFSolutionDiscount;
                $scope.formData.bFSolutionCallDiscount = $scope.viewProposalData.bFSolutionCallDiscount;
                // Essential Cap
                $scope.formData.voiceEssentialChannel = $scope.viewProposalData.voiceEssentialChannel;
                for(i = 0; i < $scope.formData.voiceEssentialChannel.length; i++) {
                    if($scope.formData.voiceEssentialChannel[i] == "196") {
                        $scope.formData.voiceEssentialChannel[i] = "4";
                    }
                    if($scope.formData.voiceEssentialChannel[i] == "294") {
                        $scope.formData.voiceEssentialChannel[i] = "6";
                    }
                    if($scope.formData.voiceEssentialChannel[i] == "392") {
                        $scope.formData.voiceEssentialChannel[i] = "8";
                    }
                    if($scope.formData.voiceEssentialChannel[i] == "490") {
                        $scope.formData.voiceEssentialChannel[i] = "10";
                    }
                    if($scope.formData.voiceEssentialChannel[i] == "588") {
                        $scope.formData.voiceEssentialChannel[i] = "12";
                    }
                    if($scope.formData.voiceEssentialChannel[i] == "686") {
                        $scope.formData.voiceEssentialChannel[i] = "14";
                    }
                    if($scope.formData.voiceEssentialChannel[i] == "784") {
                        $scope.formData.voiceEssentialChannel[i] = "16";
                    }
                    if($scope.formData.voiceEssentialChannel[i] == "882") {
                        $scope.formData.voiceEssentialChannel[i] = "18";
                    }
                    if($scope.formData.voiceEssentialChannel[i] == "980") {
                        $scope.formData.voiceEssentialChannel[i] = "20";
                    }
                }
                $scope.formData.voiceEssentialDID = $scope.viewProposalData.voiceEssentialDID;
                for(i = 0; i < $scope.formData.voiceEssentialDID.length; i++) {
                    if($scope.formData.voiceEssentialDID[i] == "9.99") {
                        $scope.formData.voiceEssentialDID[i] = "10";
                    }
                    if($scope.formData.voiceEssentialDID[i] == "15.99") {
                        $scope.formData.voiceEssentialDID[i] = "20";
                    }
                    if($scope.formData.voiceEssentialDID[i] == "19.99") {
                        $scope.formData.voiceEssentialDID[i] = "50";
                    }
                    if($scope.formData.voiceEssentialDID[i] == "25.99") {
                        $scope.formData.voiceEssentialDID[i] = "100";
                    }
                }
                $scope.formData.bECapDiscount = $scope.viewProposalData.bECapDiscount;
                $scope.formData.ipMidbandPlansVoice = $scope.viewProposalData.ipMidbandPlansVoice;
                $scope.formData.ipMidbandDownloadVoice = $scope.viewProposalData.ipMidbandDownloadVoice;
                $scope.formData.ipMidbandDis = $scope.viewProposalData.ipMidbandDis;
                // ADSL 2+
                $scope.formData.adsl2Plans = $scope.viewProposalData.adsl2Plans;
                $scope.formData.adsl2Dis = $scope.viewProposalData.adsl2Dis;
                $scope.formData.voiceCompAnalougeDSL = $scope.viewProposalData.voiceCompAnalougeDSL;
                $scope.formData.voiceCompAnalougeDSLDis = $scope.viewProposalData.voiceCompAnalougeDSLDis;
                $scope.formData.voiceCompAnalougeDisDSL = $scope.viewProposalData.voiceCompAnalougeDisDSL;
                // NBN Unlimited
                $scope.formData.UnlimitedPlans = $scope.viewProposalData.UnlimitedPlans;
                $scope.formData.nbnUnlimitedDis = $scope.viewProposalData.nbnUnlimitedDis;
                $scope.formData.voiceCompAnalougeNBNUnli = $scope.viewProposalData.voiceCompAnalougeNBNUnli;
                $scope.formData.voiceCompAnalougeDisNBNUnli = $scope.viewProposalData.voiceCompAnalougeDisNBNUnli;
                // Midband Monthly
                $scope.formData.ipMidbandPlans = $scope.viewProposalData.ipMidbandPlans;
                $scope.formData.ipMidbandDownload = $scope.viewProposalData.ipMidbandDownload;
                $scope.formData.ipMidbandDis = $scope.viewProposalData.ipMidbandDis;
                // Midband Unlimited
                $scope.formData.ipMidbandUnliPlans = $scope.viewProposalData.ipMidbandUnliPlans;
                $scope.formData.ipMidbandUnliDis = $scope.viewProposalData.ipMidbandUnliDis;
                // Ehternet (EFM) Unlimited
                $scope.formData.ethernetPlans = $scope.viewProposalData.ethernetPlans;
                $scope.formData.ethernetDis = $scope.viewProposalData.ethernetDis;
                // Fibre Unlimited
                $scope.formData.fibreUtPlans = $scope.viewProposalData.fibreUtPlans;
                $scope.formData.fibreDis = $scope.viewProposalData.fibreDis;
                // Telstra 4G Super
                $scope.formData.boltOnPlans = [];
                angular.forEach($scope.viewProposalData.telstra4gSuper,function(value, key){
                    console.log(value);
                    if(value.group != "") {
                        $scope.formData.type.configApp = true;
                        $scope.formData.mobilePortingOptions = true;
                        var BoltOnValue = $scope.viewProposalData.boltOn4gSuper[key]; 
                        console.log(BoltOnValue);
                        if(BoltOnValue == '9.89')  {
                            var  boltOn = $scope.data_bolt_on_plans[0];
                            if (angular.isUndefined($scope.formData.boltOnPlans[0])){
                                $scope.formData.boltOnPlans[0]=1;    
                            } else {
                                $scope.formData.boltOnPlans[0]++;
                            }
                        }
                        if(BoltOnValue == '31.99'){
                            var boltOn = $scope.data_bolt_on_plans[1];
                            if (angular.isUndefined($scope.formData.boltOnPlans[1])){
                                $scope.formData.boltOnPlans[1]=1;    
                            } else {
                                $scope.formData.boltOnPlans[1]++;
                            }
                        }
                        $scope.formData.newMobPort[key] = {
                            'confAccHolder':$scope.configMobPort[$scope.addMobPortCount].confAccHolder,
                            'confMobNo':'',
                            'confProvider':$scope.configMobPort[$scope.addMobPortCount].confProvider,
                            'confAccNo':$scope.configMobPort[$scope.addMobPortCount].confAccNo,
                            'confPatPlan':$scope.viewProposalData.telstra4gSuper[key],
                            boltOn
                        };
                        if (angular.isUndefined($scope.formData.telstraSuperPlans[value.itemIndex])){
                            $scope.formData.telstraSuperPlans[value.itemIndex]=1;    
                        } else {
                            $scope.formData.telstraSuperPlans[value.itemIndex]++;
                        }
                    }
                });

                $scope.configMobPort = [];
                $scope.configMobPort = $scope.formData.newMobPort;
                var sum = 0;
                for (var i = 0; i < $scope.viewProposalData.telstra4gSuperDisPrice.length; i++) {
                    sum += $scope.viewProposalData.telstra4gSuperDisPrice[i]
                }
                $scope.formData.telstra4gSuperDis = sum;

                // Telstra Wireless Super
                $scope.formData.telstraWirelessSuperPlans = $scope.viewProposalData.telstraWirelessSuperPlans;
                $scope.formData.telstraWirelessSuperDis = $scope.viewProposalData.telstraWirelessSuperDis;
                // Telstra Wireless
                $scope.formData.telstraWirelessPlans = $scope.viewProposalData.telstraWirelessPlans;
                $scope.formData.telstraWirelessDis = $scope.viewProposalData.telstraWirelessDis;
                // Mobile Untimed

                if($scope.viewProposalData.type.mobileUt == true) {
                    angular.forEach($scope.viewProposalData.untimed,function(value, key){
                        console.log(value);
                        if(value != null) {
                            $scope.formData.type.configApp = true;
                            $scope.formData.mobilePortingOptions = true;
                            $scope.formData.newMobPort[key] = {
                                'confAccHolder': '',
                                'confMobNo':'',
                                'confProvider': '',
                                'confAccNo': '',
                                'confPatPlan':$scope.viewProposalData.untimed[key]
                            };
                            if (angular.isUndefined($scope.formData.mobile4GUntimedPlans[value.itemIndex])){
                                $scope.formData.mobile4GUntimedPlans[value.itemIndex]=1;    
                            } else {
                                $scope.formData.mobile4GUntimedPlans[value.itemIndex]++;
                            }
                        }
                    });

                    $scope.configMobPort = [];
                    $scope.configMobPort = $scope.formData.newMobPort;
                     
                    $scope.formData.mobileUtDis = $scope.viewProposalData.mobile4GUntimedDisPrice;
                }
               
                // 1300/1800
                $scope.formData.rate131300.qt1800 = $scope.viewProposalData.rate131300.qt1800;
                $scope.formData.qt1800new =  $scope.viewProposalData.qt1800new;
                $scope.formData.rate131300.qt1300 = $scope.viewProposalData.rate131300.qt1300;
                $scope.formData.qt1300new =  $scope.viewProposalData.qt1300new;
                $scope.formData.rate131300.qt13 = $scope.viewProposalData.rate131300.qt13;
                $scope.formData.qt13new =  $scope.viewProposalData.qt13new;
                // 1300/1800 Discount
                $scope.formData.rate131300Dis.qt1800 = $scope.viewProposalData.rate131300Dis.qt1800;
                $scope.formData.qt1800Dis_new = $scope.viewProposalData.qt1800Dis_new;
                $scope.formData.rate131300Dis.qt1300 = $scope.viewProposalData.rate131300Dis.qt1300;
                $scope.formData.qt1300Dis_new = $scope.viewProposalData.qt1300Dis_new;
                $scope.formData.rateDis131300.discount = $scope.viewProposalData.rateDis131300.discount ;

                // console.log($scope.formData.schQty);
                $scope.formData.pIAddress = "";
                $scope.formData.pISuburb = "";
                $scope.formData.pIState = "";
                $scope.formData.pIPostcode = "";
                // $scope.formData.aBillingTerm = 60;
                // $scope.formData.aTerm = 60;
        
                $scope.formData.comLocation = {};
                $scope.formData.comLocation = {
                    'nbf_cap'               : [$scope.formData.pIAddress],
                    'nbf_std'               : [$scope.formData.pIAddress],
                    'voice_comp'              : [$scope.formData.pIAddress],
                    'voice_solution_untimed'  : [$scope.formData.pIAddress],
                    'voice_essentials_cap'    : [$scope.formData.pIAddress],
                    'cloud_connect_standard'  : [$scope.formData.pIAddress],
                    'cloud_connect_cap'       : [$scope.formData.pIAddress],
                    'voice_solution_standard' : [$scope.formData.pIAddress],
                    'data_adsl'               : [$scope.formData.pIAddress],
                    'ip_midband'              : [$scope.formData.pIAddress],
                    'ip_midbandunli'          : [$scope.formData.pIAddress],
                    'nbn'                     : [$scope.formData.pIAddress],
                    'nbnUnlimited'            : [$scope.formData.pIAddress],
                    'mobile_mega'             : [$scope.formData.pIAddress],
                    'mobile_ut'               : [$scope.formData.pIAddress],
                    'mobile_wireless'         : [$scope.formData.pIAddress],
                    '1300'                    : [$scope.formData.pIAddress],
                    '1300_discounted'         : [$scope.formData.pIAddress],
                    'fibre'                   : [$scope.formData.pIAddress],
                    'ethernet'                : [$scope.formData.pIAddress],
                    'telstraUntimed'          : [$scope.formData.pIAddress],
                    'telstraWirelessSuper'    : [$scope.formData.pIAddress],
                    'telstraWireless'         : [$scope.formData.pIAddress],
                    'printer'                 : [$scope.formData.pIAddress],
                    'schedule_of_goods'       : [$scope.formData.pIAddress]
                };
        
                $scope.formData.comSuburb = {};
                $scope.formData.comSuburb = {
                    'nbf_cap'               : [$scope.formData.pISuburb],
                    'nbf_std'               : [$scope.formData.pISuburb],
                    'voice_comp'              : [$scope.formData.pISuburb],
                    'voice_solution_untimed'  : [$scope.formData.pISuburb],
                    'voice_essentials_cap'    : [$scope.formData.pISuburb],
                    'cloud_connect_standard'  : [$scope.formData.pISuburb],
                    'cloud_connect_cap'       : [$scope.formData.pISuburb],
                    'voice_solution_standard' : [$scope.formData.pISuburb],
                    'data_adsl'               : [$scope.formData.pISuburb],
                    'ip_midband'              : [$scope.formData.pISuburb],
                    'ip_midbandunli'          : [$scope.formData.pISuburb],
                    'nbn'                     : [$scope.formData.pISuburb],
                    'nbnUnlimited'            : [$scope.formData.pISuburb],
                    'mobile_mega'             : [$scope.formData.pISuburb],
                    'mobile_ut'               : [$scope.formData.pISuburb],
                    'mobile_wireless'         : [$scope.formData.pISuburb],
                    '1300'                    : [$scope.formData.pISuburb],
                    '1300_discounted'         : [$scope.formData.pISuburb],
                    'fibre'                   : [$scope.formData.pISuburb],
                    'ethernet'                : [$scope.formData.pISuburb],
                    'telstraUntimed'          : [$scope.formData.pISuburb],
                    'telstraWirelessSuper'    : [$scope.formData.pISuburb],
                    'telstraWireless'         : [$scope.formData.pISuburb],
                    'printer'                 : [$scope.formData.pISuburb],
                    'schedule_of_goods'       : [$scope.formData.pISuburb]
                };
        
                $scope.formData.comZipCode = {};
                $scope.formData.comZipCode = {
                    'nbf_cap'               : [$scope.formData.pIPostcode],
                    'nbf_std'               : [$scope.formData.pIPostcode],
                    'voice_comp'              : [$scope.formData.pIPostcode],
                    'voice_solution_untimed'  : [$scope.formData.pIPostcode],
                    'voice_essentials_cap'    : [$scope.formData.pIPostcode],
                    'cloud_connect_standard'  : [$scope.formData.pIPostcode],
                    'cloud_connect_cap'       : [$scope.formData.pIPostcode],
                    'voice_solution_standard' : [$scope.formData.pIPostcode],
                    'data_adsl'               : [$scope.formData.pIPostcode],
                    'ip_midband'              : [$scope.formData.pIPostcode],
                    'ip_midbandunli'          : [$scope.formData.pIPostcode],
                    'nbn'                     : [$scope.formData.pIPostcode],
                    'nbnUnlimited'            : [$scope.formData.pIPostcode],
                    'mobile_mega'             : [$scope.formData.pIPostcode],
                    'mobile_ut'               : [$scope.formData.pIPostcode],
                    'mobile_wireless'         : [$scope.formData.pIPostcode],
                    '1300'                    : [$scope.formData.pIPostcode],
                    '1300_discounted'         : [$scope.formData.pIPostcode],
                    'fibre'                   : [$scope.formData.pIPostcode],
                    'ethernet'                : [$scope.formData.pIPostcode],
                    'telstraUntimed'          : [$scope.formData.pIPostcode],
                    'telstraWirelessSuper'    : [$scope.formData.pIPostcode],
                    'telstraWireless'         : [$scope.formData.pIPostcode],
                    'printer'                 : [$scope.formData.pIPostcode],
                    'schedule_of_goods'       : [$scope.formData.pIPostcode]
                };
                $scope.formData.aIdenQuestion = "";
                $scope.formData.dName = "";
                $scope.formData.cPosition = "";
                $scope.formData.dSName = "";
                $scope.formData.cSPosition = "";
                $scope.formData.cTel = "";
                $scope.formData.cFax = "";
                $scope.formData.cMobile = "";
                $scope.formData.cWebsite = "";
                $scope.formData.aAccountant = "";
                $scope.formData.aIndustry = "";
                $scope.formData.aTradeRef = "";
                $scope.formData.cNemp = "";
                $scope.formData.aCPerson = "";
                $scope.formData.aTel = "";
                $scope.formData.pName = "";
                $scope.formData.pABN = "";
                $scope.formData.pFax = "";
                $scope.formData.pEmail = "";
                $scope.formData.pAddress = "";
                $scope.formData.pSuburb = "";
                $scope.formData.pPostcode = "";
                $scope.formData.pPhone = "";
                $scope.formData.pLicense = "";
                $scope.formData.pValue = "";
                $scope.formData.pMortgage = "";
                $scope.formData.pOwned = "";
                $scope.formData.tTel = "";
                $scope.formData.aContact = "";
                $scope.formData.tContact = "";
                $scope.formData.aNemp = "";
                $scope.formData.aInsurer = "";
                $scope.formData.iEmail = "";
                $scope.formData.docuFee = "";
                $scope.formData.dPayment = "";
                $scope.formData.frequency = "";
                
                $scope.formData.aPolicy = "";
                $scope.formData.g2Name = "";
                $scope.formData.g2Address = "";
                $scope.formData.g2ABN = "";
                $scope.formData.g2Suburb = "";
                $scope.formData.g2Postcode = "";
                $scope.formData.g2Phone = "";
                $scope.formData.g2License = "";
                $scope.formData.g2Value = "";
                $scope.formData.g2Mortgage = "";
                $scope.formData.g2Owned = "";
                $scope.formData.g2Fax = "";
                $scope.formData.g2Email = "";
                $scope.formData.aYear = "";
                $scope.formData.cState = "";
                $scope.formData.aFinInst = "";
                $scope.formData.aFinBranch = "";
                $scope.formData.aAccName = "";
                $scope.formData.aBsb = "";
                $scope.formData.aAccDetails = "";
                $scope.formData.comments = "Referred by: " +  $scope.formData.cName + " " +  $scope.formData.cContact + " " + $scope.formData.cTel;
        
                // $scope.editId = $rootScope.viewProposalData.id;
                // $scope.quot_num = $rootScope.viewProposalData.quot_num;
                // $scope.proposalData = $rootScope.viewProposalData.data;
                
                $scope.proposalData.schItems = $scope.formData.schItems;
                $scope.proposalData.schOption = $scope.formData.schOption;
                $scope.proposalData.schQty = $scope.formData.schQty;
                console.log($scope.proposalData.schItems);
                console.log($scope.proposalData.schOption);
            }

            for (var index=0;index<$scope.formData.voiceStd.length;index++){
                $scope.locationCount = $scope.items['nbf_cap'].length-1;
                $scope.items['nbf_cap'][index] = index;
            }
            for (var index=0;index<$scope.formData.voiceStdChannel.length;index++){
                $scope.locationCount = $scope.items['nbf_std'].length-1;
                $scope.items['nbf_std'][index] = index;
            }
            for (var index=0;index<$scope.formData.voiceCompAnalouge.length;index++){
                $scope.locationCount = $scope.items['voice_comp'].length-1;
                $scope.items['voice_comp'][index] = index;
            }
            for (var index=0;index<$scope.formData.voiceUntimedChannel.length;index++){
                $scope.locationCount = $scope.items['voice_solution_untimed'].length-1;
                $scope.items['voice_solution_untimed'][index] = index;
            }
            for (var index=0;index<$scope.formData.voiceSolutionChannel.length;index++){
                $scope.locationCount = $scope.items['voice_solution_standard'].length-1;
                $scope.items['voice_solution_standard'][index] = index;
            }
            for (var index=0;index<$scope.formData.voiceEssentialChannel.length;index++){
                $scope.locationCount = $scope.items['voice_essentials_cap'].length-1;
                $scope.items['voice_essentials_cap'][index] = index;
            }
            for (var index=0;index<$scope.formData.cloudConnectStandardChannel.length;index++){
                $scope.locationCount = $scope.items['cloud_connect_standard'].length-1;
                $scope.items['cloud_connect_standard'][index] = index;
            }
            for (var index=0;index<$scope.formData.cloudConnectCapChannel.length;index++){
                $scope.locationCount = $scope.items['cloud_connect_cap'].length-1;
                $scope.items['cloud_connect_cap'][index] = index;
            }
            for (var index=0;index<$scope.formData.adsl2Plans.length;index++){
                $scope.locationCount = $scope.items['data_adsl'].length-1;
                $scope.items['data_adsl'][index] = index;
            }
            for (var index=0;index<$scope.formData.UnlimitedPlans.length;index++){
                $scope.locationCount = $scope.items['nbnUnlimited'].length-1;
                $scope.items['nbnUnlimited'][index] = index;
            }
            for (var index=0;index<$scope.formData.ipMidbandPlans.length;index++){
                $scope.locationCount = $scope.items['ip_midband'].length-1;
                $scope.items['ip_midband'][index] = index;
            }
            for (var index=0;index<$scope.formData.ipMidbandUnliPlans.length;index++){
                $scope.locationCount = $scope.items['ip_midbandunli'].length-1;
                $scope.items['ip_midbandunli'][index] = index;
            }
            for (var index=0;index<$scope.formData.ethernetPlans.length;index++){
                $scope.locationCount = $scope.items['ethernet'].length-1;
                $scope.items['ethernet'][index] = index;
            }
            for (var index=0;index<$scope.formData.fibreUtPlans.length;index++){
                $scope.locationCount = $scope.items['fibre'].length-1;
                $scope.items['fibre'][index] = index;
            }

            
            for (var index=0;index<$scope.formData.telstraWirelessSuperPlans.length;index++){
                $scope.locationCount = $scope.items['telstraWirelessSuper'].length-1;
                $scope.items['telstraWirelessSuper'][index] = index;
            }
            for (var index=0;index<$scope.formData.telstraWirelessPlans.length;index++){
                $scope.locationCount = $scope.items['telstraWireless'].length-1;
                $scope.items['telstraWireless'][index] = index;
            }
            for (var index=0;index<$scope.formData.rate131300.qt1800.length;index++){
                $scope.locationCount = $scope.items['1300'].length-1;
                $scope.items['1300'][index] = index;
            }
            for (var index=0;index<$scope.formData.rate131300Dis.qt1800 .length;index++){
                $scope.locationCount = $scope.items['1300_discounted'].length-1;
                $scope.items['1300_discounted'][index] = index;
            }
          });
    }
   

    $scope.tinymceOptions = {
      selector: "textarea",
      theme: "modern",
      paste_data_images: true,
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      toolbar2: "print preview media | forecolor backcolor emoticons "
    };
    
    //console.log($stateParams.id);
    // alert($stateParams.id);
    // if ($stateParams.id) {
    //     Proposals.get({
    //         id: $stateParams.id
    //     }, function(data) {
    //         $scope.formData.cName = data.companyName;
    //         $scope.formData.aTerm = data.aTerm;
    //         $scope.formData.dID[0] = data.diD;
    //         $scope.formData.cEmail = data.emailAddress;
    //         $scope.formData.cTrading = data.businessName;
    //         $scope.formData.cTel = data.contactNumber;
    //         $scope.formData.schOption = data.schOption;
    //         $scope.formData.schItems = data.schItems;
    //         // alert($scope.formData);
    //         Form.save({data:$scope.formData, user : user.id, fileName : '',
    //                 editId : $scope.editId, type:"draft"}, function(data) {
    //                     $scope.editId = data.editId;
    //         });
    //     });
    // }
        // $scope.formData.cEmail = "info@somelongcompany.com.au";
        // $scope.formData.dName = "Arthur Conan Doyle";
        // $scope.formData.cPosition = "";
        // $scope.formData.dSName = "Sherlock Holmes";
        // $scope.formData.cSPosition = "";
        // $scope.formData.cContact = "Arthur Conan Doyle";
        // $scope.formData.cTel = "(02) 4342-1232";
        // $scope.formData.cFax = "(02) 4342-1232";
        // $scope.formData.cMobile = "4532-533-267";
        // $scope.formData.cWebsite = "http://somelongcompany.com.au";
        // $scope.formData.aAccountant = "James Moriarty";
        // $scope.formData.aIndustry = "PROFESSIONAL OFFICE";
        // $scope.formData.aTradeRef = "A Study in Scarlet";
        // $scope.formData.aCPerson = "Dr. Watson";
        // $scope.formData.aTel = "(02) 2340-5444";
        // $scope.formData.pName = "Sherlock Holmes";
        // $scope.formData.pAddress = "220B Baker Street";
        // $scope.formData.pSuburb = "London";
        // $scope.formData.pPostcode = 2000;
        // $scope.formData.pPhone = "(02) 4342-1232";
        // $scope.formData.pLicense = "2332323243A";
        // $scope.formData.pValue = "$ 600,000";
        // $scope.formData.pMortgage = "$ 400,000";
        // $scope.formData.pOwned = true;
        // $scope.formData.tTel = "(02) 4342-1232";
        // $scope.formData.aContact = "Mrs. Hudson";
        // $scope.formData.tContact = "Mr. Hudson";
        // $scope.formData.aNemp = "1000";
        // $scope.formData.aInsurer = "Insurance Company";
        // $scope.formData.aPolicy = "A/127/2016";

    $scope.getRates = function(){
      $scope.formData.term = $('#term').val();
      var rates = Form.getRates({term:$scope.formData.term}, function(data) {
            if (data.success) {
            // $scope.formData.terms = data;
           }
           // console.log($scope.formData.terms);

    });
  }

    $scope.changeFibreTerm = function (fibrePlan,index){
      //console.log(fibrePlan);
      switch (fibrePlan.price){
        case 599 :
        case 699 : 
        case 1199 : 
        case 758 :
        case 1599 :
        case 1699 : $scope.formData.fibreTerm[index] = 36;
                    break;
        case 1899 : $scope.formData.fibreTerm[index] = 24;
                    break;
        case 1299 : $scope.formData.fibreTerm[index] = 48;
                    break;
      }
      console.log($scope.formData);
    };

    $scope.addNewReferal = function() {
        $scope.formData.referals.push($scope.referalCount++);
    };  
        
    $scope.removeReferal = function(index) {
        $scope.formData.referals.splice(index,1);
        $scope.formData.rBusinessName.splice(index,1);
        $scope.formData.rContactPerson.splice(index,1);
        $scope.formData.rPhoneNumber.splice(index,1);
    };

    // $scope.draftMode = function (val){
    //     $rootScope.isDraftMode = val;
    //     $scope.closeThisDialog();
    // }

    $scope.updateCardExpiry = function (year,month){
        var monthNum=month + "";
        while (monthNum.length < 2)
            monthNum = "0" + monthNum;
        $scope.formData.aCardExpiry = monthNum + '/' + year;
        //alert($scope.formData.aCardExpiry);
    }

    // $scope.preSignDocument = function() {
    //     $scope.template = "templates/forms/pre_sign.html";
    //     ngDialog.open({
    //         template: $scope.template,
    //         plan: true,
    //         controller : 'FormDataCtrl',
    //         width: 500,
    //         height: 500,
    //         scope: $scope,
    //         className: 'ngdialog-theme-default presign',
    //         showClose: true,
    //         closeByEscape: true,
    //         closeByDocument: true,
    //         preCloseCallback : function(value) {
    //             var signData = $('img.imported').attr('src');
    //             if ((!signData) && (!$rootScope.isDraftMode)) {
    //                 if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
    //                     return true;
    //                 }
    //                 return false;
    //                 $timeout(angular.noop());
    //             }
    //         }
    //     });
    // }

    $scope.orderNumberDate = function(){
        var currentDate = new Date();   
        return ("0" + (currentDate.getMonth() + 1)).slice(-2)+""+("0" + currentDate.getDate()).slice(-2);
    }

    $scope.orderNumberComp = function(){
      var name = $scope.formData.cName
      var name2 = $scope.formData.cName.substring(0,3).toUpperCase();
      return name2;
    }


    $scope.sendAsDraft = function(){
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var formHtmlData = new Array();
        var fileNameArr = new Array();
        //$interval.cancel(saveDraft);

        if (!angular.isDefined($scope.htmlData) || angular.equals({},$scope.htmlData)) {
          FlashMessage.setMessage({success:false, message:"No Forms Selected"});
          return;
        }
        $("#loading").show();
        angular.forEach($scope.htmlData, function(value,key) {
            var nametail = new Date().getTime();
            var rand = Math.floor((Math.random()*1000)+10);
            var fileName = key+rand+nametail+"-draft.pdf";
            fileNameArr.push(fileName);
            $("#downloadPdfBn").prop('disabled',true);
            formHtmlData.push({html:value, fileName : fileName});

        });
        if($scope.ord_num){
          $scope.ord_num = $scope.orderNumberDate() + $scope.orderNumberComp();
        }
        var pdf = Form.update({htmlData :formHtmlData,
            userDetails : { name : $scope.formData.dName,
                                            email :$scope.formData.cEmail},
            account: {name: userProfile.name, email: userProfile.email}},
            function(data) {
            if($rootScope.editId) {
               var editId = $rootScope.editId;
               $rootScope.editId = null;
            }
            Form.save({ data : $scope.formData,
                        user : userProfile.id,
                        editId : editId,
                        fileName : fileNameArr,
                        ord_num : $scope.ord_num,
                        type:"draft"}, function(data) {
                            if (data.success) {
                                //$scope.formData = {};
                                $("#loading").hide();
                                FlashMessage.setMessage({
                                    success:true,
                                    message:"The draft forms will be sent to your email shortly."
                                });
                            }
                        });
             });
    }

    $scope.saveDraftForm = function(templateName,formData) {
        $scope.template = "templates/forms/" + templateName + ".html?t=" + _version;
        //setting the values changed by Script.
        formData.dateVal = $("#dateVal").val();
        formData.pBirth  = $("#pBirth").val();
        formData.pIAddress = $("#pIAddress").val();
        formData.pIState = $("#pIState").val();
        formData.pIPostcode =  $("#pIPostcode").val();
        formData.aPeriod = 'monthly';
        formData.pISuburb =  $("#pISuburb").val();
        $rootScope.draftMode = true;
        $scope.userSign = "";
        // Check if exists empty location fields.
        var comLocationObj = $('input.' + templateName);
        var emptyLocationCnt = 0;
        angular.forEach(comLocationObj, function(item, index) {
            var location = $(item).val();
            if (!location) {
                emptyLocationCnt ++;                
            }
        });

        if (emptyLocationCnt) {
            return;
        }

        $(".reAssign").each(function() {
            var id = $(this).prop("id");
            formData[id] = $(this).val();
        });

        formData["type"] = {};

        $("input[name=formTypes]:checked").each(function() {
            var id = $(this).prop("id");
            formData["type"][id] = $(this).prop("checked");
        });

        if (formData.type.configApp) {
            var telNo = new Array();
            $(".telNo").each(function() {
                  var item = $(this).find("input");
                  var idCount = item.data('count');
                  var telRange = $('#telItemRange' + '-' + idCount).val()!=''?' to '+$('#telItemRange' + '-' + idCount).val():'';
                  var obj = {
                                  tel : item[0].value + telRange 
                            };
                  telNo.push(obj);
            });
            $scope.formData.configAppTelNo = telNo;
            // var mobPort = new Array();
            // $(".mob-port-box").each(function() {
            //     var confAccHolder = $(this).find(".confAccHolder");
            //     var confMobNo = $(this).find(".confMobNo");
            //     var confProvider = $(this).find(".confProvider");
            //     var confAccNo = $(this).find(".confAccNo");
            //     var confPatPlan = $(this).find(".confPatPlan");
            //     var confDob = $(this).find(".confDob");
            //     var confDLicence = $(this).find(".confDLicence");

            //     var obj = {
            //                     confAccHolder : confAccHolder[0].value,
            //                     confMobNo : confMobNo[0].value,
            //                     confProvider : confProvider[0].value,
            //                     confAccNo : confAccNo[0].value,
            //                     confPatPlan : confPatPlan[0].value,
            //                     confDob : confDob[0].value,
            //                     confDLicence : confDLicence[0].value,
            //               };
            //     mobPort.push(obj);
            // });
            $scope.formData.configMobPort = $scope.configMobPort;
        }

        if(formData.type.rental || formData.type.leasing ||formData.singleOrder || formData.type.rpm || formData.type.rental2 || formData.type.classic) {
            formData["rental"] = {};
            var rental = parseInt(formData.aPayment.replace(/[^\d.]/g,''));
            formData["aGST"] = isNaN(rental) ? '0.00' : (rental*10/100).toFixed(2);
            formData["aTotal"] = isNaN(rental) ? '0.00' : (rental + rental*10/100).toFixed(2);
        }

        // var schedule = new Array();
        // var scheduleOptions = new Array();
        // angular.forEach($scope.formData.schItems, function(data) {
        //     if (angular.isDefined(formData.schOption)) {
        //         var obj = {
        //                     qty : formData.schQty[data.count],
        //                     desc :formData.schOption[data.count].item != '_' ? formData.schOption[data.count].item : ''
        //                 };
        //         scheduleOptions.push(obj);
        //     }
        // });

        // $(".schedule-goods").each(function() {
        //     var item = $(this).find("input");
        //     var obj = { qty : item[0].value,
        //                 desc :item[1].value,
        //                 //itemNo : item[2].value,
        //                 //serNo : item[3].value
        //             };
        //     schedule.push(obj);
        // });

        // $scope.formData.scheduleOptions = scheduleOptions;
        // $scope.formData.schedule = schedule;

        if($scope.editId) {
            $rootScope.editId = $scope.editId;
        }

        $rootScope.formData = formData;

        ngDialog.open({
            template: $scope.template,
            plan: true,
            controller : 'FormCtrl',
            width: 900,
            height: 600,
            scope: $scope,
            className: 'ngdialog-theme-default',
            preCloseCallback : function(value) {
                if (confirm("This will create a draft form. Continue?")) {
                    $scope.htmlData[templateName] = $(".ngdialog-content").html();
                    $scope[templateName] = true;
                    $timeout(angular.noop());
                    $('#isSigned').val('T');
                    $('#sendDraft').show();
                }
            }
        });

        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            var newHash = 'anchor';
            if ($location.hash() !== newHash) {
                // set the $location.hash to `newHash` and
                // $anchorScroll will automatically scroll to it
                $location.hash('anchor');
            } else {
                // call $anchorScroll() explicitly,
                // since $location.hash hasn not changed
                $location.hash('anchor');
                $anchorScroll();
            }
        });
    }
    
    $scope.sendDraftForms = function($id) {

        var forms = Form.downloadForms({id:id}, function(data) {
           if (data.success) {
               angular.forEach(data.files, function(name, index){
                    var filename = name.substr(0, name.indexOf('.')) + '.pdf'; 
                    window.open("assets/files/"+filename,'_blank');
               });
           }
        });
    }

    // setTimeout(function() {
    //    if (!$('.ngdialog.presign').length) {
    //         $scope.preSignDocument();
    //     }
    // }, 1000);

    $scope.changeColTransparent = function(val) {
        console.log(val);
        if ((val.item == '_') || (val.plan=='_')) {
            $('.col-sel-desc .chosen-container').addClass('transparent');
        } else {
            $('.col-sel-desc .chosen-container').removeClass('transparent');
        }

        $scope.formData.telstraSuperPlans = [];
        $scope.telstraPlans = [];
        $scope.mobileMegaPlans = [];
        $scope.formData.mobile4GUntimedPlans = [];
        //console.log($scope.configMobPort);
        if (angular.isDefined(val.plan)){
            angular.forEach($scope.configMobPort,function(item,index){
                console.log(item);
                if (item.confPatPlan.group=="Telstra 4G Super"){
                    $scope.configMobPort[index].status = "1";
                    if (angular.isUndefined($scope.formData.telstraSuperPlans[item.confPatPlan.itemIndex]))
                        $scope.formData.telstraSuperPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.formData.telstraSuperPlans[item.confPatPlan.itemIndex]++;

                } else if (item.confPatPlan.group=="Telstra 4G"){
                    if (angular.isUndefined($scope.telstraPlans[item.confPatPlan.itemIndex]))
                        $scope.telstraPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.telstraPlans[item.confPatPlan.itemIndex]++;
                } else if(item.confPatPlan.group=="Mobile Mega"){
                    if (angular.isUndefined($scope.mobileMegaPlans[item.confPatPlan.itemIndex]))
                        $scope.mobileMegaPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.mobileMegaPlans[val.itemIndex]++;
                    item.boltOn = {};
                } else if(item.confPatPlan.group=="Mobile 4G Untimed"){
                    if (angular.isUndefined($scope.formData.mobile4GUntimedPlans[item.confPatPlan.itemIndex]))
                        $scope.formData.mobile4GUntimedPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.formData.mobile4GUntimedPlans[item.confPatPlan.itemIndex]++;
                    item.boltOn = {};
                }
            });
            // console.log($scope.telstraPlans);
            // console.log($scope.mobileMegaPlans);
            // console.log($scope.mobile4GUntimedPlans);
            $scope.processBoltOnCount();
            //console.log($scope.mobileMegaPlans);
        } 
        console.log($scope.formData);
        $scope.formData.maintenanceAgreementCost = 0;
        angular.forEach($scope.formData.schOption[0], function(value, key) {
            console.log(key);
            if(value.item == 'NEC SV9100 Handset 24 Button Digital' || value.item == 'NEC SV9100 Handset 24 Button IP' || value.item == 'NEC 24 Button Digital Handset' 
            || value.item == 'NEC 24 Button IP Handset' || value.item == 'Panasonic NS700 Handset 24 Button Digital' || (value.group == 'Panasonic NS700' && value.price == '955.55') 
            || (value.group == 'Panasonic NS700' && value.price == '655.40') || value.item == 'Panasonic NS700 NT680 Handset' || value.item == 'Cordless Handset' 
            || value.item == 'Cordless Handset with repeater' || value.item == 'CISCO IP Phone 8845 HD Video conf Handset') {
                var itemCost = $scope.formData.schQty[0][key] * 5;
                console.log($scope.formData.schQty[0][key]);
            $scope.formData.maintenanceAgreementCost += itemCost;
            }
        });
        console.log($scope.formData.maintenanceAgreementCost);
    }

    $scope.processBoltOnCount = function(){
        $scope.formData.boltOnPlans = [];
        angular.forEach($scope.configMobPort,function(item,index){
            //console.log(item.boltOn.itemIndex);
            if (angular.isDefined(item.boltOn.itemIndex)){
                if (angular.isUndefined($scope.formData.boltOnPlans[item.boltOn.itemIndex])){
                    $scope.formData.boltOnPlans[item.boltOn.itemIndex]=1;    
                } else {
                    $scope.formData.boltOnPlans[item.boltOn.itemIndex]++;
                }
            }
        });
    }

    if ($rootScope.viewFormData != null) {
        if (angular.isDefined($rootScope.viewFormData)) {
            $scope.editId = $rootScope.viewFormData.id;
        }
    }

    Form.getScheduleGoods(function(data) {
        $scope.scheduleGoodsList = data.goods;
        //console.log(data.goods);
        $scope.scheduleGoodsList.unshift({
            item: "_"
        });
    });


    function onChangeTemp(e)
    {
        //console.log(e);
    }

    $scope.handleTermChange = function (formData) {
        formData.promotedText = '';
        if ( formData.aTerm == '48promoted') {
              formData.aTermNew = 48;
              formData.PromotedText = "(First 3 Payments $0, followed by 45 payments of rental amount)"
        }
        if  (formData.aTerm =='60promoted') {
            formData.aTermNew = 60;
            formData.PromotedText = "(First 3 Payments $0, followed by 57 payments of rental amount)"
        }
        if  (formData.aTerm =='60') {
            formData.aTermNew = 60;
        }
        if (formData.aTerm =='48') {
            formData.aTermNew = 48;
        }
        if (formData.aTerm =='36') {
            formData.aTermNew = 36;
        }
        if  (formData.aTerm =='24') {
            formData.aTermNew = 24;
        }
        if (formData.aTerm =='Outright') {
            formData.aTermNew = "Outright";
        }
        return formData
    }

    $(".validUrl").on("keyup", function() {
        var val = this.value.replace(/^(http\:\/\/)/,'');
        this.value = "http://"+val;
    });

    $(".onlyAlpha").on("keyup", function() {
       var val = this.value.replace(/\d/g,'');
       this.value = val;
    });
     $(".onlyNumber").on("keyup", function() {
       var val = this.value.replace(/\D/g,'');
       this.value = val;
    });

    $(".dollarSign").on("keyup", function() {
        var val = this.value.replace(/\D/g,'');
        var parts = val.toString().split(".");
        parts =  parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        this.value = "$ "+parts;

        if (parts == "" || parts == "0") {
            this.value = "$ 0";
        } else {
            if (parts.indexOf("0") == 0) {
                parts = parts.replace("0","");
            }
            this.value = "$ "+parts;
        }
    });

    $("#dName").on("focusout", function() {
        $("#cContact").val($(this).val());
        $("#pName").val($(this).val());
    });

    $('#dSName').on("focusout", function() {
        $("#g2Name").val($(this).val());
    });

    $http.get("profile").success(function(response) {
        $scope.formData.wName = response.name;
        $scope.formData.userSign = response.sign;
    });

    $scope.transformTelFields  = function() {
        $('.tel').on("keyup", function()
        {
                var val = this.value.replace(/[^0-9]/ig, '');
                if (val.length >10) {
                    val = val.substr(0,10);
                }
                var newVal = '';
                if (val.length >2) {
                  newVal +='('+val.substr(0,2)+') ';
                  val = val.substr(2);
                }
                while (val.length > 4)   {
                  newVal += val.substr(0, 4) +'-';
                  val = val.substr(4);
                }
                newVal += val;
                this.value = newVal;
        });
        $('.telRange').on("keyup",function(){
            var val = this.value.replace(/[^0-9]/ig, '');
            if (val.length>4) {
                val = val.substr(0,4);
            }
            this.value = val;            
        });
    }

    $scope.addTel = function() {
        $timeout(function(){
            //var content = $(".addTelBtn").siblings(".telNo");
            var content = '';
            content = '<div class="row telNo form-row">\n';
            content +='     <div class="row-element-label">\n\
                                 <label class="control-label">Tel No</label>\n\
                                  \n\<i id="adTel-' + $scope.addTelCount + '" class="fa fa-times"></i>\n\
                             </div>\n\
                             <div class="row-element-field">\n\
                                <div class="col-lg-6 col-md-12">\n\
                                    <input class="telItem tel form-control" data-count="' + $scope.addTelCount + '" id="telItemConfigApp-' + $scope.addTelCount + '" type="text" autofocus placeholder="From">\n\
                                </div>\n\
                                <div class=" col-lg-6 col-md-12">\n\
                                    <input class="telItem telRange form-control" data-count="' + $scope.addTelCount + '" id="telItemRange-' + $scope.addTelCount + '" type="text" autofocus placeholder="To">\n\
                                </div>\n\
                            </div>\n\
                        </div>';
            $(".addTelBtn").before(content);
            $("#telItemConfigApp-"+$scope.addTelCount).focus();
            $("#adTel-"+$scope.addTelCount).click(function() {
                  $(this).parent().parent().remove();
                  $scope.addTelCount--;
              });
            $scope.transformTelFields();
            $scope.addTelCount++;
        });

    }

    $scope.addAccount = function(){
       $newAccount = {
            'configCarrier':'',
            'configAccNo':'',
        };
        $scope.accountLocation[index]++;
        $scope.formData.accounts.push($newAccount);
        // $scope.accountLocation++;
        console.log($scope.formData.accounts);
    }
    $scope.removeAccount = function(index) {
        $scope.formData.accounts.splice(index,1);
    }

    $scope.addPortingAuth = function() {
        // $timeout(function(){
        $newMobPort = {
            'confAccHolder':$scope.formData.configMobPort[$scope.addMobPortCount].confAccHolder,
            'confMobNo': '',
            'confProvider':$scope.formData.configMobPort[$scope.addMobPortCount].confProvider,
            'confAccNo':$scope.formData.configMobPort[$scope.addMobPortCount].confAccNo,
            'confPatPlan':$scope.mobile_rate_plans[0],
            'boltOn' : {},
            'status' : ''
        };
        $scope.addMobPortCount[index]+1;
        $scope.configMobPort.push($newMobPort);
    }

    $scope.customerSign = function(value) {
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        console.log($scope.formData.customersign);
            var x=window.confirm("Do you want to enable customer signature? The customer will receive an email")
        if (x) {
            $('body').append('<div class="loading"></div>');
            $scope.loading = true;
            
            $scope.formData.customersign = true;
             var sendCustomer =  Form.sendCustomerEmail({ 
                    data : $scope.formData,
                    userEmail : userProfile.email,
                    editId : $scope.editId,
                    ord_num : $scope.ord_num,
                    type:"Completed"}, function(data) {
                if (data.success) {
                    $scope.formData = {};
                    $scope.loading = false;
                    if ($('#form_complete').val()=='F'){
                      $scope.template = "templates/forms/summary.html";
                      ngDialog.open({
                        
                           template: $scope.template,
                           plan: true,
                           controller : 'FormDataCtrl',
                           width: '22cm',
                           className: 'ngdialog-theme-default',
                           showClose: true,
                           closeByEscape: true,
                           closeByDocument: true
                       });
                    } else {
                      FlashMessage.setMessage({
                        success:true,
                        message:"The customer will receive an email shortly."}
                      );
                      alert("The customer will receive an email shortly.");
                      $window.location.reload();
                    }
                }
            });
        } else {
            $scope.formData.customersign = false;
        }
    }
    $scope.saveAsDraft = function(formData) {
        formData.pBirth = $('#pBirth').val();
        //formData.aBillingTerm = $('#aBillingTerm').val();
        //formData.aIdenQuestion = $('#aIdenQuestion').val();
        // formData.bfCapDiscount = $('#bFCapDiscount').val();
        // console.log(formData);
        // console.log(formData);
        // console.log($scope.formData);
        // console.log($scope);
        if(angular.isUndefined($scope.formData.proposal_id)) {
            var proposal_id = "";
        } else {
            var proposal_id = $scope.formData.proposal_id;
        }
        if (!$scope.editId) {
            $scope.editId = "";
        }
        var user = Scopes.get('DefaultCtrl').profile;

        $(".reAssign").each(function() {
            var id = $(this).prop("id");
            if (id=='alarmPlans'){
                var alarmPlan = $scope.formData.alarmPlans;
                formData[id] = $(this).val();
                $scope.formData.alarmPlans = alarmPlan;
            } else {
                formData[id] = $(this).val();
            }
            if (id == "aPayout" && !$(this).val())
                formData[id] = "$ 0";

            if (id == "documentationFee" && !$(this).val()) {
                formData[id] = "$ 0";
            }
        });

        formData.configMobPort = $scope.configMobPort;
        if(angular.isUndefined($scope.ord_num)){
          $scope.ord_num = $scope.orderNumberDate() + $scope.orderNumberComp();
        }
        // console.log($scope.ord_num);
        Form.save({data:formData, user : user.id, fileName : '', proposal_id: proposal_id,
                    editId : $scope.editId, type:"draft", ord_num : $scope.ord_num}, function(data) {
                        $scope.editId = data.editId;
        });

        $scope.processcloudRateCard();
    }

    $scope.prepDiscounts = function(payment) {
      payment = payment.replace(/^\D+|\,+/g, '');
       $scope.EqPayment = payment;
      }

    $scope.updateComLocations = function (){
        $scope.formData.comLocation = {
            'nbf_cap'               : [$scope.formData.pIAddress],
            'nbf_std'               : [$scope.formData.pIAddress],
            'voice_comp'              : [$scope.formData.pIAddress],
            'voice_solution_untimed'  : [$scope.formData.pIAddress],
            'voice_essentials_cap'    : [$scope.formData.pIAddress],
            'cloud_connect_standard'  : [$scope.formData.pIAddress],
            'cloud_connect_cap'       : [$scope.formData.pIAddress],
            'voice_solution_standard' : [$scope.formData.pIAddress],
            'data_adsl'               : [$scope.formData.pIAddress],
            'ip_midband'              : [$scope.formData.pIAddress],
            'ip_midbandunli'          : [$scope.formData.pIAddress],
            'nbn'                     : [$scope.formData.pIAddress],
            'nbnUnlimited'            : [$scope.formData.pIAddress],
            'mobile_mega'             : [$scope.formData.pIAddress],
            'mobile_ut'               : [$scope.formData.pIAddress],
            'mobile_wireless'         : [$scope.formData.pIAddress],
            '1300'                    : [$scope.formData.pIAddress],
            '1300_discounted'         : [$scope.formData.pIAddress],
            'fibre'                   : [$scope.formData.pIAddress],
            'ethernet'                : [$scope.formData.pIAddress],
            'telstraUntimed'          : [$scope.formData.pIAddress],
            'telstraWirelessSuper'    : [$scope.formData.pIAddress],
            'telstraWireless'         : [$scope.formData.pIAddress],
            'printer'                 : [$scope.formData.pIAddress],
            'schedule_of_goods'       : [$scope.formData.pIAddress]
        };
    }

    $scope.updateComSuburbs = function (){
        $scope.formData.comSuburb = {
            'nbf_cap'               : [$scope.formData.pISuburb],
            'nbf_std'               : [$scope.formData.pISuburb],
            'voice_comp'              : [$scope.formData.pISuburb],
            'voice_solution_untimed'  : [$scope.formData.pISuburb],
            'voice_essentials_cap'    : [$scope.formData.pISuburb],
            'cloud_connect_standard'  : [$scope.formData.pISuburb],
            'cloud_connect_cap'       : [$scope.formData.pISuburb],
            'voice_solution_standard' : [$scope.formData.pISuburb],
            'data_adsl'               : [$scope.formData.pISuburb],
            'ip_midband'              : [$scope.formData.pISuburb],
            'ip_midbandunli'          : [$scope.formData.pISuburb],
            'nbn'                     : [$scope.formData.pISuburb],
            'nbnUnlimited'            : [$scope.formData.pISuburb],
            'mobile_mega'             : [$scope.formData.pISuburb],
            'mobile_ut'               : [$scope.formData.pISuburb],
            'mobile_wireless'         : [$scope.formData.pISuburb],
            '1300'                    : [$scope.formData.pISuburb],
            '1300_discounted'         : [$scope.formData.pISuburb],
            'fibre'                   : [$scope.formData.pISuburb],
            'ethernet'                : [$scope.formData.pISuburb],
            'telstraUntimed'          : [$scope.formData.pISuburb],
            'telstraWirelessSuper'    : [$scope.formData.pISuburb],
            'telstraWireless'         : [$scope.formData.pISuburb],
            'printer'                 : [$scope.formData.pISuburb],
            'schedule_of_goods'       : [$scope.formData.pISuburb]
        };
    }

    $scope.updateComPostCode = function (){
        $scope.formData.comZipCode = {
            'nbf_cap'               : [$scope.formData.pIPostcode],
            'nbf_std'               : [$scope.formData.pIPostcode],
            'voice_comp'              : [$scope.formData.pIPostcode],
            'voice_solution_untimed'  : [$scope.formData.pIPostcode],
            'voice_essentials_cap'    : [$scope.formData.pIPostcode],
            'cloud_connect_standard'  : [$scope.formData.pIPostcode],
            'cloud_connect_cap'       : [$scope.formData.pIPostcode],
            'voice_solution_standard' : [$scope.formData.pIPostcode],
            'data_adsl'               : [$scope.formData.pIPostcode],
            'ip_midband'              : [$scope.formData.pIPostcode],
            'ip_midbandunli'          : [$scope.formData.pIPostcode],
            'nbn'                     : [$scope.formData.pIPostcode],
            'nbnUnlimited'            : [$scope.formData.pIPostcode],
            'mobile_mega'             : [$scope.formData.pIPostcode],
            'mobile_ut'               : [$scope.formData.pIPostcode],
            'mobile_wireless'         : [$scope.formData.pIPostcode],
            '1300'                    : [$scope.formData.pIPostcode],
            '1300_discounted'         : [$scope.formData.pIPostcode],
            'fibre'                   : [$scope.formData.pIPostcode],
            'ethernet'                : [$scope.formData.pIPostcode],
            'telstraUntimed'          : [$scope.formData.pIPostcode],
            'telstraWirelessSuper'    : [$scope.formData.pIPostcode],
            'telstraWireless'         : [$scope.formData.pIPostcode],
            'printer'                 : [$scope.formData.pIPostcode],
            'schedule_of_goods'       : [$scope.formData.pIPostcode]
        };
    }

    $scope.showPlan = function(plan){
        //console.log(plan);
    }

    $scope.calculateDiscount = function(currentVal,plan, planOptionId, completeOptions,optionId) {
        var payment = 0;
        var capTotalVal =0;
        //console.log(planOptionId);
        if (planOptionId != '') {
            var planStr = $("#"+planOptionId+" option:selected").text();
            var planVal =parseInt(planStr.match(/\$(\d+)/)[1]);
        }
        currentVal = parseInt(currentVal);
        //alert(currentVal);
        if(angular.isDefined($scope.EqPayment)) {
            payment = parseInt($scope.EqPayment);
        }
        //alert(completeOptions);
        var c_options = angular.isDefined(completeOptions)?completeOptions:0;
        //alert(c_options);
        $(".main-disc").each(function() {
            capTotalVal += parseInt($(this).val());
        });
        if (plan == 'voice-cap' || plan == 'data-plan' || plan == 'mobile-plan' ) {
            if (currentVal > planVal) {
                alert("Discount cannot exceed monthly payment");
                //$('#' + optionId).val('');
            } else {
                if(capTotalVal  == currentVal) {
                    if (currentVal> planVal/2) {
                        var disc = planVal/2;
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                } else {
                    var remaining = payment-(capTotalVal-currentVal);
                    if (remaining<0) {
                        remaining = 0;
                    }

                    var disc = remaining + planVal*30/100;
                    if (currentVal > disc) {
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                }
            }
        } else if (plan == 'standard-plan') {
            if (currentVal>100) {
                alert("Maximum Discount should not go over $100");
               // $('#' + optionId).val('');
            }
        }  else if (plan == 'complete-bri') {
            if (currentVal>50*c_options) {
                alert("Maximum Discount should not go over $"+50*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-pri') {
            if (currentVal>100*c_options) {
                alert("Maximum Discount should not go over $"+100*c_options);
               // $('#' + optionId).val('');
            }
        } else if (plan == 'complete-analog') {
            if (currentVal>(10*c_options)) {
                alert("Maximum Discount should not go over $" + 10*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-call' || plan == '1300-plan' || plan == 'standard-call') {
            if (currentVal> payment) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            }
        } else if (plan == 'unlimited-plan') {
            if (currentVal> planVal) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            } else {
                if (currentVal> planVal/2) {
                    var disc = planVal/2;
                    alert("Maximum Discount should not go over $"+disc);
                   // $('#' + optionId).val('');
                }
            }
        }
    }

    $scope.outrightOptions = function(value) {
        if($scope.formData.outrightPurchase == true) {
            $scope.formData.aAccountant = "N/A";
            $scope.formData.aTel = "11";
            $scope.formData.aContact = "N/A";
            $scope.formData.aYear = "1";
            $scope.formData.aInsurer = "N/A"
            $scope.formData.iEmail = "N@A";
            $scope.formData.aTradeRef = "N/A";
            $scope.formData.tTel = "11";
            $scope.formData.tContact = "N/A";
            $scope.formData.aNemp = "1";
            $scope.formData.aPolicy = "N/A";
            $scope.formData.aTerm = "Outright";
        } else {
            $scope.formData.aTerm = "60";
        }
    }

    $scope.showOptions = function(form) {
        //alert($scope.formData.type.monitoring_solution);
        $(".optionBox").hide();
        $("#"+form+"Option").show();
        $(".form-label label").css('color','black');
        $("#"+form+"Label label").css('color','green');
        if (form=='copierAgreement'){
            $scope.processCopierAgreement();
        }
        if (form=='cloudConnectStandard'){
            $scope.processcloudRateCard();
        }

        if (form=='cloudConnectCap'){
            $scope.processcloudRateCard();
        }
        // Initialize location count and locations object.
        $scope.locationCount = 0;
    }

    $scope.processCopierAgreement = function(){
        //alert('Process Copier Agreement');
        var printers = [];
        //console.log($scope.items['schedule_of_goods']);
        angular.forEach($scope.items['schedule_of_goods'],function(item,index){
            printers[index] = [];
            //console.log($scope.formData.schItems[index]);
           angular.forEach($scope.formData.schItems[index],function(schItem){
            //console.log(schItem);
                if (!angular.isUndefined($scope.formData.schOption)){
                    if (!angular.isUndefined($scope.formData.schOption[index])){
                        if (!angular.isUndefined($scope.formData.schOption[index][schItem.count])){
                            if (($scope.formData.schOption[index][schItem.count].group.toUpperCase().indexOf('KYOCERA')!==-1) || ($scope.formData.schOption[index][schItem.count].group.toUpperCase().indexOf('FUJI')!==-1) || ($scope.formData.schOption[index][schItem.count].group.toUpperCase().indexOf('OKI')!==-1)){
                                var cpiBwAmt = $scope.formData.schQty[index][schItem.count] * $scope.formData.schOption[index][schItem.count].cpiBw;
                                if (cpiBwAmt==0){
                                    cpiBwAmt = 1.0;
                                }
                                var cpiColourAmt = $scope.formData.schQty[index][schItem.count] * $scope.formData.schOption[index][schItem.count].cpiColour;
                                if (cpiColourAmt==0){
                                    cpiColourAmt = 10.0;
                                }
                                var printer = {
                                    "model" : $scope.formData.schOption[index][schItem.count].model,
                                    "group" : $scope.formData.schOption[index][schItem.count].group,
                                    "item"  : $scope.formData.schOption[index][schItem.count].item,
                                    "qty"  : $scope.formData.schQty[index][schItem.count],
                                    "cpiBwAmt"  : cpiBwAmt.toFixed(1),
                                    "cpiColourAmt"  : cpiColourAmt.toFixed(1)
                                }
                                printers[index].push(printer);
                            }
                        }
                    }
                }
             });
        });
        angular.forEach(printers, function(printer,index){
            if (printer.length==0){
                printers.splice(index,1);
            }
        });
        //console.log(printers);
        if (printers.length!=0){
            $scope.formData.printers = printers;
        }
        else{
            alert('No printer is selected in the schedule of goods.');
        }            
    }

    $scope.processcloudRateCard = function(){
        //alert('Process Copier Agreement');
        var cloudphones = [];
        //console.log($scope.items['schedule_of_goods']);
        angular.forEach($scope.items['schedule_of_goods'],function(item,index){
            cloudphones[index] = [];
            //console.log($scope.formData.schItems[index]);
           angular.forEach($scope.formData.schItems[index],function(schItem){
            //console.log(schItem);
                if (!angular.isUndefined($scope.formData.schOption)){
                    if (!angular.isUndefined($scope.formData.schOption[index])){
                        if (!angular.isUndefined($scope.formData.schOption[index][schItem.count])){
                            if (($scope.formData.schOption[index][schItem.count].group.toUpperCase().indexOf('CISCO')!==-1)){
                                var cpiBwAmt = $scope.formData.schQty[index][schItem.count] * $scope.formData.schOption[index][schItem.count].cpiBw;
                                if (cpiBwAmt==0){
                                    cpiBwAmt = 1.0;
                                }
                                var cpiColourAmt = $scope.formData.schQty[index][schItem.count] * $scope.formData.schOption[index][schItem.count].cpiColour;
                                if (cpiColourAmt==0){
                                    cpiColourAmt = 10.0;
                                }
                                var printer = {
                                    "model" : $scope.formData.schOption[index][schItem.count].model,
                                    "group" : $scope.formData.schOption[index][schItem.count].group,
                                    "item"  : $scope.formData.schOption[index][schItem.count].item,
                                    "qty"  : $scope.formData.schQty[index][schItem.count],
                                    "cpiBwAmt"  : cpiBwAmt.toFixed(1),
                                    "cpiColourAmt"  : cpiColourAmt.toFixed(1)
                                }
                                cloudphones[index].push(printer);
                            }
                        }
                    }
                }
             });
        });
        angular.forEach(cloudphones[0], function(cloudphone,index){
            if (cloudphone.length>0){
                cloudphones[0].splice(index,1);
            }
                console.log(cloudphone);
                if(cloudphone.item == 'CISCO IP Phone 7821' || cloudphone.item == 'CISCO IP Phone 7861' || cloudphone.item == 'CISCO IP Phone 8841 Executive' || cloudphone.item == 'CISCO IP Phone 8851 - Bluetooth' || cloudphone.item == 'CISCO IP Phone 8861 - Wifi Enabled' || cloudphone.item == 'CISCO IP Phone 8845 HD Video conf Handset' || cloudphone.item == 'CISCO Headset 521 Wired Single' || cloudphone.item == 'CISCO Headset 522 Wired Dual' || cloudphone.item == 'CISCO 561 Headset Wireless Single') {
                    if(index==0) {
                        console.log("pasok");

                        $scope.formData.cloudConnectStandardChannel[0] = cloudphone.qty;
                        $scope.formData.cloudConnectCapChannel[0] = cloudphone.qty;

                    }
                }
        });

        // if (cloudphones[0].length!=0 && $scope.formData.type.cloudConnectStandard != true || $scope.formData.type.cloudConnectCap == true){
        //     $scope.formData.cloudphones = cloudphones;
        // }
        // else{
        //     alert('No cloud phone is selected in the schedule of goods.');
        // }            
    }
    // $scope.ipPlan = {
    //     10 : {rate : 499, desc : "Up to 10/10 Mbps**"},
    //     T20 : {rate : 599, desc : "Up to 20/20 Mbps**"},
    //     400 : {rate : 758, desc : "Up to 400/400Mbps**"},
    //     4 : {rate : 419, desc : "4Mbps/4Mbps"},
    //     8 : {rate : 429, desc : "8Mbps/8Mbps*"},
    //     18 : {rate : 489, desc : "18Mbps/18Mbps*"},
    //     20 : {rate : 599, desc : "20Mbps/20Mbps*"},
    //     30 : {rate : 998, desc : "30Mbps/30Mbps*"}
    // }

    // $scope.ipDownload = {
    //     50 :{plan : "50GB", rate: 69},
    //     100 :{plan : "100GB", rate : 79},
    //     200 : {plan : "200GB" , rate :139},
    //     300 : {plan : "300GB", rate : 209},
    //     500 : {plan : "500GB",rate : 339},
    //     1000 : {plan : "1000GB", rate : 639}
    // }

    $scope.nbnPlan = {
        255 : {rate : 49, desc : "NBN 25Mbps/5Mbps*"},
        2510 : {rate : 59, desc : "NBN 25Mbps/10Mbps*"},
        5020 : {rate : 69, desc : "Up to 50/20Mbps**"},
        10040 : {rate : 79, desc : "NBN 100Mbps/40Mbps*"}
    }
    $scope.nbnDownload = {
        100 :{plan : "100GB", rate : 19},
        200 : {plan : "200GB" , rate :25},
        500 : {plan : "500GB",rate : 29},
        1000 : {plan : "1000GB", rate : 49}
    }

    $scope.setValues = function() {
        $scope.formData.cName = "Some Long Name Comp.";
        $scope.formData.cAbn = 12323231321;
        $scope.formData.cTrading = "Long trade name";
        $scope.formData.cAddress = "221B Baker Street";        
        $scope.formData.cPostCode = 2000;

        $scope.formData.pIAddress = "221B Baker Street";
        $scope.formData.pISuburb = "London";
        $scope.formData.pIState = "ACT";
        $scope.formData.pIPostcode = "1234";
        $scope.formData.aBillingTerm = 60;
        $scope.formData.aTerm = 60;

        $scope.formData.comLocation = {};
        $scope.formData.comLocation = {
            'nbf_cap'               : [$scope.formData.pIAddress],
            'nbf_std'               : [$scope.formData.pIAddress],
            'voice_comp'              : [$scope.formData.pIAddress],
            'voice_solution_untimed'  : [$scope.formData.pIAddress],
            'voice_essentials_cap'    : [$scope.formData.pIAddress],
            'voice_solution_standard' : [$scope.formData.pIAddress],
            'cloud_connect_standard'  : [$scope.formData.pIAddress],
            'cloud_connect_cap'       : [$scope.formData.pIAddress],
            'data_adsl'               : [$scope.formData.pIAddress],
            'ip_midband'              : [$scope.formData.pIAddress],
            'ip_midbandunli'          : [$scope.formData.pIAddress],
            'nbn'                     : [$scope.formData.pIAddress],
            'nbnUnlimited'            : [$scope.formData.pIAddress],
            'mobile_mega'             : [$scope.formData.pIAddress],
            'mobile_ut'               : [$scope.formData.pIAddress],
            'mobile_wireless'         : [$scope.formData.pIAddress],
            '1300'                    : [$scope.formData.pIAddress],
            '1300_discounted'         : [$scope.formData.pIAddress],
            'fibre'                   : [$scope.formData.pIAddress],
            'ethernet'                : [$scope.formData.pIAddress],
            'telstraUntimed'          : [$scope.formData.pIAddress],
            'telstraWirelessSuper'    : [$scope.formData.pIAddress],
            'telstraWireless'         : [$scope.formData.pIAddress],
            'printer'                 : [$scope.formData.pIAddress],
            'schedule_of_goods'       : [$scope.formData.pIAddress]
        };

        $scope.formData.cSuburb = "London";
        $scope.formData.comSuburb = {};
        $scope.formData.comSuburb = {
            'nbf_cap'               : [$scope.formData.pISuburb],
            'nbf_std'               : [$scope.formData.pISuburb],
            'voice_comp'              : [$scope.formData.pISuburb],
            'voice_solution_untimed'  : [$scope.formData.pISuburb],
            'voice_essentials_cap'    : [$scope.formData.pISuburb],
            'voice_solution_standard' : [$scope.formData.pISuburb],
            'cloud_connect_standard'  : [$scope.formData.pISuburb],
            'cloud_connect_cap'       : [$scope.formData.pISuburb],
            'data_adsl'               : [$scope.formData.pISuburb],
            'ip_midband'              : [$scope.formData.pISuburb],
            'ip_midbandunli'          : [$scope.formData.pISuburb],
            'nbn'                     : [$scope.formData.pISuburb],
            'nbnUnlimited'            : [$scope.formData.pISuburb],
            'mobile_mega'             : [$scope.formData.pISuburb],
            'mobile_ut'               : [$scope.formData.pISuburb],
            'mobile_wireless'         : [$scope.formData.pISuburb],
            '1300'                    : [$scope.formData.pISuburb],
            '1300_discounted'         : [$scope.formData.pISuburb],
            'fibre'                   : [$scope.formData.pISuburb],
            'ethernet'                : [$scope.formData.pISuburb],
            'telstraUntimed'          : [$scope.formData.pISuburb],
            'telstraWirelessSuper'    : [$scope.formData.pISuburb],
            'telstraWireless'         : [$scope.formData.pISuburb],
            'printer'                 : [$scope.formData.pISuburb],
            'schedule_of_goods'       : [$scope.formData.pISuburb]
        };

        $scope.formData.comZipCode = {};
        $scope.formData.comZipCode = {
            'nbf_cap'               : [$scope.formData.pIPostcode],
            'nbf_std'               : [$scope.formData.pIPostcode],
            'voice_comp'              : [$scope.formData.pIPostcode],
            'voice_solution_untimed'  : [$scope.formData.pIPostcode],
            'voice_essentials_cap'    : [$scope.formData.pIPostcode],
            'voice_solution_standard' : [$scope.formData.pIPostcode],
            'cloud_connect_standard'  : [$scope.formData.pIPostcode],
            'cloud_connect_cap'       : [$scope.formData.pIPostcode],
            'data_adsl'               : [$scope.formData.pIPostcode],
            'ip_midband'              : [$scope.formData.pIPostcode],
            'ip_midbandunli'          : [$scope.formData.pIPostcode],
            'nbn'                     : [$scope.formData.pIPostcode],
            'nbnUnlimited'            : [$scope.formData.pIPostcode],
            'mobile_mega'             : [$scope.formData.pIPostcode],
            'mobile_ut'               : [$scope.formData.pIPostcode],
            'mobile_wireless'         : [$scope.formData.pIPostcode],
            '1300'                    : [$scope.formData.pIPostcode],
            '1300_discounted'         : [$scope.formData.pIPostcode],
            'fibre'                   : [$scope.formData.pIPostcode],
            'ethernet'                : [$scope.formData.pIPostcode],
            'telstraUntimed'          : [$scope.formData.pIPostcode],
            'telstraWirelessSuper'    : [$scope.formData.pIPostcode],
            'telstraWireless'         : [$scope.formData.pIPostcode],
            'printer'                 : [$scope.formData.pIPostcode],
            'schedule_of_goods'       : [$scope.formData.pIPostcode]
        };
        $scope.formData.aIdenQuestion = "";
        $scope.formData.cEmail = "info@somelongcompany.com.au";
        $scope.formData.dName = "Arthur Conan Doyle";
        $scope.formData.cPosition = "Director";
        $scope.formData.dSName = "Sherlock Holmes";
        $scope.formData.cSPosition = "Director";
        $scope.formData.cContact = "Arthur Conan Doyle";
        $scope.formData.cTel = "(02) 4342-1232";
        $scope.formData.cFax = "(02) 4342-1232";
        $scope.formData.cMobile = "4532-533-267";
        $scope.formData.cWebsite = "http://somelongcompany.com.au";
        $scope.formData.aAccountant = "James Moriarty";
        $scope.formData.aIndustry = "PROFESSIONAL OFFICE";
        $scope.formData.cNemp = "0";
        $scope.formData.aTradeRef = "A Study in Scarlet";
        $scope.formData.aCPerson = "Dr. Watson";
        $scope.formData.aTel = "(02) 2340-5444";
        $scope.formData.pName = "Sherlock Holmes";
        $scope.formData.pABN = "0919231921";
        $scope.formData.pFax = "(02) 4324-1234";
        $scope.formData.pEmail = "info@somelongcompany.com.au";
        $scope.formData.pAddress = "220B Baker Street";
        $scope.formData.pSuburb = "London";
        $scope.formData.pPostcode = 2000;
        $scope.formData.pPhone = "(02) 4342-1232";
        $scope.formData.pLicense = "2332323243A";
        $scope.formData.pValue = "$ 600,000";
        $scope.formData.pMortgage = "$ 400,000";
        $scope.formData.pOwned = true;
        $scope.formData.tTel = "(02) 4342-1232";
        $scope.formData.aContact = "Mrs. Hudson";
        $scope.formData.tContact = "Mr. Hudson";
        $scope.formData.aNemp = "1000";
        $scope.formData.aInsurer = "Insurance Company";
        $scope.formData.iEmail = "insurertest@text.com";
        $scope.formData.aPolicy = "A/127/2016";
        $scope.formData.g2Name = "John Watson";
        $scope.formData.g2Address = "220B Baker Street";
        $scope.formData.g2ABN = "5352352525";
        $scope.formData.g2Suburb = "London";
        $scope.formData.g2Postcode = 2000;
        $scope.formData.g2Phone = "(02) 4342-1232";
        $scope.formData.g2License = "2332323243A";
        $scope.formData.g2Value = "$ 600,000";
        $scope.formData.g2Mortgage = "$ 400,000";
        $scope.formData.g2Owned = true;
        $scope.formData.g2Fax = "(02) 4444-1111";
        $scope.formData.g2Email = "guarantor2@guarantor.com";
        $scope.formData.aYear = "1";
        $scope.formData.cState = "ACT";
        $scope.formData.aFinInst = "1111";
        $scope.formData.aFinBranch = "2222";
        $scope.formData.aAccName = "3333";
        $scope.formData.aBsb = "4444";
        $scope.formData.aAccDetails = "5555";
        $scope.formData.comments = "Referred by: " +  $scope.formData.cName + " " +  $scope.formData.cContact + " " + $scope.formData.cTel;
        //console.log($('#formData'));
    }
    $scope.saver = function(){
         $scope.formData.term = $('#term').val();
         $scope.formData.type = $('#type').val();
         var rates = Form.getRates({term:$scope.formData.term,
                                    type:$scope.formData.type
                                    }, function(data) {
            if (data.success) {
            $scope.rates = data.rates;
           }
            console.log($scope.rates);
            if($scope.formData.type == 'rental' || $scope.formData.type == 'rental2'|| $scope.formData.type == 'classic'){
            alert('rental');
            console.log($scope.rates);
            angular.forEach($scope.rates, function(value, key){
                
                   console.log('Rental:' + key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate + $scope.formData.perMonth);
                   if($scope.checkRange($scope.formData.perMonth,value.lowerLimit,value.upperLimit)){
                   $scope.leaser = ($scope.formData.perMonth/1000)*value.rate;
                 }
             });
            
        }
        else{
            alert('leasing');
            console.log($scope.rates);
            angular.forEach($scope.rates, function(value, key){
                
                   console.log(key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate);
                    if($scope.checkRange($scope.formData.perMonth,value.lowerLimit,value.upperLimit)){
                   $scope.leaser = ($scope.formData.perMonth/1000)*value.rate;
                 }
             });
        }
        
        });
            
  }
        $scope.checkRange = function(val,lower_limit,upper_limit){
        //alert(val + '-' + lower_limit + '-' + upper_limit);
        if (upper_limit==0){
            if (val>=lower_limit){
                return true;
            } else {
                return false;
            }
        } else {
            if (val >= lower_limit && val <= upper_limit){
                return true;
            } else {
                return false;
            }
        }
    }
    $scope.updateReferalInfo = function (){
        $scope.formData.comments =  "Referred by: ";
        if(angular.isDefined($scope.formData.cName)){
            $scope.formData.comments += $scope.formData.cName;
        } 
        if(angular.isDefined($scope.formData.cContact)){
            $scope.formData.comments += (' ' + $scope.formData.cContact);  
        }
        if(angular.isDefined($scope.formData.cTel)){
            $scope.formData.comments += (' ' + $scope.formData.cTel); 
        }
    }

    $('.mob').on("keyup", function()
    {
        var val = this.value.replace(/[^0-9]/ig, '');
        var newVal = '';
        /*if (val.length >10) {
            val = val.substr(0,10);
        }*/
        if (val.length >4) {
            newVal +=val.substr(0,4)+'-';
            val = val.substr(4);
        }
        while (val.length > 3)   {
            newVal += val.substr(0, 3) +'-';
            val = val.substr(3);

        }
        newVal += val;
        this.value = newVal;
    });

    $('.ccNo').on("keyup", function()
    {
        var val = this.value.replace(/[^0-9]/ig, '');
        var newVal = '';
        while (val.length > 4) {
            newVal += val.substr(0, 4) +'-';
            val = val.substr(4);
        }
        newVal += val;
        this.value = newVal;
    });

    $scope.transformTelFields();

    var dateObj = new Date();
    var dd = dateObj.getDate();
    var mm = dateObj.getMonth()+1;
    var yy = dateObj.getFullYear();
    var today = mm+"/"+dd+"/"+yy;
    var minYY = yy - 18; //(Applicant have to be atleast 18);
    var birthMinDate = mm+"/"+dd+"/"+minYY;

    $('.expiryDate').datetimepicker({
       format : "DD/MM/YYYY",
       minDate : today
    });

    $('.date').datetimepicker({
        format : "DD/MM/YYYY",
        defaultDate : today
    });
    $('.pBirthDate').datetimepicker({
        format : "DD/MM/YYYY",
        maxDate : birthMinDate
    });
    $('.datePayment').datetimepicker({
        format : "DD/MM/YYYY",
        defaultDate : today
    });
    $scope.pickDate = function() {
        console.log('pasok');
        $('.datePayment').datetimepicker({
            format : "DD/MM/YYYY",
            defaultDate : today
        });
    }
        
        

    $scope.getRates = function(term,type){
      Form.getRates({term:term,type:type}, function(data) {
        if (data.success) {
          $rootScope.rates = data.rates;
        }
      });
    }

    $scope.$watch("formData.type.rental", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {
                $scope.getRates($scope.formData.aTerm,'rental');
                $scope.formData.type.leasing = false;
                $scope.formData.type.outright = false;
                $scope.formData.type.chattle = false;
                $scope.formData.type.rpm = false;
                $scope.formData.type.rental2 = false;
                $scope.formData.type.classic = false;

            }
          //  $scope.showSchedule();
        }
    });

    $scope.$watch("formData.type.leasing", function(val) {
        if (angular.isDefined(val)) {
           if (val == true) {
                $scope.getRates($scope.formData.aTerm,'leasing');
                $scope.formData.type.rental = false;
                $scope.formData.type.outright = false;
                $scope.formData.type.chattle = false;
                $scope.formData.type.rpm = false;
                $scope.formData.type.rental2 = false;
                $scope.formData.type.classic = false;

            }
           // $scope.showSchedule();
        }
    });

    $scope.$watch("formData.type.outright", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {
                $scope.formData.type.rental = false;
                $scope.formData.type.leasing = false;
                $scope.formData.type.chattle = false;
                $scope.formData.type.rpm = false;
                $scope.formData.type.rental2 = false;
                $scope.formData.type.classic = false;
            }

        }
    })


    $scope.$watch("formData.type.chattle", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {

                $scope.formData.type.rental = false;
                $scope.formData.type.leasing = false;
                $scope.formData.type.outright = false;
                $scope.formData.type.rpm = false;
                $scope.formData.type.rental2 = false;
                $scope.formData.type.classic = false;
            }
        }
    });
    $scope.$watch("formData.type.rpm", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {
                $scope.getRates($scope.formData.aTerm,'rpm');
                $scope.formData.type.rental = false;
                $scope.formData.type.leasing = false;
                $scope.formData.type.chattle = false;
                $scope.formData.type.outright = false;
                $scope.formData.type.rental2 = false;
                $scope.formData.type.classic = false;
            }

        }
    });
    $scope.$watch("formData.type.rental2", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {
                $scope.getRates($scope.formData.aTerm,'rental2');
                $scope.formData.type.rental = false;
                $scope.formData.type.leasing = false;
                $scope.formData.type.chattle = false;
                $scope.formData.type.outright = false;
                $scope.formData.type.rpm = false;
                $scope.formData.type.classic = false;
            }

        }
    });
    $scope.$watch("formData.type.classic", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {
                $scope.getRates($scope.formData.aTerm,'classic');
                $scope.formData.type.rental = false;
                $scope.formData.type.leasing = false;
                $scope.formData.type.chattle = false;
                $scope.formData.type.outright = false;
                $scope.formData.type.rpm = false;
                $scope.formData.type.rental2 = false;
            }

        }
    });
    $scope.sign = function(id) {
        
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({
                color:"#000000",lineWidth:1,
                    width :490, height :98,
                    cssclass : "signature-canvas",
                   "decor-color": 'transparent'
                });
            clicked = true;
        }

    }

    $scope.presign = function(id) {
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({
                color:"#000000",
                lineWidth:1,
                width :490, 
                height :98,
                cssclass : "signature-canvas",
                "decor-color": 'transparent'
            });
        }
    }

    $scope.sendReferral = function(){
        // console.log($scope.profile.name);
        // console.log($scope.formData.comments);
        // console.log($scope.formData.cName);
        // console.log($scope.formData.rContactPerson);
        // console.log($scope.formData.rPhoneNumber);
        // console.log($scope.formData.rBusinessName);
        // alert($scope.formData.rContactPerson);
        // alert($scope.formData.rBusinessName);
        // alert($scope.formData.rPhoneNumber);
        Form.send_referrals({
                'lp_Comments'   : $scope.formData.comments + ' through ' + $scope.profile.name,
                'lp_Company'    : $scope.formData.rBusinessName,
                'lp_Phone'      : $scope.formData.rPhoneNumber,
                'lp_ContactFirstName' : $scope.formData.rContactPerson,
                'wl_salesrep'   : $scope.profile.name,
                'wl_referredby' : 'Referred by : ' + $scope.formData.cName + ' ' + $scope.formData.cContact + ' ' + $scope.formData.cTel + ' through ' + $scope.profile.name,
                'lp_UserField6' : 'Referral From Ap Nexgen',
            },function(data){
                if (data.response) {
                    // alert("response");
                    // console.log(data.response)
                }
                if (data.link) {
                    // alert("links");
                    // console.log(data.link)
                }
            }
        );
        
        // var str =  'Referred by ' +  $scope.formData.cName + '' + $scope.formData.cContact + '' + $scope.formData.cTel;
        // var regExpr = /[^a-zA-Z0-9-. ]/g;
        // var comments = $scope.formData.comments;
        // angular.forEach($scope.formData.referals, function(index,item){
        //     if ($scope.formData.rBusine ssName[index] != null && $scope.formData.rContactPerson[index] != null && $scope.formData.rPhoneNumber[index] != null ){
        //         $http({
        //             method: 'POST',
        //             url: 'https://www.crmtool.net/lp_NewLeadbeta.asp',
        //             data: { 
        //                 wl_salesrep : $scope.profile.name,
        //                 lp_Username : "NexgenWeb", 
        //                 lp_Password : "pDqEXCN2C35f",
        //                 lp_CompanyID : "12498",
        //                 lp_Comments : $scope.formData.comments,
        //                 wl_leadsource : "2",
        //                 lp_UserField6 : "Referral From Ap",
        //                 lp_Company  : $scope.formData.rBusinessName[index],
        //                 lp_ContactFirstName : $scope.formData.rContactPerson[index],
        //                 lp_Phone : $scope.formData.rPhoneNumber[index],
        //                 wl_referredby : "Referred by : " + $scope.formData.cName + ' ' + $scope.formData.cContact + ' ' + $scope.formData.configAppTelNo    
        //             },
        //             // url : 'https://www.crmtool.net/lp_NewLead.asp?' +
        //             //         'lp_Comments=' + comments.replace(/\W/g, ' ') + '&' +
        //             //         'lp_Company=' + $scope.formData.rBusinessName[index] + '&' + 
        //             //         'lp_CompanyID=12498' + '&'+
        //             //         'lp_ContactFirstName=' +  $scope.formData.rContactPerson[index] + '&' +
        //             //         'lp_Password=pDqEXCN2C35f' + '&' +
        //             //         'lp_Phone=' + $scope.formData.rPhoneNumber[index] + '&' +
        //             //         'lp_UserField6=Referral From Ap' + '&' +
        //             //         'lp_Username=NexgenWeb' + '&' +
        //             //         'wl_leadsource=2' + '&' +
        //             //         'wl_referredby=' + $scope.formData.comments + '&' +
        //             //         'wl_salesrep=' + $scope.profile.name,
        //             headers: {
        //                'Access-Control-Allow-Origin': '*',
        //                'Access-Control-Allow-Headers': 'Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With',
        //                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS'
        //              }
        //           // url: 'http://www.crmtool.net/lp_NewLead.asp?',
        //           // params: { 
        //           //   wl_salesrep : $scope.profile.name,
        //           //   lp_Username : "NexgenWeb", 
        //           //   lp_Password : "pDqEXCN2C35f",
        //           //   lp_CompanyID : "12498",
        //           //   lp_Comments : $scope.formData.comments,
        //           //   wl_leadsource : "2",
        //           //   lp_UserField6 : "Referral From Ap",
        //           //   lp_Company  : $scope.formData.rBusinessName[index],
        //           //   lp_ContactFirstName : $scope.formData.rContactPerson[index],
        //           //   lp_Phone : $scope.formData.rPhoneNumber[index],
        //           //   wl_referredby : "Referred by : " + $scope.formData.cName + ' ' + $scope.formData.cContact + ' ' + $scope.formData.configAppTelNo
                    
        //           // }
        //         }).then(function successCallback(response) {
        //                 $scope.myRes = response.data;
        //                 $scope.statuscode = response.status;
        //           }, function errorCallback(response) {
        //               $scope.myRes = response.statusText;
        //           });
        //     } else {
        //          //console.log(index);
        //     }
        // });
    } 

    $scope.downloadPdf = function() {
        $scope.dataFunction = $('a[href="#finish"]').attr('data-function');
        console.log($scope.dataFunction);
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var formHtmlData = new Array();
        var fileNameArr = new Array();
        //$interval.cancel(saveDraft);

        $scope.formData.signedTimestamps = $window.signedTimeStamps;

        if (!angular.isDefined($scope.htmlData) || angular.equals({},$scope.htmlData)) {
          FlashMessage.setMessage({success:false, message:"No Forms Selected"});
          return;
        }
        // $("#loading").show();
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;

        angular.forEach($scope.htmlData, function(value, key) {
            var nametail = new Date().getTime();
            var rand = Math.floor((Math.random()*1000)+10);
            var fileName = key + '0' + rand + nametail + ".pdf";
            fileNameArr.push(fileName);
            $("#downloadPdfBn").prop('disabled',true);

            // In case of forms with location fields.
            if ($scope.locationAvailableTemps.indexOf(key) !== -1 && $scope.items[key].length > 1) {
                angular.forEach(value, function(data, index) {
                    rand = Math.floor((Math.random() * 1000) + 10);
                    fileName = key  + '0' + rand + nametail + ".pdf";
                    formHtmlData.push({html: data, fileName : fileName});
                });
            } else {
                formHtmlData.push({html: value, fileName : fileName});
            }

            console.log(formHtmlData);
        });

        if(angular.isUndefined($scope.ord_num)){
          $scope.ord_num = $scope.orderNumberDate() + $scope.orderNumberComp();
        }
        var pdf = Form.update({
            htmlData :formHtmlData,
            userDetails : { name : $scope.formData.dName,
                            email :$scope.formData.cEmail},
            account: {name: userProfile.name, email: userProfile.email}},
            function(data) {
            if($rootScope.editId) {
               var editId = $rootScope.editId;
               $rootScope.editId = null;
            }
            Form.save({ data : $scope.formData,
                        user : userProfile.id,
                        editId : editId,
                        ord_num : $scope.ord_num,
                        fileName : fileNameArr,
                        type:"Completed"}, function(data) {
                if (data.success) {
                    $scope.formData = {};
                    // $("#loading").hide();
                    $scope.loading = false;
                    if ($('#form_complete').val()=='F'){
                      $scope.template = "templates/forms/summary.html";
                      ngDialog.open({
                            // data: angular.toJson({
                            //   proposalData: datum
                          // }),
                           template: $scope.template,
                           plan: true,
                           controller : 'FormDataCtrl',
                           width: '22cm',
                           className: 'ngdialog-theme-default',
                           showClose: true,
                           closeByEscape: true,
                           closeByDocument: true
                       });
                    } else {
                        if($scope.dataFunction == 1) {
                            FlashMessage.setMessage({
                                success:true,
                                message:"Form signing complete!"}
                              );
                                $state.params = null;
                                 $state.go('customer_sign_complete', {
                                });
                        } else {
                              FlashMessage.setMessage({
                                success:true,
                                message:"You will receive an email shortly."}
                              );
                              alert("You will receive an email shortly.");
                              $window.location.reload();
                        }
                     
                    }
                }
            });
        });
    }

    // asdf

    $scope.trustSrc = function(src) {
      return $sce.trustAsResourceUrl(src);
    }

    $scope.showPaycorpUrl = function() {       
        //console.log($('#ccInfo').prop());
        // if($('#ccInfo').val()==on
        console.log($scope.ccInfo);
        if ($scope.ccInfo){
          $scope.formData.aPaymentMethod = 'Credit Card';
          Form.showPaycorp(function(data) {
            console.log(data);
            $scope.url = data.response.paymentPageUrl;   
            $scope.reqId = data.response.reqId; 
            $scope.template = "templates/forms/paycorp.html";
            ngDialog.open({
                 template: $scope.template,
                 plan: true,
                 controller : 'FormDataCtrl',
                 width: 960,
                 height: 720,
                 scope: $scope,
                 className: 'ngdialog-theme-default',
                 showClose: true,
                 closeByEscape: true,
                 closeByDocument: true,
                 preCloseCallback : function(value) {
                    Form.getToken({ reqid : $scope.reqId },function(data){
                      $scope.formData.aCardName = data.response.holderName;
                      $scope.formData.aCardNumber = data.response.cardNumber;
                      $scope.formData.aCardExpiry = data.response.expiry;
                      $scope.formData.aCardType = data.response.type;
                      $scope.formData.aCardToken = data.response.token;
                    })
                 }
            });
           });
        } else {
          $scope.formData.aPaymentMethod = 'Debit Card';
        }
    }

    $scope.formGroups = {
        telecomService      : [
                                {file:'billing_app',id : 'configApp'},
                                {file:'monitoring_solution',id : 'monitoring_solution'} 
                              ],
        financeApplication  : [
                                {file:'rental',id : 'rental'},
                                {file: 'leasing', id : 'leasing'},
                                {file: 'chattle', id : 'chattle'},
                                {file: 'rpm', id : 'rpm'},
                                {file: 'rental2', id : 'rental2'},
                                {file: 'classic', id : 'classic'},
                              ],
        orderSpec           : [
                                {file: 'single_order_spec', id : 'singleOrder'},
                                {file: 'copierAgreement', id: 'copierAgreement'},
                                {file: 'agreement', id: 'agreement'},

                              ],
        voice               : [
                                {file: 'nbf_cap', id : 'voiceCap'},
                                {file: 'voice_comp', id : 'completeOffice'},
                                {file: 'voice_essentials_cap', id : 'voiceEssential'},
                                {file: 'voice_solution_standard', id : 'voiceSolution'},
                                {file: 'voice_solution_untimed', id : 'voiceUt'},
                                {file: 'nbf_std', id : 'focusStandard'},
                                {file: 'cloud_connect_standard', id : 'cloudConnectStandard'},
                                {file: 'cloud_connect_cap', id : 'cloudConnectCap'}
                              ],
        data                : [
                                {file: 'data_adsl', id : 'adsl2'},
                                {file: 'nbnUnlimited', id : 'nbnUnlimitedPlans'},
                                {file: 'ip_midband', id : 'ipMidband'},
                                {file: 'ip_midbandunli', id : 'ipMidbandUnli'},
                                {file: 'nbn', id : 'nbnMonthly'},
                                {file: 'ethernet', id: 'ethernet'},
                                {file: 'fibre', id : 'fibre'}
                              ],
        mobile              : [
                                {file: 'mobile_mega', id : 'mobileCap'},
                                {file: 'mobile_ut', id : 'mobileUt'},
                                {file: 'telstra4gSuper', id : 'telstra4gSuper'},
                                {file: 'mobile_wireless', id : 'mobileWireless'},
                                {file: 'telstraUntimed', id: 'telstraUntimed'},
                                {file: 'telstraWirelessSuper', id: 'telstraWirelessSuper'},
                                {file: 'telstraWireless', id: 'telstraWireless'}
                              ],
        r1300               : [
                                {file: '1300', id : 'rate131300'},
                                {file: '1300_discounted', id : 'rateDis131300'}
                              ]
    }

    $rootScope.$on('ngDialog.opened', function (e, $dialog) {
        $window.scrollTo(0, angular.element(".ngdialog-content").offsetTop);
    });

    $scope.checkAccount = function(){
      angular.forEach($scope.formData.accounts,function(data){
        if (data.configCarrier=='')
          return false;
        if (data.configAccno=='')
          return false;
      });
      return true;
    }
    $scope.tada = function(templateName, formData) {
        console.log("weeeeeeeeeeeeeee");
    }

$scope.signDocument = function(templateName, formData) {
        $(".btn-clicksign").css('display', 'block');
        $scope.template = "templates/forms/"+templateName+".html?t=" + _version;
        //setting the values changed by Script.
        formData.dateVal = $("#dateVal").val();
        formData.pBirth  = $("#pBirth").val();
        formData.pIAddress = $("#pIAddress").val();
        formData.pIState = $("#pIState").val();
        formData.pIPostcode =  $("#pIPostcode").val();
        formData.aPeriod = 'monthly';
        formData.pISuburb =  $("#pISuburb").val();
        $scope.formData.totalServiceAmount = 0;
        $scope.formData.totalServiceDiscount = 0;
        $scope.user = Scopes.get('DefaultCtrl').profile.name;
        
        window.setTimeout(function () { 
            var dddd =  document.getElementsByClassName('signatureImg');
            angular.forEach(dddd,function(item,index){
                console.log(dddd);
            });
         
        }, 2);

        $scope.proposalData = $scope.formData;
        $scope.proposalData.contactPerson = $scope.formData.cContact;
        $scope.proposalData.tradingName = $scope.formData.cName;
        $scope.proposalData.address = $scope.formData.cAddress;
        $scope.proposalData.suburb = $scope.formData.cSuburb;
        $scope.proposalData.abn = $scope.formData.cAbn;
        $scope.proposalData.phoneNumber = $scope.formData.cTel;
        $scope.proposalData.mobileNumber = $scope.formData.cMobile;
        $scope.proposalData.sum=0;
        $scope.proposalData.sumCloud=0;
        $scope.proposalData.disCloud=0;
        $scope.proposalData.dis=0;
        $scope.proposalData.callDis=0;
        $scope.proposalData.getTotalServices=0;
        $scope.proposalData.landlineNumRow = 0;
        // focus cap
          angular.forEach(formData.voiceStd,function(item,index){
            console.log(item,index,$scope.formData.totalServiceAmount);
            if (formData.type.voiceCap){
              if (item==5){
                $scope.formData.totalServiceAmount+=495;
              } else if (item==6){
                $scope.formData.totalServiceAmount+=595;
              } else if (item==8){
                $scope.formData.totalServiceAmount+=792;
              } else if (item==10){
                $scope.formData.totalServiceAmount+=990;
              } else if (item==12){
                $scope.formData.totalServiceAmount+=1188;
              } else if (item==14){
                $scope.formData.totalServiceAmount+=1106;
              } else if (item==16){
                $scope.formData.totalServiceAmount+=1264;
              } else if (item==18){
                $scope.formData.totalServiceAmount+=1422;
              } else if (item==20){
                $scope.formData.totalServiceAmount+=1580;
              } else if (item==25){
                $scope.formData.totalServiceAmount+=1975;
              }
            }
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          });

        angular.forEach(formData.voiceFaxQty,function(item,index){
        if (formData.type.voiceCap){
          $scope.formData.totalServiceAmount+=(item*9.95);
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }
        });

        angular.forEach(formData.dID,function(item,index){
        if (formData.type.voiceCap){
          $scope.formData.totalServiceAmount+=item;
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }
        });
                    // discount
        angular.forEach(formData.bFCapDiscount,function(item,index){
          if(formData.type.voiceCap){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });
        
        // focus standard

        angular.forEach(formData.voiceStdChannel,function(item,index){
          if(formData.type.focusStandard){
            if (item==5){
              $scope.formData.totalServiceAmount+=199;
            } else if (item==6){
              $scope.formData.totalServiceAmount+=219;
            } else if (item==10){
              $scope.formData.totalServiceAmount+=249;
            } else if (item==20){
              $scope.formData.totalServiceAmount+=299;
            } else if (item==30){
              $scope.formData.totalServiceAmount+=349;
            }
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceStdFaxQty,function(item,index){
        if(formData.type.focusStandard){
          $scope.formData.totalServiceAmount+= (item*9.95);
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }
        });

        angular.forEach(formData.voiceStdDID,function(item,index){
        if(formData.type.focusStandard){
          $scope.formData.totalServiceAmount+=item;
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }
        });
                    // discount
        // console.log(formData.bFStdDiscount)
        angular.forEach(formData.bFStdDiscount,function(item,index){
          if(formData.type.focusStandard){
            $scope.formData.totalServiceDiscount+=parseFloat(item);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.bFCallDiscount,function(item,index){
          if(formData.type.focusStandard){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });
        // complete office
        angular.forEach(formData.voiceCompAnalouge,function(item,index){
        if (formData.type.completeOffice){
          $scope.formData.totalServiceAmount+=(item*43.95);
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }  
        });
        angular.forEach(formData.voiceCompBri,function(item,index){
        if (formData.type.completeOffice){
          $scope.formData.totalServiceAmount+=(item*109.85);
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }  
        });

        // angular.forEach(formData.voiceCompPri,function(item,index){
        // if (formData.type.completeOffice){
        //   $scope.formData.totalServiceAmount+=item
        // }  
        // });

        angular.forEach(formData.voiceCompFaxQty,function(item,index){
        if (formData.type.completeOffice){
          $scope.formData.totalServiceAmount+=(item*9.95);
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }  
        });

        angular.forEach(formData.voiceCompDID,function(item,index){
          if(formData.type.completeOffice){
            if (item==100){ $scope.formData.totalServiceAmount+=49.95 }
                $scope.proposalData.sum+=parseFloat(item);
                $scope.proposalData.landlineNumRow++;
          }
        });

                // discount

        angular.forEach(formData.voiceCompAnalougeDis,function(item,index){
          if(formData.type.completeOffice){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceCompBriDis,function(item,index){
          if(formData.type.completeOffice){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceCompPriDis,function(item,index){
          if(formData.type.completeOffice){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceCompCallDis,function(item,index){
          if(formData.type.completeOffice){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.callDis+=parseFloat(item);
            $scope.proposalData.callDisAdded = 1;
          }
        });
        // solution cap
        angular.forEach(formData.voiceUntimedChannel,function(item,index){
          if(formData.type.voiceUt){
            if(item==3){
              $scope.formData.totalServiceAmount+=199;
            } else if(item==4){
              $scope.formData.totalServiceAmount+=259;
            } else if(item==5){
              $scope.formData.totalServiceAmount+=319;
            } else if (item==6){
              $scope.formData.totalServiceAmount+=379;
            } else if (item==7){
              $scope.formData.totalServiceAmount+=439;
            } else if (item==8){
              $scope.formData.totalServiceAmount+=499;
            } else if (item==9){
              $scope.formData.totalServiceAmount+=559;
            } else if (item==10){
              $scope.formData.totalServiceAmount+=619;
            } else if (item==11){
              $scope.formData.totalServiceAmount+=679;
            } else if (item==12){
              $scope.formData.totalServiceAmount+=739;
            } else if (item==13){
              $scope.formData.totalServiceAmount+=799;
            } else if (item==14){
              $scope.formData.totalServiceAmount+=859;
            } else if (item==15){
              $scope.formData.totalServiceAmount+=919;
            } else if (item==16){
              $scope.formData.totalServiceAmount+=979;
            } else if (item==17){
              $scope.formData.totalServiceAmount+=1039;
            } else if (item==18){
              $scope.formData.totalServiceAmount+=1099;
            } else if (item==19){
              $scope.formData.totalServiceAmount+=1159;
            } else if (item==20){
              $scope.formData.totalServiceAmount+=1219;
            } else if (item==21){
              $scope.formData.totalServiceAmount+=1279;
            } else if (item==22){
              $scope.formData.totalServiceAmount+=1339;
            } else if (item==23){
              $scope.formData.totalServiceAmount+=1399;
            } else if (item==24){
              $scope.formData.totalServiceAmount+=1459;
            } else if (item==25){
              $scope.formData.totalServiceAmount+=1519;
            } else if (item==26){
              $scope.formData.totalServiceAmount+=1579;
            } else if (item==27){
              $scope.formData.totalServiceAmount+=1639;
            } else if (item==28){
              $scope.formData.totalServiceAmount+=1699;
            } else if (item==29){
              $scope.formData.totalServiceAmount+=1759;
            } else if (item==30){
              $scope.formData.totalServiceAmount+=1819;
            }
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceUntimedFaxQty,function(item,index){
        if(formData.type.voiceUt){
          $scope.formData.totalServiceAmount+=(item*9.95);
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }
        });

        angular.forEach(formData.voiceUntimedDID,function(item,index){
        if(formData.type.voiceUt){
          if(item==10){
            $scope.formData.totalServiceAmount+=14.95
          } else if (item==50){
            $scope.formData.totalServiceAmount+=24.95
          } else if (item==100){
            $scope.formData.totalServiceAmount+=34.95
          }
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }
        });

                // discount
        angular.forEach(formData.bFUntimedDiscount,function(item,index){
          if(formData.type.voiceUt){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        // cloud connect standard
        angular.forEach(formData.cloudConnectStandardChannel,function(item,index){
          if(formData.type.cloudConnectStandard){
            if(item==2){
              $scope.formData.totalServiceAmount+=58;
            } else if(item==3){
              $scope.formData.totalServiceAmount+=87;
            } else if(item==4){
              $scope.formData.totalServiceAmount+=116;
            } else if(item==5){
              $scope.formData.totalServiceAmount+=145;
            } else if (item==6){
              $scope.formData.totalServiceAmount+=174;
            } else if (item==7){
              $scope.formData.totalServiceAmount+=203;
            } else if (item==8){
              $scope.formData.totalServiceAmount+=232;
            } else if (item==9){
              $scope.formData.totalServiceAmount+=261;
            } else if (item==10){
              $scope.formData.totalServiceAmount+=290;
            } else if (item==11){
              $scope.formData.totalServiceAmount+=319;
            } else if (item==12){
              $scope.formData.totalServiceAmount+=348;
            } else if (item==13){
              $scope.formData.totalServiceAmount+=377;
            } else if (item==14){
              $scope.formData.totalServiceAmount+=406;
            } else if (item==15){
              $scope.formData.totalServiceAmount+=435;
            } else if (item==16){
              $scope.formData.totalServiceAmount+=464;
            } else if (item==17){
              $scope.formData.totalServiceAmount+=493;
            } else if (item==18){
              $scope.formData.totalServiceAmount+=522;
            } else if (item==19){
              $scope.formData.totalServiceAmount+=551;
            } else if (item==20){
              $scope.formData.totalServiceAmount+=580;
            } else if (item==21){
              $scope.formData.totalServiceAmount+=609;
            } else if (item==22){
              $scope.formData.totalServiceAmount+=638;
            } else if (item==23){
              $scope.formData.totalServiceAmount+=667;
            } else if (item==24){
              $scope.formData.totalServiceAmount+=696;
            } else if (item==25){
              $scope.formData.totalServiceAmount+=725;
            } else if (item==26){
              $scope.formData.totalServiceAmount+=754;
            } else if (item==27){
              $scope.formData.totalServiceAmount+=783;
            } else if (item==28){
              $scope.formData.totalServiceAmount+=812;
            } else if (item==29){
              $scope.formData.totalServiceAmount+=841;
            } else if (item==30){
                $scope.formData.totalServiceAmount+=870;
            } else if (item==31){
                $scope.formData.totalServiceAmount+=899;
            } else if (item==32){
                $scope.formData.totalServiceAmount+=928;
            } else if (item==33){
                $scope.formData.totalServiceAmount+=957;
            } else if (item==34){
                $scope.formData.totalServiceAmount+=986;
            } else if (item==35){
                $scope.formData.totalServiceAmount+=1015;
            } else if (item==36){
                $scope.formData.totalServiceAmount+=1044;
            } else if (item==37){
                $scope.formData.totalServiceAmount+=1073;
            } else if (item==38){
                $scope.formData.totalServiceAmount+=1102;
            } else if (item==39){
                $scope.formData.totalServiceAmount+=1131;
            } else if (item==40){
                $scope.formData.totalServiceAmount+=1160;
            } else if (item==41){
                $scope.formData.totalServiceAmount+=1189;
            } else if (item==42){
                $scope.formData.totalServiceAmount+=1218;
            } else if (item==43){
                $scope.formData.totalServiceAmount+=1247;
            } else if (item==44){
                $scope.formData.totalServiceAmount+=1276;
            } else if (item==45){
                $scope.formData.totalServiceAmount+=1305;
            } else if (item==46){
                $scope.formData.totalServiceAmount+=1334;
            } else if (item==47){
                $scope.formData.totalServiceAmount+=1363;
            } else if (item==48){
                $scope.formData.totalServiceAmount+=1392;
            } else if (item==49){
                $scope.formData.totalServiceAmount+=1421;
            } else if (item==50){
                $scope.formData.totalServiceAmount+=1450;
            } else if (item==51){
                $scope.formData.totalServiceAmount+=1479;
            } else if (item==52){
                $scope.formData.totalServiceAmount+=1508;
            } else if (item==53){
                $scope.formData.totalServiceAmount+=1537;
            } else if (item==54){
                $scope.formData.totalServiceAmount+=1566;
            } else if (item==55){
                $scope.formData.totalServiceAmount+=1595;
            } else if (item==56){
                $scope.formData.totalServiceAmount+=1624;
            } else if (item==57){
                $scope.formData.totalServiceAmount+=1653;
            } else if (item==58){
                $scope.formData.totalServiceAmount+=1682;
            } else if (item==59){
                $scope.formData.totalServiceAmount+=1711;
            } else if (item==60){
                $scope.formData.totalServiceAmount+=1740;
            } else if (item==61){
                $scope.formData.totalServiceAmount+=1769;
            } else if (item==62){
                $scope.formData.totalServiceAmount+=1798;
            } else if (item==63){
                $scope.formData.totalServiceAmount+=1827;
            } else if (item==64){
                $scope.formData.totalServiceAmount+=1856;
            } else if (item==65){
                $scope.formData.totalServiceAmount+=1885;
            } else if (item==66){
                $scope.formData.totalServiceAmount+=1914;
            } else if (item==67){
                $scope.formData.totalServiceAmount+=1943;
            } else if (item==68){
                $scope.formData.totalServiceAmount+=1972;
            } else if (item==69){
                $scope.formData.totalServiceAmount+=2001;
            } else if (item==70){
                $scope.formData.totalServiceAmount+=2030;
            } else if (item==71){
                $scope.formData.totalServiceAmount+=2059;
            } else if (item==72){
                $scope.formData.totalServiceAmount+=2088;
            } else if (item==73){
                $scope.formData.totalServiceAmount+=2117;
            } else if (item==74){
                $scope.formData.totalServiceAmount+=2146;
            } else if (item==75){
                $scope.formData.totalServiceAmount+=2175;
            } else if (item==76){
                $scope.formData.totalServiceAmount+=2204;
            } else if (item==77){
                $scope.formData.totalServiceAmount+=2233;
            } else if (item==78){
                $scope.formData.totalServiceAmount+=2262;
            } else if (item==79){
                $scope.formData.totalServiceAmount+=2291;
            } else if (item==80){
                $scope.formData.totalServiceAmount+=2320;
            } else if (item==81){
                $scope.formData.totalServiceAmount+=2349;
            } else if (item==82){
                $scope.formData.totalServiceAmount+=2378;
            } else if (item==83){
                $scope.formData.totalServiceAmount+=2407;
            } else if (item==84){
                $scope.formData.totalServiceAmount+=2436;
            } else if (item==85){
                $scope.formData.totalServiceAmount+=2465;
            } else if (item==86){
                $scope.formData.totalServiceAmount+=2494;
            } else if (item==87){
                $scope.formData.totalServiceAmount+=2523;
            } else if (item==88){
                $scope.formData.totalServiceAmount+=2552;
            } else if (item==89){
                $scope.formData.totalServiceAmount+=2581;
            } else if (item==90){
                $scope.formData.totalServiceAmount+=2610;
            } else if (item==91){
                $scope.formData.totalServiceAmount+=2639;
            } else if (item==92){
                $scope.formData.totalServiceAmount+=2668;
            } else if (item==93){
                $scope.formData.totalServiceAmount+=2697;
            } else if (item==94){
                $scope.formData.totalServiceAmount+=2726;
            } else if (item==95){
                $scope.formData.totalServiceAmount+=2755;
            } else if (item==96){
                $scope.formData.totalServiceAmount+=2784;
            } else if (item==97){
                $scope.formData.totalServiceAmount+=2813;
            } else if (item==98){
                $scope.formData.totalServiceAmount+=2842;
            } else if (item==99){
                $scope.formData.totalServiceAmount+=2871;
            } else if (item==100){
                $scope.formData.totalServiceAmount+=2900;
            } 
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });
        //DID
        angular.forEach(formData.cloudConnectStandardDID,function(item,index){
            if(formData.type.cloudConnectStandard){
              if(item==10){
                $scope.formData.totalServiceAmount+=14.95;
              } else if (item==50){
                $scope.formData.totalServiceAmount+=24.95;
              } else if (item==100){
                $scope.formData.totalServiceAmount+=34.95;
              } else if (item==150){
                $scope.formData.totalServiceAmount+=44.95;
              } else if (item==200){
                $scope.formData.totalServiceAmount+=54.95;
              }
              $scope.proposalData.sum+=parseFloat(item);
              $scope.proposalData.landlineNumRow++;
            }
          });
  
        // discount
        angular.forEach(formData.bFUntimedDiscount,function(item,index){
        if(formData.type.cloudConnectStandard){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
        }
        });

        // cloud connect cap
        angular.forEach(formData.cloudConnectCapChannel,function(item,index){
            if(formData.type.cloudConnectCap){
                if(item==2) {
                $scope.formData.totalServiceAmount+=78;
            } else if(item==3){
                $scope.formData.totalServiceAmount+=117;
            } else if(item==4){
                $scope.formData.totalServiceAmount+=156;
            } else if(item==5){
                $scope.formData.totalServiceAmount+=195;
            } else if (item==6){
                $scope.formData.totalServiceAmount+=234;
            } else if (item==7){
                $scope.formData.totalServiceAmount+=273;
            } else if (item==8){
                $scope.formData.totalServiceAmount+=312;
            } else if (item==9){
                $scope.formData.totalServiceAmount+=351;
            } else if (item==10){
                $scope.formData.totalServiceAmount+=390;
            } else if (item==11){
                $scope.formData.totalServiceAmount+=429;
            } else if (item==12){
                $scope.formData.totalServiceAmount+=468;
            } else if (item==13){
                $scope.formData.totalServiceAmount+=507;
            } else if (item==14){
                $scope.formData.totalServiceAmount+=546;
            } else if (item==15){
                $scope.formData.totalServiceAmount+=585;
            } else if (item==16){
                $scope.formData.totalServiceAmount+=624;
            } else if (item==17){
                $scope.formData.totalServiceAmount+=663;
            } else if (item==18){
                $scope.formData.totalServiceAmount+=702;
            } else if (item==19){
                $scope.formData.totalServiceAmount+=741;
            } else if (item==20){
                $scope.formData.totalServiceAmount+=780;
            } else if (item==21){
                $scope.formData.totalServiceAmount+=819;
            } else if (item==22){
                $scope.formData.totalServiceAmount+=858;
            } else if (item==23){
                $scope.formData.totalServiceAmount+=897;
            } else if (item==24){
                $scope.formData.totalServiceAmount+=936;
            } else if (item==25){
                $scope.formData.totalServiceAmount+=975;
            } else if (item==26){
                $scope.formData.totalServiceAmount+=1014;
            } else if (item==27){
                $scope.formData.totalServiceAmount+=1053;
            } else if (item==28){
                $scope.formData.totalServiceAmount+=1092;
            } else if (item==29){
                $scope.formData.totalServiceAmount+=1131;
            } else if (item==30){
                $scope.formData.totalServiceAmount+=1170;
            } else if (item==31){
                $scope.formData.totalServiceAmount+=1209;
            } else if (item==32){
                $scope.formData.totalServiceAmount+=1248;
            } else if (item==33){
                $scope.formData.totalServiceAmount+=1287;
            } else if (item==34){
                $scope.formData.totalServiceAmount+=1326;
            } else if (item==35){
                $scope.formData.totalServiceAmount+=1365;
            } else if (item==36){
                $scope.formData.totalServiceAmount+=1404;
            } else if (item==37){
                $scope.formData.totalServiceAmount+=1443;
            } else if (item==38){
                $scope.formData.totalServiceAmount+=1482;
            } else if (item==39){
                $scope.formData.totalServiceAmount+=1521;
            } else if (item==40){
                $scope.formData.totalServiceAmount+=1560;
            } else if (item==41){
                $scope.formData.totalServiceAmount+=1599;
            } else if (item==42){
                $scope.formData.totalServiceAmount+=1638;
            } else if (item==43){
                $scope.formData.totalServiceAmount+=1677;
            } else if (item==44){
                $scope.formData.totalServiceAmount+=1716;
            } else if (item==45){
                $scope.formData.totalServiceAmount+=1755;
            } else if (item==46){
                $scope.formData.totalServiceAmount+=1794;
            } else if (item==47){
                $scope.formData.totalServiceAmount+=1833;
            } else if (item==48){
                $scope.formData.totalServiceAmount+=1872;
            } else if (item==49){
                $scope.formData.totalServiceAmount+=1911;
            } else if (item==50){
                $scope.formData.totalServiceAmount+=1950;
            } else if (item==51){
                $scope.formData.totalServiceAmount+=1989;
            } else if (item==52){
                $scope.formData.totalServiceAmount+=2028;
            } else if (item==53){
                $scope.formData.totalServiceAmount+=2067;
            } else if (item==54){
                $scope.formData.totalServiceAmount+=2106;
            } else if (item==55){
                $scope.formData.totalServiceAmount+=2145;
            } else if (item==56){
                $scope.formData.totalServiceAmount+=2184;
            } else if (item==57){
                $scope.formData.totalServiceAmount+=2223;
            } else if (item==58){
                $scope.formData.totalServiceAmount+=2262;
            } else if (item==59){
                $scope.formData.totalServiceAmount+=2301;
            } else if (item==60){
                $scope.formData.totalServiceAmount+=2340;
            } else if (item==61){
                $scope.formData.totalServiceAmount+=2379;
            } else if (item==62){
                $scope.formData.totalServiceAmount+=2418;
            } else if (item==63){
                $scope.formData.totalServiceAmount+=2457;
            } else if (item==64){
                $scope.formData.totalServiceAmount+=2496;
            } else if (item==65){
                $scope.formData.totalServiceAmount+=2535;
            } else if (item==66){
                $scope.formData.totalServiceAmount+=2574;
            } else if (item==67){
                $scope.formData.totalServiceAmount+=2613;
            } else if (item==68){
                $scope.formData.totalServiceAmount+=2652;
            } else if (item==69){
                $scope.formData.totalServiceAmount+=2691;
            } else if (item==70){
                $scope.formData.totalServiceAmount+=2730;
            } else if (item==71){
                $scope.formData.totalServiceAmount+=2769;
            } else if (item==72){
                $scope.formData.totalServiceAmount+=2808;
            } else if (item==73){
                $scope.formData.totalServiceAmount+=2847;
            } else if (item==74){
                $scope.formData.totalServiceAmount+=2886;
            } else if (item==75){
                $scope.formData.totalServiceAmount+=2925;
            } else if (item==76){
                $scope.formData.totalServiceAmount+=2964;
            } else if (item==77){
                $scope.formData.totalServiceAmount+=3003;
            } else if (item==78){
                $scope.formData.totalServiceAmount+=3042;
            } else if (item==79){
                $scope.formData.totalServiceAmount+=3081;
            } else if (item==80){
                $scope.formData.totalServiceAmount+=3120;
            } else if (item==81){
                $scope.formData.totalServiceAmount+=3159;
            } else if (item==82){
                $scope.formData.totalServiceAmount+=3198;
            } else if (item==83){
                $scope.formData.totalServiceAmount+=3237;
            } else if (item==84){
                $scope.formData.totalServiceAmount+=3276;
            } else if (item==85){
                $scope.formData.totalServiceAmount+=3315;
            } else if (item==86){
                $scope.formData.totalServiceAmount+=3354;
            } else if (item==87){
                $scope.formData.totalServiceAmount+=3393;
            } else if (item==88){
                $scope.formData.totalServiceAmount+=3432;
            } else if (item==89){
                $scope.formData.totalServiceAmount+=3471;
            } else if (item==90){
                $scope.formData.totalServiceAmount+=3510;
            } else if (item==91){
                $scope.formData.totalServiceAmount+=3549;
            } else if (item==92){
                $scope.formData.totalServiceAmount+=3588;
            } else if (item==93){
                $scope.formData.totalServiceAmount+=3627;
            } else if (item==94){
                $scope.formData.totalServiceAmount+=3666;
            } else if (item==95){
                $scope.formData.totalServiceAmount+=3705;
            } else if (item==96){
                $scope.formData.totalServiceAmount+=3744;
            } else if (item==97){
                $scope.formData.totalServiceAmount+=3783;
            } else if (item==98){
                $scope.formData.totalServiceAmount+=3822;
            } else if (item==99){
                $scope.formData.totalServiceAmount+=3861;
            } else if (item==100){
                $scope.formData.totalServiceAmount+=3900;
            }
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
            }
        });
        //DID
        angular.forEach(formData.cloudConnectCapDID,function(item,index){
            if(formData.type.cloudConnectCap){
                if(item==10){
                $scope.formData.totalServiceAmount+=14.95;
                } else if (item==50){
                $scope.formData.totalServiceAmount+=24.95;
                } else if (item==100){
                $scope.formData.totalServiceAmount+=34.95;
                } else if (item==150){
                $scope.formData.totalServiceAmount+=44.95;
                } else if (item==200){
                $scope.formData.totalServiceAmount+=54.95;
                }
                $scope.proposalData.sum+=parseFloat(item);
                $scope.proposalData.landlineNumRow++;
            }
            });

        // discount
        angular.forEach(formData.bFUntimedDiscount,function(item,index){
        if(formData.type.cloudConnectCap){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
        }
        });

        // Solution Standard
        angular.forEach(formData.voiceSolutionChannel,function(item,index){
          if(formData.type.voiceSolution){
            if(item==3){
               $scope.formData.totalServiceAmount+=149.00;
            } else if (item==4){
              $scope.formData.totalServiceAmount+=159.00;
            } else if (item==5){
              $scope.formData.totalServiceAmount+=169.00;
            } else if (item==6){
              $scope.formData.totalServiceAmount+=179.00;
            } else if (item==7){
              $scope.formData.totalServiceAmount+=189.00;
            } else if (item==8){
              $scope.formData.totalServiceAmount+=199.00;
            } else if (item==9){
              $scope.formData.totalServiceAmount+=209.00;
            } else if (item==10){
              $scope.formData.totalServiceAmount+=219.00;
            } else if (item==11){
              $scope.formData.totalServiceAmount+=229.00;
            } else if (item==12){
              $scope.formData.totalServiceAmount+=239.00;
            } else if (item==13){
              $scope.formData.totalServiceAmount+=249.00;
            } else if (item==14){
              $scope.formData.totalServiceAmount+=259.00;
            } else if (item==15){
              $scope.formData.totalServiceAmount+=269.00;
            } else if (item==16){
              $scope.formData.totalServiceAmount+=279.00;
            } else if (item==17){
              $scope.formData.totalServiceAmount+=289.00;
            } else if (item==18){
              $scope.formData.totalServiceAmount+=299.00;
            } else if (item==19){
              $scope.formData.totalServiceAmount+=309.00;
            } else if (item==20){
              $scope.formData.totalServiceAmount+=319.00;
            } else if (item==21){
              $scope.formData.totalServiceAmount+=329.00;
            } else if (item==22){
              $scope.formData.totalServiceAmount+=339.00;
            } else if (item==23){
              $scope.formData.totalServiceAmount+=349.00;
            } else if (item==24){
              $scope.formData.totalServiceAmount+=359.00;
            } else if (item==25){
              $scope.formData.totalServiceAmount+=369.00;
            } else if (item==26){
              $scope.formData.totalServiceAmount+=379.00;
            } else if (item==27){
              $scope.formData.totalServiceAmount+=389.00;
            } else if (item==28){
              $scope.formData.totalServiceAmount+=399.00;
            } else if (item==29){
              $scope.formData.totalServiceAmount+=409.00;
            } else if (item==30){
              $scope.formData.totalServiceAmount+=419.00;
            }
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceSolutionFaxQty,function(item,index){
        if(formData.type.voiceSolution){
          $scope.formData.totalServiceAmount+= (item*9.95);
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }
        });

        angular.forEach(formData.voiceSolutionDID,function(item,index){
          if(formData.type.voiceSolution){
            if(item==10){
              $scope.formData.totalServiceAmount+=14.95;
            } else if (item==50){
              $scope.formData.totalServiceAmount+=24.95;
            } else if (item==100){
              $scope.formData.totalServiceAmount+=34.95;
            }
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

                  // discount
        angular.forEach(formData.bFSolutionDiscount,function(item,index){
          if(formData.type.voiceSolution){
            $scope.formData.totalServiceDiscount+=parseFloat(item);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.bFSolutionCallDiscount,function(item,index){
          if(formData.type.voiceSolution){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.callDisAdded = 1;
            $scope.proposalData.landlineNumRow++;
          }
        });
        // Essential cap
        angular.forEach(formData.voiceEssentialChannel,function(item,index){
          if(formData.type.voiceEssential){
            if(item==4){
              $scope.formData.totalServiceAmount+=196.00;
            } else if(item==6){
              $scope.formData.totalServiceAmount+=294.00;
            } else if (item==8){
              $scope.formData.totalServiceAmount+=392.00;
            } else if (item==10){
              $scope.formData.totalServiceAmount+=490.00;
            } else if (item==12){
              $scope.formData.totalServiceAmount+=588.00;
            } else if (item==14){
              $scope.formData.totalServiceAmount+=686.00;
            } else if (item==16){
              $scope.formData.totalServiceAmount+=784.00;
            } else if (item==18){
              $scope.formData.totalServiceAmount+=882.00;
            } else if (item==20){
              $scope.formData.totalServiceAmount+=980.00;
            }
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceEssentialDID,function(item,index){
          if(formData.type.voiceEssential){
            if(item==10){
              $scope.formData.totalServiceAmount+=9.99;
            }else if (item==20){
              $scope.formData.totalServiceAmount+=15.99;
            } else if (item==50){
              $scope.formData.totalServiceAmount+=19.99;
            } else if (item==100){
              $scope.formData.totalServiceAmount+=25.99;
            }
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.ipMidbandPlansVoice,function(item,index){
          if(formData.type.voiceEssential){
            $scope.formData.totalServiceAmount+=item.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.ipMidbandDownloadVoice,function(item,index){
          if(formData.type.voiceEssential){
            $scope.formData.totalServiceAmount+=item.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

                // discount
        angular.forEach(formData.bECapDiscount,function(item,index){
          if(formData.type.voiceEssential){
            $scope.formData.totalServiceDiscount+=parseFloat(item);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.ipMidbandDis,function(item,index){
          if(formData.type.voiceEssential){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        // adsl 2+
        angular.forEach(formData.adsl2Plans,function(item,index){
          if(formData.type.adsl2){
             $scope.formData.totalServiceAmount+=item.price;
             $scope.proposalData.sum+=parseFloat(item);
             $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceCompAnalougeDSL,function(item,index){
          if(formData.type.adsl2){
            $scope.formData.totalServiceAmount+=(item*39.95);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

                // discount
        angular.forEach(formData.adsl2Dis,function(item,index){
          if(formData.type.adsl2){
            $scope.formData.totalServiceDiscount+=parseFloat(item);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceCompAnalougeDisDSL,function(item,index){
          if(formData.type.adsl2){
            $scope.formData.totalServiceDiscount+=parseFloat(item);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        // NBN unlimited
        angular.forEach(formData.UnlimitedPlans,function(item,index){
          if(formData.type.nbnUnlimitedPlans){
            $scope.formData.totalServiceAmount+=parseFloat(item.price);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceCompAnalougeNBNUnli,function(item,index){
          if(formData.type.nbnUnlimitedPlans){
            $scope.formData.totalServiceAmount+=(item*39.95 );
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });
                  // discount
        angular.forEach(formData.nbnUnlimitedDis,function(item,index){
          if(formData.type.nbnUnlimitedPlans){
            $scope.formData.totalServiceDiscount+=parseFloat(item);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.voiceCompAnalougeDisNBNUnli,function(item,index){
          if(formData.type.nbnUnlimitedPlans){
            $scope.formData.totalServiceDiscount+=parseFloat(item);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        // Midband Monthly
        angular.forEach(formData.ipMidbandPlans,function(item,index){
          if(formData.type.ipMidband){
            $scope.formData.totalServiceAmount+=item.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.ipMidbandDownload,function(item,index){
          if(formData.type.ipMidband){
            $scope.formData.totalServiceAmount+=item.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

                  // discount
        angular.forEach(formData.ipMidbandDis,function(item,index){
          if(formData.type.ipMidband){
            $scope.formData.totalServiceDiscount+=parseFloat(item);
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        // Midband Unlimited

        angular.forEach(formData.ipMidbandUnliPlans,function(item,index){
          if(formData.type.ipMidbandUnli){
            $scope.formData.totalServiceAmount+=item.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });
                    // discount
        angular.forEach(formData.ipMidbandUnliDis,function(item,index){
          if(formData.type.ipMidbandUnli){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        //ethernet
        angular.forEach(formData.ethernetPlans,function(item,index){
          if(formData.type.ethernet){
            $scope.formData.totalServiceAmount+=item.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.ethernetDis,function(item,index){
          if(formData.type.ethernet){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        // Fibre Unlimited
        angular.forEach(formData.fibreUtPlans,function(item,index){
          if(formData.type.fibre){
            $scope.formData.totalServiceAmount+=item.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });
              // discount
        angular.forEach(formData.fibreDis,function(item,index){
          if(formData.type.fibre){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        // telstra untimed item.boltOn.price
        angular.forEach(formData.configMobPort,function(item,index){
          if(formData.type.telstraUntimed){
            $scope.formData.totalServiceAmount+=item.confPatPlan.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.configMobPort,function(item,index){
          if(formData.type.telstraUntimed){
            $scope.formData.totalServiceAmount+=item.boltOn.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

                // discount
        if(formData.type.telstraUntimed){
          $scope.formData.totalServiceDiscount+=parseFloat(formData.telstraUntimedDis);
          $scope.proposalData.sum+=parseFloat(item);
          $scope.proposalData.landlineNumRow++;
        }

        // telstra Wireless Super
        angular.forEach(formData.telstraWirelessSuperPlans,function(item,index){
          if(formData.type.telstraWirelessSuper){
            $scope.formData.totalServiceAmount+=item.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        angular.forEach(formData.telstraWirelessSuperDis,function(item,index){
          if(formData.type.telstraWirelessSuper){
            $scope.formData.totalServiceDiscount+=item;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        // telstra Wireless
        angular.forEach(formData.telstraWirelessPlans,function(item,index){
            if(formData.type.telstraWireless){
              $scope.formData.totalServiceAmount+=item.price;
              $scope.proposalData.sum+=parseFloat(item);
              $scope.proposalData.landlineNumRow++;
            }
          });
  
          angular.forEach(formData.telstraWirelessDis,function(item,index){
            if(formData.type.telstraWireless){
              $scope.formData.totalServiceDiscount+=item;
              $scope.proposalData.sum+=parseFloat(item);
              $scope.proposalData.landlineNumRow++;
            }
          });

        // Mobile Untimed
        angular.forEach(formData.configMobPort,function(item,index){
          if(formData.type.mobileUt){
            $scope.formData.totalServiceAmount+=item.confPatPlan.price;
            $scope.proposalData.sum+=parseFloat(item);
            $scope.proposalData.landlineNumRow++;
          }
        });

        if(formData.type.mobileUt){
          $scope.formData.totalServiceDiscount+=parseFloat(formData.mobileUtDis);
        }        


           // 1300/1800 Rate
           angular.forEach(formData.rate131300.qt1800,function(item,index){
            if($scope.formData.type.rate131300){
                $scope.formData.totalServiceAmount+= (item*19.95);
                $scope.proposalData.sum+=parseFloat(item);
                $scope.proposalData.landlineNumRow++;
            }
          });
          angular.forEach(formData.rate131300.qt1300,function(item,index){
            if($scope.formData.type.rate131300){
                $scope.formData.totalServiceAmount+= (item*19.95);
                $scope.proposalData.sum+=parseFloat(item);
                $scope.proposalData.landlineNumRow++;
            }
          });
          angular.forEach(formData.rate131300.qt13,function(item,index){
            if($scope.formData.type.rate131300){
                $scope.formData.totalServiceAmount+= (item*19.95);
                $scope.proposalData.sum+=parseFloat(item);
                $scope.proposalData.landlineNumRow++;
            }
          });
  
        // 1300/1800 Rate
        angular.forEach(formData.rate131300Dis.qt1800,function(item,index){
            if($scope.formData.type.rateDis131800){
                $scope.formData.totalServiceAmount+= (item*19.95);
                $scope.proposalData.sum+=parseFloat(item);
                $scope.proposalData.landlineNumRow++;
            }
            });
            angular.forEach(formData.rate131300Dis.qt1300,function(item,index){
            if($scope.formData.type.rateDis131300){
                $scope.formData.totalServiceAmount+= (item*19.95);
                $scope.proposalData.sum+=parseFloat(item);
                $scope.proposalData.landlineNumRow++;
            }
            });
            angular.forEach(formData.rateDis131300.discount,function(item,index){
            if($scope.formData.type.rateDis131300){
                $scope.formData.totalServiceDiscount+=item;
                $scope.proposalData.sum+=parseFloat(item);
                $scope.proposalData.landlineNumRow++;
            }
        });

        // end asdf

        if(undefined !== $scope.formData.schItems[0] && $scope.formData.schItems[0].length) {
            var item1 = $scope.formData.schItems[0].length;
        } else {
            var item1 = 0;
        }
        if(undefined !== $scope.formData.schItems[1] && $scope.formData.schItems[1].length) {
            var item2 = $scope.formData.schItems[1].length;
        } else {
            var item2 = 0;
        }
        if(undefined !== $scope.formData.schItems[2] && $scope.formData.schItems[2].length) {
            var item3 = $scope.formData.schItems[2].length;
        } else {
            var item3 = 0;
        }
        if(undefined !== $scope.formData.schItems[3] && $scope.formData.schItems[3].length) {
            var item4 = $scope.formData.schItems[3].length;
        } else {
            var item4 = 0;
        }
        if(undefined !== $scope.formData.schItems[4] && $scope.formData.schItems[4].length) {
            var item5 = $scope.formData.schItems[4].length;
        } else {
            var item5 = 0;
        }
        if(undefined !==  $scope.formData.schAddItems[0] && $scope.formData.schAddItems[0].length) {
            var item6 = $scope.formData.schAddItems[0].length;
        } else {
            var item6 = 0;
        }
        if(undefined !==  $scope.formData.schAddItems[1] && $scope.formData.schAddItems[1].length) {
            var item7 = $scope.formData.schAddItems[1].length;
        } else {
            var item7 = 0;
        }
        if(undefined !==  $scope.formData.schAddItems[2] && $scope.formData.schAddItems[2].length) {
            var item8 = $scope.formData.schAddItems[2].length;
        } else {
            var item8 = 0;
        }
        if(undefined !==  $scope.formData.schAddItems[3] && $scope.formData.schAddItems[3].length) {
            var item9 = $scope.formData.schAddItems[3].length;
        } else {
            var item9 = 0;
        }
        if(undefined !==  $scope.formData.schAddItems[4] && $scope.formData.schAddItems[4].length) {
            var item10 = $scope.formData.schAddItems[4].length;
        } else {
            var item10 = 0;
        }

        //    for call discount to add 1 row 
        if($scope.proposalData.callDisAdded == 1) {
            $scope.proposalData.landlineNumRow++;
        }
        $scope.proposalData.dis = $scope.proposalData.dis.toFixed(2);
        $scope.proposalData.callDis = $scope.proposalData.callDis.toFixed(2);

        $scope.proposalData.totalDis = parseFloat($scope.proposalData.dis) + parseFloat($scope.proposalData.callDis);

        $scope.proposalData.subSum = $scope.proposalData.sum - $scope.proposalData.dis;

        $scope.proposalData.subSum = $scope.proposalData.subSum.toFixed(2);

        console.log( $scope.proposalData.billingType);
        if ($scope.proposalData.billingType == 'outright') {
            $scope.proposalData.totalSum = parseFloat($scope.proposalData.subSum);
        } else {
            $scope.proposalData.totalSum = parseFloat($scope.proposalData.subSum) + parseFloat($scope.proposalData.leaser);
        }
        $scope.proposalData.totalSum = $scope.proposalData.totalSum.toFixed(2);

        console.log($scope.proposalData);
        // Get total row of free text equipment
        var totalAddEquiptRow = $scope.proposalData.schAddItems[0].reduce(function (n, product) {
            return n + (product.status > 0);
        }, 0);
        //   For terms and condition to make it responsive
        var totalEquiptRow = $scope.proposalData.schItems[0].reduce(function (n, product) {
            return n + (product.status > 0);
        }, 0);
        
        $scope.proposalData.totalNumEquiptRow = parseInt(totalEquiptRow) + parseInt(totalAddEquiptRow);
        $scope.proposalData.getTotalServices = parseInt($scope.proposalData.landlineNumRow) + parseInt(totalEquiptRow) + parseInt(totalAddEquiptRow);

                
        
         $scope.formData.totalEquipments = parseInt(item1) + parseInt(item2) + parseInt(item3) + parseInt(item4) + parseInt(item5) + parseInt(item6) + parseInt(item7) + parseInt(item8) + parseInt(item9) + parseInt(item10);

        console.log("Total : " + $scope.formData.totalServiceAmount);
        $rootScope.draftMode = false;
        //$scope.sum = $scope.formData.dID-$scope.formData.bFCapDiscount;
        // Check if exists empty location fields.
        var comLocationObj = $('input.' + templateName);
        var emptyLocationCnt = 0;
        angular.forEach(comLocationObj, function(item, index) {
            var location = $(item).val();
            if (!location) {
                emptyLocationCnt ++;                
            }
        });
        
        if (emptyLocationCnt) {
            alert("Please ensure that all locations are filled-up properly.")
            return;
        }

        $(".reAssign").each(function() {
            var id = $(this).prop("id");
            if (id=='alarmPlans'){
                var alarmPlan = $scope.formData.alarmPlans;
                formData[id] = $(this).val();
                $scope.formData.alarmPlans = alarmPlan;
            } else {
                formData[id] = $(this).val();
            }
        });

        formData["type"] = {};

        $("input[name=formTypes]:checked").each(function() {
            var id = $(this).prop("id");
            formData["type"][id] = $(this).prop("checked");
        });

        if (formData.type.configApp) {
            var telNo = new Array();
            $(".telNo").each(function() {
                  var item = $(this).find("input");
                  var idCount = item.data('count');
                  var telRange = $('#telItemRange' + '-' + idCount).val()!=''?' to '+$('#telItemRange' + '-' + idCount).val():'';
                  var obj = {
                                  tel : item[0].value + telRange 
                            };
                  telNo.push(obj);
            });
            $scope.formData.configAppTelNo = telNo;
            // var mobPort = new Array();
            // $(".mob-port-box").each(function() {
            //     var confAccHolder = $(this).find(".confAccHolder");
            //     var confMobNo = $(this).find(".confMobNo");
            //     var confProvider = $(this).find(".confProvider");
            //     var confAccNo = $(this).find(".confAccNo");
            //     var confPatPlan = $(this).find(".confPatPlan");
            //     var confDob = $(this).find(".confDob");
            //     var confDLicence = $(this).find(".confDLicence");

            //     var obj = {
            //                     confAccHolder : confAccHolder[0].value,
            //                     confMobNo : confMobNo[0].value,
            //                     confProvider : confProvider[0].value,
            //                     confAccNo : confAccNo[0].value,
            //                     confPatPlan : confPatPlan[0].value,
            //                     confDob : confDob[0].value,
            //                     confDLicence : confDLicence[0].value,
            //               };
            //     mobPort.push(obj);
            // });
            $scope.formData.configMobPort = $scope.configMobPort;

            // if ((!$scope.formData.configCarrier) || (!$scope.formData.configAccno)) {
        if (!$scope.checkAccount()){
              alert("Please  assign a Carrier or Account Number to the Billing Form.");
              return;
            }
        }

        if (formData.type.monitoring_solution){
            if (($scope.formData.advancedBussinessProject) && (!$scope.formData.panelType)){
                alert("Please complete the details for Panel Type.");
                return;
            }
        }

        if(formData.type.rental || formData.type.leasing ||formData.singleOrder || formData.type.rpm || formData.type.rental2 || formData.type.classic) {
            formData["rental"] = {};
            var rental = parseInt(formData.aPayment.replace(/[^\d.]/g,''));
            formData["aGST"] = isNaN(rental) ? '0.00' : (rental*10/100).toFixed(2);
            formData["aTotal"] = isNaN(rental) ? '0.00' : (rental + rental*10/100).toFixed(2);
        }

        var schedule = new Array();
        var scheduleOptions = new Array();
        angular.forEach($scope.formData.schItems, function(data) {
            if (angular.isDefined($scope.formData.schOption)){
                if (angular.isDefined($scope.formData.schOption[data.count])) {
                    var obj = {
                                qty : $scope.formData.schQty[data.count],
                                desc :$scope.formData.schOption[data.count].item != '_' ? $scope.formData.schOption[data.count].item : ''
                            };
                    scheduleOptions.push(obj);
                }
            }
        });


        $(".schedule-goods").each(function() {
            var item = $(this).find("input");
            var obj = { qty : item[0].value,
                        desc :item[1].value,
                        //itemNo : item[2].value,
                        //serNo : item[3].value
                    };
            schedule.push(obj);
        });

        $scope.formData.scheduleOptions = scheduleOptions;
        $scope.formData.schedule = schedule;

        if($scope.editId) {
            $rootScope.editId = $scope.editId;
        }
        //console.log(formData);
        $rootScope.formData = formData;
        ///$scope.formData.totalServiceAmount=totalServiceAmount;
        ngDialog.open({
            template: $scope.template,
            plan: true,
            controller : 'FormCtrl',
            width: 900,
            height: 600,
            scope: $scope,
            className: 'ngdialog-theme-default',
            preCloseCallback : function(value) {
                var allSigned = true;
                $(".btn-clicksign").css('display', 'none');             
                  $("#sign-counter").css('display', 'none');
                $(".signatureImg").each(function() {
                    console.log($(this).attr('id'),$(this).children("img").length);
                    if ($(this).children("img").length == 0) {
                        allSigned = false;
                        return;
                    }
                });
           
                if (!allSigned && templateName !== 'assessment_questionnaire') {
                  if (templateName!='newsummary'){
                   $('#isSigned').val('F');
                  }
                   if (confirm("You have not signed all the fields.Do you want to exit?")) {
                       return true;
                   }
                   return false;
                    $timeout(angular.noop());
                } else {
                    $('#isSigned').val('T');
                    if (templateName=='newsummary'){
                      $('#form_complete').val('T');
                    }
                    // var button = $('#formData').find('a[href="#finish"]');
                    // button.show();
                    //alert($('#isSigned').val());                  // In case of forms with location fields.
                    if ($scope.locationAvailableTemps.indexOf(templateName) !== -1 && $scope.items[templateName].length > 1) {
                        $scope.htmlData[templateName] = [];
                        angular.forEach($scope.items[templateName], function(item, index) {
                            var clone = $('.ngdialog-content').clone();
                            clone.find('[class*="preview"]').each(function(divIndex,data){
                                //console.log(divIndex,data,index);  
                                if (index!=divIndex)  {
                                    clone.find('.preview-' + divIndex).remove();
                                    //console.log(clone.html());
                                }                        
                            });
                            //clone.find('.preview-' + index).remove();
                            var tableHtml = clone.html();
                            $scope.htmlData[templateName].push(tableHtml);
                        });
                    } else {
                        $scope.htmlData[templateName] = $(".ngdialog-content").html();
                    }

                    $scope[templateName] = true;
                    $timeout(angular.noop());
                }
                // assessment questionnaire required fields and sign
                if (templateName == 'assessment_questionnaire') {
                    $scope.formData.assessment = {};
                    $scope.formData.assessment.q1 = document.getElementById("assessment-q1").value;
                    $scope.formData.assessment.q1drop1  = document.getElementById("assessment-q1drop1").value;
                    $scope.formData.assessment.q1drop2  = document.getElementById("assessment-q1drop2").value;
                    $scope.formData.assessment.q2  = document.getElementById("assessment-q2").value;
                    $scope.formData.assessment.q2text  = document.getElementById("assessment-q2text").value;
                    console.log($scope.formData.assessment.q2);
                    console.log($scope.formData.assessment.q1);
                    if ($('#assessment-cus-sg-1').find('img').length == 0 && confirm("You have not signed all the fields.")) {
                        return false;
                    } else if( $scope.formData.assessment.q1 == '? undefined:undefined ?' ||  $scope.formData.assessment.q1drop1 == '? undefined:undefined ?' ||  $scope.formData.assessment.q1drop2 == '? undefined:undefined ?' || $scope.formData.assessment.q2 == '? string:? undefined:undefined ? ?' ) {
                        confirm("All fields are required");
                        return false;
                    } else if( $scope.formData.assessment.q2 == 'yes' &&  $scope.formData.assessment.q2text == '') {
                        confirm("All fields are required");
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        });
      
        $('#btn-signed').click(function(){
            var vehicles = document.querySelectorAll(".signatureImg");
            if(vehicles[length] == undefined) {
                var vehicles = document.querySelectorAll(".signature");
            }
            var ids = [].map.call(vehicles, function(elem) {
                    if ($('#'+elem.id).find('img').length == 0) {
                        return elem.id;  
                    }
            });
            var filtered = ids.filter(function (el) {
                return el != null;
            });
             if(filtered.length == 1 || filtered.length == 0) {
                setTimeout(function () {
                    ngDialog.close();
                }, 2000);
            }
        });

        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            var newHash = 'anchor';
            if ($location.hash() !== newHash) {
                // set the $location.hash to `newHash` and
                // $anchorScroll will automatically scroll to it
                $location.hash('anchor');
            } else {
                // call $anchorScroll() explicitly,
                // since $location.hash hasn not changed
                $location.hash('anchor');
                $anchorScroll();
            }
        });
        // Proposal
        $scope.proposalData = $scope.formData;
        console.log($scope.proposalData);
        console.log($scope.proposalData.getTotalServices);
        $scope.proposalData.schItems = $scope.formData.schItems;
        $scope.proposalData.schOption = $scope.formData.schOption;
        $scope.proposalData.schQty = $scope.formData.schQty;
        // console.log($scope.proposalData.schItems);
        // console.log($scope.proposalData.schOption);
        
            $scope.count = 0;
            if(templateName == 'landscape') {
                $scope.proposalData.orientation = 'A4-L';
                var size = 1150;
            } else {
                $scope.proposalData.orientation = 'A4';
                var size = 900;
            }
            angular.forEach( $scope.proposalData.type, function(value, key) {
                if (value == true) {
                    $scope.count = $scope.count + 1;
                }
            });
            $scope.proposalData.footerCount = $scope.count;
            console.log($scope.proposalData.footerCount);
            console.log($scope.items);
            var userProfile =  Scopes.get('DefaultCtrl').profile;
            var datum = $scope.proposalData;
            datum.quot_num = $scope.quot_num;
            datum.tradingName = datum.tradingName;
            console.log($scope.proposalData.schOption);
            var datePrepared = new Date();
            datum.datePrepared = datePrepared.toDateString();
            datum.preparedBy = userProfile.name;
            datum.preparedEmail = userProfile.email;
            datum.preparePhone = userProfile.phone;
            // $scope.template = "templates/proposal/"+templateName+".html?t=" + _version;        
            $rootScope.formData = formData;
            // console.log($rootScope.formData)
            console.log(datum);
            
            // ngDialog.open({
            //     data: angular.toJson({
            //         proposalData: datum
            //     }),
            //     template: $scope.template,
            //     plan: true,
            //     controller : 'ProposalCtrl',
            //     width: size,
            //     height: 600,
            //     scope: $scope,
            //     className: 'ngdialog-theme-default',
            //     preCloseCallback : function(value) {
            //         var allSigned = true;
    
            //         $(".signatureImg").each(function() {
            //             console.log($(this).attr('id'),$(this).children("img").length);
            //             if ($(this).children("img").length == 0) {
            //                 allSigned = false;
            //                 return;
            //             }
            //         });
    
            //         if (!allSigned) {
            //            $('#isSigned').val('F');
            //            if (confirm("You have not signed all the fields.Do you want to exit?")) {
            //                return true;
            //            }
            //            return false;
            //             $timeout(angular.noop());
            //         } else {
            //             $('#isSigned').val('T');
            //             if ($scope.items[templateName].length > 1) {
            //                 $scope.htmlData[templateName] = [];
            //                 angular.forEach($scope.items[templateName], function(item, index) {
            //                     var clone = $('.ngdialog-content').clone();
            //                     clone.find('[class*="preview"]').each(function(divIndex,data){
            //                         //console.log(divIndex,data,index);  
            //                         if (index!=divIndex)  {
            //                             clone.find('.preview-' + divIndex).remove();
            //                             // console.log(clone.html());
            //                             console.log($scope.items[templateName]);
            //                         }                        
            //                     });
            //                     //clone.find('.preview-' + index).remove();
            //                     var tableHtml = clone.html();
            //                     $scope.htmlData[templateName].push(tableHtml);
            //                 });
            //             } else {
            //                 $scope.htmlData[templateName] = $(".ngdialog-content").html();
            //             }
            //             $scope[templateName] = true;
            //             $timeout(angular.noop());
            //         }
            //     }
            //     });
                console.log($scope.proposalData.type);
             console.log($scope.htmlData);
    }
    $scope.isDraftMode = function (){
        //alert($rootScope.draftMode);
        return $rootScope.draftMode;
    }
    $scope.isSigned = function(form, formName) {
        //alert(form,formName);
        if ($scope[form] && formName) {
            return true;
        } else {
            if (formName == false) {
                if ( angular.isDefined($scope.htmlData[form])) {
                    delete $scope.htmlData[form];
                }
                    $scope[form] = false;
                }

            return false;
        }
    }

    $scope.checkGroupSigned = function(forms, group) {
        var flag = false;
        if (angular.isDefined($scope.formGroups[group]) && angular.isDefined(forms)) {
            angular.forEach($scope.formGroups[group], function(item) {
                if (angular.isDefined(forms[item.id]) && forms[item.id] == true) {
                    if (forms[item.id] == true && !$scope[item.file]) {
                        flag = false;
                        return;
                    } else if (forms[item.id] == true && $scope[item.file]) {
                        flag = true;
                    }
                }
            });
        }
        return flag;
    }

    //console.log($scope.formData.schItems);

    $scope.addRentalGoods =  function(index) {
        // console.log($scope.schAddItemCount[index]);
        $scope.schAddItemCount[index]++;
        $scope.formData.schAddItems[index].push({count:$scope.schAddItemCount[index]+1, status: 1});
    };

    $scope.removeRentalGood = function(count,index) {
        var spliceIndex = $scope.formData.schAddItems[index].indexOf({count:count});
        $scope.formData.schAddItems[index].splice(spliceIndex,1);
    };

    $scope.removePortingAuth = function(index) {

        $scope.configMobPort[index].status = "0";
        
        if($scope.formData.telstraSuperPlans.length>0) {
            $scope.formData.telstraSuperPlans[$scope.configMobPort[index].confPatPlan.itemIndex]--;
            if($scope.configMobPort[index].boltOn.price == '9.89') {
                $scope.formData.boltOnPlans[0]--;
            } 
            if($scope.configMobPort[index].boltOn.price == '31.99') {
                $scope.formData.boltOnPlans[1]--;
            }
        } else {
            // $scope.formData.mobile4GUntimedPlans.splice(index,1);
            $scope.formData.mobile4GUntimedPlans[$scope.configMobPort[index].confPatPlan.itemIndex]--;
        }
        $scope.configMobPort.splice(index,1);

    }

    $scope.$watch("formData.schOption", function(){

    });

    // console.log($scope.schItemCount[0],$scope.formData.schItems[0]);

    $scope. addRentalGoodsOption =  function(index) {
        $scope.schItemCount[index]++;
        $scope.formData.schItems[index].push({count:$scope.schItemCount[index]+1, status: 1});
        // console.log("Installation Location:",index,"Item Count",$scope.schItemCount[index],"Items",$scope.formData.schItems[index]);
    };

    $scope.removeRentalGoodsOption = function(count,index) {
        // $scope.schItemCount[index]--;
        var spliceIndex = $scope.formData.schItems[index].indexOf({count:count});
        $scope.formData.schItems[index].splice(spliceIndex,1);
        // console.log("Installation Location:",index,"Item Count:",$scope.schItemCount[index],"Items",$scope.formData.schItems[index]);
    };


    //

    
    // Add location function
    $scope.addCompanyLocation = function(form_type) {
        if ($scope.items[form_type].length!=0){
            $scope.locationCount = $scope.items[form_type].length-1;
        }
        $scope.locationCount++;
        if (form_type=='voice_solution_standard'){
            $scope.formData.voiceSolutionChannel[$scope.locationCount] = 3;
            $scope.formData.voiceSolutionDID[$scope.locationCount] = 10;
            $scope.formData.bFSolutionDiscount[$scope.locationCount] = 0;
            $scope.formData.bFSolutionCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_solution_untimed'){
            $scope.formData.voiceUntimedChannel[$scope.locationCount] = 199;
            $scope.formData.voiceUntimedDID[$scope.locationCount] = 14.95;
            $scope.formData.bFUntimedDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_comp'){
            $scope.formData.voiceCompAnalouge[$scope.locationCount] = 0;
            $scope.formData.voiceCompBri[$scope.locationCount] = 0;
            $scope.formData.voiceCompPri[$scope.locationCount] = 0;
            $scope.formData.voiceCompDID[$scope.locationCount] = 0;
            $scope.formData.voiceCompBriDis[$scope.locationCount] = 0;
            $scope.formData.voiceCompPriDis[$scope.locationCount] = 0;
            $scope.formData.voiceCompCallDis[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_std'){
            $scope.formData.voiceStdChannel[$scope.locationCount] = 199;
            $scope.formData.voiceStdDID[$scope.locationCount] = 9.99;
            $scope.formData.bFStdDiscount[$scope.locationCount] = 0;
            $scope.formData.bFCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_cap'){
            $scope.formData.voiceStd[$scope.locationCount] = 5;
            $scope.formData.dID[$scope.locationCount] = 10;
            $scope.formData.bFCapDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_essentials_cap'){
            $scope.formData.ipMidbandPlansVoice[$scope.locationCount] = $scope.ip_midband_plans[0];
            $scope.formData.ipMidbandDownloadVoice[$scope.locationCount] = $scope.download_plans[0];
            $scope.formData.voiceEssentialChannel[$scope.locationCount] = 196;
            $scope.formData.voiceEssentialDID[$scope.locationCount] = 9.99;
            $scope.formData.bECapDiscount[$scope.locationCount] = 0;
            $scope.formData.ipMidbandDis[$scope.locationCount] = 0;
            // $scope.formData.ipMidbandDownloadVoice[$scope.locationCount] = 0;
        } 
        if (form_type=='cloud_connect_standard'){
            $scope.formData.cloudConnectStandardChannel[$scope.locationCount] = 2;
            $scope.formData.cloudConnectStandardDID[$scope.locationCount] = 0;
            $scope.formData.cCStandardDiscount[$scope.locationCount] = 0;
            $scope.formData.cCStandardCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='cloud_connect_cap'){
            $scope.formData.cloudConnectCapChannel[$scope.locationCount] = 2;
            $scope.formData.cloudConnectCapDID[$scope.locationCount] = 0;
            $scope.formData.cCCapDiscount[$scope.locationCount] = 0;
            // $scope.formData.cCCapCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='data_adsl'){
            // $scope.formData.adsl2Plans[$scope.locationCount] = 79;
            $scope.formData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
            $scope.formData.adsl2Dis[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDSL[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midband'){
            $scope.formData.ipMidbandDownload[$scope.locationCount] = $scope.download_plans[1];
            $scope.formData.ipMidbandPlans[$scope.locationCount] = $scope.ip_midband_plans[1];
            $scope.formData.ipMidbandDis[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midbandunli'){
            $scope.formData.ipMidbandUnliPlans[$scope.locationCount] = $scope.ip_midbandunli_plans[0];
            $scope.formData.ipMidbandUnliProf[$scope.locationCount] = $scope.professional_install[0];
            $scope.formData.ipMidbandUnliDis[$scope.locationCount] = 0;
        }
        if (form_type=='1300_discounted'){
            $scope.formData.rate131300Dis.qt1800[$scope.locationCount] = 0;
            $scope.formData.rate131300Dis.qt1300[$scope.locationCount] = 0;
            $scope.formData.rateDis131300.discount[$scope.locationCount] = 0;
        }
        if (form_type=='1300'){
            $scope.formData.rate131300.qt1800[$scope.locationCount] = 0;
            $scope.formData.rate131300.qt1300[$scope.locationCount] = 0;
            $scope.formData.rate131300.qt13[$scope.locationCount] = 0;
        }
        if (form_type=='mobile_wireless'){
            $scope.formData.mobileWirelessPlans[$scope.locationCount] = $scope.wireless_cap_plans[0];
            // $scope.formData.mobileWirelessPlans[$scope.locationCount] = 15;
            $scope.formData.mobileWirelessDis[$scope.locationCount] = 0;
        }        
        if (form_type=='fibre'){
            $scope.formData.fibreUtPlans[$scope.locationCount] = $scope.fibre_unli_plans[0];
            $scope.formData.fibreDis[$scope.locationCount] = 0;
        }
        // if (form_type=='adsl'){
        //     $scope.formData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
        // }
        if (form_type=='ethernet'){
            $scope.formData.ethernetPlans[$scope.locationCount] = $scope.ethernet_plans[0];
            $scope.formData.ethernetDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbnUnlimited'){
            $scope.formData.UnlimitedPlans[$scope.locationCount] = $scope.nbnUnlimited_plans[0];
            $scope.formData.nbnUnlimitedDis[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDisNBNUnli[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeNBNUnli[$scope.locationCount] =0;
        }
        if (form_type=='telstra4gSuper'){
            $scope.formData.telstra4gSuperPlans[$scope.locationCount] = $scope.telstra4gSuper_plans[0];
        }
        if (form_type=='telstraUntimed'){
            $scope.formData.telstraUntimedPlans[$scope.locationCount] = $scope.telstraUntimed_plans[0];
        }
        if (form_type=='telstraWirelessSuper'){
            $scope.formData.telstraWirelessSuperPlans[$scope.locationCount] = $scope.telstraWirelessSuper_plans[0];
            $scope.formData.telstraWirelessDis[$scope.locationCount] = 0;
            $scope.formData.comLocation['telstraWirelessSuper'][$scope.locationCount] = $scope.formData.comLocation['telstraWirelessSuper'][0];
            $scope.formData.comSuburb['telstraWirelessSuper'][$scope.locationCount] = $scope.formData.comSuburb['telstraWirelessSuper'][0];
            $scope.formData.comZipCode['telstraWirelessSuper'][$scope.locationCount] = $scope.formData.comZipCode['telstraWirelessSuper'][0];
        }
        if (form_type=='telstraWireless'){
            $scope.formData.telstraWirelessPlans[$scope.locationCount] = $scope.telstraWireless_plans[0];
            $scope.formData.telstraWirelessDis[$scope.locationCount] = 0;
            $scope.formData.comLocation['telstraWireless'][$scope.locationCount] = $scope.formData.comLocation['telstraWireless'][0];
            $scope.formData.comSuburb['telstraWireless'][$scope.locationCount] = $scope.formData.comSuburb['telstraWireless'][0];
            $scope.formData.comZipCode['telstraWireless'][$scope.locationCount] = $scope.formData.comZipCode['telstraWireless'][0];
        }
        if (form_type=='schedule_of_goods'){
            $scope.formData.schItems[$scope.locationCount] = [{count:1,status:1}];
            $scope.schItemCount[$scope.locationCount] = 1;
            $scope.formData.schAddItems[$scope.locationCount] = [{count:1, status:1}];
            $scope.schAddItemCount[$scope.locationCount] = 1;
            //console.log($scope.formData.schItems);
        }
        $scope.items[form_type].push($scope.locationCount);
    };

    $scope.removeOptionsBlock = function(form_type, index) {
        var comLocationObj = $scope.formData.comLocation[form_type];
        comLocationObj.splice(index, 1);
        $scope.items[form_type].splice(index, 1);
        if (form_type[form_type]=='schedule_of_goods'){
            $scope.formData.schItems.splice(index,1);
        }
    };

    $scope.formData.wPosition = "Sales";
    
    if ($rootScope.viewFormData)    {
        $scope.formData = $rootScope.viewFormData.data;
        // console.log($scope.formData.printers);

            if(angular.isUndefined($scope.formData.comLocation['nbf_cap']) ||angular.isUndefined($scope.formData.comLocation['schedule_of_goods']) ||
                angular.isUndefined($scope.formData.comLocation['nbf_std']) ||angular.isUndefined($scope.formData.comLocation['voice_solution_untimed']) ||
                angular.isUndefined($scope.formData.comLocation['voice_solution_standard']) ||angular.isUndefined($scope.formData.comLocation['voice_essentials_cap']) ||
                angular.isUndefined($scope.formData.comLocation['cloud_connect_standard']) ||
                angular.isUndefined($scope.formData.comLocation['cloud_connect_cap']) ||
                angular.isUndefined($scope.formData.comLocation['voice_comp']) ||angular.isUndefined($scope.formData.comLocation['1300']) ||
                angular.isUndefined($scope.formData.comLocation['nbnUnlimited']) ||angular.isUndefined($scope.formData.comLocation['ethernet']) ||
                angular.isUndefined($scope.formData.comLocation['fibre']) ||angular.isUndefined($scope.formData.comLocation['telstraWireless']) ||
                angular.isUndefined($scope.formData.comLocation['telstraWirelessSuper']) ||
                angular.isUndefined($scope.formData.comLocation['ip_midbandunli']) ||angular.isUndefined($scope.formData.comLocation['ip_midband']) ||
                angular.isUndefined($scope.formData.comLocation['mobile_wireless']) || angular.isUndefined($scope.formData.comLocation['data_adsl'])) 
            {
                // alert("null");
            } else{
                for (var index=0;index<$scope.formData.comLocation['schedule_of_goods'].length;index++){
                    $scope.locationCount = $scope.items['schedule_of_goods'].length-1;
                    $scope.items['schedule_of_goods'][index] = index;

                    angular.forEach($scope.formData.schItems[index],function(data,ind){
                        // console.log("value",data,"index",ind);
                        $scope.schItemCount[index] = data.count-1;
                    });
                    // console.log("value1",$scope.formData.schAddItems[index]);
                    angular.forEach($scope.formData.schAddItems[index],function(add,indx){
                        // console.log("value1",add,"index1",indx);
                        $scope.schAddItemCount[index] = add.count-1;       
                    });
                }
                for (var index=0;index<$scope.formData.comLocation['nbf_cap'].length;index++){
                    $scope.locationCount = $scope.items['nbf_cap'].length-1;
                    $scope.items['nbf_cap'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['nbf_std'].length;index++){
                    $scope.locationCount = $scope.items['nbf_std'].length-1;
                    $scope.items['nbf_std'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['voice_solution_untimed'].length;index++){
                    $scope.locationCount = $scope.items['voice_solution_untimed'].length-1;
                    $scope.items['voice_solution_untimed'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['voice_solution_standard'].length;index++){
                    $scope.locationCount = $scope.items['voice_solution_standard'].length-1;
                    $scope.items['voice_solution_standard'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['voice_essentials_cap'].length;index++){
                    $scope.locationCount = $scope.items['voice_essentials_cap'].length-1;
                    $scope.items['voice_essentials_cap'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['cloud_connect_standard'].length;index++){
                    $scope.locationCount = $scope.items['cloud_connect_standard'].length-1;
                    $scope.items['cloud_connect_standard'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['cloud_connect_cap'].length;index++){
                    $scope.locationCount = $scope.items['cloud_connect_cap'].length-1;
                    $scope.items['cloud_connect_cap'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['voice_comp'].length;index++){
                    $scope.locationCount = $scope.items['voice_comp'].length-1;
                    $scope.items['voice_comp'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['1300'].length;index++){
                    $scope.locationCount = $scope.items['1300'].length-1;
                    $scope.items['1300'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['1300_discounted'].length;index++){
                    $scope.locationCount = $scope.items['1300_discounted'].length-1;
                    $scope.items['1300_discounted'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['data_adsl'].length;index++){
                    $scope.locationCount = $scope.items['data_adsl'].length-1;
                    $scope.items['data_adsl'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['nbnUnlimited'].length;index++){
                    $scope.locationCount = $scope.items['nbnUnlimited'].length-1;
                    $scope.items['nbnUnlimited'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['ethernet'].length;index++){
                    $scope.locationCount = $scope.items['ethernet'].length-1;
                    $scope.items['ethernet'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['fibre'].length;index++){
                    $scope.locationCount = $scope.items['fibre'].length-1;
                    $scope.items['fibre'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['telstraWirelessSuper'].length;index++){
                    $scope.locationCount = $scope.items['telstraWirelessSuper'].length-1;
                    $scope.items['telstraWirelessSuper'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['telstraWireless'].length;index++){
                    $scope.locationCount = $scope.items['telstraWireless'].length-1;
                    $scope.items['telstraWireless'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['ip_midbandunli'].length;index++){
                    $scope.locationCount = $scope.items['ip_midbandunli'].length-1;    
                    $scope.items['ip_midbandunli'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['ip_midband'].length;index++){
                    $scope.locationCount = $scope.items['ip_midband'].length-1;
                    $scope.items['ip_midband'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['mobile_wireless'].length;index++){
                    $scope.locationCount = $scope.items['mobile_wireless'].length-1;
                    $scope.items['mobile_wireless'][index] = index;
                    }
            }

        $scope.editId = $rootScope.viewFormData.id;
         if (angular.isDefined($scope.formData.schedule)) {
            angular.forEach($scope.formData.schedule, function(item, count) {
                if (count == 0) {
                  $(".qunatity").val(item.qty);
                  $(".desc").val(item.desc);
                } else {
                  var wrapped = '<div class="row">\n\
                                            <div class="col-md-2 col-sm-2">\n\
                                                <button class="btn btn-primary" id="schedule-'+count+'">\n\
                                                   <i class="fa fa-times"></i> \n\
                                                </button></div>\n\
                                            <div class="col-md-10 schedule-goods">\n\
                                                <div class="col-md-3 col-sm-3">\n\
                                                    <div class="form-group">\n\
                                                        <div class="col-xs-4">\n\
                                                            <label class="control-label">Quantity</label>\n\
                                                        </div>\n\
                                                        <div class ="col-xs-8">\n\
                                                            <input type="number" value='+item.qty+' class="qunatity form-control">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="col-md-9 col-sm-9">\n\
                                                    <div class="form-group">\n\
                                                        <div class="col-xs-3 text-right">\n\
                                                                <label class="control-label">Description</label>\n\
                                                        </div>\n\
                                                        <div class ="col-xs-9">\n\
                                                            <input type="text"  value='+item.desc+' class ="desc form-control">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                </div>';
                    $(".rentalGoods").append(wrapped);
                    $("#schedule-"+count).click(function() {
                        $(this).parent().parent().remove();
                    });
                    $scope.scheduleCount = count;
                }
            });
        }
        if (angular.isDefined($scope.formData.cState)) {
            $scope.formData.cState = $scope.formData.cState;
        }
        if (angular.isDefined($scope.formData.type)) {
            if ($scope.formData.type.configApp) {
                if (angular.isDefined($scope.formData.configAppTelNo)) {
                    angular.forEach($scope.formData.configAppTelNo, function(item,count) {
                      $scope.addTel();
                      $timeout(function() {
                          $("#telItemConfigApp-"+count).val(item.tel);
                      });
                    });
                }

                if (angular.isDefined($scope.formData.configMobPort)) {
                    $timeout(function() {
                        $scope.configMobPort = $scope.formData.configMobPort;
                    });
                }
                if ($scope.formData.config) {
                   if ($scope.formData.config.pstn) {
                       $scope.formData.config.pstn = true;
                   } else {
                        $scope.formData.config.pstn = false;
                   }
                   if ($scope.formData.config.lineHunt) {
                       $scope.formData.config.lineHunt = true;
                   }
                   if ($scope.formData.config.ISDN) {
                       $scope.formData.config.ISDN = true;
                   } else {
                        $scope.formData.config.pstn = false;
                   }
                   if ($scope.formData.config.indial) {
                       $scope.formData.config.indial = true;
                   }
                   if ($scope.formData.config.port ) {
                       $scope.formData.config.port = true;
                   }
                }
            }


        }

        $rootScope.viewFormData = null;
    }

    $("input[name=formTypes]").on("click", function() {
       if ($(this).prop("checked") === true) {
          var className = $(this).prop("class");
          var id = $(this).prop("id");
          if (className) {
              $("."+className).each(function() {
                 $(this).prop("checked",false);
              });
              $("#"+id).prop("checked", true);
          }
       }
    });

    $("#sameAddress").on("click", function() {
        var checked = $("#sameAddress").is(":checked");
        if (checked) {
            $("#pIAddress").val( $("#cAddress").val());
            $("#pIState").val( $("#cState").val());
            $("#pIPostcode").val( $("#cPostCode").val());
            $("#pISuburb").val( $("#cSuburb").val());
        } else {
            $("#pIAddress").val("");
            $("#pIState").val("");
            $("#pIPostcode").val("");
            $("#pISuburb").val("");
        }
        $scope.formData.pIAddress = $('#pIAddress').val();
        $scope.formData.pIState = $('#pIState').val();
        $scope.formData.pIPostcode = $('#pIPostcode').val();
        $scope.formData.pISuburb = $('#pISuburb').val();
        $scope.updateComLocations();
        $scope.updateComSuburbs();
        $scope.updateComPostCode();
    });

    $scope.load = function() {
        $scope.downloadPdf();
    }

    $scope.transformTelFields();
    /*var saveDraft =  $interval(function() {
       $scope.saveAsDraft($scope.formData);
    },300000);*/
});
Qms.controller("FormFieldCtrl", function($scope, $state, $rootScope) {
    $scope.formData = $rootScope.formData;

        $scope.load = function(formData) {
            $("#fields").validate();
            if ( $("#fields").valid() == true) {
                $rootScope.formData = formData;
             //  $state.go('rate_card');
            }
        }

});

Qms.controller("FormCtrl", function($scope, $filter, $timeout, $state, $rootScope, Form, Scopes, $locale,$q) {
    var user = Scopes.get('DefaultCtrl').profile; 
    // $scope.rates = [];
    if ($scope.isDraftMode()){
        $scope.userSign = false;
    } else {
        $scope.userSign = $rootScope.sign;
    }

    $scope.roundVal = function(val) {
        return val.toFixed(2);
    }
   
    /**
     * Retunrs true if form is signed otherwise false.
     * @param formName String name of the form as per the parent td element.
     */
    // $scope.isSigned = function(formName) {
    //     var isFound = false;
    //     if(!angular.isDefined($scope.formData.signedTimestamps)) {
    //         return false;
    //     }
    //     if( Object.prototype.toString.call( $scope.formData.signedTimestamps ) === '[object Array]' ) {
    //         $scope.formData.signedTimestamps.forEach(function(e) {
    //             if(e.form_name==formName) {
    //                 isFound = true;
    //                 return isFound;
    //             }
    //         });
    //     } else {
    //         if($scope.formData.signedTimestamps.form_name==formName) {
    //             isFound = true;
    //             return isFound;
    //         }
    //     }
    //     return isFound;
    // }

    /**
     * Returns the timestamp of the signature if exists.
     */
    $scope.getSignedTime = function(formName,userSign) {
        //console.log('getSigned');
        var time = '';
        if(!angular.isDefined($scope.formData.signedTimestamps)) {
            return 'Sign Here';
        }
        if( Object.prototype.toString.call( $scope.formData.signedTimestamps ) === '[object Array]' ) {
            $scope.formData.signedTimestamps.forEach(function(e) {
                if(e.form_name==formName) {
                    time = e.timestamp;
                }
            });
        } else {
            if($scope.formData.signedTimestamps.form_name==formName) {
                time = $scope.formData.signedTimestamps.timestamp;
            }
        }
        var currentDate = Date.parse(time);
        //console.log('curDate');
        //console.log(currentDate);
        var formatted = $filter('date')(time, 'HH:mm:ss dd-MM-yyyy');
        return formatted;
    }


    $scope.getCurrentDate = function(){
        var currentDate = new Date();   
        return (currentDate.getHours() < 10 ? "0" : "") + currentDate.getHours() + ":" + (currentDate.getMinutes() < 10 ? "0" : "") + currentDate.getMinutes() + ":" + (currentDate.getSeconds() < 10 ? "0" : "") + currentDate.getSeconds()+" "+currentDate.getDate()+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getFullYear();
    }
    $scope.getCurrentDateMin = function(){
        var currentDate = new Date();   
        return ("0" + currentDate.getDate()).slice(-2)+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getFullYear();
    }

    $scope.checkRange = function(val,lower_limit,upper_limit){
        //alert(val + '-' + lower_limit + '-' + upper_limit);
        if (upper_limit==0){
            if (val>=lower_limit){
                return true;
            } else {
                return false;
            }
        } else {
            if (val >= lower_limit && val <= upper_limit){
                return true;
            } else {
                return false;
            }
        }
    }

    $scope.setCheckboxValue = function(model){
        //console.log(model,$scope.formData.config.pstn);
        if ((model=='pstn') && (!$scope.formData.config.pstn)){
            $scope.formData.config.pstn_qty = '0';
        }
        if ((model=='isdn') && (!$scope.formData.config.ISDN)){
            $scope.formData.config.isdn_qty = '0';
        }
    }
 

    
    $scope.financedAmount = function(rental_per_month, term) {
        var rental_per_month  = Number(rental_per_month.replace(/^\D+|\,+/g, ''));
        var term  = Number(term.replace(/^\D+|\,+/g, ''));
             
        // var term_rates = $scope.getRates(term,'rental');

        var subrate = 0;
        var rates = [];
        var found = 0;
        if($rootScope.rates) {
            $rootScope.rates[0].rate;
        } else {
            $rootScope.rates = 0;
        }
        var financeAmt = 0;        
        $.each ($rootScope.rates,function(index,value){
            subrate = (rental_per_month/value.rate)*1000; 
            //rates.push(subrate);
            if($scope.checkRange(subrate,value.lowerLimit,value.upperLimit)){
              financeAmt = subrate;
            }
            console.log
        });

          // terms[24] = [55.143,49.51,49.12,48.52,47.97];
          // terms[36] = [39.336,35.15,34.69,34.2,33.7];
          // terms[48] = [31.702,27.97,27.53,27.18,26.90];
          // terms[60] = [26.477,23.75,23.25,22.95,22.72];
          // var financeAmt = 0;
          // if ($scope.checkRange(rates[0],1,9999.99)){
          //     financeAmt = rates[0];
          // }

          // if ($scope.checkRange(rates[1],10000,19999.99)){
          //     financeAmt = rates[1];
          // }

          // if ($scope.checkRange(rates[2],20000,34999.99)){
          //     financeAmt = rates[2];
          // }

          // if ($scope.checkRange(rates[3],35000,49999.99)){
          //     financeAmt = rates[3];
          // }

          // if ($scope.checkRange(rates[4],50000,0)){
          //     financeAmt = rates[4];
          // } 
          return $scope.roundVal(financeAmt).toLocaleString();
        }

    $scope.leasedAmount = function(lease_per_month, term) {
        var lease_per_month  = Number(lease_per_month.replace(/^\D+|\,+/g, ''));
        var term  = Number(term.replace(/^\D+|\,+/g, ''));

        // var terms = [];
        // terms[24] = [55.627,49.95,49.56,49.02,49.02];
        // terms[36] = [40.469,35.62,35.15,34.66,34.66];
        // terms[48] = [32.956,28.46,28.02,27.65,27.65];
        // terms[60] = [28.292,24.2,23.75,23.45,23.45];

        var subrate = 0;
        var rates = [];
        if($rootScope.rates) {
            $rootScope.rates[0].rate;
        } else {
            $rootScope.rates = 0;
        }
        var found = 0;
        var leaseAmt = 0;
        // $.each (terms[term],function(index,value){
        //     subrate = (lease_per_month/value.rate)*1000; 
        //     // rates.push(subrate); 
        //     if ($scope.checkRange(rates[0],1,9999.99)){
        //     leaseAmt = rates[0];
        // }
        // });
        $.each ($rootScope.rates,function(index,value){
            subrate = (lease_per_month/value.rate)*1000; 
            //rates.push(subrate);
            if($scope.checkRange(subrate,value.lowerLimit,value.upperLimit)){
              leaseAmt = subrate;
            }
        });

        //console.log(rates);

        // var leaseAmt = 0;
        // if ($scope.checkRange(rates[0],1,9999.99)){
        //     leaseAmt = rates[0];
        // }

        // if ($scope.checkRange(rates[1],10000,19999.99)){
        //     leaseAmt = rates[1];
        // }

        // if ($scope.checkRange(rates[2],20000,34999.99)){
        //     leaseAmt = rates[2];
        // }

        // if ($scope.checkRange(rates[3],35000,49999.99)){
        //     leaseAmt = rates[3];
        // }

        // if ($scope.checkRange(rates[4],50000,0)){
        //     leaseAmt = rates[4];
        // } 

        //console.log(leaseAmt);
        // if (rental_amt >=2000 && rental_amt <= 9999.99) {
        //     bracket = 1;
        // } else if (rental_amt >=10000 && rental_amt <= 19999.99) {
        //     bracket = 2;
        // } else if (rental_amt >=20000 && rental_amt <= 34999.99) {
        //     bracket = 3;
        // } else if (rental_amt >= 35000 && rental_amt <= 49999.99) {
        //     bracket = 4;
        // } else if (rental_amt >=50000 && rental_amt <= 199999) {
        //     bracket = 5;
        // }


        // if (bracket == 1) {
        //     if (term == 24) {
        //         constVal = 55.14;
        //     } else if (term == 36) {
        //          constVal = 39.34;
        //     } else if (term == 48) {
        //          constVal = 31.70;
        //     } else if (term == 60) {
        //          constVal = 26.48;
        //     }

        // } else if (bracket == 2) {
        //     if (term == 24) {
        //         constVal = 49.51;
        //     } else if (term == 36) {
        //          constVal = 35.15;
        //     } else if (term == 48) {
        //          constVal = 27.97;
        //     } else if (term == 60) {
        //          constVal = 23.75;
        //     }

        // } else if(bracket == 3) {
        //     if (term == 24) {
        //         constVal = 49.12;
        //     } else if (term == 36) {
        //          constVal = 34.69;
        //     } else if (term == 48) {
        //          constVal = 27.53;
        //     } else if (term == 60) {
        //          constVal = 23.25;
        //     }

        // } else if(bracket == 4)  {
        //     if (term == 24) {
        //         constVal = 48.52;
        //     } else if (term == 36) {
        //          constVal = 34.20;
        //     } else if (term == 48) {
        //          constVal = 27.97;
        //     } else if (term == 60) {
        //          constVal = 22.95;
        //     }

        // } else if (bracket == 5) {
        //     if (term == 24) {
        //         constVal = 47.97;
        //     } else if (term == 36) {
        //          constVal = 33.70;
        //     } else if (term == 48) {
        //          constVal = 26.90;
        //     } else if (term == 60) {
        //          constVal = 22.72;
        //     }
        // }

        //financeAmt = $scope.roundVal(rental_per_month * 1000/constVal).toLocaleString();
        //alert(financeAmt);
        return $scope.roundVal(leaseAmt).toLocaleString();
    }

    $scope.rentofferedAmount = function(rentoffer_per_month, term) {
        var rentoffer_per_month  = Number(rentoffer_per_month.replace(/^\D+|\,+/g, ''));
        var term  = Number(term.replace(/^\D+|\,+/g, ''));
        var rentoffer_amt = rentoffer_per_month * term;

        var constVal = 0;
        var bracket = 0;

        if (rentoffer_amt >=2000 && rentoffer_amt <= 4999.99) {
            bracket = 1;
        } else if (rentoffer_amt >=5000 && rentoffer_amt <= 9999.99) {
            bracket = 2;
        } else if (rentoffer_amt >=10000 && rentoffer_amt <= 19999.99) {
            bracket = 3;
        } else if (rentoffer_amt >=20000 && rentoffer_amt <= 99999.99) {
            bracket = 4;
        // } else if (rentoffer_amt >=50000 && rentoffer_amt <= 99999) {
        //     bracket = 5;
        }

        if (bracket == 1) {
            if (term == 48) {
                constVal = 31.68;
            } else if (term == 60) {
                 constVal = 26.20;
            }

        } else if (bracket == 2) {
            if (term == 48) {
                constVal = 31.05;
            } else if (term == 60) {
                 constVal = 25.35;
            }

        } else if(bracket == 3) {
            if (term == 48) {
                constVal = 30.45;
            } else if (term == 60) {
                 constVal = 25.09;
            }

        } else if(bracket == 4)  {
            if (term == 48) {
                constVal = 30.31;
            } else if (term == 60) {
                 constVal = 24.82;
            }
        }

       //console.log(constVal);
       rentofferAmt = $scope.roundVal(rentoffer_per_month * 1000/constVal).toLocaleString();
       return rentofferAmt;
    }


    if (!angular.isDefined($scope.formData.aTermNew) ) {
        $scope.formData.aTermNew = 60;
    }


    if(angular.isDefined($scope.formData.type.voiceUt) && $scope.formData.type.voiceUt) {
        var suToE = 9.95;
        var suMob = 7.95;
        var rateStdUt = {
          3 : 199,
          4 : 259,
          5 : 319,
          6 : 379,
          7 : 439,
          8 : 499,
          9 : 559,
          10 : 619,
          11 : 679,
          12 : 739,
          13 : 799,
          14 : 859,
          15 : 919,
          16 : 979,
          17 : 1039,
          18 : 1099,
          19 : 1159,
          20 : 1219,
          21 : 1279,
          22 : 1339,
          23 : 1399,
          24 : 1459,
          25 : 1519,
          26 : 1579,
          27 : 1639,
          28 : 1699,
          29 : 1759,
          30 : 1819,
        };

        var sUDID = {
            10 : 14.95,
            50 : 24.95,
            100: 34.95
        }

        var monthlyTotalRate = [];
        angular.forEach($scope.items['voice_solution_untimed'], function(item, index) {
            if (!$scope.formData.voiceUntimedFaxToEmail[index] || angular.isUndefined($scope.formData.voiceUntimedFaxQty[index])) {
                $scope.formData.voiceUntimedFaxQty[index] = 0;
            }
            
            if (!$scope.formData.voiceUntimedMobility[index] || angular.isUndefined($scope.formData.voiceUntimedMobQty[index])) {
                $scope.formData.voiceUntimedMobQty[index] = 0;
            }

            var total = suToE * $scope.formData.voiceUntimedFaxQty[index] + suMob * $scope.formData.voiceUntimedMobQty[index] + sUDID[$scope.formData.voiceUntimedDID[index]] + rateStdUt[$scope.formData.voiceUntimedChannel[index]];
            
            monthlyTotalRate.push(total);
        });

        $scope.voiceUt = {
            template : "templates/forms/voice_solution_untimed.html?t=" + _version,
            rate : rateStdUt,
            sfToE : suToE,
            sMob : suMob,
            sDID : sUDID,
            totalRate : monthlyTotalRate
        };
    }

    if($scope.formData.type.voiceSolution) {

        var ssToE = 9.95;
        var ssMob = 7.95;
        var rateStdSolution = {
            3 : 149,
            4 : 159,
            5 : 169,
            6 : 179,
            7 : 189,
            8 : 199,
            9 : 209,
            10 : 219,
            11 : 229,
            12 : 239,
            13 : 249,
            14 : 259,
            15 : 269,
            16 : 279,
            17 : 289,
            18 : 299,
            19 : 309,
            20 : 319,
            21 : 329,
            22 : 339,
            23 : 349,
            24 : 359,
            25 : 369,
            26 : 370,
            27 : 389,
            28 : 399,
            29 : 409,
            30 : 419,
        };

        var sSolutionDID = {
            10 : 14.95,
            50 : 24.95,
            100: 34.95
        };
        
        var monthlyTotalRate = [];
        angular.forEach($scope.items['voice_solution_standard'], function(item, index) {
            if (!$scope.formData.voiceSolutionFaxToEmail[index] || angular.isUndefined($scope.formData.voiceSolutionFaxQty[index])) {
                $scope.formData.voiceSolutionFaxQty[index] = 0;
            }
            
            if (!$scope.formData.voiceSolutionMobility[index] || angular.isUndefined($scope.formData.voiceSolutionMobQty[index])) {
                $scope.formData.voiceSolutionMobQty[index] = 0;
            }

            var total = ssToE * $scope.formData.voiceSolutionFaxQty[index] + ssMob * $scope.formData.voiceSolutionMobQty[index] + sSolutionDID[$scope.formData.voiceSolutionDID[index]] + rateStdSolution[$scope.formData.voiceSolutionChannel[index]];
            
            monthlyTotalRate.push(total);
        });

        $scope.voiceSolution = {
            template : "templates/forms/voice_solution_standard.html?t=" + _version,
            rate : rateStdSolution,
            sfToE : ssToE,
            sMob : ssMob,
            sDID : sSolutionDID,
            totalRate : monthlyTotalRate
        };
    }

    if($scope.formData.type.voiceCap) {
        var fToE = 9.95;
        var mob = 7.95;
        var rate = {
            3 : 50,
            5 : 150,
            6 : 170,
            8 : 210,
            10: 300,
            12: 350,
            14: 400,
            16: 450,
            18: 500,
            20: 600,
            25: 800
        };
        var cap = {
            3 : 99,
            5 : 99,
            6 : 99,
            8 : 99,
            10: 99,
            12: 99,
            14: 79,
            16: 79,
            18: 79,
            20: 79,
            25: 79

        };
        var dID = {
            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100: 25.99
        }
        
        var monthlyTotalRate = [];

        angular.forEach($scope.items['nbf_cap'], function(item, index) {
            if (!$scope.formData.voiceFaxToEmail[index] || angular.isUndefined($scope.formData.voiceFaxQty[index])) {
                $scope.formData.voiceFaxQty[index] = 0;
            }
            
            if (!$scope.formData.voiceMobility[index] || angular.isUndefined($scope.formData.voiceMobQty[index])) {
                $scope.formData.voiceMobQty[index] = 0;
            }

            var total = (cap[$scope.formData.voiceStd[index]] * $scope.formData.voiceStd[index]) +
                        fToE * $scope.formData.voiceFaxQty[index] + mob * $scope.formData.voiceMobQty[index]+
                        dID[$scope.formData.dID[index]];
            
            monthlyTotalRate.push(total);
        });

        $scope.voiceCap = {
            template : "templates/forms/nbf_cap.html?t=" + _version,
            rate : rate,
            fToE : fToE,
            mob : mob,
            dID : dID,
            cap : cap,
            total: monthlyTotalRate
        };
    }

    if ($scope.formData.type.copierAgreement){
        $scope.copierAgreement = {
            template : "templates/forms/copierAgreement.html?t=" + _version
        }
    }
    if ($scope.formData.type.agreement){
        $scope.agreement = {
            template : "templates/forms/agreement.html?t=" + _version
        }
    }
    if ($scope.formData.type.singleOrder) {
          $scope.formData.aTermNew = $scope.formData.aTermNew.toString();
          //console.log($scope.formData.aTermNew);
          if ($scope.formData.aTermNew!='Outright'){
            var financeAmount = $scope.financedAmount($scope.formData.aPayment, $scope.formData.aTermNew);
       
            var leaseAmount = $scope.leasedAmount($scope.formData.aPayment, $scope.formData.aTermNew);
          }
          var rentofferAmount = $scope.rentofferedAmount($scope.formData.aPayment, $scope.formData.aTermNew);

        // $scope.singleOrder = {
        //     template : "templates/forms/single_order_spec.html",
        //     financeAmount : !isNaN(financeAmount) ? financeAmount : 0,
        //     leaseAmount : !isNaN(leaseAmount) ? leaseAmount : 0,
        //     rentofferAmount : !isNaN(rentofferAmount) ? rentofferAmount : 0
        // }

        $scope.singleOrder = {
            template : "templates/forms/single_order_spec.html?t=" + _version,
            financeAmount : financeAmount,
            leaseAmount : leaseAmount,
            rentofferAmount : rentofferAmount
        }

    }


    if($scope.formData.type.focusStandard) {
        var sfToE = 9.95;
        var sMob = 7.95;
        var rateStd = {
            5 : 199,
            6 : 219,
            10: 249,
            20: 299,
            30: 349
        };
        var sDID = {

            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100: 25.99
        }

        var monthlyTotalRate = [];
        angular.forEach($scope.items['nbf_std'], function(item, index) {
            if (!$scope.formData.voiceStdFaxToEmail[index] || angular.isUndefined($scope.formData.voiceStdFaxQty[index])) {
                $scope.formData.voiceStdFaxQty[index] = 0;
            }

            if (!$scope.formData.voiceStdMobility[index] || angular.isUndefined($scope.formData.voiceStdMobQty[index])) {
                $scope.formData.voiceStdMobQty[index] = 0;
            }

            var total = sfToE * $scope.formData.voiceStdFaxQty[index] + sMob * $scope.formData.voiceStdMobQty[index] + rateStd[$scope.formData.voiceStdChannel[index]] + sDID[$scope.formData.voiceStdDID[index]];

            monthlyTotalRate.push(total);
        });

        $scope.voiceStd = {
            template : "templates/forms/nbf_std.html?t=" + _version,
            rate : rateStd,
            sfToE : sfToE,
            sMob : sMob,
            sDID : sDID,
            totalRate : monthlyTotalRate
        };
    }

    if ($scope.formData.type.voiceEssential) {
        var rateEssential = {
            4 : 196,
            6 : 294,
            8 : 392,
            10 : 490,
            12 : 588
        };

        var eDID = {
            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100 : 25.99
        };

        var monthlyTotalRate = [];
        var totalDiscount = []
        //ip Midband Plans
        var ipMidbandMonthlyRate = [];
        var ipMidbandPlans = [];
        var ipMidbandDownloads = [];
        //console.log($scope.formData.ipMidbandPlans);
        //console.log($scope.formData.ipMidbandDownload);
        angular.forEach($scope.items['voice_essentials_cap'], function(item, index) {
            var total = rateEssential[$scope.formData.voiceEssentialChannel[index]] + eDID[$scope.formData.voiceEssentialDID[index]];
            monthlyTotalRate.push(total);
            var totalIpFee = $scope.formData.ipMidbandPlansVoice[index].price+$scope.formData.ipMidbandDownloadVoice[index].price;
            var discount = parseFloat($scope.formData.ipMidbandDis[index]) + parseFloat($scope.formData.bECapDiscount[index]);
            ipMidbandPlans.push($scope.formData.ipMidbandPlansVoice[index]);
            ipMidbandDownloads.push($scope.formData.ipMidbandDownloadVoice[index]);
            ipMidbandMonthlyRate.push(totalIpFee);
            totalDiscount.push(discount);
        });

        $scope.voiceEssential = {
            template: "templates/forms/voice_essentials_cap.html?t=" + _version,
            rate : rateEssential,
            eDID : eDID,
            totalRate : monthlyTotalRate,
            plan : ipMidbandPlans,
            download : ipMidbandDownloads,
            ipTotalRate : ipMidbandMonthlyRate,
            totalDiscount : totalDiscount
        }
    }

    if($scope.formData.type.cloudConnectStandard) {

        var ssToE = 9.95;
        var ssMob = 7.95;
        var rateCloudConStandard = {
            2 : 58,
            3 : 87,
            4 : 116,
            5 : 145,
            6 : 174,
            7 : 203,
            8 : 232,
            9 : 261,
            10 : 290,
            11 : 319,
            12 : 348,
            13 : 377,
            14 : 406,
            15 : 435,
            16 : 464,
            17 : 493,
            18 : 522,
            19 : 551,
            20 : 580,
            21 : 609,
            22 : 638,
            23 : 667,
            24 : 696,
            25 : 725,
            26 : 754,
            27 : 783,
            28 : 812,
            29 : 841,
            30 : 870,
            31 : 899,
            32 : 928,
            33 : 957,
            34 : 986,
            35 : 1015,
            36 : 1044,
            37 : 1073,
            38 : 1102,
            39 : 1131,
            40 : 1160,
            41 : 1189,
            42 : 1218,
            43 : 1247,
            44 : 1276,
            45 : 1305,
            46 : 1334,
            47 : 1363,
            48 : 1392,
            49 : 1421,
            50 : 1450,
            51 : 1479,
            52 : 1508,
            53 : 1537,
            54 : 1566,
            55 : 1595,
            56 : 1624,
            57 : 1653,
            58 : 1682,
            59 : 1711,
            60 : 1740,
            61 : 1769,
            62 : 1798,
            63 : 1827,
            64 : 1856,
            65 : 1885,
            66 : 1914,
            67 : 1943,
            68 : 1972,
            69 : 2001,
            70 : 2030,
            71 : 2059,
            72 : 2088,
            73 : 2117,
            74 : 2146,
            75 : 2175,
            76 : 2204,
            77 : 2233,
            78 : 2262,
            79 : 2291,
            80 : 2320,
            81 : 2349,
            82 : 2378,
            83 : 2407,
            84 : 2436,
            85 : 2465,
            86 : 2494,
            87 : 2523,
            88 : 2552,
            89 : 2581,
            90 : 2610,
            91 : 2639,
            92 : 2668,
            93 : 2697,
            94 : 2726,
            95 : 2755,
            96 : 2784,
            97 : 2813,
            98 : 2842,
            99 : 2871,
            100 : 2900,
        };

        var cloudConnectStandardDID = {
            10 : 14.95,
            50 : 24.95,
            100: 34.95,
            150: 44.95,
            200: 54.95,
        };
        
        var monthlyTotalRate = [];
        var monthlyAnalogue = [];
        var monthlyTservicesDiscount = [];
        var monthlyTotalDis = [];
        var cCStandardCallDiscount = [];
        var totalMthOptDIDservPlanFee = [];
        var cloudConnectStandardDID = [];
        if(angular.isUndefined($scope.formData.cloudConnectStandardFaxQty) == true) {
            $scope.formData.cloudConnectStandardFaxToEmail = [];
            $scope.formData.cloudConnectStandardFaxQty = [];
            $scope.formData.cloudConnectStandardMobQty = [];
            $scope.formData.cloudConnectStandardMobility = [];
            $scope.formData.cloudConnectStandardAnalouge = [];
            $scope.formData.cloudConnectStandardDID = [];
            $scope.formData.cloudConnectStandardChannel = [];
            $scope.formData.cloudConnectStandarduConeApp = [];
            $scope.formData.cloudConnectStandardAddAutoAttndnt = [];
            $scope.formData.cloudConnectStandardAddCallQueue = [];
            $scope.formData.cloudConnectStandardAddVoicemail = [];
            $scope.formData.cloudConnectStandardRepClientApp = [];
            $scope.formData.cloudConnectStandardCallRecording = [];
            $scope.formData.cloudConnectStandardCallRecordingPremium = [];

        }
        angular.forEach($scope.items['cloud_connect_standard'], function(item, index) {
            // console.log($scope.formData.cloudConnectStandardFaxToEmail[index]);
            if (!$scope.formData.cloudConnectStandardFaxToEmail[index] || angular.isUndefined($scope.formData.cloudConnectStandardFaxQty[index])) {
                $scope.formData.cloudConnectStandardFaxQty[index] = 0;
            }
            
            if (!$scope.formData.cloudConnectStandardMobility[index] || angular.isUndefined($scope.formData.cloudConnectStandardMobQty[index])) {
                $scope.formData.cloudConnectStandardMobQty[index] = 0;
            }
         
            if($scope.formData.cloudConnectStandardAnalouge[index] > 0) {
                var monthlyAnalogue = 43.95 * $scope.formData.cloudConnectStandardAnalouge[index];
            } else {
                var monthlyAnalogue = 0;
            }


            if(cloudConnectStandardDID[$scope.formData.cloudConnectStandardDID[index]] > 0) {
                var DIDvalue = cloudConnectStandardDID[$scope.formData.cloudConnectStandardDID[index] ];
            } else {
                var DIDvalue = 0;
            }
            var total =  ssMob * $scope.formData.cloudConnectStandardMobQty[index] + DIDvalue + rateCloudConStandard[$scope.formData.cloudConnectStandardChannel[index]];
            monthlyTotalRate.push(total);

            if($scope.formData.cCStandardDiscount[index] > 0) {
                var cCStandardDiscount = $scope.formData.cCStandardDiscount[index];
            } else {
                var cCStandardDiscount = 0;
            }
            if($scope.formData.cCStandardAnalogueDis[index] > 0) {
                var cCStandardAnalogueDis = $scope.formData.cCStandardAnalogueDis[index];
            } else {
                var cCStandardAnalogueDis = 0;
            }
            if($scope.formData.cCStandardCallRecordingDiscount[index] > 0) {
                var cCStandardCallRecordingDiscount = $scope.formData.cCStandardCallRecordingDiscount[index];
            } else {
                var cCStandardCallRecordingDiscount = 0;
            }
            if($scope.formData.cCStandardCallRecordingPremiumDiscount[index] > 0) {
                var cCStandardCallRecordingPremiumDiscount = $scope.formData.cCStandardCallRecordingPremiumDiscount[index];
            } else {
                var cCStandardCallRecordingPremiumDiscount = 0;
            }
            if($scope.formData.cCStandardCallDiscount[index] > 0) {
                var CallDiscount = $scope.formData.cCStandardCallDiscount[index];
            } else {
                var CallDiscount = 0;
            }
            cCStandardCallDiscount.push(CallDiscount);
            console.log(cCStandardCallDiscount);

            var servicesDiscount = parseFloat(cCStandardAnalogueDis) + parseFloat(cCStandardCallRecordingDiscount) + parseFloat(cCStandardCallRecordingPremiumDiscount) ;
            monthlyTservicesDiscount.push(servicesDiscount);
            console.log(monthlyTservicesDiscount);
            var totalDiscount = parseFloat(cCStandardDiscount) + parseFloat(cCStandardAnalogueDis) + parseFloat(cCStandardCallRecordingDiscount) + parseFloat(cCStandardCallRecordingPremiumDiscount) ;
            monthlyTotalDis.push(totalDiscount);

            if($scope.formData.cloudConnectStandarduConeApp[index] > 0) {
                var coneApp = 14.95;
                var coneAppQty = $scope.formData.cloudConnectStandarduConeApp[index];
            } else {
                var coneApp = 0;
                var coneAppQty = 0;
            }
            if($scope.formData.cloudConnectStandardAddAutoAttndnt[index] > 0) {
                var autoAttndnt = 29.95;
            } else {
                var autoAttndnt = 0;
            }
            if($scope.formData.cloudConnectStandardAddCallQueue[index] > 0) {
                var CallQueue = 29.95;
            } else {
                var CallQueue = 0;
            }
            if($scope.formData.cloudConnectStandardAddVoicemail[index] > 0) {
                var AddVoicemail = 9.95;
            } else {
                var AddVoicemail = 0;
            }
            if($scope.formData.cloudConnectStandardRepClientApp[index] > 0) {
                var RepClientApp = 29.95;
                var RepClientAppQty = $scope.formData.cloudConnectStandardRepClientApp[index];
            } else {
                var RepClientApp = 0;
                var RepClientAppQty = 0;
            }
            if($scope.formData.cloudConnectStandardFaxQty[index] > 0) {
                var FaxQtyPrice = 9.95;
                var FaxQty = $scope.formData.cloudConnectStandardFaxQty[index];
            } else {
                var FaxQtyPrice = 0;
                var FaxQty = 0;
            }
            if($scope.formData.cloudConnectStandardDID[index]  == 10) {
                var DID = 14.95;
            } else if($scope.formData.cloudConnectStandardDID[index]  == 50) {
                var DID = 24.95;
            } else if($scope.formData.cloudConnectStandardDID[index]  == 100) {
                var DID = 34.95;
            } else if($scope.formData.cloudConnectStandardDID[index]  == 150) {
                var DID = 44.95;
            } else if($scope.formData.cloudConnectStandardDID[index]  == 200) {
                var DID = 54.95;
            } else {
                var DID = 0;
            }
            if($scope.formData.cloudConnectStandardCallRecording[index] > 0) {
                var CallRecording = 29.95;
                var CallRecordingQty = $scope.formData.cloudConnectStandardCallRecording[index];
            } else {
                var CallRecording = 0;
                var CallRecordingQty = 0;
            }
            if($scope.formData.cloudConnectStandardCallRecordingPremium[index] > 0) {
                var CallRecordingPremium = 49.95;
                var CallRecordingPremiumQty = $scope.formData.cloudConnectStandardCallRecordingPremium[index];
            } else {
                var CallRecordingPremium = 0;
                var CallRecordingPremiumQty = 0;
            }
            var totalMthOpt = parseFloat(monthlyAnalogue) + (parseFloat(RepClientApp) * parseFloat(RepClientAppQty)) + (parseFloat(coneApp) * parseFloat(coneAppQty)) + (parseFloat(CallRecording) * parseFloat(CallRecordingQty)) + (parseFloat(CallRecordingPremium) * parseFloat(CallRecordingPremiumQty)) + parseFloat(autoAttndnt) + parseFloat(CallQueue) + parseFloat(AddVoicemail)  + (parseFloat(FaxQtyPrice) * parseFloat(FaxQty)) + parseFloat(DID);
            totalMthOptDIDservPlanFee.push(totalMthOpt);
            console.log(totalMthOptDIDservPlanFee);
        });

        $scope.cloudConnectStandard = {
            template : "templates/forms/cloud_connect_standard.html?t=" + _version,
            rate : rateCloudConStandard,
            sfToE : ssToE,
            sMob : ssMob,
            analogueFee : monthlyAnalogue,
            cDID : cloudConnectStandardDID,
            totalRate : monthlyTotalRate,
            monthlyTservicesDiscount: monthlyTservicesDiscount,
            monthlyTotalDis : monthlyTotalDis,
            cCStandardCallDiscount: cCStandardCallDiscount,
            totalMthOptDIDservPlanFee: totalMthOptDIDservPlanFee
        };
    }

    if($scope.formData.type.cloudConnectCap) {

        var ssToE = 9.95;
        var ssMob = 7.95;
        var rateCloudConCap = {
            2 : 78,
            3 : 117,
            4 : 156,
            5 : 195,
            6 : 234,
            7 : 273,
            8 : 312,
            9 : 351,
            10 : 390,
            11 : 429,
            12 : 468,
            13 : 507,
            14 : 546,
            15 : 585,
            16 : 624,
            17 : 663,
            18 : 702,
            19 : 741,
            20 : 780,
            21 : 819,
            22 : 858,
            23 : 897,
            24 : 936,
            25 : 975,
            26 : 1014,
            27 : 1053,
            28 : 1092,
            29 : 1131,
            30 : 1170,
            31 : 1209,
            32 : 1248,
            33 : 1287,
            34 : 1326,
            35 : 1365,
            36 : 1404,
            37 : 1443,
            38 : 1482,
            39 : 1521,
            40 : 1560,
            41 : 1599,
            42 : 1638,
            43 : 1677,
            44 : 1716,
            45 : 1755,
            46 : 1794,
            47 : 1833,
            48 : 1872,
            49 : 1911,
            50 : 1950,
            51 : 1989,
            52 : 2028,
            53 : 2067,
            54 : 2106,
            55 : 2145,
            56 : 2184,
            57 : 2223,
            58 : 2262,
            59 : 2301,
            60 : 2340,
            61 : 2379,
            62 : 2418,
            63 : 2457,
            64 : 2496,
            65 : 2535,
            66 : 2574,
            67 : 2613,
            68 : 2652,
            69 : 2691,
            70 : 2730,
            71 : 2769,
            72 : 2808,
            73 : 2847,
            74 : 2886,
            75 : 2925,
            76 : 2964,
            77 : 3003,
            78 : 3042,
            79 : 3081,
            80 : 3120,
            81 : 3159,
            82 : 3198,
            83 : 3237,
            84 : 3276,
            85 : 3315,
            86 : 3354,
            87 : 3393,
            88 : 3432,
            89 : 3471,
            90 : 3510,
            91 : 3549,
            92 : 3588,
            93 : 3627,
            94 : 3666,
            95 : 3705,
            96 : 3744,
            97 : 3783,
            98 : 3822,
            99 : 3861,
            100 : 3900,
        };

        var cloudConnectCapDID = {
            10 : 14.95,
            50 : 24.95,
            100: 34.95,
            150: 44.95,
            200: 54.95,
        };
        
        var monthlyTotalRate = [];
        var monthlyAnalogue = [];
        var monthlyTservicesDiscount = [];
        var monthlyTotalDis = [];
        var totalMthOptDIDservPlanFee = [];
        var cloudConnectCapDID = [];
        if(angular.isUndefined($scope.formData.cloudConnectCapFaxQty) == true) {
            $scope.formData.cloudConnectCapFaxToEmail = [];
            $scope.formData.cloudConnectCapFaxQty = [];
            $scope.formData.cloudConnectCapMobQty = [];
            $scope.formData.cloudConnectCapMobility = [];
            $scope.formData.cloudConnectCapAnalouge = [];
            $scope.formData.cloudConnectCapDID = [];
            $scope.formData.cloudConnectCapChannel = [];
            $scope.formData.cloudConnectCapuConeApp = [];
            $scope.formData.cloudConnectCapAddAutoAttndnt = [];
            $scope.formData.cloudConnectCapAddCallQueue = [];
            $scope.formData.cloudConnectCapAddVoicemail = [];
            $scope.formData.cloudConnectCapRepClientApp = [];
            $scope.formData.cloudConnectCapCallRecording = [];
            $scope.formData.cloudConnectCapCallRecordingPremium = [];
        }
        angular.forEach($scope.items['cloud_connect_cap'], function(item, index) {
            if (!$scope.formData.cloudConnectCapFaxToEmail[index] || angular.isUndefined($scope.formData.cloudConnectCapFaxQty[index])) {
                $scope.formData.cloudConnectCapFaxQty[index] = 0;
            }
            
            if (!$scope.formData.cloudConnectCapMobility[index] || angular.isUndefined($scope.formData.cloudConnectCapMobQty[index])) {
                $scope.formData.cloudConnectCapMobQty[index] = 0;
            }
            if($scope.formData.cloudConnectCapAnalouge[index] > 0) {
                var monthlyAnalogue = 43.95 * $scope.formData.cloudConnectCapAnalouge[index];
            } else {
                var monthlyAnalogue = 0;
            }
            if(cloudConnectCapDID[$scope.formData.cloudConnectCapDID[index]] > 0) {
                var DIDvalue = cloudConnectCapDID[$scope.formData.cloudConnectCapDID[index]];
            } else {
                var DIDvalue = 0;
            }
            var total = ssMob * $scope.formData.cloudConnectCapMobQty[index] + DIDvalue + rateCloudConCap[$scope.formData.cloudConnectCapChannel[index]];
            monthlyTotalRate.push(total);

            if($scope.formData.cCCapDiscount[index] > 0) {
                var cCCapDiscount = $scope.formData.cCCapDiscount[index];
            } else {
                var cCCapDiscount = 0;
            }
            if($scope.formData.cCCapAnalogueDis[index] > 0) {
                var cCCapAnalogueDis = $scope.formData.cCCapAnalogueDis[index];
            } else {
                var cCCapAnalogueDis = 0;
            }
            if($scope.formData.cCCapCallRecordingDiscount[index] > 0) {
                var cCCapCallRecordingDiscount = $scope.formData.cCCapCallRecordingDiscount[index];
            } else {
                var cCCapCallRecordingDiscount = 0;
            }
            if($scope.formData.cCCapCallRecordingPremiumDiscount[index] > 0) {
                var cCCapCallRecordingPremiumDiscount = $scope.formData.cCCapCallRecordingPremiumDiscount[index];
            } else {
                var cCCapCallRecordingPremiumDiscount = 0;
            }
            // if($scope.formData.cCCapCallDiscount[index] > 0) {
            //     var cCCapCallDiscount = $scope.formData.cCCapCallDiscount[index];
            // } else {
            //     var cCCapCallDiscount = 0;
            // }

            var servicesDiscount = parseFloat(cCCapAnalogueDis) + parseFloat(cCCapCallRecordingDiscount) + parseFloat(cCCapCallRecordingPremiumDiscount) ;
            monthlyTservicesDiscount.push(servicesDiscount);
            console.log(monthlyTservicesDiscount);
            var totalDiscount = parseFloat(cCCapDiscount) + parseFloat(cCCapAnalogueDis) + parseFloat(cCCapCallRecordingDiscount) + parseFloat(cCCapCallRecordingPremiumDiscount) ;
            monthlyTotalDis.push(totalDiscount);
            if($scope.formData.cloudConnectCapuConeApp[index] > 0) {
                var coneApp = 14.95;
                var coneAppQty = $scope.formData.cloudConnectCapuConeApp[index];
            } else {
                var coneApp = 0;
                var coneAppQty = 0;
            }
            if($scope.formData.cloudConnectCapAddAutoAttndnt[index] > 0) {
                var autoAttndnt = 29.95;
            } else {
                var autoAttndnt = 0;
            }
            if($scope.formData.cloudConnectCapAddCallQueue[index] > 0) {
                var CallQueue = 29.95;
            } else {
                var CallQueue = 0;
            }
            if($scope.formData.cloudConnectCapAddVoicemail[index] > 0) {
                var AddVoicemail = 9.95;
            } else {
                var AddVoicemail = 0;
            }
            if($scope.formData.cloudConnectCapRepClientApp[index] > 0) {
                var RepClientApp = 29.95;
                var RepClientAppQty = $scope.formData.cloudConnectCapRepClientApp[index];
            } else {
                var RepClientApp = 0;
                var RepClientAppQty = 0;
            }
            if($scope.formData.cloudConnectCapFaxQty[index] > 0) {
                var FaxQtyPrice = 9.95;
                var FaxQty = $scope.formData.cloudConnectCapFaxQty[index];
            } else {
                var FaxQtyPrice = 0;
                var FaxQty = 0;
            }
            if($scope.formData.cloudConnectCapDID[index]  == 10) {
                var DID = 14.95;
            } else if($scope.formData.cloudConnectCapDID[index]  == 50) {
                var DID = 24.95;
            } else if($scope.formData.cloudConnectCapDID[index]  == 100) {
                var DID = 34.95;
            } else if($scope.formData.cloudConnectCapDID[index]  == 150) {
                var DID = 44.95;
            } else if($scope.formData.cloudConnectCapDID[index]  == 200) {
                var DID = 54.95;
            } else {
                var DID = 0;
            }
            if($scope.formData.cloudConnectCapCallRecording[index] > 0) {
                var CallRecording = 29.95;
                var CallRecordingQty = $scope.formData.cloudConnectCapCallRecording[index];
            } else {
                var CallRecording = 0;
                var CallRecordingQty = 0;
            }
            if($scope.formData.cloudConnectCapCallRecordingPremium[index] > 0) {
                var CallRecordingPremium = 49.95;
                var CallRecordingPremiumQty = $scope.formData.cloudConnectCapCallRecordingPremium[index];
            } else {
                var CallRecordingPremium = 0;
                var CallRecordingPremiumQty = 0;
            }
            var totalMthOpt = (parseFloat(monthlyAnalogue)) + (parseFloat(RepClientApp) * parseFloat(RepClientAppQty)) + (parseFloat(coneApp) * parseFloat(coneAppQty)) + (parseFloat(CallRecording) * parseFloat(CallRecordingQty)) + (parseFloat(CallRecordingPremium) * parseFloat(CallRecordingPremiumQty)) + parseFloat(autoAttndnt) + parseFloat(CallQueue) + parseFloat(AddVoicemail)  + (parseFloat(FaxQtyPrice) * parseFloat(FaxQty)) + parseFloat(DID);
            totalMthOptDIDservPlanFee.push(totalMthOpt);
        });

        $scope.cloudConnectCap = {
            template : "templates/forms/cloud_connect_cap.html?t=" + _version,
            rate : rateCloudConCap,
            sfToE : ssToE,
            sMob : ssMob,
            analogueFee : monthlyAnalogue,
            cDID : cloudConnectCapDID,
            totalRate : monthlyTotalRate,
            monthlyTservicesDiscount: monthlyTservicesDiscount,
            monthlyTotalDis : monthlyTotalDis,
            totalMthOptDIDservPlanFee: totalMthOptDIDservPlanFee
        };
    }


    if($scope.formData.type.completeOffice) {
        var cfToE = 9.95;
        var cMob = 7.95;

        var priRates = {
            0 : 0,
            10: 309.85,
            20:509.85,
            30:709.85
        };

        var cDID = {
            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100: 49.95
        };

        var monthlyTotalRate = [];
        var monthlyAnalogue = [];
        var monthlyBri = [];
        var monthlyPri = [];
        angular.forEach($scope.items['voice_comp'], function(item, index) {
            var total = 0;

            if (!$scope.formData.voiceCompFaxToEmail[index] || angular.isUndefined($scope.formData.voiceCompFaxQty[index])) {
                $scope.formData.voiceCompFaxQty[index] = 0;
            }
            
            if (!$scope.formData.voiceCompMobility[index] || angular.isUndefined($scope.formData.voiceCompMobQty[index])) {
                $scope.formData.voiceCompMobQty[index] = 0;
            }
            monthlyAnalogue.push(43.95 * $scope.formData.voiceCompAnalouge[index]);
            monthlyBri.push(109.85 * $scope.formData.voiceCompBri[index]);
            monthlyPri.push(priRates[$scope.formData.voiceCompPri[index]]);
            total = (43.95 * $scope.formData.voiceCompAnalouge[index]) + (109.85 * $scope.formData.voiceCompBri[index]) + priRates[$scope.formData.voiceCompPri[index]] + (cfToE * $scope.formData.voiceCompFaxQty[index]) + (cMob * $scope.formData.voiceCompMobQty[index]);

            if ($scope.formData.voiceCompDID[index]) {
                total += cDID[$scope.formData.voiceCompDID[index]];
            }

            // Calcuate the total plan fee based on discount amount.
            if (angular.isDefined($scope.formData.voiceCompAnalougeDis[index])) {
                total -= $scope.formData.voiceCompAnalougeDis[index];
            }

            if (angular.isDefined($scope.formData.voiceCompBriDis[index])) {
                total -= $scope.formData.voiceCompBriDis[index];
            }

            if (angular.isDefined($scope.formData.voiceCompPriDis[index])) {
                total -= $scope.formData.voiceCompPriDis[index];
            }

            monthlyTotalRate.push(total);
        });

        $scope.voiceComp = {
            template : "templates/forms/voice_comp.html?t=" + _version,
            cfToE : cfToE,
            cMob : cMob,
            cDID : cDID,
            total: monthlyTotalRate,
            analogueFee : monthlyAnalogue,
            briFee : monthlyBri,
            priFee : monthlyPri
        };
    }

    if ($scope.formData.type.adsl2) {
        var monthlyTotalRate = [];
        var analoguePrice = [];
        var discount = [];
        
        angular.forEach($scope.items['data_adsl'], function(item, index) {
            var total = $scope.formData.adsl2Plans[index].price;
            var analogueAmt = $scope.formData.voiceCompAnalougeDSL[index]*43.95;
            if(angular.isUndefined($scope.formData.voiceCompAnalougeDisDSL)){
                $scope.formData.voiceCompAnalougeDisDSL = [];
                $scope.formData.voiceCompAnalougeDisDSL[index] = 0;
            }
            var planDis = $scope.formData.voiceCompAnalougeDisDSL[index] + $scope.formData.adsl2Dis[index];
            var totalRate = (total+analogueAmt);
            monthlyTotalRate.push(totalRate);
            analoguePrice.push(analogueAmt);
            discount.push(planDis);
        });

        $scope.dataAdsl = {
            template : "templates/forms/data_adsl.html?t=" + _version,
            totalFee : monthlyTotalRate,
            analoguePrice : analoguePrice,
            discount : discount,
            rate : rate
        };
    }
    if ($scope.formData.type.telstra4gSuper) {
        var monthlyTotalRate = 0;
        //console.log($scope.formData.telstraUntimedDis);
        var discount = isNaN($scope.formData.telstra4gSuperDis) || $scope.formData.telstra4gSuperDis == null?0:parseFloat($scope.formData.telstra4gSuperDis);
        var ratePerMob = 0;
        var dataBoltOnRate = 0;
        angular.forEach($scope.formData.telstraSuperPlans,function(item,index){
            console.log($scope.telstra4gSuper_plans[index].price);

            ratePerMob = $scope.telstra4gSuper_plans[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        angular.forEach($scope.formData.boltOnPlans,function(item,index){
            console.log(dataBoltOnRate = $scope.data_bolt_on_plans[index].price);
            dataBoltOnRate = $scope.data_bolt_on_plans[index].price * item;
            monthlyTotalRate += dataBoltOnRate;
        });

        $scope.telstra4gSuper = {
            template : "templates/forms/telstra4gSuper.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };
    }

    if ($scope.formData.type.telstraUntimed) {
        var monthlyTotalRate = 0;
        //console.log($scope.formData.telstraUntimedDis);
        var discount = isNaN($scope.formData.telstraUntimedDis) || $scope.formData.telstraUntimedDis == null?0:parseFloat($scope.formData.telstraUntimedDis);
        var ratePerMob = 0;
        var dataBoltOnRate = 0;
        angular.forEach($scope.telstraPlans,function(item,index){
            console.log($scope.telstraPlans);

            ratePerMob = $scope.telstraUntimed_plans[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        angular.forEach($scope.boltOnPlans,function(item,index){
            dataBoltOnRate = $scope.data_bolt_on_plans[index].price * item;
            monthlyTotalRate += dataBoltOnRate;
        });

        $scope.telstraUntimed = {
            template : "templates/forms/telstraUntimed.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };
    }

    if ($scope.formData.type.telstraWirelessSuper){
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['telstraWirelessSuper'],function(item,index){
            var total = $scope.formData.telstraWirelessSuperPlans[index].price;
            var planDis = $scope.formData.telstraWirelessSuperDis[index];
            monthlyTotalRate.push(total);
            discount.push(planDis);
        });

        $scope.telstraWirelessSuper = {
            template : "template/forms/telstraWirelessSuper.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };
    }

    if ($scope.formData.type.telstraWireless){
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['telstraWireless'],function(item,index){
            var total = $scope.formData.telstraWirelessPlans[index].price;
            var planDis = $scope.formData.telstraWirelessDis[index];
            monthlyTotalRate.push(total);
            discount.push(planDis);
        });

        $scope.telstraWireless = {
            template : "template/forms/telstraWireless.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };
    }

    if ($scope.formData.type.ethernet){
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['ethernet'],function(item,index){
            monthlyTotalRate.push($scope.formData.ethernetPlans[index].price);
            if(angular.isDefined($scope.formData.ethernetDis[index])){
                discount.push($scope.formData.ethernetDis[index]);
            }
            else{
                $scope.formData.ethernetDis[index] = 0;
                discount.push($scope.formData.ethernetDis[index]);
            }
        });

        $scope.ethernet = {
            template : "templates/forms/ethernet.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        }
    }

    if ($scope.formData.type.ipMidband) {
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['ip_midband'], function(item, index) {
            var totalIpFee = $scope.formData.ipMidbandPlans[index].price+$scope.formData.ipMidbandDownload[index].price;
            monthlyTotalRate.push(totalIpFee);
            discount.push($scope.formData.ipMidbandDis[index]);
        });

        $scope.ipMidband = {
            template : "templates/forms/ip_midband.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        }
    }

    if ($scope.formData.type.fibre){
        var monthlyTotalRate = [];
        var discount = [];

        angular.forEach($scope.items['fibre'],function(item,index){
            monthlyTotalRate.push($scope.formData.fibreUtPlans[index].price);
            discount.push ($scope.formData.fibreDis[index]);
        });

        $scope.fibre = {
            template : "templates/form/fibre.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        }
    }

    if ($scope.formData.type.ipMidbandUnli){
        var monthlyTotalRate = [];
        var discount = [];
        // var plans = [];
        // var prof_install = [];
        angular.forEach($scope.items['ip_midbandunli'],function(item,index){
            //var totalIpFee = $scope.formData.ipMidbandUnliPlans[index].price + $scope.formData.ipMidbandUnliProf[index].price;
            // plans.push($scope.formData.ipMidbandUnliPlans[index]);
            // prof_install.push($scope.formData.ipMidbandUnliProf[index]);
            monthlyTotalRate.push($scope.formData.ipMidbandUnliPlans[index].price);
            discount.push($scope.formData.ipMidbandUnliDis[index])
        });

        $scope.ipMidbandUnli = {
            template : "templates/forms/ip_midbandunli.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
            // plan : plans,
            // prof_install : prof_install
        }
    }

    if ($scope.formData.type.nbnMonthly) {
        var nbnPlan = {
            255 : {rate : 99, desc : "NBN 25Mbps/5Mbps*"},
            2510 : {rate : 109, desc : "NBN 25Mbps/10Mbps*"},
            5020 : {rate : 119, desc : "Up to 50/20Mbps**"},
            10040 : {rate : 129, desc : "NBN 100Mbps/40Mbps*"}
        };
        var nbnDownload = {
            100 :{plan : "100GB", rate : 39},
            150 :{plan : "150GB", rate : 49},
            200 : {plan : "200GB" , rate :59},
            300 :{plan : "300GB", rate : 69},
            500 : {plan : "500GB",rate : 79},
            1000 : {plan : "1000GB", rate : 99}
        };

        var plans = [];
        var downloads = [];
        var monthlyTotalRate = [];
        var totalDiscount = [];
        var analoguePrice = [];
        angular.forEach($scope.items['nbn'], function(item, index) {
            var totalNbnFee = nbnPlan[$scope.formData.nbnPlans[index]].rate + nbnDownload[$scope.formData.nbnDownload[index]].rate;
            var price = $scope.formData.voiceCompAnalougeNBNMonthly[index] * 39.95;
            monthlyTotalRate.push(totalNbnFee + price);
            plans.push(nbnPlan[$scope.formData.nbnPlans[index]]);
            downloads.push(nbnDownload[$scope.formData.nbnDownload[index]]);
            totalDiscount.push($scope.formData.voiceCompAnalougeDisNBNMonthly[index] + $scope.formData.nbnDis[index]);
            analoguePrice.push(price);
        });

        $scope.nbn = {
            template : "templates/forms/nbn.html?t=" + _version,
            totalFee : monthlyTotalRate,
            plan : plans,
            download : downloads,
            analoguePrice : analoguePrice,
            discount : totalDiscount
        }
    }

    if ($scope.formData.type.nbnUnlimitedPlans) {
        var monthlyTotalRate = [];
        var plans = [];
        var prices = [];
        var analoguePrice = [];
        var discount = [];
        angular.forEach($scope.items['nbnUnlimited'], function(item, index) {
            var totalNbnUnlimitedFee = $scope.formData.UnlimitedPlans[index].price;
            var price = $scope.formData.voiceCompAnalougeNBNUnli[index] * 39.95;
            monthlyTotalRate.push(totalNbnUnlimitedFee + price);
            discount.push($scope.formData.voiceCompAnalougeDisNBNUnli[index] + $scope.formData.nbnUnlimitedDis[index]);
            analoguePrice.push(price);
        });

        $scope.nbnUnlimitedPlans = {
            template : "templates/forms/nbnUnlimited.html?t=" + _version,
            totalFee : monthlyTotalRate,
            analoguePrice : analoguePrice,
            discount : discount
        }
    }

    $scope.addBlankSpace = function(count) {
        var spacer = ""
        for (var i = 0; i<count; i++) {
            spacer += '\xa0\xa0';
        }
        return spacer;
    }

    $scope.processFields = function(content, maxLimit) {
        var length = content.length;
        var space = $scope.addBlankSpace(maxLimit-(length*2));
        return content+space;
    }

    if (($scope.formData.type.monitoring_solution) || ($scope.formData.type.alarm_monitor)){
        
    }

    if ($scope.formData.type.configApp) {
        var rowWidth = 3;
        $scope.telRow = new Array();
        var tempRow = new Array();
        var tdCount = 0;

        //Put tel no 3 per row
        angular.forEach($scope.formData.configAppTelNo, function(data) {
            //alert('Billing Telephone');
            //console.log(data);
            if (tdCount < rowWidth) {
                tempRow.push(data.tel);
                tdCount++;
            }
            if (tdCount == rowWidth) {
                $scope.telRow.push(tempRow);
                tdCount = 0;
                tempRow = new Array();
            }
        });
        if (tdCount > 0 ) {
            $scope.telRow.push(tempRow);
        }

        if ($scope.formData.additionalServices){
            if (!$scope.formData.config.pstn){
                $scope.formData.config.pstn_qty = 0;
            }

            if (!$scope.formData.config.ISDN){
                $scope.formData.config.Isdn_qty = 0;
            }
        }
        
        $scope.configApp = {
            template : "templates/forms/billing_app.html?t=" + _version,
        }
    }

    if ($scope.formData.type.rental) {
        $scope.rental = {
           template : "templates/forms/rental.html?t=" + _version,
        };
    }
    if ($scope.formData.type.leasing) {
        $scope.leasing = {
           template : "templates/forms/leasing.html?t=" + _version,
        };
    }

    if ($scope.formData.type.chattle) {
        $scope.chattle = {
            template : "templates/forms/chattle.html?t=" + _version,
        }
    }
    
    if ($scope.formData.type.mobileCap) {
        var monthlyTotalRate = 0;
        var discount = isNaN($scope.formData.mobileCapDis) || $scope.formData.mobileCapDis == null?0:parseFloat($scope.formData.mobileCapDis);
        //console.log(discount);
        var ratePerMob = 0;

        angular.forEach($scope.mobileMegaPlans,function(item,index){
            ratePerMob = $scope.mobile_mega_plans[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        $scope.mobileCap = {
            template : "templates/forms/mobile_mega.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };

        // var mobCPlan = {
        //     39 : {desc:" $250 inc calls 500MB"},
        //     49 : {desc:" $2500 inc calls 2000MB"},
        //     59 : {desc:" $2500 inc calls 3000MB"}
        // }
        // $scope.mobileCap = {
        //    template : "templates/forms/mobile_mega.html",
        //    plan : mobCPlan
        // };
    }

    if ($scope.formData.type.mobileUt) {
        var monthlyTotalRate = 0;
        var discount = isNaN($scope.formData.mobileUtDis) || $scope.formData.mobileUtDis == null?0:parseFloat($scope.formData.mobileUtDis);
        //console.log(discount);
        var ratePerMob = 0;

        angular.forEach($scope.formData.mobile4GUntimedPlans,function(item,index){
            ratePerMob = $scope.mobile_4G_Untimed_Calls[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        $scope.mobileUt = {
            template : "templates/forms/mobile_mega.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };

        // $scope.mobileUt = {
        //    template : "templates/forms/mobile_ut.html",
        // };
    }

    if ($scope.formData.type.mobileWireless) {
        $scope.mobileWireless = {
           template : "templates/forms/mobile_wireless.html?t=" + _version,
        };
    }


    if ($scope.formData.type.rate131300) {
        var monthlyTotalRate = [];
        angular.forEach($scope.items['1300'], function(item, index) {
            var totalqt1800Fee = $scope.formData.rate131300.qt1800[index] * 19.95;
            var totalqt1300Fee = $scope.formData.rate131300.qt1300[index] * 19.95;
            var totalqt13Fee = $scope.formData.rate131300.qt13[index] * 19.95;
            monthlyTotalRate.push(totalqt1800Fee + totalqt1300Fee + totalqt13Fee);

        });

        $scope.rate131300 = {
            template : "templates/forms/1300.html?t=" + _version,
            totalFee : monthlyTotalRate,
        }
    }

    if ($scope.formData.type.rateDis131300) {
        var monthlyTotalRateDis = [];
        angular.forEach($scope.items['1300_discounted'], function(item, index) {
            var totalqt1800DisFee = $scope.formData.rate131300Dis.qt1800[index] * 19.95;
            var totalqt1300DisFee = $scope.formData.rate131300Dis.qt1300[index] * 19.95;
            monthlyTotalRateDis.push(totalqt1800DisFee + totalqt1300DisFee);
        });
        $scope.rateDis131300 = {
            template : "templates/forms/1300_discounted.html?t=" + _version,
            totalFeeDis : monthlyTotalRateDis,
        }
    }


    // if ($scope.formData.type.rateDis131300) {
    //     $scope.rateDis131300 = {
    //         template : "templates/forms/1300_discounted.html?t=" + _version,
    //     }
    // }

    $scope.oneAtATime = true;

    $scope.sign = function(id) {
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({color:"#000000",lineWidth:1,
                                    width :490, height :98,
                                    cssclass : "signature-canvas",
                                   "decor-color": 'transparent'
                                  });
            clicked = true;
        }
    }

    $scope.currentSign = 0;
    $scope.signNavigate = function(direction) {
        var signItems = angular.element(".signItem");
        var elementId = "";
        if (direction =="next") {
            if ($scope.currentSign == 0) {
               currentItem = signItems[$scope.currentSign];
               $scope.currentSign +=1;
            } else if ($scope.currentSign < signItems.length -1) {
                $scope.currentSign +=1;
                currentItem = signItems[$scope.currentSign];
            } else {
                currentItem = signItems[$scope.currentSign];
            }

        } else if (direction == "previous") {
            if ($scope.currentSign == 0) {
               currentItem = signItems[$scope.currentSign];
            } else if ($scope.currentSign > 0) {
                $scope.currentSign -=1;
                currentItem = signItems[$scope.currentSign];
            }

        }

        elementId = currentItem.getAttribute("id");

        var top = $("#"+elementId).position().top;
        $(window).scrollTop(top-300);
        $(".signItem").removeClass("selected-to-sign");
        $("#"+elementId).addClass("selected-to-sign");
       // $(document).scrollTo("#"+elementId, 500 , {offset: -$(window).height()/2});
    }

    $scope.downloadPdf = function() {
        angular.forEach($rootScope.formData.type, function(value,key) {
             var nametail = new Date().getTime();
            //  var rand = Math.floor((Math.random()*1000)+10);
             var rand = randomNumber(3, 1000) +10;
             var fileName = key+rand+nametail+".pdf";
             $("#loading").show();
             $("#downloadPdfBn").prop('disabled',true);
             if (value == true) {
                var html =  $("#"+key).html();
                var pdf = Form.update({html:html, fileName : fileName}, function(data) {
                   // window.open("assets/files/"+fileName,'_blank');
                        $("#downloadPdfBn").prop('disabled',false);
                        $("#loadingStatus").html("Loading "+key);
                    });
             }

        });
        if($rootScope.editId) {
            var editId = $rootScope.editId;
            $rootScope.editId = null;
        }
        if(angular.isUndefined($scope.ord_num)){
          $scope.ord_num = $scope.orderNumberDate() + $scope.orderNumberComp();
        }
        Form.save({data:$scope.formData, user : user.id, editId : editId, ord_num : $scope.ord_num});
        $state.go('forms');
        $("#loading").hide();
    }

});


Qms.controller('ViewSalesRepFormsCtrl', function($scope, $state,$filter, Scopes, Form, $rootScope, FlashMessage, $http, ngDialog,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder){
    $scope.vmf = {};
            $scope.vmf.dtInstance = {};   
            $scope.vmf.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [3, 'desc'])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.vmr = {};
            $scope.vmr.dtInstance = {};   
            //$scope.vmr.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vmr.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [[6, 'desc']])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.pages = 0;
    $scope.itemsPerPage = 10;
    $scope.count = 0;
    $scope.status = "";

    $scope.sortType = 'creator_name';
    $scope.reverse = false;

    $http.get("profile").success(function(response) {
        $scope.user = response;
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;   
        Form.showSalesRepForms({user:$scope.user.id}, function(data) {
            $scope.loading = false;
            $scope.forms = data.formData;
            $scope.userStatus = 'all';
            if (data.userStatus == 'user') {
                $scope.userStatus = 5;
            }
        });
    });

    $scope.filterForms = function(status) {
       $scope.status = status;
    }
    $scope.orderNumberDate = function(){
        var currentDate = new Date();   
        return ("0" + (currentDate.getMonth() + 1)).slice(-2)+""+("0" + currentDate.getDate()).slice(-2);
    }

    $scope.orderNumberComp = function(){
      var name = $scope.formData.cName
      var name2 = $scope.formData.cName.substring(0,3).toUpperCase();
      return name2;
    }

    // $scope.edit = function(id) {
    //     var x=window.confirm("Editing Sales Rep application form is disabled")
    //   }

    $scope.edit = function(id) {
        var x=window.confirm("Do you want to Modify this?")
          if (x)
          var data = Form.show({id : id}, function(data) {
              angular.forEach(data.formData,function(name,index){
                  //console.log(name.comLocation['schedule_of_goods'].length);
                  if (index==data){
                      //alert(index);
                      angular.forEach(name,function(val,index){
                          if (index=='voiceEssentialChannel'){
                              //alert(angular.isArray(name));
                              if (!angular.isArray(name)){
                                  var key = val;
                                  delete val;
                                  name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
                              }
                          }
                          if (index=='voiceEssentialDID'){
                              //alert(angular.isArray(name));
                              if (!angular.isArray(val)){
                                  var key = val;
                                  delete val;
                                  name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
                              }
                          }
                      });
                  } 
              })
              $rootScope.viewFormData = data.formData;
              console.log($rootScope.viewFormData);
              $state.go('forms');
          });
      }

      $scope.downloadForms = function(id) {
        var forms = Form.downloadForms({id:id}, function(data) {
           if (data.success) {
               angular.forEach(data.files, function(name, index){
                    window.open("assets/files/"+name,'_blank');
               });

           }
        });
    }
});


Qms.controller('CustomerSignCtrl', function($scope, $state,$filter, Scopes, Form, $rootScope, FlashMessage, $http, ngDialog,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder,CustomerSign){
        console.log($state.params);
    if($state.params.id > 0) {
        var data = CustomerSign.show({id : $state.params},function(data) {
            angular.forEach(data.formData,function(name,index){
                //console.log(name.comLocation['schedule_of_goods'].length);
                if (index==data){
                    //alert(index);
                    angular.forEach(name,function(val,index){
                        if (index=='voiceEssentialChannel'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(name)){
                                var key = val;
                                delete val;
                                name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
                            }
                        }
                        if (index=='voiceEssentialDID'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(val)){
                                var key = val;
                                delete val;
                                name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
                            }
                        }
                    });
                } 
            })
            $rootScope.viewFormData = data.formData;
            console.log($rootScope.viewFormData);

            $state.go('forms', {
                id: $state.params,
                
            });
        });
    } else {
        // completed signing
        var data = CustomerSign.complete({id : $state.params},function(data) {
            // $state.go('customer_sign_done', {
            // });
        });

    }
    
});


    
 Qms.controller('ViewFormsCtrl', function($scope, $state,$filter, Scopes, Form, $rootScope, FlashMessage, $http, ngDialog,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder){
    $scope.vmf = {};
            $scope.vmf.dtInstance = {};   
            $scope.vmf.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [3, 'desc'])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.vmr = {};
            $scope.vmr.dtInstance = {};   
            //$scope.vmr.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vmr.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [[6, 'desc']])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.pages = 0;
    $scope.itemsPerPage = 10;
    $scope.count = 0;
    $scope.status = "";

    $scope.sortType = 'creator_name';
    $scope.reverse = false;

    $http.get("profile").success(function(response) {
        $scope.user = response;
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;   
        Form.show({user:$scope.user.id}, function(data) {
            $scope.loading = false;
            $scope.forms = data.formData;
            $scope.userStatus = 'all';
            if (data.userStatus == 'user') {
                $scope.userStatus = 5;
            }
        });
    });

    $scope.filterForms = function(status) {
       $scope.status = status;
    }
    $scope.orderNumberDate = function(){
        var currentDate = new Date();   
        return ("0" + (currentDate.getMonth() + 1)).slice(-2)+""+("0" + currentDate.getDate()).slice(-2);
    }

    $scope.orderNumberComp = function(){
      var name = $scope.formData.cName
      var name2 = $scope.formData.cName.substring(0,3).toUpperCase();
      return name2;
    }
 
//     console.log($state.params);
//     if($state.params) {
//    var data = Form.show({id : $state.params},function(data) {
//         angular.forEach(data.formData,function(name,index){
//             //console.log(name.comLocation['schedule_of_goods'].length);
//             if (index==data){
//                 //alert(index);
//                 angular.forEach(name,function(val,index){
//                     if (index=='voiceEssentialChannel'){
//                         //alert(angular.isArray(name));
//                         if (!angular.isArray(name)){
//                             var key = val;
//                             delete val;
//                             name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
//                         }
//                     }
//                     if (index=='voiceEssentialDID'){
//                         //alert(angular.isArray(name));
//                         if (!angular.isArray(val)){
//                             var key = val;
//                             delete val;
//                             name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
//                         }
//                     }
//                 });
//             } 
//         })
//         $rootScope.viewFormData = data.formData;
//         console.log($rootScope.viewFormData);
//     });
//     }
 
    
    $scope.edit = function(id) {
      var x=window.confirm("Do you want to Modify this?")
        if (x)
        var data = Form.show({id : id}, function(data) {
            angular.forEach(data.formData,function(name,index){
                //console.log(name.comLocation['schedule_of_goods'].length);
                if (index==data){
                    //alert(index);
                    angular.forEach(name,function(val,index){
                        if (index=='voiceEssentialChannel'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(name)){
                                var key = val;
                                delete val;
                                name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
                            }
                        }
                        if (index=='voiceEssentialDID'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(val)){
                                var key = val;
                                delete val;
                                name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
                            }
                        }
                    });
                } 
            })
            $rootScope.viewFormData = data.formData;
            console.log($rootScope.viewFormData);
            $state.go('forms');
        });
    }

    $scope.downloadForms = function(id) {
        var forms = Form.downloadForms({id:id}, function(data) {
           if (data.success) {
               angular.forEach(data.files, function(name, index){
                    window.open("assets/files/"+name,'_blank');
               });

           }
        });
    }

    $scope.closedialog = function(){
        $scope.closeThisDialog();
    }

    $scope.view_referals = function(id) {
        var forms = Form.view_referals({id:id}, function(data) {
           if (data.success) {
               // console.log(data.data);
               $scope.data = data.data;
               $scope.template = "templates/forms/referals.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ViewFormsCtrl',
                     width: 900,
                     height: 500,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                     // preCloseCallback : function(value) {
                     //     var signData = $('img.imported').attr('src');
                     //     if ((!signData) && (!$rootScope.isDraftMode)) {
                     //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                     //             return true;
                     //         }
                     //         return false;
                     //         $timeout(angular.noop());
                     //     }
                     // }
                 });
           }
        });
    }

    $scope.convertToCSV = function (objArray){
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        return str;
    }

    $scope.to_csv = function() {
        $scope.csvButton = !$scope.csvButton;
        var forms = Form.to_csv({referals:$scope.referals}, function(response) {
            if(response.success){
                $scope.csvFile = response.filename;
                $('#referalCSV').click();
            }
        });

    }
    $scope.view_all = function() {
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;         
        var forms = Form.view_all(function(data) {
            $scope.loading = false;         
            if (data.success) {
                $scope.loading = false; 
                $scope.referals = data.response;
                // console.log($scope.referals);
                $scope.template = "templates/forms/view-all.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ViewFormsCtrl',
                     width: 1080,
                     height: 500,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                     // preCloseCallback : function(value) {
                     //     var signData = $('img.imported').attr('src');
                     //     if ((!signData) && (!$rootScope.isDraftMode)) {
                     //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                     //             return true;
                     //         }
                     //         return false;
                     //         $timeout(angular.noop());
                     //     }
                     // }
                 });
            }
        });
    }

    $scope.delete = function(id) {
        $('<div></div>').appendTo('body')
        .html('<div><h6>Are you really sure to delete this item?</h6></div>')
        .dialog({
            modal: true,
            title: 'You want to delete this item?',
            zIndex: 10000,
            autoOpen: true,
            width: '315px',
            resizable: false,
            buttons: {
                Yes: function () {
                    var data = Form.delete({id : id}, function(data) {
                        if (data.success) {
                            FlashMessage.setMessage(data);
                            Form.show({user:$scope.user.id}, function(data) {
                                $scope.forms = data.formData;
                                $scope.userStatus = 'all';
                                if (data.userStatus == 'user') {
                                    $scope.userStatus = 5;
                                    // $window.location.reload();
                                }
                            });
                        }
                    });
                    $(this).dialog("close");
                },
                No: function () {
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });

    }

});

Qms.directive('numberFormatter', ['$filter', function ($filter) {
        var formatter = function (num) {
          num = num || 0;
          return '$' + $filter('number')(num);
        };

        var unformatter = function (str) {
          return parseFloat(str.replace(/[^0-9\.]/g, '')).toFixed(0);
        };

        return {
          restrict: 'A',
          require: 'ngModel',
          link: function (scope, element, attr, ngModel) {
            ngModel.$formatters.unshift(formatter);
            ngModel.$parsers.unshift(unformatter);

            element.bind('blur', function () {
              element.val(formatter(ngModel.$modelValue))
            });

            element.bind('focus', function () {
              element.val(ngModel.$modelValue);
            });
          }
        };
      }]);
Qms.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('renderFinish');
                });
            }
        }
    }
});

Qms.directive('integer', function(){
    return {
        require: 'ngModel',
        link: function(scope, ele, attr, ctrl){
            ctrl.$parsers.unshift(function(viewValue){
                return parseInt(viewValue, 10);
            });
        }
    };
});


Qms.directive('rowTools', function() {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'templates/common/row_tools.html',
        link: function($scope, $element, attrs) {
            $scope.row_id = attrs['rowId'];
        }
    };
});

Qms.directive('rowToolsProp', function() {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'templates/common/row_tools_prop.html',
        link: function($scope, $element, attrs) {
            $scope.row_id = attrs['rowId'];
        }
    };
});

Qms.directive("flashMessage", function($timeout) {
    return {
        restrict: 'E',
        templateUrl: 'templates/common/msg.html?t=" + _version,',
        link: function($scope) {
           $scope.$watch(function() {
               $scope.flashMessage = $scope.$parent.flashMessage;
               if ($scope.flashMessage.header) {
                   $timeout(function() {
                      $scope.flashMessage.header= null;
                   },3000);
               }
           });
           $scope.remove = function() {
               $scope.flashMessage.header = null;
           }
        }
    }
});


Qms.directive("passwordMatch", function(FlashMessage, $timeout) {
    return {
        restrict: 'EA',
        link: function($scope, $element, $attrs) {

            $element.on("keyup", function() {
                if ($scope.user.password != $scope.user.cpassword) {
                    $element

                }
            })

        }
    }
});

// Qms.directive('currentYear',function(){
//     return function(){
//         var now = new Date();
//         console.log(now.getFullYear());
//         return now.getFullYear();
//     }
// });

Qms.filter('range',function(){
    return function(input, min, max) {
    min = parseInt(min);
    max = parseInt(max);
    for (var i=min; i<=max; i++)
      input.push(i);
    return input;
  };
});

Qms.filter('range_printer',function(){
    return function(input, min, max) {
    var min_r = parseFloat(min);
    var max_r = parseFloat(max);
    //console.log(min + '-' + max);
    var i = min_r;
    while (i<=max_r){
        var num = i.toFixed(1);
        input.push(num);
        i+=0.1;
    }
    return input;
  };
});

Qms.filter('capitalizefirstletter', function() {
    return function(input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) : '';
    }
});
Qms.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);
        
      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

Qms.filter('removecurrent', function() {
    //console.log(1);
    //console.log($scope);
    return function(input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) : '';
    }
});


/* move to directives*/

Qms.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('change',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                        return false;
                    }
                });
            }
        };
    }
]);

Qms.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});

Qms.directive('onlyDigits', function () {

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput !== inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

Qms.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  };
});

Qms.filter('unsafe', function($sce) {

    return function(value) {

        //mod
        wordwise = true;
        max = 50;
        tail = "...";
        max = parseInt(50, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        value + (tail || ' Ã¯Â¿Â½');
        //mod

        return $sce.trustAsHtml(value);

    };

});


Qms.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' Ã¯Â¿Â½');
        };
});


Qms.directive('realTimeCurrency', function ($filter, $locale) {
var decimalSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;
    var toNumberRegex = new RegExp('[^0-9\\' + decimalSep + ']', 'g');
    var trailingZerosRegex = new RegExp('\\' + decimalSep + '0+$');
    /*var filterFunc = function (value) {
        return $filter('currency')(value);
    };*/
    var currencyFilter = $filter('currency');
    var formats = $locale.NUMBER_FORMATS;
    var filterFunc = function(value) {
        var value = currencyFilter(value);
        var sep = value.indexOf(formats.DECIMAL_SEP);
        if(value >= 0) {
            return value.substring(0, sep);
        }
        return value.substring(0, sep);
    };

    function getCaretPosition(input){
        if (!input) return 0;
        if (input.selectionStart !== undefined) {
            return input.selectionStart;
        } else if (document.selection) {
            // Curse you IE
            input.focus();
            var selection = document.selection.createRange();
            selection.moveStart('character', input.value ? -input.value.length : 0);
            return selection.text.length;
        }
        return 0;
    }

    function setCaretPosition(input, pos){
        if (!input) return 0;
        if (input.offsetWidth === 0 || input.offsetHeight === 0) {
            return; // Input's hidden
        }
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(pos, pos);
        }
        else if (input.createTextRange) {
            // Curse you IE
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    }

    function toNumber(currencyStr) {
        return parseFloat(currencyStr.replace(toNumberRegex, ''), 10);
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function postLink(scope, elem, attrs, modelCtrl) {
            modelCtrl.$formatters.push(filterFunc);
            modelCtrl.$parsers.push(function (newViewValue) {
                var oldModelValue = modelCtrl.$modelValue;
                var newModelValue = toNumber(newViewValue);
                modelCtrl.$viewValue = filterFunc(newModelValue);
                var pos = getCaretPosition(elem[0]);
                elem.val(modelCtrl.$viewValue);
                var newPos = pos + modelCtrl.$viewValue.length -
                                   newViewValue.length;
                if ((oldModelValue === undefined) || isNaN(oldModelValue)) {
                    newPos -= 3;
                }
                setCaretPosition(elem[0], newPos);
                return newModelValue;
            });
        }
    };
});

Qms.factory('Scopes', function ($rootScope) {
    var mem = {};

    return {
        store: function (key, value) {
            mem[key] = value;
        },
        get: function (key) {
            return mem[key];
        }
    };
});

//
//Qms.factory('NgRepeatSkip', function() {
//  return function(values) {
//    var list = {};
//
//    list.index = 0;
//
//    list.next = function() {
//      list.index += 1;
//    };
//
//     list.next2 = function() {
//      list.index += 2;
//    };
//
//     list.next3 = function() {
//      list.index += 3;
//    };
//
//     list.next4 = function() {
//      list.index += 4;
//    };
//
//    list.previous = function() {
//      list.index -= 1;
//    };
//
//    list.value = function() {
//      return values[list.index];
//    };
//  };
//});
//

Qms.run(function ($rootScope) {
    $rootScope.$on('scope.stored', function (event, data) {
        //console.log("scope.stored", data);
    });
});

Qms.run(function($rootScope, $templateCache) {
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (typeof(current) !== 'undefined'){
            //console.log($templateCache);
            //$templateCache.remove(current.templateUrl);
        }
    });
});

Qms.directive('multipleEmails', function () {
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;

        function validateAll(ctrl, validatorName, value) {
            var validity = ctrl.$isEmpty(value) || value.split(';').every(
                function (email) {
                    return EMAIL_REGEXP.test(email.trim());
                }
            );

            ctrl.$setValidity(validatorName, validity);
            return validity ? value : undefined;
        }

        return {
            restrict: 'A',
            require: 'ngModel',
            link: function postLink(scope, elem, attrs, modelCtrl) {
                function multipleEmailsValidator(value) {
                    return validateAll(modelCtrl, 'multipleEmails', value);
                };

                modelCtrl.$formatters.push(multipleEmailsValidator);
                modelCtrl.$parsers.push(multipleEmailsValidator);
            } 
        };
    });

Qms.directive('removeNgEmailValidation', function(){
    return {
        require : 'ngModel',
        link : function(scope, element, attrs, ngModel) {
            ngModel.$validators["email"] = function () {
                return true;
            };
        }
    }
});

Qms.directive('exportToCsv',function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var el = element[0];
            element.bind('click', function(e){
                var table = $('#referalTable')[0];
                //console.log(attrs);
                var csvString = '';
                for(var i=0; i<table.rows.length;i++){
                    var rowData = table.rows[i].cells;
                    for(var j=0; j<rowData.length;j++){
                        csvString = csvString + rowData[j].innerHTML + ",";
                    }
                    csvString = csvString.substring(0,csvString.length - 1);
                    csvString = csvString + "\n";
                }
                csvString = csvString.substring(0, csvString.length - 1);
                var a = $('<a/>', {
                    style:'display:none',
                    href:'data:application/octet-stream;base64,'+btoa(csvString),
                    download: attrs.company + ' Referrals.csv'
                }).appendTo('body')
                a[0].click()
                a.remove();
            });
        }
    }
});

Qms.$inject = ['$scope', '$filter'];

Qms.directive("customSort", function() {
return {
    restrict: 'A',
    transclude: true,    
    scope: {
      order: '=',
      sort: '='
    },
    template : 
      ' <a ng-click="sort_by(order)" style="color: #555555;">'+
      '    <span ng-transclude></span>'+
      '    <i ng-class="selectedCls(order)"></i>'+
      '</a>',
    link: function(scope) {
                
    // change sorting order
    scope.sort_by = function(newSortingOrder) {       
        var sort = scope.sort;
        
        if (sort.sortingOrder == newSortingOrder){
            sort.reverse = !sort.reverse;
        }                    

        sort.sortingOrder = newSortingOrder;        
    };
    
   
    scope.selectedCls = function(column) {
        if(column == scope.sort.sortingOrder){
            return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
        }
        else{            
            return'icon-sort' 
        } 
    };      
  }// end link
}
});
Qms.directive("checkboxGroup", function() {
        return {
            restrict: "A",
            link: function(scope, elem, attrs) {
                // Determine initial checked boxes
                if (scope.array.indexOf(scope.item.id) !== -1) {
                    elem[0].checked = true;
                }

                // Update array on click
                elem.bind('click', function() {
                    var index = scope.array.indexOf(scope.item);
                    // Add if checked
                    if (elem[0].checked) {
                        if (index === -1) scope.array.push(scope.item);
                        // alert(scope.array);
                    }
                    // Remove if unchecked
                    else {
                        if (index !== -1) scope.array.splice(index, 1);
                    }
                    // Sort and update DOM display
                    scope.$apply(scope.array.sort(function(a, b) {
                        return a - b
                    }));
                });
            }
        }
    });


Qms.controller('CloudCtrl',function($scope,$sce,$stateParams,Proposals, $rootScope, $interval, $window, $anchorScroll, $location, $timeout, $http, Scopes, Cloud, Rates, FlashMessage, ngDialog, NgMap,GeoCoder){
    $scope.vm = this;
    $scope.scheduleCount = 0;
    $scope.locationCount = 0;
    $scope.accountLocation = 0;
    $scope.addTelCount = 0;
    $scope.addMobPortCount = 0;
    $scope.htmlData = {};
    $scope.discountArr = {};
    $scope.companyLocations = [];
    $rootScope.draftMode = false;
    $scope.autoCompleteOptions = {
        componentRestrictions: {
            country: 'AU',
            postalCode: '2000'
          },
        types: 'geocode'
    }

    $scope.locationAvailableTemps = [
        'nbf_cap',
        'nbf_std',
        'voice_comp',
        'voice_solution_untimed',
        'voice_essentials_cap',
        'cloud_connect_standard',
        'cloud_connect_cap',
        'voice_solution_standard',
        'data_adsl',
        'ip_midband',
        'ip_midbandunli',
        'nbnUnlimited',
        'nbn',
        'mobile_mega',
        'mobile_ut',
        'mobile_wireless',
        '1300',
        '1300_discounted',
        'fibre',
        'ethernet',
        'telstraUntimed',
        'telstraWireless',
        'printer',
        'schedule_of_goods',
        'summary'
    ];

    $scope.items = {
        'nbf_cap'               : [0],
        'nbf_std'               : [0],
        'voice_comp'            : [0],
        'voice_solution_untimed'  : [0],
        'voice_essentials_cap'    : [0],
        'cloud_connect_standard'  : [0],
        'cloud_connect_cap'       : [0],
        'voice_solution_standard' : [0],
        'data_adsl'               : [0],
        'ip_midband'              : [0],
        'ip_midbandunli'          : [0],
        'nbn'                     : [0],
        'nbnUnlimited'            : [0],
        'mobile_mega'             : [0],
        'mobile_ut'               : [0],
        'mobile_wireless'         : [0],
        '1300'                    : [0],
        '1300_discounted'         : [0],
        'fibre'                   : [0],
        'ethernet'                : [0],
        'telstraUntimed'          : [0],
        'telstraWireless'         : [0],
        'printer'                 : [0],
        'schedule_of_goods'       : [0],
        'summary'                 : [0]
    };

    $scope.recurring_services = [
        {'description' : 'Monitoring Centre connect 24 hours','price' : 59.00},
        {'description' : "Monitoring Centre connect 24 hours (Including NBN/3G Wireless Unit)",'price' : 89}
    ];

    $scope.ethernet_plans = [
        {'description' : 'Up to 10Mbps/10Mbps* Unlimited','price':499},
        {'description' : 'Up to 20Mbps/20Mbps* Unlimited','price':599},
    ];

    $scope.nbnUnlimited_plans = [
        {'description' : 'NBN Standard 25','inclusion' : 'Download speeds Between 5Mbps and 25Mbps*<br/>Upload speeds Between 1Mbps and 5Mbps*','price':159},
        {'description' : 'NBN Fast 50','inclusion' : 'Download speeds Between 12Mbps and 50Mbps*<br/>Upload speeds Between 1Mbps and 20Mbps*','price':179},
        {'description' : 'NBN WIRELESS 75','inclusion' : 'Download speeds Between 12Mbps and 75Mbps*<br/>Upload speeds Between 1Mbps and 10Mbps*','price':179},
        {'description' : 'NBN Ultra fast 100','inclusion' : 'Download speeds Between 12Mbps and 100Mbps*<br/>Upload speeds Between 1Mbps and 40Mbps*','price':189}
    ];

    $scope.mobile_mega_plans = [
        {'description' : 'Cap 39','inclusions' : '$250 inc. calls 500MB','price':39},
        {'description' : 'Cap 49','inclusions' : '$2500 inc. calls 2GB','price':49},
        {'description' : 'Cap 59','inclusions' : '$2500 inc. calls 3GB','price':59},
    ];
    $scope.data_bolt_on_plans = [
        {'description' : 'Data Bolt-on 1GB','memory': '1GB','price':9.89,'itemIndex':0},
        {'description' : 'Data Bolt-on 5GB','memory': '5GB','price':31.99,'itemIndex':1}
    ];
    
    $scope.mobile_rate_plans = [
        {'plan' : 'Select a Mobile Plan','inclusions':''},
        // {'group':'Telstra 4G','plan':'Telstra 4G Lite','inclusions':'$1000 National Calls 1GB Data','price':49,'itemIndex':0},
        // {'group':'Telstra 4G','plan':'Telstra 4G Business Executive','inclusions':'Unlimited Calls 3GB Data','price':69,'itemIndex':1},
        // {'group':'Telstra 4G','plan':'Telstra 4G Business Professional','inclusions':'Unlimited Calls 10GB Data','price':79,'itemIndex':2},
        // {'group':'Telstra 4G','plan':'Telstra 4G Business Maximum','inclusions':'Unlimited Calls 20GB Data','price':89,'itemIndex':3},
        // {'group':'Mobile Mega','plan':'Mobile Mega Cap 39','inclusions':'$250 inc. calls 500Mb','price':39,'itemIndex':0},
        // {'group':'Mobile Mega','plan':'Mobile Mega Cap 49','inclusions':'$2500 inc. calls 2GB','price':49,'itemIndex':1},
        // {'group':'Mobile Mega','plan':'Mobile Mega Cap 59','inclusions':'$2500 inc. calls 3GB','price':59,'itemIndex':2},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 49','inclusions':'Includes 700MB','price':49,'itemIndex':0},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 59','inclusions':'Includes 2GB','price':59,'itemIndex':1},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 79','inclusions':'Includes 4GB and 300 INTERNATIONAL MINS','price':79,'itemIndex':2},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 89','inclusions':'Includes 6GB and 300 INTERNATIONAL MINS','price':89,'itemIndex':3}
    ];

    $scope.configMobPort = [
        {'confAccHolder':'','confMobNo':'','confProvider':'','confAccNo':'','confPatPlan':$scope.mobile_rate_plans[0],'boltOn':{}}
    ];

    $scope.adsl_plans = [
        {'description' : 'ADSL 2 + LITE','details' : '(Unlimited Lite (no static IP))','price':79},
        {'description' : 'ADSL 2 + Unlimited','details' : 'Business Grade (ON NET)','price':109},
        {'description' : 'ADSL 2 + 100GB','details' : 'Business Grade (OFF NET)','price':109},
        {'description' : 'ADSL 2 + 200GB','details' : 'Business Grade (OFF NET)','price':129},
        {'description' : 'ADSL 2 + 300GB','details' : 'Business Grade (OFF NET)','price':139},
        {'description' : 'ADSL 2 + 500GB','details' : 'Business Grade (OFF NET)','price':149},
        {'description' : 'ADSL 2 + 1TB','details' : 'Business Grade (OFF NET)','price':169},
    ];

    $scope.telstraUntimed_plans = [
        {'description' : 'Lite','inclusions' : 'INCLUDES $1000 National Calls,INCLUDES 1GB,UNLIMITED NATIONAL SMS,$0.69 PER MMS','price':49},
        {'description' : 'Business Executive','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 3GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,100 INTERNATIONAL MINS*','price':69},
        {'description' : 'Business Professional','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 10GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':79},
        {'description' : 'Business Maximum','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 20GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':89},
    ];


    $scope.telstraWireless_plans = [
        {'description' : 'Broadband Wireless 3GB','price':43},
        {'description' : 'Broadband Wireless 6GB','price':64},
        {'description' : 'Broadband Wireless 10GB','price':93}
    ];

    $scope.ip_midbandunli_plans = [
        {'description' : '10Mbps/10Mbps*','price':549},
        {'description' : '20Mbps/20Mbps*','price':699},
        {'description' : '40Mbps/40Mbps*','price':999}
    ];

    $scope.ip_midband_plans = [
        {'description' : 'None','price':0},
        {'description' : '6Mbps/6Mbps*','price':319},
        {'description' : '8Mbps/8Mbps*','price':349},
        {'description' : '18Mbps/18Mbps*','price':399},
        {'description' : '20Mbps/20Mbps*','price':449},
        {'description' : '30Mbps/30Mbps*','price':799},
        {'description' : '40Mbps/40Mbps*','price':849}
    ];

    $scope.professional_install = [
        {'description' : 'ASDL 2+ 20/1Mbps','price':399},
        {'description' : 'ASDL 2+ 20/3 to 40Mbps Midband','price':599},
    ];

    $scope.download_plans = [
        {'description' : 'None','price':0},
        {'description' : '50 GB','price':19},
        {'description' : '100 GB','price':35},
        {'description' : '200 GB','price':69},
        {'description' : '300 GB','price':99},
        {'description' : '500 GB','price':149},
        {'description' : '1000 GB','price':289}
    ];

    $scope.mobile_4G_Untimed_Calls = [
        {'description' : '4G Untimed 49','inclusions' : 'Includes 700MB','price':49},
        {'description' : '4G Untimed 59','inclusions' : 'Includes 2GB','price':59},
        {'description' : '4G Untimed 79','inclusions' : 'Includes 4GB,300 INTERNATIONAL MINS*','price':79},
        {'description' : '4G Untimed 89','inclusions' : 'Includes 6GB,300 INTERNATIONAL MINS*','price':89}
    ];

    $scope.wireless_cap_plans = [
        {'description' : 'Wireless 15 - 500MB','price':15},
        {'description' : 'Wireless 19 - 1GB','price':19},
        {'description' : 'Wireless 29 - 2GB','price':29},
        {'description' : 'Wireless 39 - 3GB','price':39},  
        {'description' : 'Wireless 44 - 4GB','price':44},
        {'description' : 'Wireless 49 - 6GB','price':49},
        {'description' : 'Wireless 59 - 8GB','price':59},
        {'description' : 'Wireless 119 - 12GB','price':119}
    ];
    
    $scope.fibre_unli_plans = [
        {'description' : '10Mbps / 10Mbps Unlimited - 36 month term','price':599},
        {'description' : '20Mbps / 20Mbps Unlimited - 36 month term','price':699},
        {'description' : '100Mbps / 100Mbps Unlimited - 36 month term','price':1199},
        {'description' : '400Mbps / 400Mbps Unlimited - 36 month term','price':758},
        {'description' : '500Mbps / 500Mbps Unlimited - 36 month term','price':1599},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 24 month term','price':1899},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 36 month term','price':1699},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 48 month term','price':1299},
    ];

    $http.get("profile").success(function(response) {
        $scope.profile = response;
        $rootScope.profile = response;
    });
    
    $scope.dollarSign = function(val){
        var strCurrency = '';
        if (angular.isDefined(val))
            strCurrency = '$' + val;
        //alert(strCurrency);
        return strCurrency;
    }

    $scope.isEmptyArray = function(arr) {
        return angular.equals([], arr);
    }

    $scope.reset = function() {
        $scope.cloudData = {};
        $scope.cloudData.fibreTerm = [];
        $scope.cloudData.fibreTerm[0] = 36;
        $scope.cloudData.schItems = [];
        $scope.cloudData.schItems[0] = [{count:1}];
        $scope.schItemCount = [];
        $scope.schItemCount[0] = 1;
        $scope.cloudData.aBillingTerm = 60;
        $scope.cloudData.aTerm = 60;
        $scope.cloudData.schAddItems = [];
        $scope.cloudData.schAddItems[0] = [{count:1}];
        $scope.schAddItemCount = [];
        $scope.schAddItemCount[0] = 1;
        $scope.cloudData.cPosition = "";
        $scope.cloudData.cSPosition = "";
        $scope.cloudData.aIdenQuestion = "";
        $scope.cloudData.referals = [0,1,2,3,4];
        $scope.cloudData.rBusinessName = [null,null,null,null,null];
        $scope.cloudData.rContactPerson = [null,null,null,null,null];
        $scope.cloudData.rPhoneNumber = [null,null,null,null,null];
        $scope.referalCount=5;
        $scope.cloudData.dID = [];
        $scope.cloudData.dID[0] = 10;
        $scope.cloudData.voiceStd = [];
        $scope.cloudData.voiceStd[0] = 5;
        $scope.cloudData.voiceStdDID = [];
        $scope.cloudData.voiceStdDID[0] = 10;
        $scope.cloudData.voiceStdChannel = [];
        $scope.cloudData.voiceStdChannel[0] = 5;
        $scope.cloudData.bFCapDiscount = [];
        $scope.cloudData.bFCapDiscount[0] = 0;
        $scope.cloudData.bFStdDiscount = [];
        $scope.cloudData.bFStdDiscount[0] = 0;
        $scope.cloudData.bFCallDiscount = [];
        $scope.cloudData.bFCallDiscount[0] = 0;
        $scope.cloudData.voiceUntimedChannel = [];
        $scope.cloudData.voiceUntimedChannel[0] = 3;
        $scope.cloudData.voiceUntimedDID = [];
        $scope.cloudData.voiceUntimedDID[0] = 10;
        $scope.cloudData.bFUntimedDiscount = [];
        $scope.cloudData.bFUntimedDiscount[0] = 0;
        $scope.cloudData.voiceCompDID = [];
        $scope.cloudData.voiceCompDID[0] = 0;    
        $scope.cloudData.voiceSolutionChannel = [];
        $scope.cloudData.voiceSolutionChannel[0] = 3;  
        $scope.cloudData.voiceSolutionDID = [];
        $scope.cloudData.voiceSolutionDID[0] = 10;  
        $scope.cloudData.bFSolutionDiscount = [];
        $scope.cloudData.bFSolutionDiscount[0] = 0;  
        $scope.cloudData.bFSolutionCallDiscount = [];
        $scope.cloudData.bFSolutionCallDiscount[0] = 0;  
        $scope.cloudData.voiceCompAnalougeDisNBNUnli = [];
        $scope.cloudData.voiceCompAnalougeDisNBNUnli[0] = 0;
        $scope.cloudData.voiceEssentialChannel = [];
        $scope.cloudData.voiceEssentialChannel[0] = 4; 
        $scope.cloudData.voiceEssentialDID = [];
        $scope.cloudData.voiceEssentialDID[0] = 10;  
        $scope.cloudData.bECapDiscount = [];
        $scope.cloudData.bECapDiscount[0] = 0; 
        $scope.cloudData.ipMidbandDis = [];
        $scope.cloudData.ipMidbandDis[0] = 0; 
        $scope.cloudData.voiceCompAnalougeDis = [];
        $scope.cloudData.voiceCompAnalougeDis[0] = 0;
        $scope.cloudData.voiceCompBriDis = [];
        $scope.cloudData.voiceCompBriDis[0] = 0;
        $scope.cloudData.voiceCompPri = [];
        $scope.cloudData.voiceCompPri[0] = 0;
        $scope.cloudData.voiceCompPriDis = [];
        $scope.cloudData.voiceCompPriDis[0] = 0;
        $scope.cloudData.voiceCompCallDis = [];
        $scope.cloudData.voiceCompCallDis[0] = 0;
        $scope.cloudData.voiceCompAnalougeDisDSL = [];
        $scope.cloudData.voiceCompAnalougeDisDSL[0] = 0;
        $scope.cloudData.UnlimitedPlans = [];
        $scope.cloudData.UnlimitedPlans[0] = 159;
        $scope.cloudData.mobileWirelessDis = [];
        $scope.cloudData.mobileWirelessDis[0] = 0;
        $scope.cloudData.nbnUnlimitedDis = [];
        $scope.cloudData.nbnUnlimitedDis[0] = 0;
        $scope.cloudData.voiceCompAnalougeNBNUnli = [];
        $scope.cloudData.voiceCompAnalougeNBNUnli[0] = 0;
        $scope.cloudData.ipMidbandUnliDis = [];
        $scope.cloudData.ipMidbandUnliDis[0] = 0;
        $scope.cloudData.ethernetDis = [];
        $scope.cloudData.ethernetDis[0] = 0;
        $scope.cloudData.fibreDis = [];
        $scope.cloudData.fibreDis[0] = 0;
        $scope.cloudData.telstraUntimedDis = 0;
        $scope.cloudData.mobileUtDis = 0;
        $scope.cloudData.mobileCapDis = 0;
        $scope.cloudData.telstraWirelessDis = [];
        $scope.cloudData.telstraWirelessDis[0] = 0;
        $scope.cloudData.rate131300 = {};
        $scope.cloudData.rate131300.qt1800 = [];
        $scope.cloudData.rate131300.qt1800[0] = 0;
        $scope.cloudData.rate131300.qt1300 = [];
        $scope.cloudData.rate131300.qt1300[0] = 0;
        $scope.cloudData.rate131300.qt13 = [];
        $scope.cloudData.rate131300.qt13[0] = 0;
        $scope.cloudData.rate131300Dis = {};
        $scope.cloudData.rate131300Dis.qt1800 = [];
        $scope.cloudData.rate131300Dis.qt1800[0] = 0;
        $scope.cloudData.rate131300Dis.qt1300 = [];
        $scope.cloudData.rate131300Dis.qt1300[0] = 0;
        $scope.cloudData.rateDis131300 = {};
        $scope.cloudData.rateDis131300.discount = [];
        $scope.cloudData.rateDis131300.discount[0] = 0;
        $scope.cloudData.siteType = {};
        $scope.cloudData.siteType = "Existing Site";
        $scope.cloudData.adsl2Dis = [];
        $scope.cloudData.adsl2Dis[0] = 0;
        $scope.cloudData.accounts = [];
        $scope.cloudData.aPaymentMethod = 'Debit Card';
        $scope.cloudData.aCardNumber = "";
        $scope.cloudData.aCardName = "";
        $scope.cloudData.aCardExpiry = "";
        $scope.cloudData.aCardType = "";
        $scope.cloudData.aCardToken = "";

        $scope.cloudData.accounts = [
            {'configCarrier':'','configAccno':''}
        ];

        $scope.cloudData.comLocation = {};
        $scope.cloudData.comLocation = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'cloud_connect_standard'  : [],
            'cloud_connect_cap'       : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWireless'         : [],
            'printer'                 : [],
            'schedule_of_goods'       : []
        };

        $scope.cloudData.comSuburb = {};
        $scope.cloudData.comSuburb = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'cloud_connect_standard'  : [],
            'cloud_connect_cap'       : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWireless'         : [],
            'printer'                 : [],
            'schedule_of_goods'       : []
        };

        $scope.cloudData.comZipCode = {};
        $scope.cloudData.comZipCode = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWireless'         : [],
            'printer'                 : [],
            'schedule_of_goods'       : []       
        };

        $scope.cloudData.voiceFaxToEmail = {};
        $scope.cloudData.voiceFaxQty = {};
        $scope.cloudData.voiceMobility = {};
        $scope.cloudData.voiceMobQty = {};

        $scope.cloudData.voiceStdFaxToEmail = {};
        $scope.cloudData.voiceStdFaxQty = {};
        $scope.cloudData.voiceStdMobility = {};
        $scope.cloudData.voiceStdMobQty = {};

        $scope.cloudData.voiceCompAnalouge = {};
        $scope.cloudData.voiceCompBri = {};
        $scope.cloudData.voiceCompFaxToEmail = {};
        $scope.cloudData.voiceCompFaxQty = {};
        $scope.cloudData.voiceCompMobility = {};
        $scope.cloudData.voiceCompMobQty = {};
        // $scope.cloudData.voiceCompDID = {};
        // $scope.cloudData.voiceCompAnalougeDis = {};
        // $scope.cloudData.voiceCompBriDis = {};
        // $scope.cloudData.voiceCompPriDis = {};
        //$scope.cloudData.voiceCompPri = {};

        $scope.cloudData.voiceUntimedFaxToEmail = {};
        $scope.cloudData.voiceUntimedFaxQty = {};
        $scope.cloudData.voiceUntimedMobility = {};
        $scope.cloudData.voiceUntimedMobQty = {};

        $scope.cloudData.voiceSolutionFaxToEmail = {};
        $scope.cloudData.voiceSolutionFaxQty = {};
        $scope.cloudData.voiceSolutionMobility = {};
        $scope.cloudData.voiceSolutionMobQty = {};

        $scope.cloudData.ipMidbandDownload = {
            0:$scope.download_plans[1]
        };

        $scope.cloudData.alarmPlans = $scope.recurring_services[0];

        //console.log($scope.cloudData.alarmPlans);

        $scope.cloudData.transactionDetails = "Commercial Premises";
        $scope.cloudData.hardwareInstalled = "MPM (multi path monitoring device)";
        $scope.cloudData.monitoredServices = "Burglary";
        $scope.cloudData.panelType = "NESS";

        $scope.cloudData.adsl2Plans = {
            0:$scope.adsl_plans[0]
        }

        $scope.cloudData.telstraUntimedPlans = {
            0:$scope.telstraUntimed_plans[0]
        }

        $scope.cloudData.telstraWirelessPlans = {
            0:$scope.telstraWireless_plans[0]
        }

        $scope.cloudData.ethernetPlans = {
            0:$scope.ethernet_plans[0]
        }

        $scope.cloudData.UnlimitedPlans = {
            0:$scope.nbnUnlimited_plans[0]
        }

        $scope.cloudData.ipMidbandPlans = {
            0:$scope.ip_midband_plans[1]
        };

        $scope.cloudData.ipMidbandUnliPlans = {
            0:$scope.ip_midbandunli_plans[0]
        };

        $scope.cloudData.ipMidbandUnliProf = {
            0:$scope.professional_install[0]
        };

        $scope.cloudData.ipMidbandDownloadVoice = {
            0:$scope.download_plans[0]
        };

        $scope.cloudData.ipMidbandPlansVoice = {
            0:$scope.ip_midband_plans[0]
        };

        $scope.cloudData.mobileUtPlans = {
            0:$scope.mobile_4G_Untimed_Calls[0]
        };

        $scope.cloudData.fibreUtPlans = {
            0:$scope.fibre_unli_plans[0]
        };

        $scope.cloudData.mobileWirelessPlans = {
            0:$scope.wireless_cap_plans[0]
        };

        $scope.cloudData.voiceCompAnalougeDSL = {
            0:0
        };

        $scope.cloudData.voiceCompAnalougeNBNMonthly = {
            0:0
        };

        $scope.cloudData.voiceCompAnalougeNBNUnli = {
            0:0
        };

        $scope.cloudData.voiceCompAnalouge = {
            0:0
        };

        $scope.cloudData.voiceCompBri = {
            0:0
        };

        $scope.cloudData.qt1300new = {};
        $scope.cloudData.qt800new = {};
        $scope.cloudData.qt13new = {};
        $scope.cloudData.aCardExpiry = '';
        $scope.cloudData.rates = [];
        $scope.rates = [];
        $scope.cloudData.orderNumber = '';
    };

    $scope.reset();

    $scope.tinymceOptions = {
      selector: "textarea",
      theme: "modern",
      paste_data_images: true,
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      toolbar2: "print preview media | forecolor backcolor emoticons "
    };
    
  
    $scope.getRates = function(){
      $scope.cloudData.term = $('#term').val();
      var rates = Cloud.getRates({term:$scope.cloudData.term}, function(data) {
            if (data.success) {
            // $scope.cloudData.terms = data;
           }
           // console.log($scope.cloudData.terms);

    });
  }

    $scope.changeFibreTerm = function (fibrePlan,index){
      //console.log(fibrePlan);
      switch (fibrePlan.price){
        case 599 :
        case 699 : 
        case 1199 : 
        case 758 :
        case 1599 :
        case 1699 : $scope.cloudData.fibreTerm[index] = 36;
                    break;
        case 1899 : $scope.cloudData.fibreTerm[index] = 24;
                    break;
        case 1299 : $scope.cloudData.fibreTerm[index] = 48;
                    break;
      }
      console.log($scope.cloudData);
    };

    $scope.addNewReferal = function() {
        $scope.cloudData.referals.push($scope.referalCount++);
    };  
        
    $scope.removeReferal = function(index) {
        $scope.cloudData.referals.splice(index,1);
        $scope.cloudData.rBusinessName.splice(index,1);
        $scope.cloudData.rContactPerson.splice(index,1);
        $scope.cloudData.rPhoneNumber.splice(index,1);
    };

    $scope.updateCardExpiry = function (year,month){
        var monthNum=month + "";
        while (monthNum.length < 2)
            monthNum = "0" + monthNum;
        $scope.cloudData.aCardExpiry = monthNum + '/' + year;
    }

    $scope.sendAsDraft = function(){
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var formHtmlData = new Array();
        var fileNameArr = new Array();
        //$interval.cancel(saveDraft);

        if (!angular.isDefined($scope.htmlData) || angular.equals({},$scope.htmlData)) {
          FlashMessage.setMessage({success:false, message:"No Forms Selected"});
          return;
        }
        $("#loading").show();
        angular.forEach($scope.htmlData, function(value,key) {
            var nametail = new Date().getTime();
            var rand = Math.floor((Math.random()*1000)+10);
            var fileName = key+rand+nametail+"-draft.pdf";
            fileNameArr.push(fileName);
            $("#downloadPdfBn").prop('disabled',true);
            formHtmlData.push({html:value, fileName : fileName});

        });

        var pdf = Cloud.update({htmlData :formHtmlData,
            userDetails : { name : $scope.cloudData.dName,
                                            email :$scope.cloudData.cEmail},
            account: {name: userProfile.name, email: userProfile.email}},
            function(data) {
            if($rootScope.editId) {
               var editId = $rootScope.editId;
               $rootScope.editId = null;
            }
            Cloud.save({ data : $scope.cloudData,
                        user : userProfile.id,
                        editId : editId,
                        fileName : fileNameArr,
                        type:"draft"}, function(data) {
                            if (data.success) {
                                //$scope.cloudData = {};
                                $("#loading").hide();
                                FlashMessage.setMessage({
                                    success:true,
                                    message:"The draft forms will be sent to your email shortly."
                                });
                            }
                        });
             });
    }

    $scope.saveDraftForm = function(templateName,cloudData) {
        $scope.template = "templates/cloud/" + templateName + ".html?t=" + _version;
        //setting the values changed by Script.
        cloudData.dateVal = $("#dateVal").val();
        cloudData.pBirth  = $("#pBirth").val();
        cloudData.pIAddress = $("#pIAddress").val();
        cloudData.pIState = $("#pIState").val();
        cloudData.pIPostcode =  $("#pIPostcode").val();
        cloudData.aPeriod = 'monthly';
        cloudData.pISuburb =  $("#pISuburb").val();
        $rootScope.draftMode = true;
        $scope.userSign = "";
        // Check if exists empty location fields.
        var comLocationObj = $('input.' + templateName);
        var emptyLocationCnt = 0;
        angular.forEach(comLocationObj, function(item, index) {
            var location = $(item).val();
            if (!location) {
                emptyLocationCnt ++;                
            }
        });

        if (emptyLocationCnt) {
            return;
        }

        $(".reAssign").each(function() {
            var id = $(this).prop("id");
            cloudData[id] = $(this).val();
        });

        cloudData["type"] = {};

        $("input[name=formTypes]:checked").each(function() {
            var id = $(this).prop("id");
            cloudData["type"][id] = $(this).prop("checked");
        });

        if (cloudData.type.configApp) {
            var telNo = new Array();
            $(".telNo").each(function() {
                  var item = $(this).find("input");
                  var idCount = item.data('count');
                  var telRange = $('#telItemRange' + '-' + idCount).val()!=''?' to '+$('#telItemRange' + '-' + idCount).val():'';
                  var obj = {
                                  tel : item[0].value + telRange 
                            };
                  telNo.push(obj);
            });
            $scope.cloudData.configAppTelNo = telNo;
            // var mobPort = new Array();
            // $(".mob-port-box").each(function() {
            //     var confAccHolder = $(this).find(".confAccHolder");
            //     var confMobNo = $(this).find(".confMobNo");
            //     var confProvider = $(this).find(".confProvider");
            //     var confAccNo = $(this).find(".confAccNo");
            //     var confPatPlan = $(this).find(".confPatPlan");
            //     var confDob = $(this).find(".confDob");
            //     var confDLicence = $(this).find(".confDLicence");

            //     var obj = {
            //                     confAccHolder : confAccHolder[0].value,
            //                     confMobNo : confMobNo[0].value,
            //                     confProvider : confProvider[0].value,
            //                     confAccNo : confAccNo[0].value,
            //                     confPatPlan : confPatPlan[0].value,
            //                     confDob : confDob[0].value,
            //                     confDLicence : confDLicence[0].value,
            //               };
            //     mobPort.push(obj);
            // });
            $scope.cloudData.configMobPort = $scope.configMobPort;
        }

        if(cloudData.type.rental || cloudData.type.leasing ||cloudData.singleOrder) {
            cloudData["rental"] = {};
            var rental = parseInt(cloudData.aPayment.replace(/[^\d.]/g,''));
            cloudData["aGST"] = isNaN(rental) ? '0.00' : (rental*10/100).toFixed(2);
            cloudData["aTotal"] = isNaN(rental) ? '0.00' : (rental + rental*10/100).toFixed(2);
        }

        // var schedule = new Array();
        // var scheduleOptions = new Array();
        // angular.forEach($scope.cloudData.schItems, function(data) {
        //     if (angular.isDefined(cloudData.schOption)) {
        //         var obj = {
        //                     qty : cloudData.schQty[data.count],
        //                     desc :cloudData.schOption[data.count].item != '_' ? cloudData.schOption[data.count].item : ''
        //                 };
        //         scheduleOptions.push(obj);
        //     }
        // });

        // $(".schedule-goods").each(function() {
        //     var item = $(this).find("input");
        //     var obj = { qty : item[0].value,
        //                 desc :item[1].value,
        //                 //itemNo : item[2].value,
        //                 //serNo : item[3].value
        //             };
        //     schedule.push(obj);
        // });

        // $scope.cloudData.scheduleOptions = scheduleOptions;
        // $scope.cloudData.schedule = schedule;

        if($scope.editId) {
            $rootScope.editId = $scope.editId;
        }

        $rootScope.cloudData = cloudData;

        ngDialog.open({
            template: $scope.template,
            plan: true,
            controller : 'CloudCtrl',
            width: 900,
            height: 600,
            scope: $scope,
            className: 'ngdialog-theme-default',
            preCloseCallback : function(value) {
                if (confirm("This will create a draft form. Continue?")) {
                    $scope.htmlData[templateName] = $(".ngdialog-content").html();
                    $scope[templateName] = true;
                    $timeout(angular.noop());
                    $('#isSigned').val('T');
                    $('#sendDraft').show();
                }
            }
        });

        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            var newHash = 'anchor';
            if ($location.hash() !== newHash) {
                // set the $location.hash to `newHash` and
                // $anchorScroll will automatically scroll to it
                $location.hash('anchor');
            } else {
                // call $anchorScroll() explicitly,
                // since $location.hash hasn not changed
                $location.hash('anchor');
                $anchorScroll();
            }
        });
    }
    
    $scope.sendDraftForms = function($id) {

        var forms = Form.downloadForms({id:id}, function(data) {
           if (data.success) {
               angular.forEach(data.files, function(name, index){
                    var filename = name.substr(0, name.indexOf('.')) + '.pdf'; 
                    window.open("assets/files/"+filename,'_blank');
               });
           }
        });
    }

    // setTimeout(function() {
    //    if (!$('.ngdialog.presign').length) {
    //         $scope.preSignDocument();
    //     }
    // }, 1000);
    
    $scope.editColTransparent = function() {

    }
    

    $scope.changeColTransparent = function(val) {
        //console.log(val);
        if ((val.item == '_') || (val.plan=='_')) {
            $('.col-sel-desc .chosen-container').addClass('transparent');
        } else {
            $('.col-sel-desc .chosen-container').removeClass('transparent');
        }

        $scope.telstraPlans = [];
        $scope.mobileMegaPlans = [];
        $scope.mobile4GUntimedPlans = [];
        //console.log($scope.configMobPort);
        if (angular.isDefined(val.plan)){
            angular.forEach($scope.configMobPort,function(item,index){
                //console.log(item);
                if (item.confPatPlan.group=="Telstra 4G"){
                    if (angular.isUndefined($scope.telstraPlans[item.confPatPlan.itemIndex]))
                        $scope.telstraPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.telstraPlans[item.confPatPlan.itemIndex]++;
                } else if(item.confPatPlan.group=="Mobile Mega"){
                    if (angular.isUndefined($scope.mobileMegaPlans[item.confPatPlan.itemIndex]))
                        $scope.mobileMegaPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.mobileMegaPlans[val.itemIndex]++;
                    item.boltOn = {};
                } else if(item.confPatPlan.group=="Mobile 4G Untimed"){
                    if (angular.isUndefined($scope.mobile4GUntimedPlans[item.confPatPlan.itemIndex]))
                        $scope.mobile4GUntimedPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.mobile4GUntimedPlans[item.confPatPlan.itemIndex]++;
                    item.boltOn = {};
                }
            });
            // console.log($scope.telstraPlans);
            // console.log($scope.mobileMegaPlans);
            // console.log($scope.mobile4GUntimedPlans);
            $scope.processBoltOnCount();
            //console.log($scope.mobileMegaPlans);
        }        
    }

    $scope.processBoltOnCount = function(){
        $scope.boltOnPlans = [];
        angular.forEach($scope.configMobPort,function(item,index){
            //console.log(item.boltOn.itemIndex);
            if (angular.isDefined(item.boltOn.itemIndex)){
                if (angular.isUndefined($scope.boltOnPlans[item.boltOn.itemIndex])){
                    $scope.boltOnPlans[item.boltOn.itemIndex]=1;    
                } else {
                    $scope.boltOnPlans[item.boltOn.itemIndex]++;
                }
            }
        });
    }

    if ($rootScope.viewFormData != null) {
        if (angular.isDefined($rootScope.viewFormData)) {
            $scope.editId = $rootScope.viewFormData.id;
        }
    }

    Cloud.getScheduleGoods(function(data) {
        $scope.scheduleGoodsList = data.goods;
        //console.log(data.goods);
        $scope.scheduleGoodsList.unshift({
            item: "_"
        });
    });


    function onChangeTemp(e)
    {
        //console.log(e);
    }

    $scope.handleTermChange = function (cloudData) {
        cloudData.promotedText = '';
        if ( cloudData.aTerm == '48promoted') {
              cloudData.aTermNew = 48;
              cloudData.PromotedText = "(First 3 Payments $0, followed by 45 payments of rental amount)"
        }
        if  (cloudData.aTerm =='60promoted') {
            cloudData.aTermNew = 60;
            cloudData.PromotedText = "(First 3 Payments $0, followed by 57 payments of rental amount)"
        }
        if  (cloudData.aTerm =='60') {
            cloudData.aTermNew = 60;
        }
        if (cloudData.aTerm =='48') {
            cloudData.aTermNew = 48;
        }
        if (cloudData.aTerm =='36') {
            cloudData.aTermNew = 36;
        }
        if  (cloudData.aTerm =='24') {
            cloudData.aTermNew = 24;
        }
        if (cloudData.aTerm =='Outright') {
            cloudData.aTermNew = "Outright";
        }
        return cloudData
    }

    $(".validUrl").on("keyup", function() {
        var val = this.value.replace(/^(http\:\/\/)/,'');
        this.value = "http://"+val;
    });

    $(".onlyAlpha").on("keyup", function() {
       var val = this.value.replace(/\d/g,'');
       this.value = val;
    });
     $(".onlyNumber").on("keyup", function() {
       var val = this.value.replace(/\D/g,'');
       this.value = val;
    });

    $(".dollarSign").on("keyup", function() {
        var val = this.value.replace(/\D/g,'');
        var parts = val.toString().split(".");
        parts =  parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        this.value = "$ "+parts;

        if (parts == "" || parts == "0") {
            this.value = "$ 0";
        } else {
            if (parts.indexOf("0") == 0) {
                parts = parts.replace("0","");
            }
            this.value = "$ "+parts;
        }
    });

    $("#dName").on("focusout", function() {
        $("#cContact").val($(this).val());
        $("#pName").val($(this).val());
    });

    $('#dSName').on("focusout", function() {
        $("#g2Name").val($(this).val());
    });

    $http.get("profile").success(function(response) {
        $scope.cloudData.wName = response.name;
        $scope.cloudData.userSign = response.sign;
    });

    $scope.transformTelFields  = function() {
        $('.tel').on("keyup", function()
        {
                var val = this.value.replace(/[^0-9]/ig, '');
                if (val.length >10) {
                    val = val.substr(0,10);
                }
                var newVal = '';
                if (val.length >2) {
                  newVal +='('+val.substr(0,2)+') ';
                  val = val.substr(2);
                }
                while (val.length > 4)   {
                  newVal += val.substr(0, 4) +'-';
                  val = val.substr(4);
                }
                newVal += val;
                this.value = newVal;
        });
        $('.telRange').on("keyup",function(){
            var val = this.value.replace(/[^0-9]/ig, '');
            if (val.length>4) {
                val = val.substr(0,4);
            }
            this.value = val;            
        });
    }

    $scope.addTel = function() {
        $timeout(function(){
            var content = '';
            content = '<div class="row telNo form-row">\n';
            content +='     <div class="row-element-label">\n\
                                 <label class="control-label">Tel No</label>\n\
                                  \n\<i id="adTel-' + $scope.addTelCount + '" class="fa fa-times"></i>\n\
                             </div>\n\
                             <div class="row-element-field">\n\
                                <div class="col-lg-6 col-md-12">\n\
                                    <input class="telItem tel form-control" data-count="' + $scope.addTelCount + '" id="telItemConfigApp-' + $scope.addTelCount + '" type="text" autofocus placeholder="From">\n\
                                </div>\n\
                                <div class=" col-lg-6 col-md-12">\n\
                                    <input class="telItem telRange form-control" data-count="' + $scope.addTelCount + '" id="telItemRange-' + $scope.addTelCount + '" type="text" autofocus placeholder="To">\n\
                                </div>\n\
                            </div>\n\
                        </div>';
            $(".addTelBtn").before(content);
            $("#telItemConfigApp-"+$scope.addTelCount).focus();
            $("#adTel-"+$scope.addTelCount).click(function() {
                  $(this).parent().parent().remove();
                  $scope.addTelCount--;
              });
            $scope.transformTelFields();
            $scope.addTelCount++;
        });

    }

    $scope.addAccount = function(){
       $newAccount = {
            'configCarrier':'',
            'configAccNo':'',
        };
        $scope.accountLocation[index]++;
        $scope.cloudData.accounts.push($newAccount);
        // $scope.accountLocation++;
        console.log($scope.cloudData.accounts);
    }
    $scope.removeAccount = function(index) {
        $scope.cloudData.accounts.splice(index,1);
    }

    $scope.addPortingAuth = function() {
        // $timeout(function(){
        $newMobPort = {
            'confAccHolder':$scope.configMobPort[$scope.addMobPortCount].confAccHolder,
            'confMobNo':'',
            'confProvider':$scope.configMobPort[$scope.addMobPortCount].confProvider,
            'confAccNo':$scope.configMobPort[$scope.addMobPortCount].confAccNo,
            'confPatPlan':$scope.mobile_rate_plans[0],
            'boltOn' : {}
        };
        $scope.addMobPortCount[index]+1;
        $scope.configMobPort.push($newMobPort);
        //     //var content = $(".addTelBtn").siblings(".telNo");
        //     var content =$(".mob-port-box").html();
        //     var wrapped = "<div class='form-row mob-port-box' id='mob-port"+$scope.addMobPortCount+"'>"+content+"</div>";
        //     $(".portingAuthBtn").before(wrapped);
        //     $('.date').datetimepicker({
        //         format : "DD/MM/YYYY",
        //         defaultDate : today
        //     });
        //     $scope.transformTelFields();
        
        // });
    }

    $scope.saveAsDraft = function(cloudData) {
        cloudData.pBirth = $('#pBirth').val();
        //cloudData.aBillingTerm = $('#aBillingTerm').val();
        //cloudData.aIdenQuestion = $('#aIdenQuestion').val();
        // cloudData.bfCapDiscount = $('#bFCapDiscount').val();
        // console.log(cloudData);
        // console.log(cloudData);
        if (!$scope.editId) {
            $scope.editId = "";
        }
        var user = Scopes.get('DefaultCtrl').profile;
      // console.log(user);
        $(".reAssign").each(function() {
            var id = $(this).prop("id");
            if (id=='alarmPlans'){
                var alarmPlan = $scope.cloudData.alarmPlans;
                cloudData[id] = $(this).val();
                $scope.cloudData.alarmPlans = alarmPlan;
            } else {
                cloudData[id] = $(this).val();
            }
            if (id == "aPayout" && !$(this).val())
                cloudData[id] = "$ 0";
        });

        cloudData.configMobPort = $scope.configMobPort;
    //
        Cloud.save({data:cloudData, user : user.id, fileName : '',
                    editId : $scope.editId, type:"draft"}, function(data) {
                        $scope.editId = data.editId;
        });
    }

    $scope.prepDiscounts = function(payment) {
      payment = payment.replace(/^\D+|\,+/g, '');
       $scope.EqPayment = payment;
      }

    $scope.updateComLocations = function (){
        $scope.cloudData.comLocation = {
            'nbf_cap'               : [$scope.cloudData.pIAddress],
            'nbf_std'               : [$scope.cloudData.pIAddress],
            'voice_comp'              : [$scope.cloudData.pIAddress],
            'voice_solution_untimed'  : [$scope.cloudData.pIAddress],
            'voice_essentials_cap'    : [$scope.cloudData.pIAddress],
            'voice_solution_standard' : [$scope.cloudData.pIAddress],
            'data_adsl'               : [$scope.cloudData.pIAddress],
            'ip_midband'              : [$scope.cloudData.pIAddress],
            'ip_midbandunli'          : [$scope.cloudData.pIAddress],
            'nbn'                     : [$scope.cloudData.pIAddress],
            'nbnUnlimited'            : [$scope.cloudData.pIAddress],
            'mobile_mega'             : [$scope.cloudData.pIAddress],
            'mobile_ut'               : [$scope.cloudData.pIAddress],
            'mobile_wireless'         : [$scope.cloudData.pIAddress],
            '1300'                    : [$scope.cloudData.pIAddress],
            '1300_discounted'         : [$scope.cloudData.pIAddress],
            'fibre'                   : [$scope.cloudData.pIAddress],
            'ethernet'                : [$scope.cloudData.pIAddress],
            'telstraUntimed'          : [$scope.cloudData.pIAddress],
            'telstraWireless'         : [$scope.cloudData.pIAddress],
            'printer'                 : [$scope.cloudData.pIAddress],
            'schedule_of_goods'       : [$scope.cloudData.pIAddress]
        };
    }

    $scope.updateComSuburbs = function (){
        $scope.cloudData.comSuburb = {
            'nbf_cap'               : [$scope.cloudData.pISuburb],
            'nbf_std'               : [$scope.cloudData.pISuburb],
            'voice_comp'              : [$scope.cloudData.pISuburb],
            'voice_solution_untimed'  : [$scope.cloudData.pISuburb],
            'voice_essentials_cap'    : [$scope.cloudData.pISuburb],
            'voice_solution_standard' : [$scope.cloudData.pISuburb],
            'data_adsl'               : [$scope.cloudData.pISuburb],
            'ip_midband'              : [$scope.cloudData.pISuburb],
            'ip_midbandunli'          : [$scope.cloudData.pISuburb],
            'nbn'                     : [$scope.cloudData.pISuburb],
            'nbnUnlimited'            : [$scope.cloudData.pISuburb],
            'mobile_mega'             : [$scope.cloudData.pISuburb],
            'mobile_ut'               : [$scope.cloudData.pISuburb],
            'mobile_wireless'         : [$scope.cloudData.pISuburb],
            '1300'                    : [$scope.cloudData.pISuburb],
            '1300_discounted'         : [$scope.cloudData.pISuburb],
            'fibre'                   : [$scope.cloudData.pISuburb],
            'ethernet'                : [$scope.cloudData.pISuburb],
            'telstraUntimed'          : [$scope.cloudData.pISuburb],
            'telstraWireless'         : [$scope.cloudData.pISuburb],
            'printer'                 : [$scope.cloudData.pISuburb],
            'schedule_of_goods'       : [$scope.cloudData.pISuburb]
        };
    }

    $scope.updateComPostCode = function (){
        $scope.cloudData.comZipCode = {
            'nbf_cap'               : [$scope.cloudData.pIPostcode],
            'nbf_std'               : [$scope.cloudData.pIPostcode],
            'voice_comp'              : [$scope.cloudData.pIPostcode],
            'voice_solution_untimed'  : [$scope.cloudData.pIPostcode],
            'voice_essentials_cap'    : [$scope.cloudData.pIPostcode],
            'voice_solution_standard' : [$scope.cloudData.pIPostcode],
            'data_adsl'               : [$scope.cloudData.pIPostcode],
            'ip_midband'              : [$scope.cloudData.pIPostcode],
            'ip_midbandunli'          : [$scope.cloudData.pIPostcode],
            'nbn'                     : [$scope.cloudData.pIPostcode],
            'nbnUnlimited'            : [$scope.cloudData.pIPostcode],
            'mobile_mega'             : [$scope.cloudData.pIPostcode],
            'mobile_ut'               : [$scope.cloudData.pIPostcode],
            'mobile_wireless'         : [$scope.cloudData.pIPostcode],
            '1300'                    : [$scope.cloudData.pIPostcode],
            '1300_discounted'         : [$scope.cloudData.pIPostcode],
            'fibre'                   : [$scope.cloudData.pIPostcode],
            'ethernet'                : [$scope.cloudData.pIPostcode],
            'telstraUntimed'          : [$scope.cloudData.pIPostcode],
            'telstraWireless'         : [$scope.cloudData.pIPostcode],
            'printer'                 : [$scope.cloudData.pIPostcode],
            'schedule_of_goods'       : [$scope.cloudData.pIPostcode]
        };
    }

    $scope.showPlan = function(plan){
        //console.log(plan);
    }

    $scope.calculateDiscount = function(currentVal,plan, planOptionId, completeOptions,optionId) {
        var payment = 0;
        var capTotalVal =0;
        //console.log(planOptionId);
        if (planOptionId != '') {
            var planStr = $("#"+planOptionId+" option:selected").text();
            var planVal =parseInt(planStr.match(/\$(\d+)/)[1]);
        }
        currentVal = parseInt(currentVal);
        //alert(currentVal);
        if(angular.isDefined($scope.EqPayment)) {
            payment = parseInt($scope.EqPayment);
        }
        //alert(completeOptions);
        var c_options = angular.isDefined(completeOptions)?completeOptions:0;
        //alert(c_options);
        $(".main-disc").each(function() {
            capTotalVal += parseInt($(this).val());
        });
        if (plan == 'voice-cap' || plan == 'data-plan' || plan == 'mobile-plan' ) {
            if (currentVal > planVal) {
                alert("Discount cannot exceed monthly payment");
                //$('#' + optionId).val('');
            } else {
                if(capTotalVal  == currentVal) {
                    if (currentVal> planVal/2) {
                        var disc = planVal/2;
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                } else {
                    var remaining = payment-(capTotalVal-currentVal);
                    if (remaining<0) {
                        remaining = 0;
                    }

                    var disc = remaining + planVal*30/100;
                    if (currentVal > disc) {
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                }
            }
        } else if (plan == 'standard-plan') {
            if (currentVal>100) {
                alert("Maximum Discount should not go over $100");
               // $('#' + optionId).val('');
            }
        }  else if (plan == 'complete-bri') {
            if (currentVal>50*c_options) {
                alert("Maximum Discount should not go over $"+50*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-pri') {
            if (currentVal>100*c_options) {
                alert("Maximum Discount should not go over $"+100*c_options);
               // $('#' + optionId).val('');
            }
        } else if (plan == 'complete-analog') {
            if (currentVal>(10*c_options)) {
                alert("Maximum Discount should not go over $" + 10*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-call' || plan == '1300-plan' || plan == 'standard-call') {
            if (currentVal> payment) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            }
        } else if (plan == 'unlimited-plan') {
            if (currentVal> planVal) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            } else {
                if (currentVal> planVal/2) {
                    var disc = planVal/2;
                    alert("Maximum Discount should not go over $"+disc);
                   // $('#' + optionId).val('');
                }
            }
        }
    }

    $scope.showOptions = function(form) {
        //alert($scope.cloudData.type.monitoring_solution);
        $(".optionBox").hide();
        $("#"+form+"Option").show();
        $(".form-label label").css('color','black');
        $("#"+form+"Label label").css('color','green');
        if (form=='copierAgreement'){
            $scope.processCopierAgreement();
        }
        // Initialize location count and locations object.
        $scope.locationCount = 0;
    }

    $scope.processCopierAgreement = function(){
        //alert('Process Copier Agreement');
        var printers = [];
        //console.log($scope.items['schedule_of_goods']);
        angular.forEach($scope.items['schedule_of_goods'],function(item,index){
            printers[index] = [];
            //console.log($scope.cloudData.schItems[index]);
           angular.forEach($scope.cloudData.schItems[index],function(schItem){
            //console.log(schItem);
                if (!angular.isUndefined($scope.cloudData.schOption)){
                    if (!angular.isUndefined($scope.cloudData.schOption[index])){
                        if (!angular.isUndefined($scope.cloudData.schOption[index][schItem.count])){
                            if (($scope.cloudData.schOption[index][schItem.count].group.toUpperCase().indexOf('KYOCERA')!==-1) || ($scope.cloudData.schOption[index][schItem.count].group.toUpperCase().indexOf('FUJI')!==-1) || ($scope.cloudData.schOption[index][schItem.count].group.toUpperCase().indexOf('OKI')!==-1)){
                                var cpiBwAmt = $scope.cloudData.schQty[index][schItem.count] * $scope.cloudData.schOption[index][schItem.count].cpiBw;
                                if (cpiBwAmt==0){
                                    cpiBwAmt = 1.0;
                                }
                                var cpiColourAmt = $scope.cloudData.schQty[index][schItem.count] * $scope.cloudData.schOption[index][schItem.count].cpiColour;
                                if (cpiColourAmt==0){
                                    cpiColourAmt = 10.0;
                                }
                                var printer = {
                                    "model" : $scope.cloudData.schOption[index][schItem.count].model,
                                    "group" : $scope.cloudData.schOption[index][schItem.count].group,
                                    "item"  : $scope.cloudData.schOption[index][schItem.count].item,
                                    "qty"  : $scope.cloudData.schQty[index][schItem.count],
                                    "cpiBwAmt"  : cpiBwAmt.toFixed(1),
                                    "cpiColourAmt"  : cpiColourAmt.toFixed(1)
                                }
                                printers[index].push(printer);
                            }
                        }
                    }
                }
             });
        });
        angular.forEach(printers, function(printer,index){
            if (printer.length==0){
                printers.splice(index,1);
            }
        });
        //console.log(printers);
        if (printers.length!=0){
            $scope.cloudData.printers = printers;
        }
        else{
            alert('No printer is selected in the schedule of goods.');
        }            
    }

    // $scope.ipPlan = {
    //     10 : {rate : 499, desc : "Up to 10/10 Mbps**"},
    //     T20 : {rate : 599, desc : "Up to 20/20 Mbps**"},
    //     400 : {rate : 758, desc : "Up to 400/400Mbps**"},
    //     4 : {rate : 419, desc : "4Mbps/4Mbps"},
    //     8 : {rate : 429, desc : "8Mbps/8Mbps*"},
    //     18 : {rate : 489, desc : "18Mbps/18Mbps*"},
    //     20 : {rate : 599, desc : "20Mbps/20Mbps*"},
    //     30 : {rate : 998, desc : "30Mbps/30Mbps*"}
    // }

    // $scope.ipDownload = {
    //     50 :{plan : "50GB", rate: 69},
    //     100 :{plan : "100GB", rate : 79},
    //     200 : {plan : "200GB" , rate :139},
    //     300 : {plan : "300GB", rate : 209},
    //     500 : {plan : "500GB",rate : 339},
    //     1000 : {plan : "1000GB", rate : 639}
    // }

    $scope.nbnPlan = {
        255 : {rate : 49, desc : "NBN 25Mbps/5Mbps*"},
        2510 : {rate : 59, desc : "NBN 25Mbps/10Mbps*"},
        5020 : {rate : 69, desc : "Up to 50/20Mbps**"},
        10040 : {rate : 79, desc : "NBN 100Mbps/40Mbps*"}
    }
    $scope.nbnDownload = {
        100 :{plan : "100GB", rate : 19},
        200 : {plan : "200GB" , rate :25},
        500 : {plan : "500GB",rate : 29},
        1000 : {plan : "1000GB", rate : 49}
    }

    $scope.setValues = function() {
        $scope.cloudData.cName = "Some Long Name Comp.";
        $scope.cloudData.cAbn = 12323231321;
        $scope.cloudData.cTrading = "Long trade name";
        $scope.cloudData.cAddress = "221B Baker Street";        
        $scope.cloudData.cPostCode = 2000;

        $scope.cloudData.pIAddress = "221B Baker Street";
        $scope.cloudData.pISuburb = "London";
        $scope.cloudData.pIState = "ACT";
        $scope.cloudData.pIPostcode = "1234";
        $scope.cloudData.aBillingTerm = 60;
        $scope.cloudData.aTerm = 60;

        $scope.cloudData.comLocation = {};
        $scope.cloudData.comLocation = {
            'nbf_cap'               : [$scope.cloudData.pIAddress],
            'nbf_std'               : [$scope.cloudData.pIAddress],
            'voice_comp'              : [$scope.cloudData.pIAddress],
            'voice_solution_untimed'  : [$scope.cloudData.pIAddress],
            'voice_essentials_cap'    : [$scope.cloudData.pIAddress],
            'voice_solution_standard' : [$scope.cloudData.pIAddress],
            'data_adsl'               : [$scope.cloudData.pIAddress],
            'ip_midband'              : [$scope.cloudData.pIAddress],
            'ip_midbandunli'          : [$scope.cloudData.pIAddress],
            'nbn'                     : [$scope.cloudData.pIAddress],
            'nbnUnlimited'            : [$scope.cloudData.pIAddress],
            'mobile_mega'             : [$scope.cloudData.pIAddress],
            'mobile_ut'               : [$scope.cloudData.pIAddress],
            'mobile_wireless'         : [$scope.cloudData.pIAddress],
            '1300'                    : [$scope.cloudData.pIAddress],
            '1300_discounted'         : [$scope.cloudData.pIAddress],
            'fibre'                   : [$scope.cloudData.pIAddress],
            'ethernet'                : [$scope.cloudData.pIAddress],
            'telstraUntimed'          : [$scope.cloudData.pIAddress],
            'telstraWireless'         : [$scope.cloudData.pIAddress],
            'printer'                 : [$scope.cloudData.pIAddress],
            'schedule_of_goods'       : [$scope.cloudData.pIAddress]
        };

        $scope.cloudData.cSuburb = "London";
        $scope.cloudData.comSuburb = {};
        $scope.cloudData.comSuburb = {
            'nbf_cap'               : [$scope.cloudData.pISuburb],
            'nbf_std'               : [$scope.cloudData.pISuburb],
            'voice_comp'              : [$scope.cloudData.pISuburb],
            'voice_solution_untimed'  : [$scope.cloudData.pISuburb],
            'voice_essentials_cap'    : [$scope.cloudData.pISuburb],
            'voice_solution_standard' : [$scope.cloudData.pISuburb],
            'data_adsl'               : [$scope.cloudData.pISuburb],
            'ip_midband'              : [$scope.cloudData.pISuburb],
            'ip_midbandunli'          : [$scope.cloudData.pISuburb],
            'nbn'                     : [$scope.cloudData.pISuburb],
            'nbnUnlimited'            : [$scope.cloudData.pISuburb],
            'mobile_mega'             : [$scope.cloudData.pISuburb],
            'mobile_ut'               : [$scope.cloudData.pISuburb],
            'mobile_wireless'         : [$scope.cloudData.pISuburb],
            '1300'                    : [$scope.cloudData.pISuburb],
            '1300_discounted'         : [$scope.cloudData.pISuburb],
            'fibre'                   : [$scope.cloudData.pISuburb],
            'ethernet'                : [$scope.cloudData.pISuburb],
            'telstraUntimed'          : [$scope.cloudData.pISuburb],
            'telstraWireless'         : [$scope.cloudData.pISuburb],
            'printer'                 : [$scope.cloudData.pISuburb],
            'schedule_of_goods'       : [$scope.cloudData.pISuburb]
        };

        $scope.cloudData.comZipCode = {};
        $scope.cloudData.comZipCode = {
            'nbf_cap'               : [$scope.cloudData.pIPostcode],
            'nbf_std'               : [$scope.cloudData.pIPostcode],
            'voice_comp'              : [$scope.cloudData.pIPostcode],
            'voice_solution_untimed'  : [$scope.cloudData.pIPostcode],
            'voice_essentials_cap'    : [$scope.cloudData.pIPostcode],
            'voice_solution_standard' : [$scope.cloudData.pIPostcode],
            'data_adsl'               : [$scope.cloudData.pIPostcode],
            'ip_midband'              : [$scope.cloudData.pIPostcode],
            'ip_midbandunli'          : [$scope.cloudData.pIPostcode],
            'nbn'                     : [$scope.cloudData.pIPostcode],
            'nbnUnlimited'            : [$scope.cloudData.pIPostcode],
            'mobile_mega'             : [$scope.cloudData.pIPostcode],
            'mobile_ut'               : [$scope.cloudData.pIPostcode],
            'mobile_wireless'         : [$scope.cloudData.pIPostcode],
            '1300'                    : [$scope.cloudData.pIPostcode],
            '1300_discounted'         : [$scope.cloudData.pIPostcode],
            'fibre'                   : [$scope.cloudData.pIPostcode],
            'ethernet'                : [$scope.cloudData.pIPostcode],
            'telstraUntimed'          : [$scope.cloudData.pIPostcode],
            'telstraWireless'         : [$scope.cloudData.pIPostcode],
            'printer'                 : [$scope.cloudData.pIPostcode],
            'schedule_of_goods'       : [$scope.cloudData.pIPostcode]
        };
        $scope.cloudData.aIdenQuestion = "";
        $scope.cloudData.cEmail = "info@somelongcompany.com.au";
        $scope.cloudData.dName = "Arthur Conan Doyle";
        $scope.cloudData.cPosition = "Director";
        $scope.cloudData.dSName = "Sherlock Holmes";
        $scope.cloudData.cSPosition = "Director";
        $scope.cloudData.cContact = "Arthur Conan Doyle";
        $scope.cloudData.cTel = "(02) 4342-1232";
        $scope.cloudData.cFax = "(02) 4342-1232";
        $scope.cloudData.cMobile = "4532-533-267";
        $scope.cloudData.cWebsite = "http://somelongcompany.com.au";
        $scope.cloudData.aAccountant = "James Moriarty";
        $scope.cloudData.aIndustry = "PROFESSIONAL OFFICE";
        $scope.cloudData.aTradeRef = "A Study in Scarlet";
        $scope.cloudData.aCPerson = "Dr. Watson";
        $scope.cloudData.aTel = "(02) 2340-5444";
        $scope.cloudData.pName = "Sherlock Holmes";
        $scope.cloudData.pABN = "0919231921";
        $scope.cloudData.pFax = "(02) 4324-1234";
        $scope.cloudData.pEmail = "info@somelongcompany.com.au";
        $scope.cloudData.pAddress = "220B Baker Street";
        $scope.cloudData.pSuburb = "London";
        $scope.cloudData.pPostcode = 2000;
        $scope.cloudData.pPhone = "(02) 4342-1232";
        $scope.cloudData.pLicense = "2332323243A";
        $scope.cloudData.pValue = "$ 600,000";
        $scope.cloudData.pMortgage = "$ 400,000";
        $scope.cloudData.pOwned = true;
        $scope.cloudData.tTel = "(02) 4342-1232";
        $scope.cloudData.aContact = "Mrs. Hudson";
        $scope.cloudData.tContact = "Mr. Hudson";
        $scope.cloudData.aNemp = "1000";
        $scope.cloudData.aInsurer = "Insurance Company";
        $scope.cloudData.aPolicy = "A/127/2016";
        $scope.cloudData.g2Name = "John Watson";
        $scope.cloudData.g2Address = "220B Baker Street";
        $scope.cloudData.g2ABN = "5352352525";
        $scope.cloudData.g2Suburb = "London";
        $scope.cloudData.g2Postcode = 2000;
        $scope.cloudData.g2Phone = "(02) 4342-1232";
        $scope.cloudData.g2License = "2332323243A";
        $scope.cloudData.g2Value = "$ 600,000";
        $scope.cloudData.g2Mortgage = "$ 400,000";
        $scope.cloudData.g2Owned = true;
        $scope.cloudData.g2Fax = "(02) 4444-1111";
        $scope.cloudData.g2Email = "guarantor2@guarantor.com";
        $scope.cloudData.aYear = "1";
        $scope.cloudData.cState = "ACT";
        $scope.cloudData.aFinInst = "1111";
        $scope.cloudData.aFinBranch = "2222";
        $scope.cloudData.aAccName = "3333";
        $scope.cloudData.aBsb = "4444";
        $scope.cloudData.aAccDetails = "5555";
        $scope.cloudData.comments = "Referred by: " +  $scope.cloudData.cName + " " +  $scope.cloudData.cContact + " " + $scope.cloudData.cTel;
        //console.log($('#cloudData'));
    }
    $scope.saver = function(){
         $scope.cloudData.term = $('#term').val();
         $scope.cloudData.type = $('#type').val();
         var rates = Cloud.getRates({term:$scope.cloudData.term,
                                    type:$scope.cloudData.type
                                    }, function(data) {
            if (data.success) {
            $scope.rates = data.rates;
           }
            console.log($scope.rates);
            if($scope.cloudData.type == 'rental'){
            alert('rental');
            console.log($scope.rates);
            angular.forEach($scope.rates, function(value, key){
                
                   console.log('Rental:' + key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate + $scope.cloudData.perMonth);
                   if($scope.checkRange($scope.cloudData.perMonth,value.lowerLimit,value.upperLimit)){
                   $scope.leaser = ($scope.cloudData.perMonth/1000)*value.rate;
                 }
             });
            
        }
        else{
            alert('leasing');
            console.log($scope.rates);
            angular.forEach($scope.rates, function(value, key){
                
                   console.log(key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate);
                    if($scope.checkRange($scope.cloudData.perMonth,value.lowerLimit,value.upperLimit)){
                   $scope.leaser = ($scope.cloudData.perMonth/1000)*value.rate;
                 }
             });
        }
        
        });
            
  }
        $scope.checkRange = function(val,lower_limit,upper_limit){
        //alert(val + '-' + lower_limit + '-' + upper_limit);
        if (upper_limit==0){
            if (val>=lower_limit){
                return true;
            } else {
                return false;
            }
        } else {
            if (val >= lower_limit && val <= upper_limit){
                return true;
            } else {
                return false;
            }
        }
    }
    $scope.updateReferalInfo = function (){
        $scope.cloudData.comments =  "Referred by: ";
        if(angular.isDefined($scope.cloudData.cName)){
            $scope.cloudData.comments += $scope.cloudData.cName;
        } 
        if(angular.isDefined($scope.cloudData.cContact)){
            $scope.cloudData.comments += (' ' + $scope.cloudData.cContact);  
        }
        // if(angular.isDefined($scope.formData.cTel)){
        //     $scope.cloudData.comments += (' ' + $scope.cloudData.cTel); 
        // } 
    }

    $('.mob').on("keyup", function()
    {
        var val = this.value.replace(/[^0-9]/ig, '');
        var newVal = '';
        /*if (val.length >10) {
            val = val.substr(0,10);
        }*/
        if (val.length >4) {
            newVal +=val.substr(0,4)+'-';
            val = val.substr(4);
        }
        while (val.length > 3)   {
            newVal += val.substr(0, 3) +'-';
            val = val.substr(3);

        }
        newVal += val;
        this.value = newVal;
    });

    $('.ccNo').on("keyup", function()
    {
        var val = this.value.replace(/[^0-9]/ig, '');
        var newVal = '';
        while (val.length > 4) {
            newVal += val.substr(0, 4) +'-';
            val = val.substr(4);
        }
        newVal += val;
        this.value = newVal;
    });

    $scope.transformTelFields();

    var dateObj = new Date();
    var dd = dateObj.getDate();
    var mm = dateObj.getMonth()+1;
    var yy = dateObj.getFullYear();
    var today = mm+"/"+dd+"/"+yy;
    var minYY = yy - 18; //(Applicant have to be atleast 18);
    var birthMinDate = mm+"/"+dd+"/"+minYY;

    $('.expiryDate').datetimepicker({
       format : "DD/MM/YYYY",
       minDate : today
    });

    $('.date').datetimepicker({
        format : "DD/MM/YYYY",
        defaultDate : today
    });
    $('.pBirthDate').datetimepicker({
        format : "DD/MM/YYYY",
        maxDate : birthMinDate


    });

    $scope.getRates = function(term,type){
      Cloud.getRates({term:term,type:type}, function(data) {
        if (data.success) {
          $rootScope.rates = data.rates;
        }
      });
    }

    $scope.$watch("cloudData.type.rental", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {
                $scope.getRates($scope.cloudData.aTerm,'rental');
                $scope.cloudData.type.leasing = false;
                $scope.cloudData.type.outright = false;
                $scope.cloudData.type.chattle = false;
            }
          //  $scope.showSchedule();
        }
    });

    $scope.$watch("cloudData.type.leasing", function(val) {
        if (angular.isDefined(val)) {
           if (val == true) {
                $scope.getRates($scope.cloudData.aTerm,'leasing');
                $scope.cloudData.type.rental = false;
                $scope.cloudData.type.outright = false;
                $scope.cloudData.type.chattle = false;
            }
           // $scope.showSchedule();
        }
    });

    $scope.$watch("cloudData.type.outright", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {
                $scope.cloudData.type.rental = false;
                $scope.cloudData.type.leasing = false;
                $scope.cloudData.type.chattle = false;
            }

        }
    });

    $scope.$watch("cloudData.type.chattle", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {

                $scope.cloudData.type.rental = false;
                $scope.cloudData.type.leasing = false;
                $scope.cloudData.type.outright = false;
            }
        }
    });

    $scope.sign = function(id) {
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({
                color:"#000000",lineWidth:1,
                    width :490, height :98,
                    cssclass : "signature-canvas",
                   "decor-color": 'transparent'
                });
            clicked = true;
        }
    }


    $scope.presign = function(id) {
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({
                color:"#000000",
                lineWidth:1,
                width :490, 
                height :98,
                cssclass : "signature-canvas",
                "decor-color": 'transparent'
            });
        }
    }

    $scope.sendReferral = function(){
        Cloud.send_referrals({
                'lp_Comments'   : $scope.cloudData.comments + ' through ' + $scope.profile.name,
                'lp_Company'    : $scope.cloudData.rBusinessName,
                'lp_Phone'      : $scope.cloudData.rPhoneNumber,
                'lp_ContactFirstName' : $scope.cloudData.rContactPerson,
                'wl_salesrep'   : $scope.profile.name,
                'wl_referredby' : 'Referred by : ' + $scope.cloudData.cName + ' ' + $scope.cloudData.cContact + ' ' + $scope.cloudData.cTel + ' through ' + $scope.profile.name,
                'lp_UserField6' : 'Referral From Ap Nexgen',
            },function(data){
                if (data.response) {
                    
                }
                if (data.link) {
                   
                }
            }
        );
        
       
    } 

    $scope.downloadPdf = function() {
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var formHtmlData = new Array();
        var fileNameArr = new Array();
        //$interval.cancel(saveDraft);

        $scope.cloudData.signedTimestamps = $window.signedTimeStamps;

        if (!angular.isDefined($scope.htmlData) || angular.equals({},$scope.htmlData)) {
          FlashMessage.setMessage({success:false, message:"No Forms Selected"});
          return;
        }
        // $("#loading").show();
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;

        angular.forEach($scope.htmlData, function(value, key) {
            var nametail = new Date().getTime();
            var rand = Math.floor((Math.random()*1000)+10);
            var fileName = key + rand + nametail + ".pdf";
            fileNameArr.push(fileName);
            $("#downloadPdfBn").prop('disabled',true);

            // In case of forms with location fields.
            if ($scope.locationAvailableTemps.indexOf(key) !== -1 && $scope.items[key].length > 1) {
                angular.forEach(value, function(data, index) {
                    rand = Math.floor((Math.random() * 1000) + 10);
                    fileName = key + rand + nametail + ".pdf";
                    formHtmlData.push({html: data, fileName : fileName});
                });
            } else {
                formHtmlData.push({html: value, fileName : fileName});
            }
        });

        var pdf = Cloud.update({
            htmlData :formHtmlData,
            userDetails : { name : $scope.cloudData.dName,
                            email :$scope.cloudData.cEmail},
            account: {name: userProfile.name, email: userProfile.email}},
            function(data) {
            if($rootScope.editId) {
               var editId = $rootScope.editId;
               $rootScope.editId = null;
            }
            Cloud.save({ data : $scope.cloudData,
                        user : userProfile.id,
                        editId : editId,
                        fileName : fileNameArr,
                        type:"Completed"}, function(data) {
                if (data.success) {
                    $scope.cloudData = {};
                    // $("#loading").hide();
                    $scope.loading = false;
                    if ($('#form_complete').val()=='F'){
                      $scope.template = "templates/cloud/summary.html";
                      ngDialog.open({
                            // data: angular.toJson({
                            //   proposalData: datum
                          // }),
                           template: $scope.template,
                           plan: true,
                           controller : 'CloudCtrl',
                           width: '22cm',
                           className: 'ngdialog-theme-default',
                           showClose: true,
                           closeByEscape: true,
                           closeByDocument: true
                       });
                    } else {
                      FlashMessage.setMessage({
                        success:true,
                        message:"The forms will be sent to your email shortly."}
                      );
                     // alert("The forms will be to your email shortly..");
                      $window.location.reload();
                    }
                }
            });
        });
    }

    $scope.trustSrc = function(src) {
      return $sce.trustAsResourceUrl(src);
    }

    $scope.showPaycorpUrl = function() {       
        //console.log($('#ccInfo').prop());
        // if($('#ccInfo').val()==on
        console.log($scope.ccInfo);
        if ($scope.ccInfo){
          $scope.cloudData.aPaymentMethod = 'Credit Card';
          Cloud.showPaycorp(function(data) {
            console.log(data);
            $scope.url = data.response.paymentPageUrl;   
            $scope.reqId = data.response.reqId; 
            $scope.template = "templates/cloud/paycorp.html";
            ngDialog.open({
                 template: $scope.template,
                 plan: true,
                 controller : 'CloudCtrl',
                 width: 960,
                 height: 720,
                 scope: $scope,
                 className: 'ngdialog-theme-default',
                 showClose: true,
                 closeByEscape: true,
                 closeByDocument: true,
                 preCloseCallback : function(value) {
                    Cloud.getToken({ reqid : $scope.reqId },function(data){
                      $scope.cloudData.aCardName = data.response.holderName;
                      $scope.cloudData.aCardNumber = data.response.cardNumber;
                      $scope.cloudData.aCardExpiry = data.response.expiry;
                      $scope.cloudData.aCardType = data.response.type;
                      $scope.cloudData.aCardToken = data.response.token;
                    })
                 }
            });
           });
        } else {
          $scope.cloudData.aPaymentMethod = 'Debit Card';
        }
    }

    $scope.formGroups = {
        telecomService      : [
                                {file:'billing_app',id : 'configApp'},
                                {file:'monitoring_solution',id : 'monitoring_solution'} 
                              ],
        financeApplication  : [
                                {file:'rental',id : 'rental'},
                                {file: 'leasing', id : 'leasing'},
                                {file: 'chattle', id : 'chattle'}
                              ],
        orderSpec           : [
                                {file: 'single_order_spec', id : 'singleOrder'},
                                {file: 'copierAgreement', id: 'copierAgreement'}
                              ],
        voice               : [
                                {file: 'nbf_cap', id : 'voiceCap'},
                                {file: 'voice_comp', id : 'completeOffice'},
                                {file: 'voice_essentials_cap', id : 'voiceEssential'},
                                {file: 'voice_solution_standard', id : 'voiceSolution'},
                                {file: 'voice_solution_untimed', id : 'voiceUt'},
                                {file: 'nbf_std', id : 'focusStandard'}
                              ],
        data                : [
                                {file: 'data_adsl', id : 'adsl2'},
                                {file: 'ip_midband', id : 'ipMidband'},
                                {file: 'ip_midbandunli', id : 'ipMidbandUnli'},
                                {file: 'nbn', id : 'nbnMonthly'},
                                {file: 'ethernet', id: 'ethernet'},
                                {file: 'fibre', id : 'fibre'}
                              ],
        mobile              : [
                                {file: 'mobile_mega', id : 'mobileCap'},
                                {file: 'mobile_ut', id : 'mobileUt'},
                                {file: 'mobile_wireless', id : 'mobileWireless'},
                                {file: 'telstraUntimed', id: 'telstraUntimed'},
                                {file: 'telstraWireless', id: 'telstraWireless'}
                              ],
        r1300               : [
                                {file: '1300', id : 'rate131300'},
                                {file: '1300_discounted', id : 'rateDis131300'}
                              ]
    }

    $rootScope.$on('ngDialog.opened', function (e, $dialog) {
        $window.scrollTo(0, angular.element(".ngdialog-content").offsetTop);
    });

    $scope.checkAccount = function(){
      angular.forEach($scope.cloudData.accounts,function(data){
        if (data.configCarrier=='')
          return false;
        if (data.configAccno=='')
          return false;
      });
      return true;
    }


$scope.signDocument = function(templateName, cloudData) {
        $scope.template = "templates/cloud/"+templateName+".html?t=" + _version;
        //setting the values changed by Script.
        cloudData.dateVal = $("#dateVal").val();
        cloudData.pBirth  = $("#pBirth").val();
        cloudData.pIAddress = $("#pIAddress").val();
        cloudData.pIState = $("#pIState").val();
        cloudData.pIPostcode =  $("#pIPostcode").val();
        cloudData.aPeriod = 'monthly';
        cloudData.pISuburb =  $("#pISuburb").val();
        $scope.cloudData.totalServiceAmount = 0;
        $scope.cloudData.totalServiceDiscount = 0;
        console.log(cloudData);    
        console.log($scope.cloudData.schAddOptionQty)
        // focus cap
          angular.forEach(cloudData.voiceStd,function(item,index){
            console.log(item,index,$scope.cloudData.totalServiceAmount);
            if (cloudData.type.voiceCap){
              if (item==5){
                $scope.cloudData.totalServiceAmount+=495;
              } else if (item==6){
                $scope.cloudData.totalServiceAmount+=595;
              } else if (item==8){
                $scope.cloudData.totalServiceAmount+=792;
              } else if (item==10){
                $scope.cloudData.totalServiceAmount+=990;
              } else if (item==12){
                $scope.cloudData.totalServiceAmount+=1188;
              } else if (item==14){
                $scope.cloudData.totalServiceAmount+=1106;
              } else if (item==16){
                $scope.cloudData.totalServiceAmount+=1264;
              } else if (item==18){
                $scope.cloudData.totalServiceAmount+=1422;
              } else if (item==20){
                $scope.cloudData.totalServiceAmount+=1580;
              } else if (item==25){
                $scope.cloudData.totalServiceAmount+=1975;
              }
            }
          });

        angular.forEach(cloudData.voiceFaxQty,function(item,index){
        if (cloudData.type.voiceCap){
          $scope.cloudData.totalServiceAmount+=(item*9.95)
        }
        });

        angular.forEach(cloudData.dID,function(item,index){
        if (cloudData.type.voiceCap){
          $scope.cloudData.totalServiceAmount+=item
        }
        });
                    // discount
        angular.forEach(cloudData.bFCapDiscount,function(item,index){
          if(cloudData.type.voiceCap){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });
        
        // focus standard

        angular.forEach(cloudData.voiceStdChannel,function(item,index){
          if(cloudData.type.focusStandard){
            if (item==5){
              $scope.cloudData.totalServiceAmount+=199;
            } else if (item==6){
              $scope.cloudData.totalServiceAmount+=219;
            } else if (item==10){
              $scope.cloudData.totalServiceAmount+=249;
            } else if (item==20){
              $scope.cloudData.totalServiceAmount+=299;
            } else if (item==30){
              $scope.cloudData.totalServiceAmount+=349;
            }
          }
        });

        angular.forEach(cloudData.voiceStdFaxQty,function(item,index){
        if(cloudData.type.focusStandard){
          $scope.cloudData.totalServiceAmount+= (item*9.95)
        }
        });

        angular.forEach(cloudData.voiceStdDID,function(item,index){
        if(cloudData.type.focusStandard){
          $scope.cloudData.totalServiceAmount+=item
        }
        });
                    // discount
        // console.log(formData.bFStdDiscount)
        angular.forEach(cloudData.bFStdDiscount,function(item,index){
          if(cloudData.type.focusStandard){
            $scope.cloudData.totalServiceDiscount+=parseFloat(item);
          }
        });

        angular.forEach(cloudData.bFCallDiscount,function(item,index){
          if(cloudData.type.focusStandard){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });

        // complete office

        angular.forEach(cloudData.voiceCompAnalouge,function(item,index){
        if (cloudData.type.completeOffice){
          $scope.cloudData.totalServiceAmount+=(item*39.95)
        }  
        });

        angular.forEach(cloudData.voiceCompBri,function(item,index){
        if (cloudData.type.completeOffice){
          $scope.cloudData.totalServiceAmount+=(item*99.85)
        }  
        });

        // angular.forEach(formData.voiceCompPri,function(item,index){
        // if (formData.type.completeOffice){
        //   $scope.formData.totalServiceAmount+=item
        // }  
        // });

        angular.forEach(cloudData.voiceCompFaxQty,function(item,index){
        if (cloudData.type.completeOffice){
          $scope.cloudData.totalServiceAmount+=(item*9.95)
        }  
        });

        angular.forEach(cloudData.voiceCompDID,function(item,index){
          if(cloudData.type.completeOffice){
            if (item==100){ $scope.cloudData.totalServiceAmount+=49.95 }
          }
        });

                // discount

        angular.forEach(cloudData.voiceCompAnalougeDis,function(item,index){
          if(cloudData.type.completeOffice){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });

        angular.forEach(cloudData.voiceCompBriDis,function(item,index){
          if(cloudData.type.completeOffice){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });

        angular.forEach(cloudData.voiceCompPriDis,function(item,index){
          if(cloudData.type.completeOffice){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });

        angular.forEach(cloudData.voiceCompCallDis,function(item,index){
          if(cloudData.type.completeOffice){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });
        // solution cap
        angular.forEach(cloudData.voiceUntimedChannel,function(item,index){
          if(cloudData.type.voiceUt){
            if(item==3){
              $scope.cloudData.totalServiceAmount+=199;
            } else if(item==4){
              $scope.cloudData.totalServiceAmount+=259;
            } else if(item==5){
              $scope.cloudData.totalServiceAmount+=319;
            } else if (item==6){
              $scope.cloudData.totalServiceAmount+=379;
            } else if (item==7){
              $scope.cloudData.totalServiceAmount+=439;
            } else if (item==8){
              $scope.cloudData.totalServiceAmount+=499;
            } else if (item==9){
              $scope.cloudData.totalServiceAmount+=559;
            } else if (item==10){
              $scope.cloudData.totalServiceAmount+=619;
            } else if (item==11){
              $scope.cloudData.totalServiceAmount+=679;
            } else if (item==12){
              $scope.cloudData.totalServiceAmount+=739;
            } else if (item==13){
              $scope.cloudData.totalServiceAmount+=799;
            } else if (item==14){
              $scope.cloudData.totalServiceAmount+=859;
            } else if (item==15){
              $scope.cloudData.totalServiceAmount+=919;
            } else if (item==16){
              $scope.cloudData.totalServiceAmount+=979;
            } else if (item==17){
              $scope.cloudData.totalServiceAmount+=1039;
            } else if (item==18){
              $scope.cloudData.totalServiceAmount+=1099;
            } else if (item==19){
              $scope.cloudData.totalServiceAmount+=1159;
            } else if (item==20){
              $scope.cloudData.totalServiceAmount+=1219;
            } else if (item==21){
              $scope.cloudData.totalServiceAmount+=1279;
            } else if (item==22){
              $scope.cloudData.totalServiceAmount+=1339;
            } else if (item==23){
              $scope.cloudData.totalServiceAmount+=1399;
            } else if (item==24){
              $scope.cloudData.totalServiceAmount+=1459;
            } else if (item==25){
              $scope.cloudData.totalServiceAmount+=1519;
            } else if (item==26){
              $scope.cloudData.totalServiceAmount+=1579;
            } else if (item==27){
              $scope.cloudData.totalServiceAmount+=1639;
            } else if (item==28){
              $scope.cloudData.totalServiceAmount+=1699;
            } else if (item==29){
              $scope.cloudData.totalServiceAmount+=1759;
            } else if (item==30){
              $scope.cloudData.totalServiceAmount+=1819;
            }
          }
        });

        angular.forEach(cloudData.voiceUntimedFaxQty,function(item,index){
        if(cloudData.type.voiceUt){
          $scope.cloudData.totalServiceAmount+=(item*9.95)
        }
        });

        angular.forEach(cloudData.voiceUntimedDID,function(item,index){
        if(cloudData.type.voiceUt){
          if(item==10){
            $scope.cloudData.totalServiceAmount+=14.95
          } else if (item==50){
            $scope.cloudData.totalServiceAmount+=24.95
          } else if (item==100){
            $scope.cloudData.totalServiceAmount+=34.95
          }
        }
        });

                // discount 
        angular.forEach(cloudData.bFUntimedDiscount,function(item,index){
          if(cloudData.type.voiceUt){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });

        // Solution Standard
        angular.forEach(cloudData.voiceSolutionChannel,function(item,index){
          if(cloudData.type.voiceSolution){
            if(item==3){
               $scope.cloudData.totalServiceAmount+=149.00;
            } else if (item==4){
              $scope.cloudData.totalServiceAmount+=159.00;
            } else if (item==5){
              $scope.cloudData.totalServiceAmount+=169.00;
            } else if (item==6){
              $scope.cloudData.totalServiceAmount+=179.00;
            } else if (item==7){
              $scope.cloudData.totalServiceAmount+=189.00;
            } else if (item==8){
              $scope.cloudData.totalServiceAmount+=199.00;
            } else if (item==9){
              $scope.cloudData.totalServiceAmount+=209.00;
            } else if (item==10){
              $scope.cloudData.totalServiceAmount+=219.00;
            } else if (item==11){
              $scope.cloudData.totalServiceAmount+=229.00;
            } else if (item==12){
              $scope.cloudData.totalServiceAmount+=239.00;
            } else if (item==13){
              $scope.cloudData.totalServiceAmount+=249.00;
            } else if (item==14){
              $scope.cloudData.totalServiceAmount+=259.00;
            } else if (item==15){
              $scope.cloudData.totalServiceAmount+=269.00;
            } else if (item==16){
              $scope.cloudData.totalServiceAmount+=279.00;
            } else if (item==17){
              $scope.cloudData.totalServiceAmount+=289.00;
            } else if (item==18){
              $scope.cloudData.totalServiceAmount+=299.00;
            } else if (item==19){
              $scope.cloudData.totalServiceAmount+=309.00;
            } else if (item==20){
              $scope.cloudData.totalServiceAmount+=319.00;
            } else if (item==21){
              $scope.cloudData.totalServiceAmount+=329.00;
            } else if (item==22){
              $scope.cloudData.totalServiceAmount+=339.00;
            } else if (item==23){
              $scope.cloudData.totalServiceAmount+=349.00;
            } else if (item==24){
              $scope.cloudData.totalServiceAmount+=359.00;
            } else if (item==25){
              $scope.cloudData.totalServiceAmount+=369.00;
            } else if (item==26){
              $scope.cloudData.totalServiceAmount+=379.00;
            } else if (item==27){
              $scope.cloudData.totalServiceAmount+=389.00;
            } else if (item==28){
              $scope.cloudData.totalServiceAmount+=399.00;
            } else if (item==29){
              $scope.cloudData.totalServiceAmount+=409.00;
            } else if (item==30){
              $scope.cloudData.totalServiceAmount+=419.00;
            }
          }
        });

        angular.forEach(cloudData.voiceSolutionFaxQty,function(item,index){
        if(cloudData.type.voiceSolution){
          $scope.cloudData.totalServiceAmount+= (item*9.95)
        }
        });

        angular.forEach(cloudData.voiceSolutionDID,function(item,index){
          if(cloudData.type.voiceSolution){
            if(item==10){
              $scope.cloudData.totalServiceAmount+=14.95;
            } else if (item==50){
              $scope.cloudData.totalServiceAmount+=24.95;
            } else if (item==100){
              $scope.cloudData.totalServiceAmount+=34.95;
            }
          }
        });

                  // discount
        angular.forEach(cloudData.bFSolutionDiscount,function(item,index){
          if(cloudData.type.voiceSolution){
            $scope.cloudData.totalServiceDiscount+=parseFloat(item)
          }
        });

        angular.forEach(cloudData.bFSolutionCallDiscount,function(item,index){
          if(cloudData.type.voiceSolution){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });
        // Essential cap
        angular.forEach(cloudData.voiceEssentialChannel,function(item,index){
          if(cloudData.type.voiceEssential){
            if(item==4){
              $scope.cloudData.totalServiceAmount+=196.00;
            } else if(item==6){
              $scope.cloudData.totalServiceAmount+=294.00;
            } else if (item==8){
              $scope.cloudData.totalServiceAmount+=392.00;
            } else if (item==10){
              $scope.cloudData.totalServiceAmount+=490.00;
            } else if (item==12){
              $scope.cloudData.totalServiceAmount+=588.00;
            } else if (item==14){
              $scope.cloudData.totalServiceAmount+=686.00;
            } else if (item==16){
              $scope.cloudData.totalServiceAmount+=784.00;
            } else if (item==18){
              $scope.cloudData.totalServiceAmount+=882.00;
            } else if (item==20){
              $scope.cloudData.totalServiceAmount+=980.00;
            }
          }
        });

        angular.forEach(cloudData.voiceEssentialDID,function(item,index){
          if(cloudData.type.voiceEssential){
            if(item==10){
              $scope.cloudData.totalServiceAmount+=9.99;
            }else if (item==20){
              $scope.cloudData.totalServiceAmount+=15.99;
            } else if (item==50){
              $scope.cloudData.totalServiceAmount+=19.99;
            } else if (item==100){
              $scope.cloudData.totalServiceAmount+=25.99;
            }
          }
        });

        angular.forEach(cloudData.ipMidbandPlansVoice,function(item,index){
          if(cloudData.type.voiceEssential){
            $scope.cloudData.totalServiceAmount+=item.price
          }
        });

        angular.forEach(cloudData.ipMidbandDownloadVoice,function(item,index){
          if(cloudData.type.voiceEssential){
            $scope.cloudData.totalServiceAmount+=item.price
          }
        });

                // discount
        angular.forEach(cloudData.bECapDiscount,function(item,index){
          if(cloudData.type.voiceEssential){
            $scope.cloudData.totalServiceDiscount+=parseFloat(item)
          }
        });

        angular.forEach(cloudData.ipMidbandDis,function(item,index){
          if(cloudData.type.voiceEssential){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });

        // adsl 2+
        angular.forEach(cloudData.adsl2Plans,function(item,index){
          if(cloudData.type.adsl2){
             $scope.cloudData.totalServiceAmount+=item.price
          }
        });

        angular.forEach(cloudData.voiceCompAnalougeDSL,function(item,index){
          if(cloudData.type.adsl2){
            $scope.cloudData.totalServiceAmount+=(item*39.95)
          }
        });

                // discount
        angular.forEach(cloudData.adsl2Dis,function(item,index){
          if(cloudData.type.adsl2){
            $scope.cloudData.totalServiceDiscount+=parseFloat(item)
          }
        });

        angular.forEach(cloudData.voiceCompAnalougeDisDSL,function(item,index){
          if(cloudData.type.adsl2){
            $scope.cloudData.totalServiceDiscount+=parseFloat(item)
          }
        });

        // NBN unlimited
        angular.forEach(cloudData.UnlimitedPlans,function(item,index){
          if(cloudData.type.nbnUnlimitedPlans){
            $scope.cloudData.totalServiceAmount+=parseFloat(item.price)
          }
        });

        angular.forEach(cloudData.voiceCompAnalougeNBNUnli,function(item,index){
          if(cloudData.type.nbnUnlimitedPlans){
            $scope.cloudData.totalServiceAmount+=(item*39.95 )
          }
        });
                  // discount
        angular.forEach(cloudData.nbnUnlimitedDis,function(item,index){
          if(cloudData.type.nbnUnlimitedPlans){
            $scope.cloudData.totalServiceDiscount+=parseFloat(item)
          }
        });

        angular.forEach(cloudData.voiceCompAnalougeDisNBNUnli,function(item,index){
          if(cloudData.type.nbnUnlimitedPlans){
            $scope.cloudData.totalServiceDiscount+=parseFloat(item)
          }
        });

        // Midband Monthly
        angular.forEach(cloudData.ipMidbandPlans,function(item,index){
          if(cloudData.type.ipMidband){
            $scope.cloudData.totalServiceAmount+=item.price
          }
        });

        angular.forEach(cloudData.ipMidbandDownload,function(item,index){
          if(cloudData.type.ipMidband){
            $scope.cloudData.totalServiceAmount+=item.price
          }
        });

                  // discount
        angular.forEach(cloudData.ipMidbandDis,function(item,index){
          if(cloudData.type.ipMidband){
            $scope.cloudData.totalServiceDiscount+=parseFloat(item)
          }
        });

        // Midband Unlimited

        angular.forEach(cloudData.ipMidbandUnliPlans,function(item,index){
          if(cloudData.type.ipMidbandUnli){
            $scope.cloudData.totalServiceAmount+=item.price
          }
        });
                    // discount
        angular.forEach(cloudData.ipMidbandUnliDis,function(item,index){
          if(cloudData.type.ipMidbandUnli){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });

        //ethernet
        angular.forEach(cloudData.ethernetPlans,function(item,index){
          if(cloudData.type.ethernet){
            $scope.cloudData.totalServiceAmount+=item.price
          }
        });

        angular.forEach(cloudData.ethernetDis,function(item,index){
          if(cloudData.type.ethernet){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });

        // Fibre Unlimited
        angular.forEach(cloudData.fibreUtPlans,function(item,index){
          if(cloudData.type.fibre){
            $scope.cloudData.totalServiceAmount+=item.price
          }
        });
              // discount
        angular.forEach(cloudData.fibreDis,function(item,index){
          if(cloudData.type.fibre){
            $scope.cloudData.totalServiceDiscount+=item
          }
        });

        // telstra untimed item.boltOn.price
        angular.forEach(cloudData.configMobPort,function(item,index){
          if(cloudData.type.telstraUntimed){
            $scope.cloudData.totalServiceAmount+=item.confPatPlan.price
          }
        });

        angular.forEach(cloudData.configMobPort,function(item,index){
          if(cloudData.type.telstraUntimed){
            $scope.cloudData.totalServiceAmount+=item.boltOn.price
          }
        });

                // discount
        if(cloudData.type.telstraUntimed){
          $scope.cloudData.totalServiceDiscount+=parseFloat(cloudData.telstraUntimedDis);
        }

        // telstra Wireless
        angular.forEach(cloudData.telstraWirelessPlans,function(item,index){
          if(cloudData.type.telstraWireless){
            $scope.cloudData.totalServiceAmount+=item.price
          }
        });

        angular.forEach(cloudData.telstraWirelessDis,function(item,index){
          if(cloudData.type.telstraWireless){
            $scope.cloudData.totalServiceDiscount+=item;
          }
        });

        // Mobile Untimed
        angular.forEach(cloudData.configMobPort,function(item,index){
          if(cloudData.type.mobileUt){
            $scope.cloudData.totalServiceAmount+=item.confPatPlan.price
          }
        });

        if(cloudData.type.mobileUt){
          $scope.cloudData.totalServiceDiscount+=parseFloat(cloudData.mobileUtDis);
        }        

        // end asdf

        console.log("Total : " + $scope.cloudData.totalServiceAmount);
        $rootScope.draftMode = false;
        //$scope.sum = $scope.cloudData.dID-$scope.cloudData.bFCapDiscount;
        // Check if exists empty location fields.
        var comLocationObj = $('input.' + templateName);
        var emptyLocationCnt = 0;
        angular.forEach(comLocationObj, function(item, index) {
            var location = $(item).val();
            if (!location) {
                emptyLocationCnt ++;                
            }
        });
        
        if (emptyLocationCnt) {
            alert("Please ensure that all locations are filled-up properly.")
            return;
        }

        $(".reAssign").each(function() {
            var id = $(this).prop("id");
            if (id=='alarmPlans'){
                var alarmPlan = $scope.cloudData.alarmPlans;
                cloudData[id] = $(this).val();
                $scope.cloudData.alarmPlans = alarmPlan;
            } else {
                cloudData[id] = $(this).val();
            }
        });

        cloudData["type"] = {};

        $("input[name=formTypes]:checked").each(function() {
            var id = $(this).prop("id");
            cloudData["type"][id] = $(this).prop("checked");
        });

        if (cloudData.type.configApp) {
            var telNo = new Array();
            $(".telNo").each(function() {
                  var item = $(this).find("input");
                  var idCount = item.data('count');
                  var telRange = $('#telItemRange' + '-' + idCount).val()!=''?' to '+$('#telItemRange' + '-' + idCount).val():'';
                  var obj = {
                                  tel : item[0].value + telRange 
                            };
                  telNo.push(obj);
            });
            $scope.cloudData.configAppTelNo = telNo;
            // var mobPort = new Array();
            // $(".mob-port-box").each(function() {
            //     var confAccHolder = $(this).find(".confAccHolder");
            //     var confMobNo = $(this).find(".confMobNo");
            //     var confProvider = $(this).find(".confProvider");
            //     var confAccNo = $(this).find(".confAccNo");
            //     var confPatPlan = $(this).find(".confPatPlan");
            //     var confDob = $(this).find(".confDob");
            //     var confDLicence = $(this).find(".confDLicence");

            //     var obj = {
            //                     confAccHolder : confAccHolder[0].value,
            //                     confMobNo : confMobNo[0].value,
            //                     confProvider : confProvider[0].value,
            //                     confAccNo : confAccNo[0].value,
            //                     confPatPlan : confPatPlan[0].value,
            //                     confDob : confDob[0].value,
            //                     confDLicence : confDLicence[0].value,
            //               };
            //     mobPort.push(obj);
            // }); 
            $scope.cloudData.configMobPort = $scope.configMobPort;

            // if ((!$scope.formData.configCarrier) || (!$scope.formData.configAccno)) {
        if (!$scope.checkAccount()){
              alert("Please  assign a Carrier or Account Number to the Billing Form.");
              return;
            }
        }

        if (cloudData.type.monitoring_solution){
            if (($scope.cloudData.advancedBussinessProject) && (!$scope.cloudData.panelType)){
                alert("Please complete the details for Panel Type.");
                return;
            }
        }

        if(cloudData.type.rental || cloudData.type.leasing ||cloudData.singleOrder) {
            cloudData["rental"] = {};
            var rental = parseInt(cloudData.aPayment.replace(/[^\d.]/g,''));
            cloudData["aGST"] = isNaN(rental) ? '0.00' : (rental*10/100).toFixed(2);
            cloudData["aTotal"] = isNaN(rental) ? '0.00' : (rental + rental*10/100).toFixed(2);
        }

        var schedule = new Array();
        var scheduleOptions = new Array();
        angular.forEach($scope.cloudData.schItems, function(data) {
            if (angular.isDefined($scope.cloudData.schOption)){
                if (angular.isDefined($scope.cloudData.schOption[data.count])) {
                    var obj = {
                                qty : $scope.cloudData.schQty[data.count],
                                desc :$scope.cloudData.schOption[data.count].item != '_' ? $scope.cloudData.schOption[data.count].item : ''
                            };
                    scheduleOptions.push(obj);
                }
            }
        });

        $(".schedule-goods").each(function() {
            var item = $(this).find("input");
            var obj = { qty : item[0].value,
                        desc :item[1].value,
                        //itemNo : item[2].value,
                        //serNo : item[3].value
                    };
            schedule.push(obj);
        });

        $scope.cloudData.scheduleOptions = scheduleOptions;
        $scope.cloudData.schedule = schedule;

        if($scope.editId) {
            $rootScope.editId = $scope.editId;
        }
        //console.log(formData);
        $rootScope.cloudData = cloudData;
        ///$scope.formData.totalServiceAmount=totalServiceAmount;
        ngDialog.open({
            template: $scope.template,
            plan: true,
            controller : 'CloudFormCtrl',
            width: 900,
            height: 600,
            scope: $scope,
            className: 'ngdialog-theme-default',
            preCloseCallback : function(value) {
                var allSigned = true;

                $(".signatureImg").each(function() {
                    console.log($(this).attr('id'),$(this).children("img").length);
                    if ($(this).children("img").length == 0) {
                        allSigned = false;
                        return;
                    }
                });

                if (!allSigned) {
                  if (templateName!='newsummary'){
                   $('#isSigned').val('F');
                  }
                   if (confirm("You have not signed all the fields.Do you want to exit")) {
                       return true;
                   }
                   return false;
                    $timeout(angular.noop());
                } else {
                    $('#isSigned').val('T');
                    if (templateName=='newsummary'){
                      $('#form_complete').val('T');
                    }
                    // var button = $('#cloudData').find('a[href="#finish"]');
                    // button.show();
                    // alert($('#isSigned').val());                  // In case of forms with location fields.
                    if ($scope.locationAvailableTemps.indexOf(templateName) !== -1 && $scope.items[templateName].length > 1) {
                        $scope.htmlData[templateName] = [];
                        angular.forEach($scope.items[templateName], function(item, index) {
                            var clone = $('.ngdialog-content').clone();
                            clone.find('[class*="preview"]').each(function(divIndex,data){
                                //console.log(divIndex,data,index);  
                                if (index!=divIndex)  {
                                    clone.find('.preview-' + divIndex).remove();
                                    //console.log(clone.html());
                                }                        
                            });
                            //clone.find('.preview-' + index).remove();
                            var tableHtml = clone.html();
                            $scope.htmlData[templateName].push(tableHtml);
                        });
                    } else {
                        $scope.htmlData[templateName] = $(".ngdialog-content").html();
                    }

                    $scope[templateName] = true;
                    $timeout(angular.noop());
                }
            }
        });

        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            var newHash = 'anchor';
            if ($location.hash() !== newHash) {
                // set the $location.hash to `newHash` and
                // $anchorScroll will automatically scroll to it
                $location.hash('anchor');
            } else {
                // call $anchorScroll() explicitly,
                // since $location.hash hasn not changed
                $location.hash('anchor');
                $anchorScroll();
            }
        });
    }
    $scope.isDraftMode = function (){
        //alert($rootScope.draftMode);
        return $rootScope.draftMode;
    }
    $scope.isSigned = function(form, formName) {
        //alert(form,formName);
        if ($scope[form] && formName) {
            return true;
        } else {
            if (formName == false) {
                if ( angular.isDefined($scope.htmlData[form])) {
                    delete $scope.htmlData[form];
                }
                    $scope[form] = false;
                }

            return false;
        }
    }

    $scope.checkGroupSigned = function(forms, group) {
        var flag = false;
        if (angular.isDefined($scope.formGroups[group]) && angular.isDefined(forms)) {
            angular.forEach($scope.formGroups[group], function(item) {
                if (angular.isDefined(forms[item.id]) && forms[item.id] == true) {
                    if (forms[item.id] == true && !$scope[item.file]) {
                        flag = false;
                        return;
                    } else if (forms[item.id] == true && $scope[item.file]) {
                        flag = true;
                    }
                }
            });
        }
        return flag;
    }

    //console.log($scope.formData.schItems);

    $scope.addRentalGoods =  function(index) {
        console.log(index);
        $scope.schAddItemCount[index]++;
        $scope.cloudData.schAddItems[index].push({count:$scope.schAddItemCount[index]+1, status: 1});
       
    };

    $scope.removeRentalGood = function(count,index) {
        var spliceIndex = $scope.cloudData.schAddItems[index].indexOf({count:count});
        $scope.cloudData.schAddItems[index].splice(spliceIndex,1);
    };

    $scope.removePortingAuth = function(index) {
        $scope.configMobPort.splice(index,1);
    }

    //console.log($scope.cloudData.schItems);
    

    $scope.$watch("cloudData.schOption", function(){

    });

    // console.log($scope.schItemCount[0],$scope.formData.schItems[0]);

    $scope. addRentalGoodsOption =  function(index) {
        $scope.schItemCount[index]++;
        $scope.cloudData.schItems[index].push({count:$scope.schItemCount[index]+1});
        // console.log("Installation Location:",index,"Item Count",$scope.schItemCount[index],"Items",$scope.formData.schItems[index]);
    };

    $scope.removeRentalGoodsOption = function(count,index) {
        // $scope.schItemCount[index]--;
        var spliceIndex = $scope.cloudData.schItems[index].indexOf({count:count});
        $scope.cloudData.schItems[index].splice(spliceIndex,1);
        // console.log("Installation Location:",index,"Item Count:",$scope.schItemCount[index],"Items",$scope.formData.schItems[index]);
    };


    //

    
    // Add location function
    $scope.addCompanyLocation = function(form_type) {
        if ($scope.items[form_type].length!=0){
            $scope.locationCount = $scope.items[form_type].length-1;
        }
        $scope.locationCount++;
        if (form_type=='voice_solution_standard'){
            $scope.cloudData.voiceSolutionChannel[$scope.locationCount] = 3;
            $scope.cloudData.voiceSolutionDID[$scope.locationCount] = 10;
            $scope.cloudData.bFSolutionDiscount[$scope.locationCount] = 0;
            $scope.cloudData.bFSolutionCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_solution_untimed'){
            $scope.cloudData.voiceUntimedChannel[$scope.locationCount] = 199;
            $scope.cloudData.voiceUntimedDID[$scope.locationCount] = 14.95;
            $scope.cloudData.bFUntimedDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_comp'){
            $scope.cloudData.voiceCompAnalouge[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompBri[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompPri[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompDID[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompBriDis[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompPriDis[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompCallDis[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompAnalougeDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_std'){
            $scope.cloudData.voiceStdChannel[$scope.locationCount] = 199;
            $scope.cloudData.voiceStdDID[$scope.locationCount] = 9.99;
            $scope.cloudData.bFStdDiscount[$scope.locationCount] = 0;
            $scope.cloudData.bFCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_cap'){
            $scope.cloudData.voiceStd[$scope.locationCount] = 5;
            $scope.cloudData.dID[$scope.locationCount] = 10;
            $scope.cloudData.bFCapDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_essentials_cap'){
            $scope.cloudData.ipMidbandPlansVoice[$scope.locationCount] = $scope.ip_midband_plans[0];
            $scope.cloudData.ipMidbandDownloadVoice[$scope.locationCount] = $scope.download_plans[0];
            $scope.cloudData.voiceEssentialChannel[$scope.locationCount] = 196;
            $scope.cloudData.voiceEssentialDID[$scope.locationCount] = 9.99;
            $scope.cloudData.bECapDiscount[$scope.locationCount] = 0;
            $scope.cloudData.ipMidbandDis[$scope.locationCount] = 0;
            // $scope.cloudData.ipMidbandDownloadVoice[$scope.locationCount] = 0;
        } 
        if (form_type=='data_adsl'){
            // $scope.cloudData.adsl2Plans[$scope.locationCount] = 79;
            $scope.cloudData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
            $scope.cloudData.adsl2Dis[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompAnalougeDSL[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midband'){
            $scope.cloudData.ipMidbandDownload[$scope.locationCount] = $scope.download_plans[1];
            $scope.cloudData.ipMidbandPlans[$scope.locationCount] = $scope.ip_midband_plans[1];
            $scope.cloudData.ipMidbandDis[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midbandunli'){
            $scope.cloudData.ipMidbandUnliPlans[$scope.locationCount] = $scope.ip_midbandunli_plans[0];
            $scope.cloudData.ipMidbandUnliProf[$scope.locationCount] = $scope.professional_install[0];
            $scope.cloudData.ipMidbandUnliDis[$scope.locationCount] = 0;
        }
        if (form_type=='1300_discounted'){
            $scope.cloudData.rate131300Dis.qt1800[$scope.locationCount] = 0;
            $scope.cloudData.rate131300Dis.qt1300[$scope.locationCount] = 0;
            $scope.cloudData.rateDis131300.discount[$scope.locationCount] = 0;
        }
        if (form_type=='1300'){
            $scope.cloudData.rate131300.qt1800[$scope.locationCount] = 0;
            $scope.cloudData.rate131300.qt1300[$scope.locationCount] = 0;
            $scope.cloudData.rate131300.qt13[$scope.locationCount] = 0;
        }
        if (form_type=='mobile_wireless'){
            $scope.cloudData.mobileWirelessPlans[$scope.locationCount] = $scope.wireless_cap_plans[0];
            // $scope.cloudData.mobileWirelessPlans[$scope.locationCount] = 15;
            $scope.cloudData.mobileWirelessDis[$scope.locationCount] = 0;
        }        
        if (form_type=='fibre'){
            $scope.cloudData.fibreUtPlans[$scope.locationCount] = $scope.fibre_unli_plans[0];
            $scope.cloudData.fibreDis[$scope.locationCount] = 0;
        }
        // if (form_type=='adsl'){
        //     $scope.cloudData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
        // }
        if (form_type=='ethernet'){
            $scope.cloudData.ethernetPlans[$scope.locationCount] = $scope.ethernet_plans[0];
            $scope.cloudData.ethernetDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbnUnlimited'){
            $scope.cloudData.UnlimitedPlans[$scope.locationCount] = $scope.nbnUnlimited_plans[0];
            $scope.cloudData.nbnUnlimitedDis[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompAnalougeDisNBNUnli[$scope.locationCount] = 0;
            $scope.cloudData.voiceCompAnalougeNBNUnli[$scope.locationCount] =0;
        }
        if (form_type=='telstraUntimed'){
            $scope.cloudData.telstraUntimedPlans[$scope.locationCount] = $scope.telstraUntimed_plans[0];
        }
        if (form_type=='telstraWireless'){
            $scope.cloudData.telstraWirelessPlans[$scope.locationCount] = $scope.telstraWireless_plans[0];
            $scope.cloudData.telstraWirelessDis[$scope.locationCount] = 0;
            $scope.cloudData.comLocation['telstraWireless'][$scope.locationCount] = $scope.cloudData.comLocation['telstraWireless'][0];
            $scope.cloudData.comSuburb['telstraWireless'][$scope.locationCount] = $scope.cloudData.comSuburb['telstraWireless'][0];
            $scope.cloudData.comZipCode['telstraWireless'][$scope.locationCount] = $scope.cloudData.comZipCode['telstraWireless'][0];
        }
        if (form_type=='schedule_of_goods'){
            $scope.cloudData.schItems[$scope.locationCount] = [{count:1}];
            $scope.schItemCount[$scope.locationCount] = 1;
            $scope.cloudData.schAddItems[$scope.locationCount] = [{count:1}];
            $scope.schAddItemCount[$scope.locationCount] = 1;
            //console.log($scope.formData.schItems);
        }
        $scope.items[form_type].push($scope.locationCount);
    };

    $scope.removeOptionsBlock = function(form_type, index) {
        var comLocationObj = $scope.cloudData.comLocation[form_type];
        comLocationObj.splice(index, 1);
        $scope.items[form_type].splice(index, 1);
        if (form_type[form_type]=='schedule_of_goods'){
            $scope.cloudData.schItems.splice(index,1);
        }
    };

    $scope.cloudData.wPosition = "Sales";
    
    if ($rootScope.viewFormData)    {
        $scope.cloudData = $rootScope.viewFormData.data;
        console.log($scope.cloudData.printers);

            if(angular.isUndefined($scope.cloudData.comLocation['nbf_cap']) ||angular.isUndefined($scope.cloudData.comLocation['schedule_of_goods']) ||
                angular.isUndefined($scope.cloudData.comLocation['nbf_std']) ||angular.isUndefined($scope.cloudData.comLocation['voice_solution_untimed']) ||
                angular.isUndefined($scope.cloudData.comLocation['voice_solution_standard']) ||angular.isUndefined($scope.cloudData.comLocation['voice_essentials_cap']) ||
                angular.isUndefined($scope.cloudData.comLocation['voice_comp']) ||angular.isUndefined($scope.cloudData.comLocation['1300']) ||
                angular.isUndefined($scope.cloudData.comLocation['nbnUnlimited']) ||angular.isUndefined($scope.cloudData.comLocation['ethernet']) ||
                angular.isUndefined($scope.cloudData.comLocation['fibre']) ||angular.isUndefined($scope.cloudData.comLocation['telstraWireless']) ||
                angular.isUndefined($scope.cloudData.comLocation['ip_midbandunli']) ||angular.isUndefined($scope.cloudData.comLocation['ip_midband']) ||
                angular.isUndefined($scope.cloudData.comLocation['mobile_wireless']) || angular.isUndefined($scope.cloudData.comLocation['data_adsl'])) 
            {
                alert("null");
            } else{
                for (var index=0;index<$scope.cloudData.comLocation['schedule_of_goods'].length;index++){
                    $scope.locationCount = $scope.items['schedule_of_goods'].length-1;
                    $scope.items['schedule_of_goods'][index] = index;

                    angular.forEach($scope.cloudData.schItems[index],function(data,ind){
                        // console.log("value",data,"index",ind);
                        $scope.schItemCount[index] = data.count-1;
                    });
                    // console.log("value1",$scope.formData.schAddItems[index]);
                    angular.forEach($scope.cloudData.schAddItems[index],function(add,indx){
                        // console.log("value1",add,"index1",indx);
                        $scope.schAddItemCount[index] = add.count-1;       
                    });
                }
                for (var index=0;index<$scope.cloudData.comLocation['nbf_cap'].length;index++){
                    $scope.locationCount = $scope.items['nbf_cap'].length-1;
                    $scope.items['nbf_cap'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['nbf_std'].length;index++){
                    $scope.locationCount = $scope.items['nbf_std'].length-1;
                    $scope.items['nbf_std'][index] = index;
                    } 
                for (var index=0;index<$scope.cloudData.comLocation['voice_solution_untimed'].length;index++){
                    $scope.locationCount = $scope.items['voice_solution_untimed'].length-1;
                    $scope.items['voice_solution_untimed'][index] = index;
                    } 
                for (var index=0;index<$scope.cloudData.comLocation['voice_solution_standard'].length;index++){
                    $scope.locationCount = $scope.items['voice_solution_standard'].length-1;
                    $scope.items['voice_solution_standard'][index] = index;
                    } 
                for (var index=0;index<$scope.cloudData.comLocation['voice_essentials_cap'].length;index++){
                    $scope.locationCount = $scope.items['voice_essentials_cap'].length-1;
                    $scope.items['voice_essentials_cap'][index] = index;
                    } 
                for (var index=0;index<$scope.cloudData.comLocation['voice_comp'].length;index++){
                    $scope.locationCount = $scope.items['voice_comp'].length-1;
                    $scope.items['voice_comp'][index] = index;
                    } 
                for (var index=0;index<$scope.cloudData.comLocation['1300'].length;index++){
                    $scope.locationCount = $scope.items['1300'].length-1;
                    $scope.items['1300'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['1300_discounted'].length;index++){
                    $scope.locationCount = $scope.items['1300_discounted'].length-1;
                    $scope.items['1300_discounted'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['data_adsl'].length;index++){
                    $scope.locationCount = $scope.items['data_adsl'].length-1;
                    $scope.items['data_adsl'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['nbnUnlimited'].length;index++){
                    $scope.locationCount = $scope.items['nbnUnlimited'].length-1;
                    $scope.items['nbnUnlimited'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['ethernet'].length;index++){
                    $scope.locationCount = $scope.items['ethernet'].length-1;
                    $scope.items['ethernet'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['fibre'].length;index++){
                    $scope.locationCount = $scope.items['fibre'].length-1;
                    $scope.items['fibre'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['telstraWireless'].length;index++){
                    $scope.locationCount = $scope.items['telstraWireless'].length-1;
                    $scope.items['telstraWireless'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['ip_midbandunli'].length;index++){
                    $scope.locationCount = $scope.items['ip_midbandunli'].length-1;    
                    $scope.items['ip_midbandunli'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['ip_midband'].length;index++){
                    $scope.locationCount = $scope.items['ip_midband'].length-1;
                    $scope.items['ip_midband'][index] = index;
                    }
                for (var index=0;index<$scope.cloudData.comLocation['mobile_wireless'].length;index++){
                    $scope.locationCount = $scope.items['mobile_wireless'].length-1;
                    $scope.items['mobile_wireless'][index] = index;
                    }
            }

        $scope.editId = $rootScope.viewFormData.id;
         if (angular.isDefined($scope.cloudData.schedule)) {
            angular.forEach($scope.cloudData.schedule, function(item, count) {
                if (count == 0) {
                  $(".qunatity").val(item.qty);
                  $(".desc").val(item.desc);
                } else {
                  var wrapped = '<div class="row">\n\
                                            <div class="col-md-2 col-sm-2">\n\
                                                <button class="btn btn-primary" id="schedule-'+count+'">\n\
                                                   <i class="fa fa-times"></i> \n\
                                                </button></div>\n\
                                            <div class="col-md-10 schedule-goods">\n\
                                                <div class="col-md-3 col-sm-3">\n\
                                                    <div class="form-group">\n\
                                                        <div class="col-xs-4">\n\
                                                            <label class="control-label">Quantity</label>\n\
                                                        </div>\n\
                                                        <div class ="col-xs-8">\n\
                                                            <input type="number" value='+item.qty+' class="qunatity form-control">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="col-md-9 col-sm-9">\n\
                                                    <div class="form-group">\n\
                                                        <div class="col-xs-3 text-right">\n\
                                                                <label class="control-label">Description</label>\n\
                                                        </div>\n\
                                                        <div class ="col-xs-9">\n\
                                                            <input type="text"  value='+item.desc+' class ="desc form-control">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                </div>';
                    $(".rentalGoods").append(wrapped);
                    $("#schedule-"+count).click(function() {
                        $(this).parent().parent().remove();
                    });
                    $scope.scheduleCount = count;
                }
            });
        }
        if (angular.isDefined($scope.cloudData.cState)) {
            $scope.cloudData.cState = $scope.cloudData.cState;
        }
        if (angular.isDefined($scope.cloudData.type)) {
            if ($scope.cloudData.type.configApp) {
                if (angular.isDefined($scope.cloudData.configAppTelNo)) {
                    angular.forEach($scope.cloudData.configAppTelNo, function(item,count) {
                      $scope.addTel();
                      $timeout(function() {
                          $("#telItemConfigApp-"+count).val(item.tel);
                      });
                    });
                }

                if (angular.isDefined($scope.cloudData.configMobPort)) {
                    $timeout(function() {
                        $scope.configMobPort = $scope.cloudData.configMobPort;
                    });
                }
                if ($scope.cloudData.config) {
                   if ($scope.cloudData.config.pstn) {
                       $scope.cloudData.config.pstn = true;
                   } else {
                        $scope.cloudData.config.pstn = false;
                   }
                   if ($scope.cloudData.config.lineHunt) {
                       $scope.cloudData.config.lineHunt = true;
                   }
                   if ($scope.cloudData.config.ISDN) {
                       $scope.cloudData.config.ISDN = true;
                   } else {
                        $scope.cloudData.config.pstn = false;
                   }
                   if ($scope.cloudData.config.indial) {
                       $scope.cloudData.config.indial = true;
                   }
                   if ($scope.cloudData.config.port ) {
                       $scope.cloudData.config.port = true;
                   }
                }
            }


        }

        $rootScope.viewFormData = null;
    }

    $("input[name=formTypes]").on("click", function() {
       if ($(this).prop("checked") === true) {
          var className = $(this).prop("class");
          var id = $(this).prop("id");
          if (className) {
              $("."+className).each(function() {
                 $(this).prop("checked",false);
              });
              $("#"+id).prop("checked", true);
          }
       }
    });

    $("#sameAddress").on("click", function() {
        var checked = $("#sameAddress").is(":checked");
        if (checked) {
            $("#pIAddress").val( $("#cAddress").val());
            $("#pIState").val( $("#cState").val());
            $("#pIPostcode").val( $("#cPostCode").val());
            $("#pISuburb").val( $("#cSuburb").val());
        } else {
            $("#pIAddress").val("");
            $("#pIState").val("");
            $("#pIPostcode").val("");
            $("#pISuburb").val("");
        }
        $scope.cloudData.pIAddress = $('#pIAddress').val();
        $scope.cloudData.pIState = $('#pIState').val();
        $scope.cloudData.pIPostcode = $('#pIPostcode').val();
        $scope.cloudData.pISuburb = $('#pISuburb').val();
        $scope.updateComLocations();
        $scope.updateComSuburbs();
        $scope.updateComPostCode();
    });

    $scope.load = function() {
        $scope.downloadPdf();
    }

    $scope.transformTelFields();
    /*var saveDraft =  $interval(function() {
       $scope.saveAsDraft($scope.formData);
    },300000);*/


    NgMap.getMap().then(function(evtMap) {
        //console.log(evtMap);
        $scope.vm.map = evtMap;
        var ll = evtMap.markers[0].position;
        GeoCoder.geocode({'latLng': ll}).then(function(result){
            //console.log(result);
            $scope.deleteMarkers();
            $scope.vm.positions.push({lat:ll.lat(),lng:ll.lng()});
            $scope.locationReference = result[0];
            $scope.cloudData.cAddress = result[0];
        });
    });

    $scope.getCurrentLocation = function(){
        var position;
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(
                function(position){
                    position = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                }
            );
        } else {
            position = {lat:-33.867926,lng:151.205392};
        }
        if (angular.isUndefined(position)){
            position = {lat:-33.867926,lng:151.205392};   
        }
        //console.log(position);
        return position;
    }

    $scope.vm.positions = [{
                            lat: $scope.getCurrentLocation().lat,
                            lng: $scope.getCurrentLocation().lng
                        }];

    $scope.loadAddress = function(){
        var address = $('#locationReference').val();
        GeoCoder.geocode({'address': address}).then(function(result){
            $scope.locationReference = result[0];
            $('#locationReference').val($scope.locationReference.formatted_address);
        });
    }

    $scope.cloudAddress = function(){
        var address = $('#cAddress').val();
        GeoCoder.geocode({'address': address}).then(function(result){
            $scope.cloudData.cAddress = result[0];
            $('#cAddress').val($scope.cloudData.cAddress.formatted_address);
        });
    }

    $scope.deleteMarkers = function(){
        $scope.vm.positions = [];
    }

    $scope.placeChanged = function(){
        $scope.vm.place = this.getPlace();
        var location = $scope.vm.place.geometry.location();
        $scope.deleteMarkers();
        $scope.vm.positions.push({lat : location.lat(),lng: location.lng()});
    }

    $scope.addMarker = function(event){
        //nsole.log(event);
        var ll = event.latLng;
        GeoCoder.geocode({'latLng': ll}).then(function(result){
            //console.log(result);
            $scope.deleteMarkers();
            $scope.vm.positions.push({lat:ll.lat(),lng:ll.lng()});
            $scope.locationReference = result[0];
            $('#locationReference').val($scope.locationReference.formatted_address);
        });
    }

    $scope.cloudaddMarker = function(event){
        //nsole.log(event);
        var ll = event.latLng;
        GeoCoder.geocode({'latLng': ll}).then(function(result){
            //console.log(result);
            $scope.deleteMarkers();
            $scope.vm.positions.push({lat:ll.lat(),lng:ll.lng()});
            $scope.cloudData.cAddress = result[0];
            $('#cAddress').val($scope.cloudData.cAddress.formatted_address);
        });
    }

    $scope.showMarkers = function(){
        for(var key in $scope.vm.markers){
            vm.map.markers[key].setMap($scope.vm.map);
        }
    }

    $scope.searchAddress = function(){
        // $scope.clearMarkers();
        var location = $scope.locationReference.geometry.location;
        $scope.deleteMarkers();
        //console.log(location);
        $scope.vm.positions.push({lat : location.lat(),lng: location.lng()});
    }

    $scope.cloudsearchAddress = function(){
        // $scope.clearMarkers();
        var location = $scope.cloudData.cAddress.geometry.location;
        $scope.deleteMarkers();
        //console.log(location);
        $scope.vm.positions.push({lat : location.lat(),lng: location.lng()});
    }


})


Qms.controller("CloudFormCtrl", function($scope, $filter, $timeout, $state, $rootScope, Cloud, Scopes, $locale,$q) {
    var user = Scopes.get('DefaultCtrl').profile; 
    // $scope.rates = [];
    if ($scope.isDraftMode()){
        $scope.userSign = false;
    } else {
        $scope.userSign = $rootScope.sign;
    }

    $scope.roundVal = function(val) {
        return val.toFixed(2);
    }

    /**
     * Returns the timestamp of the signature if exists.
     */
    $scope.getSignedTime = function(formName,userSign) {
        //console.log('getSigned');
        var time = '';
        if(!angular.isDefined($scope.cloudData.signedTimestamps)) {
            return 'Sign Here';
        }
        if( Object.prototype.toString.call( $scope.cloudData.signedTimestamps ) === '[object Array]' ) {
            $scope.cloudData.signedTimestamps.forEach(function(e) {
                if(e.form_name==formName) {
                    time = e.timestamp;
                }
            });
        } else {
            if($scope.cloudData.signedTimestamps.form_name==formName) {
                time = $scope.cloudData.signedTimestamps.timestamp;
            }
        }
        var currentDate = Date.parse(time);
        var formatted = $filter('date')(time, 'HH:mm:ss dd-MM-yyyy');
        return formatted;
    }


    $scope.getCurrentDate = function(){
        var currentDate = new Date();   
        return (currentDate.getHours() < 10 ? "0" : "") + currentDate.getHours() + ":" + (currentDate.getMinutes() < 10 ? "0" : "") + currentDate.getMinutes() + ":" + (currentDate.getSeconds() < 10 ? "0" : "") + currentDate.getSeconds()+" "+currentDate.getDate()+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getFullYear();
    }
    
    $scope.checkRange = function(val,lower_limit,upper_limit){
        //alert(val + '-' + lower_limit + '-' + upper_limit);
        if (upper_limit==0){
            if (val>=lower_limit){
                return true;
            } else {
                return false;
            }
        } else {
            if (val >= lower_limit && val <= upper_limit){
                return true;
            } else {
                return false;
            }
        }
    }

    $scope.setCheckboxValue = function(model){
        //console.log(model,$scope.formData.config.pstn);
        if ((model=='pstn') && (!$scope.cloudData.config.pstn)){
            $scope.cloudData.config.pstn_qty = '0';
        }
        if ((model=='isdn') && (!$scope.cloudData.config.ISDN)){
            $scope.cloudData.config.isdn_qty = '0';
        }
    }
 

    
    $scope.financedAmount = function(rental_per_month, term) {
        var rental_per_month  = Number(rental_per_month.replace(/^\D+|\,+/g, ''));
        var term  = Number(term.replace(/^\D+|\,+/g, ''));


        var subrate = 0;
        var rates = [];
        var found = 0;
        $rootScope.rates[0].rate
        var financeAmt = 0;        
        $.each ($rootScope.rates,function(index,value){
            subrate = (rental_per_month/value.rate)*1000; 
            //rates.push(subrate);
            if($scope.checkRange(subrate,value.lowerLimit,value.upperLimit)){
              financeAmt = subrate;
            }
            console.log
        });

          return $scope.roundVal(financeAmt).toLocaleString();
        }

    $scope.leasedAmount = function(lease_per_month, term) {
        var lease_per_month  = Number(lease_per_month.replace(/^\D+|\,+/g, ''));
        var term  = Number(term.replace(/^\D+|\,+/g, ''));

        var subrate = 0;
        var rates = [];
        if($rootScope.rates) {
            $rootScope.rates[0].rate;
        } else {
            $rootScope.rates = 0;
        }
        var found = 0;
        var leaseAmt = 0;
        $.each ($rootScope.rates,function(index,value){
            subrate = (lease_per_month/value.rate)*1000; 
            //rates.push(subrate);
            if($scope.checkRange(subrate,value.lowerLimit,value.upperLimit)){
              leaseAmt = subrate;
            }
            console.log
        });

       
        return $scope.roundVal(leaseAmt).toLocaleString();
    }

    $scope.rentofferedAmount = function(rentoffer_per_month, term) {
        var rentoffer_per_month  = Number(rentoffer_per_month.replace(/^\D+|\,+/g, ''));
        var term  = Number(term.replace(/^\D+|\,+/g, ''));
        var rentoffer_amt = rentoffer_per_month * term;

        var constVal = 0;
        var bracket = 0;

        if (rentoffer_amt >=2000 && rentoffer_amt <= 4999.99) {
            bracket = 1;
        } else if (rentoffer_amt >=5000 && rentoffer_amt <= 9999.99) {
            bracket = 2;
        } else if (rentoffer_amt >=10000 && rentoffer_amt <= 19999.99) {
            bracket = 3;
        } else if (rentoffer_amt >=20000 && rentoffer_amt <= 99999.99) {
            bracket = 4;
        // } else if (rentoffer_amt >=50000 && rentoffer_amt <= 99999) {
        //     bracket = 5;
        }

        if (bracket == 1) {
            if (term == 48) {
                constVal = 31.68;
            } else if (term == 60) {
                 constVal = 26.20;
            }

        } else if (bracket == 2) {
            if (term == 48) {
                constVal = 31.05;
            } else if (term == 60) {
                 constVal = 25.35;
            }

        } else if(bracket == 3) {
            if (term == 48) {
                constVal = 30.45;
            } else if (term == 60) {
                 constVal = 25.09;
            }

        } else if(bracket == 4)  {
            if (term == 48) {
                constVal = 30.31;
            } else if (term == 60) {
                 constVal = 24.82;
            }
        }

       //console.log(constVal);
       rentofferAmt = $scope.roundVal(rentoffer_per_month * 1000/constVal).toLocaleString();
       return rentofferAmt;
    }


    if (!angular.isDefined($scope.cloudData.aTermNew) ) {
        $scope.cloudData.aTermNew = 60;
    }


    if(angular.isDefined($scope.cloudData.type.voiceUt) && $scope.cloudData.type.voiceUt) {
        var suToE = 9.95;
        var suMob = 7.95;
        var rateStdUt = {
          3 : 199,
          4 : 259,
          5 : 319,
          6 : 379,
          7 : 439,
          8 : 499,
          9 : 559,
          10 : 619,
          11 : 679,
          12 : 739,
          13 : 799,
          14 : 859,
          15 : 919,
          16 : 979,
          17 : 1039,
          18 : 1099,
          19 : 1159,
          20 : 1219,
          21 : 1279,
          22 : 1339,
          23 : 1399,
          24 : 1459,
          25 : 1519,
          26 : 1579,
          27 : 1639,
          28 : 1699,
          29 : 1759,
          30 : 1819,
        };

        var sUDID = {
            10 : 14.95,
            50 : 24.95,
            100: 34.95
        }

        var monthlyTotalRate = [];
        angular.forEach($scope.items['voice_solution_untimed'], function(item, index) {
            if (!$scope.cloudData.voiceUntimedFaxToEmail[index] || angular.isUndefined($scope.cloudData.voiceUntimedFaxQty[index])) {
                $scope.cloudData.voiceUntimedFaxQty[index] = 0;
            }
            
            if (!$scope.cloudData.voiceUntimedMobility[index] || angular.isUndefined($scope.cloudData.voiceUntimedMobQty[index])) {
                $scope.cloudData.voiceUntimedMobQty[index] = 0;
            }

            var total = suToE * $scope.cloudData.voiceUntimedFaxQty[index] + suMob * $scope.cloudData.voiceUntimedMobQty[index] + sUDID[$scope.cloudData.voiceUntimedDID[index]] + rateStdUt[$scope.cloudData.voiceUntimedChannel[index]];
            
            monthlyTotalRate.push(total);
        });

        $scope.voiceUt = {
            template : "templates/cloud/voice_solution_untimed.html?t=" + _version,
            rate : rateStdUt,
            sfToE : suToE,
            sMob : suMob,
            sDID : sUDID,
            totalRate : monthlyTotalRate
        };
    }

    if($scope.cloudData.type.voiceSolution) {

        var ssToE = 9.95;
        var ssMob = 7.95;
        var rateStdSolution = {
            3 : 149,
            4 : 159,
            5 : 169,
            6 : 179,
            7 : 189,
            8 : 199,
            9 : 209,
            10 : 219,
            11 : 229,
            12 : 239,
            13 : 249,
            14 : 259,
            15 : 269,
            16 : 279,
            17 : 289,
            18 : 299,
            19 : 309,
            20 : 319,
            21 : 329,
            22 : 339,
            23 : 349,
            24 : 359,
            25 : 369,
            26 : 370,
            27 : 389,
            28 : 399,
            29 : 409,
            30 : 419,
        };

        var sSolutionDID = {
            10 : 14.95,
            50 : 24.95,
            100: 34.95
        };
        
        var monthlyTotalRate = [];
        angular.forEach($scope.items['voice_solution_standard'], function(item, index) {
            if (!$scope.cloudData.voiceSolutionFaxToEmail[index] || angular.isUndefined($scope.cloudData.voiceSolutionFaxQty[index])) {
                $scope.cloudData.voiceSolutionFaxQty[index] = 0;
            }
            
            if (!$scope.cloudData.voiceSolutionMobility[index] || angular.isUndefined($scope.cloudData.voiceSolutionMobQty[index])) {
                $scope.cloudData.voiceSolutionMobQty[index] = 0;
            }

            var total = ssToE * $scope.cloudData.voiceSolutionFaxQty[index] + ssMob * $scope.cloudData.voiceSolutionMobQty[index] + sSolutionDID[$scope.cloudData.voiceSolutionDID[index]] + rateStdSolution[$scope.cloudData.voiceSolutionChannel[index]];
            
            monthlyTotalRate.push(total);
        });

        $scope.voiceSolution = {
            template : "templates/cloud/voice_solution_standard.html?t=" + _version,
            rate : rateStdSolution,
            sfToE : ssToE,
            sMob : ssMob,
            sDID : sSolutionDID,
            totalRate : monthlyTotalRate
        };
    }

    if($scope.cloudData.type.voiceCap) {
        var fToE = 9.95;
        var mob = 7.95;
        var rate = {
            3 : 50,
            5 : 150,
            6 : 170,
            8 : 210,
            10: 300,
            12: 350,
            14: 400,
            16: 450,
            18: 500,
            20: 600,
            25: 800
        };
        var cap = {
            3 : 99,
            5 : 99,
            6 : 99,
            8 : 99,
            10: 99,
            12: 99,
            14: 79,
            16: 79,
            18: 79,
            20: 79,
            25: 79

        };
        var dID = {
            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100: 25.99
        }
        
        var monthlyTotalRate = [];

        angular.forEach($scope.items['nbf_cap'], function(item, index) {
            if (!$scope.cloudData.voiceFaxToEmail[index] || angular.isUndefined($scope.cloudData.voiceFaxQty[index])) {
                $scope.cloudData.voiceFaxQty[index] = 0;
            }
            
            if (!$scope.cloudData.voiceMobility[index] || angular.isUndefined($scope.cloudData.voiceMobQty[index])) {
                $scope.cloudData.voiceMobQty[index] = 0;
            }

            var total = (cap[$scope.cloudData.voiceStd[index]] * $scope.cloudData.voiceStd[index]) +
                        fToE * $scope.cloudData.voiceFaxQty[index] + mob * $scope.cloudData.voiceMobQty[index]+
                        dID[$scope.cloudData.dID[index]];
            
            monthlyTotalRate.push(total);
        });

        $scope.voiceCap = {
            template : "templates/cloud/nbf_cap.html?t=" + _version,
            rate : rate,
            fToE : fToE,
            mob : mob,
            dID : dID,
            cap : cap,
            total: monthlyTotalRate
        };
    }

    if ($scope.cloudData.type.copierAgreement){
        $scope.copierAgreement = {
            template : "templates/cloud/copierAgreement.html?t=" + _version
        }
    }

    if ($scope.cloudData.type.singleOrder) {
          $scope.cloudData.aTermNew = $scope.cloudData.aTermNew.toString();
          //console.log($scope.formData.aTermNew);
          if ($scope.cloudData.aTermNew!='Outright'){
            var financeAmount = $scope.financedAmount($scope.cloudData.aPayment, $scope.cloudData.aTermNew);
            //alert(financeAmount);
            var leaseAmount = $scope.leasedAmount($scope.cloudData.aPayment, $scope.cloudData.aTermNew);
          }
          var rentofferAmount = $scope.rentofferedAmount($scope.cloudData.aPayment, $scope.cloudData.aTermNew);

        // $scope.singleOrder = {
        //     template : "templates/cloud/single_order_spec.html",
        //     financeAmount : !isNaN(financeAmount) ? financeAmount : 0,
        //     leaseAmount : !isNaN(leaseAmount) ? leaseAmount : 0,
        //     rentofferAmount : !isNaN(rentofferAmount) ? rentofferAmount : 0
        // }

        $scope.singleOrder = {
            template : "templates/cloud/single_order_spec.html?t=" + _version,
            financeAmount : financeAmount,
            leaseAmount : leaseAmount,
            rentofferAmount : rentofferAmount
        }

    }


    if($scope.cloudData.type.focusStandard) {
        var sfToE = 9.95;
        var sMob = 7.95;
        var rateStd = {
            5 : 199,
            6 : 219,
            10: 249,
            20: 299,
            30: 349
        };
        var sDID = {

            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100: 25.99
        }

        var monthlyTotalRate = [];
        angular.forEach($scope.items['nbf_std'], function(item, index) {
            if (!$scope.cloudData.voiceStdFaxToEmail[index] || angular.isUndefined($scope.cloudData.voiceStdFaxQty[index])) {
                $scope.cloudData.voiceStdFaxQty[index] = 0;
            }

            if (!$scope.cloudData.voiceStdMobility[index] || angular.isUndefined($scope.cloudData.voiceStdMobQty[index])) {
                $scope.cloudData.voiceStdMobQty[index] = 0;
            }

            var total = sfToE * $scope.cloudData.voiceStdFaxQty[index] + sMob * $scope.cloudData.voiceStdMobQty[index] + rateStd[$scope.cloudData.voiceStdChannel[index]] + sDID[$scope.cloudData.voiceStdDID[index]];

            monthlyTotalRate.push(total);
        });

        $scope.voiceStd = {
            template : "templates/cloud/nbf_std.html?t=" + _version,
            rate : rateStd,
            sfToE : sfToE,
            sMob : sMob,
            sDID : sDID,
            totalRate : monthlyTotalRate
        };
    }

    if ($scope.cloudData.type.voiceEssential) {
        var rateEssential = {
            4 : 196,
            6 : 294,
            8 : 392,
            10 : 490,
            12 : 588
        };

        var eDID = {
            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100 : 25.99
        };

        var monthlyTotalRate = [];
        var totalDiscount = []
        //ip Midband Plans
        var ipMidbandMonthlyRate = [];
        var ipMidbandPlans = [];
        var ipMidbandDownloads = [];
        //console.log($scope.formData.ipMidbandPlans);
        //console.log($scope.formData.ipMidbandDownload);
        angular.forEach($scope.items['voice_essentials_cap'], function(item, index) {
            var total = rateEssential[$scope.cloudData.voiceEssentialChannel[index]] + eDID[$scope.cloudData.voiceEssentialDID[index]];
            monthlyTotalRate.push(total);
            var totalIpFee = $scope.cloudData.ipMidbandPlansVoice[index].price+$scope.cloudData.ipMidbandDownloadVoice[index].price;
            var discount = parseFloat($scope.cloudData.ipMidbandDis[index]) + parseFloat($scope.cloudData.bECapDiscount[index]);
            ipMidbandPlans.push($scope.cloudData.ipMidbandPlansVoice[index]);
            ipMidbandDownloads.push($scope.cloudData.ipMidbandDownloadVoice[index]);
            ipMidbandMonthlyRate.push(totalIpFee);
            totalDiscount.push(discount);
        });

        $scope.voiceEssential = {
            template: "templates/cloud/voice_essentials_cap.html?t=" + _version,
            rate : rateEssential,
            eDID : eDID,
            totalRate : monthlyTotalRate,
            plan : ipMidbandPlans,
            download : ipMidbandDownloads,
            ipTotalRate : ipMidbandMonthlyRate,
            totalDiscount : totalDiscount
        }
    }

    if($scope.cloudData.type.completeOffice) {
        var cfToE = 9.95;
        var cMob = 7.95;

        var priRates = {
            0 : 0,
            10: 299.85,
            20:499.85,
            30:699.85
        };

        var cDID = {
            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100: 49.95
        };

        var monthlyTotalRate = [];
        var monthlyAnalogue = [];
        var monthlyBri = [];
        var monthlyPri = [];
        angular.forEach($scope.items['voice_comp'], function(item, index) {
            var total = 0;

            if (!$scope.cloudData.voiceCompFaxToEmail[index] || angular.isUndefined($scope.cloudData.voiceCompFaxQty[index])) {
                $scope.cloudData.voiceCompFaxQty[index] = 0;
            }
            
            if (!$scope.cloudData.voiceCompMobility[index] || angular.isUndefined($scope.cloudData.voiceCompMobQty[index])) {
                $scope.cloudData.voiceCompMobQty[index] = 0;
            }
            monthlyAnalogue.push(39.95 * $scope.cloudData.voiceCompAnalouge[index]);
            monthlyBri.push(99.85 * $scope.cloudData.voiceCompBri[index]);
            monthlyPri.push(priRates[$scope.cloudData.voiceCompPri[index]]);
            total = (39.95 * $scope.cloudData.voiceCompAnalouge[index]) + (99.85 * $scope.cloudData.voiceCompBri[index]) + priRates[$scope.cloudData.voiceCompPri[index]] + (cfToE * $scope.cloudData.voiceCompFaxQty[index]) + (cMob * $scope.cloudData.voiceCompMobQty[index]);

            if ($scope.cloudData.voiceCompDID[index]) {
                total += cDID[$scope.cloudData.voiceCompDID[index]];
            }

            // Calcuate the total plan fee based on discount amount.
            if (angular.isDefined($scope.cloudData.voiceCompAnalougeDis[index])) {
                total -= $scope.cloudData.voiceCompAnalougeDis[index];
            }

            if (angular.isDefined($scope.cloudData.voiceCompBriDis[index])) {
                total -= $scope.cloudData.voiceCompBriDis[index];
            }

            if (angular.isDefined($scope.cloudData.voiceCompPriDis[index])) {
                total -= $scope.cloudData.voiceCompPriDis[index];
            }

            monthlyTotalRate.push(total);
        });

        $scope.voiceComp = {
            template : "templates/cloud/voice_comp.html?t=" + _version,
            cfToE : cfToE,
            cMob : cMob,
            cDID : cDID,
            total: monthlyTotalRate,
            analogueFee : monthlyAnalogue,
            briFee : monthlyBri,
            priFee : monthlyPri
        };
    }

    if ($scope.cloudData.type.adsl2) {
        var monthlyTotalRate = [];
        var analoguePrice = [];
        var discount = [];
        
        angular.forEach($scope.items['data_adsl'], function(item, index) {
            var total = $scope.cloudData.adsl2Plans[index].price;
            var analogueAmt = $scope.cloudData.voiceCompAnalougeDSL[index]*39.95;
            if(angular.isUndefined($scope.cloudData.voiceCompAnalougeDisDSL)){
                $scope.cloudData.voiceCompAnalougeDisDSL = [];
                $scope.cloudData.voiceCompAnalougeDisDSL[index] = 0;
            }
            var planDis = $scope.cloudData.voiceCompAnalougeDisDSL[index] + $scope.cloudData.adsl2Dis[index];
            var totalRate = (total+analogueAmt);
            monthlyTotalRate.push(totalRate);
            analoguePrice.push(analogueAmt);
            discount.push(planDis);
        });

        $scope.dataAdsl = {
            template : "templates/cloud/data_adsl.html?t=" + _version,
            totalFee : monthlyTotalRate,
            analoguePrice : analoguePrice,
            discount : discount,
            rate : rate
        };
    }

    if ($scope.cloudData.type.telstraUntimed) {
        var monthlyTotalRate = 0;
        //console.log($scope.formData.telstraUntimedDis);
        var discount = isNaN($scope.cloudData.telstraUntimedDis) || $scope.cloudData.telstraUntimedDis == null?0:parseFloat($scope.cloudData.telstraUntimedDis);
        var ratePerMob = 0;
        var dataBoltOnRate = 0;
        angular.forEach($scope.telstraPlans,function(item,index){
            ratePerMob = $scope.telstraUntimed_plans[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        angular.forEach($scope.boltOnPlans,function(item,index){
            dataBoltOnRate = $scope.data_bolt_on_plans[index].price * item;
            monthlyTotalRate += dataBoltOnRate;
        });

        $scope.telstraUntimed = {
            template : "templates/cloud/telstraUntimed.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };
    }

    if ($scope.cloudData.type.telstraWireless){
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['telstraWireless'],function(item,index){
            var total = $scope.cloudData.telstraWirelessPlans[index].price;
            var planDis = $scope.cloudData.telstraWirelessDis[index];
            monthlyTotalRate.push(total);
            discount.push(planDis);
        });

        $scope.telstraWireless = {
            template : "template/cloud/telstraWireless.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };
    }

    if ($scope.cloudData.type.ethernet){
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['ethernet'],function(item,index){
            monthlyTotalRate.push($scope.cloudData.ethernetPlans[index].price);
            if(angular.isDefined($scope.cloudData.ethernetDis[index])){
                discount.push($scope.cloudData.ethernetDis[index]);
            }
            else{
                $scope.cloudData.ethernetDis[index] = 0;
                discount.push($scope.cloudData.ethernetDis[index]);
            }
        });

        $scope.ethernet = {
            template : "templates/cloud/ethernet.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        }
    }

    if ($scope.cloudData.type.ipMidband) {
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['ip_midband'], function(item, index) {
            var totalIpFee = $scope.cloudData.ipMidbandPlans[index].price+$scope.cloudData.ipMidbandDownload[index].price;
            monthlyTotalRate.push(totalIpFee);
            discount.push($scope.cloudData.ipMidbandDis[index]);
        });

        $scope.ipMidband = {
            template : "templates/cloud/ip_midband.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        }
    }

    if ($scope.cloudData.type.fibre){
        var monthlyTotalRate = [];
        var discount = [];

        angular.forEach($scope.items['fibre'],function(item,index){
            monthlyTotalRate.push($scope.cloudData.fibreUtPlans[index].price);
            discount.push ($scope.cloudData.fibreDis[index]);
        });

        $scope.fibre = {
            template : "templates/cloud/fibre.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        }
    }

    if ($scope.cloudData.type.ipMidbandUnli){
        var monthlyTotalRate = [];
        var discount = [];
        // var plans = [];
        // var prof_install = [];
        angular.forEach($scope.items['ip_midbandunli'],function(item,index){
            //var totalIpFee = $scope.formData.ipMidbandUnliPlans[index].price + $scope.formData.ipMidbandUnliProf[index].price;
            // plans.push($scope.formData.ipMidbandUnliPlans[index]);
            // prof_install.push($scope.formData.ipMidbandUnliProf[index]);
            monthlyTotalRate.push($scope.cloudData.ipMidbandUnliPlans[index].price);
            discount.push($scope.cloudData.ipMidbandUnliDis[index])
        });

        $scope.ipMidbandUnli = {
            template : "templates/cloud/ip_midbandunli.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
            // plan : plans,
            // prof_install : prof_install
        }
    }

    if ($scope.cloudData.type.nbnMonthly) {
        var nbnPlan = {
            255 : {rate : 99, desc : "NBN 25Mbps/5Mbps*"},
            2510 : {rate : 109, desc : "NBN 25Mbps/10Mbps*"},
            5020 : {rate : 119, desc : "Up to 50/20Mbps**"},
            10040 : {rate : 129, desc : "NBN 100Mbps/40Mbps*"}
        };
        var nbnDownload = {
            100 :{plan : "100GB", rate : 39},
            150 :{plan : "150GB", rate : 49},
            200 : {plan : "200GB" , rate :59},
            300 :{plan : "300GB", rate : 69},
            500 : {plan : "500GB",rate : 79},
            1000 : {plan : "1000GB", rate : 99}
        };

        var plans = [];
        var downloads = [];
        var monthlyTotalRate = [];
        var totalDiscount = [];
        var analoguePrice = [];
        angular.forEach($scope.items['nbn'], function(item, index) {
            var totalNbnFee = nbnPlan[$scope.cloudData.nbnPlans[index]].rate + nbnDownload[$scope.cloudData.nbnDownload[index]].rate;
            var price = $scope.cloudData.voiceCompAnalougeNBNMonthly[index] * 39.95;
            monthlyTotalRate.push(totalNbnFee + price);
            plans.push(nbnPlan[$scope.cloudData.nbnPlans[index]]);
            downloads.push(nbnDownload[$scope.cloudData.nbnDownload[index]]);
            totalDiscount.push($scope.cloudData.voiceCompAnalougeDisNBNMonthly[index] + $scope.cloudData.nbnDis[index]);
            analoguePrice.push(price);
        });

        $scope.nbn = {
            template : "templates/cloud/nbn.html?t=" + _version,
            totalFee : monthlyTotalRate,
            plan : plans,
            download : downloads,
            analoguePrice : analoguePrice,
            discount : totalDiscount
        }
    }

    if ($scope.cloudData.type.nbnUnlimitedPlans) {
        var monthlyTotalRate = [];
        var plans = [];
        var prices = [];
        var analoguePrice = [];
        var discount = [];
        angular.forEach($scope.items['nbnUnlimited'], function(item, index) {
            var totalNbnUnlimitedFee = $scope.cloudData.UnlimitedPlans[index].price;
            var price = $scope.cloudData.voiceCompAnalougeNBNUnli[index] * 39.95;
            monthlyTotalRate.push(totalNbnUnlimitedFee + price);
            discount.push($scope.cloudData.voiceCompAnalougeDisNBNUnli[index] + $scope.cloudData.nbnUnlimitedDis[index]);
            analoguePrice.push(price);
        });

        $scope.nbnUnlimitedPlans = {
            template : "templates/cloud/nbnUnlimited.html?t=" + _version,
            totalFee : monthlyTotalRate,
            analoguePrice : analoguePrice,
            discount : discount
        }
    }

    $scope.addBlankSpace = function(count) {
        var spacer = ""
        for (var i = 0; i<count; i++) {
            spacer += '\xa0\xa0';
        }
        return spacer;
    }

    $scope.processFields = function(content, maxLimit) {
        var length = content.length;
        var space = $scope.addBlankSpace(maxLimit-(length*2));
        return content+space;
    }

    if (($scope.cloudData.type.monitoring_solution) || ($scope.cloudData.type.alarm_monitor)){
        
    }

    if ($scope.cloudData.type.configApp) {
        var rowWidth = 3;
        $scope.telRow = new Array();
        var tempRow = new Array();
        var tdCount = 0;

        //Put tel no 3 per row
        angular.forEach($scope.cloudData.configAppTelNo, function(data) {
            //alert('Billing cloudData');
            //console.log(data);
            if (tdCount < rowWidth) {
                tempRow.push(data.tel);
                tdCount++;
            }
            if (tdCount == rowWidth) {
                $scope.telRow.push(tempRow);
                tdCount = 0;
                tempRow = new Array();
            }
        });
        if (tdCount > 0 ) {
            $scope.telRow.push(tempRow);
        }

        if ($scope.cloudData.additionalServices){
            if (!$scope.cloudData.config.pstn){
                $scope.cloudData.config.pstn_qty = 0;
            }

            if (!$scope.cloudData.config.ISDN){
                $scope.cloudData.config.Isdn_qty = 0;
            }
        }
        
        $scope.configApp = {
            template : "templates/cloud/billing_app.html?t=" + _version,
        }
    }

    if ($scope.cloudData.type.rental) {
        $scope.rental = {
           template : "templates/cloud/rental.html?t=" + _version,
        };
    }
    if ($scope.cloudData.type.leasing) {
        $scope.leasing = {
           template : "templates/cloud/leasing.html?t=" + _version,
        };
    }

    if ($scope.cloudData.type.chattle) {
        $scope.chattle = {
            template : "templates/cloud/chattle.html?t=" + _version,
        }
    }
    
    if ($scope.cloudData.type.mobileCap) {
        var monthlyTotalRate = 0;
        var discount = isNaN($scope.cloudData.mobileCapDis) || $scope.cloudData.mobileCapDis == null?0:parseFloat($scope.cloudData.mobileCapDis);
        //console.log(discount);
        var ratePerMob = 0;

        angular.forEach($scope.mobileMegaPlans,function(item,index){
            ratePerMob = $scope.mobile_mega_plans[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        $scope.mobileCap = {
            template : "templates/cloud/mobile_mega.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };

        // var mobCPlan = {
        //     39 : {desc:" $250 inc calls 500MB"},
        //     49 : {desc:" $2500 inc calls 2000MB"},
        //     59 : {desc:" $2500 inc calls 3000MB"}
        // }
        // $scope.mobileCap = {
        //    template : "templates/cloud/mobile_mega.html",
        //    plan : mobCPlan
        // };
    }

    if ($scope.cloudData.type.mobileUt) {
        var monthlyTotalRate = 0;
        var discount = isNaN($scope.cloudData.mobileUtDis) || $scope.cloudData.mobileUtDis == null?0:parseFloat($scope.cloudData.mobileUtDis);
        //console.log(discount);
        var ratePerMob = 0;

        angular.forEach($scope.mobile4GUntimedPlans,function(item,index){
            ratePerMob = $scope.mobile_4G_Untimed_Calls[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        $scope.mobileUt = {
            template : "templates/cloud/mobile_mega.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };

        // $scope.mobileUt = {
        //    template : "templates/cloud/mobile_ut.html",
        // };
    }

    if ($scope.cloudData.type.mobileWireless) {
        $scope.mobileWireless = {
           template : "templates/cloud/mobile_wireless.html?t=" + _version,
        };
    }

    if ($scope.cloudData.type.rate131300) {
        $scope.rate131300 = {
            template : "templates/cloud/1300.html?t=" + _version,
        }
    }

    if ($scope.cloudData.type.rateDis131300) {
        $scope.rateDis131300 = {
            template : "templates/cloud/1300_discounted.html?t=" + _version,
        }
    }

    $scope.oneAtATime = true;

    $scope.sign = function(id) {
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({color:"#000000",lineWidth:1,
                                    width :490, height :98,
                                    cssclass : "signature-canvas",
                                   "decor-color": 'transparent'
                                  });
            clicked = true;
        }
    }

    $scope.currentSign = 0;
    $scope.signNavigate = function(direction) {
        var signItems = angular.element(".signItem");
        var elementId = "";
        if (direction =="next") {
            if ($scope.currentSign == 0) {
               currentItem = signItems[$scope.currentSign];
               $scope.currentSign +=1;
            } else if ($scope.currentSign < signItems.length -1) {
                $scope.currentSign +=1;
                currentItem = signItems[$scope.currentSign];
            } else {
                currentItem = signItems[$scope.currentSign];
            }

        } else if (direction == "previous") {
            if ($scope.currentSign == 0) {
               currentItem = signItems[$scope.currentSign];
            } else if ($scope.currentSign > 0) {
                $scope.currentSign -=1;
                currentItem = signItems[$scope.currentSign];
            }

        }

        elementId = currentItem.getAttribute("id");

        var top = $("#"+elementId).position().top;
        $(window).scrollTop(top-300);
        $(".signItem").removeClass("selected-to-sign");
        $("#"+elementId).addClass("selected-to-sign");
       // $(document).scrollTo("#"+elementId, 500 , {offset: -$(window).height()/2});
    }

    $scope.downloadPdf = function() {
        angular.forEach($rootScope.cloudData.type, function(value,key) {
             var nametail = new Date().getTime();
             var rand = Math.floor((Math.random()*1000)+10);
             var fileName = key+rand+nametail+".pdf";
             $("#loading").show();
             $("#downloadPdfBn").prop('disabled',true);
             if (value == true) {
                var html =  $("#"+key).html();
                var pdf = Cloud.update({html:html, fileName : fileName}, function(data) {
                   // window.open("assets/files/"+fileName,'_blank');
                        $("#downloadPdfBn").prop('disabled',false);
                        $("#loadingStatus").html("Loading "+key);
                    });
             }

        });
        if($rootScope.editId) {
            var editId = $rootScope.editId;
            $rootScope.editId = null;
        }
        Cloud.save({data:$scope.cloudData, user : user.id, editId : editId});
        $state.go('cloud');
        $("#loading").hide();
    }

});

 Qms.controller('ViewCloudCtrl', function($scope, $state,$filter, Scopes,Form, Cloud, $rootScope, FlashMessage, $http, ngDialog,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder){
    $scope.vmf = {};
            $scope.vmf.dtInstance = {};   
            $scope.vmf.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [3, 'desc'])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.vmr = {};
            $scope.vmr.dtInstance = {};   
            //$scope.vmr.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()]; 
            $scope.vmr.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [[6, 'desc']])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.pages = 0;
    $scope.itemsPerPage = 10;
    $scope.count = 0;
    $scope.status = "";

    $scope.sortType = 'creator_name';
    $scope.reverse = false;

    $http.get("profile").success(function(response) {
        $scope.user = response;
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;   
        Cloud.show({user:$scope.user.id}, function(data) {
            $scope.loading = false;
            $scope.clouds = data.cloudData;
            $scope.userStatus = 'all';
            if (data.userStatus == 'user') {
                $scope.userStatus = 5;
            }
        });
    });

    $scope.filterForms = function(status) {
       $scope.status = status;
    } 

    $scope.edit = function(id) {
      var x=window.confirm("Do you want to Modify this?")
        if (x)
        var data = Cloud.show({id : id}, function(data) {
            angular.forEach(data.cloudData,function(name,index){
                //console.log(name.comLocation['schedule_of_goods'].length);
                if (index==data){
                    //alert(index);
                    angular.forEach(name,function(val,index){
                        if (index=='voiceEssentialChannel'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(name)){
                                var key = val;
                                delete val;
                                name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
                            }
                        }
                        if (index=='voiceEssentialDID'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(val)){
                                var key = val;
                                delete val;
                                name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
                            }
                        }
                    });
                } 
            })
            $rootScope.viewFormData = data.cloudData;
           // console.log($rootScope.viewFormData);
            $state.go('cloud');
        });
    }

    $scope.downloadForms = function(id) {
        var forms = Cloud.downloadForms({id:id}, function(data) {
           if (data.success) {
               angular.forEach(data.files, function(name, index){
                    window.open("assets/files/"+name,'_blank');
               });

           }
        });
    }

    $scope.closedialog = function(){
        $scope.closeThisDialog();
    }

    $scope.delete = function(id) {
        $('<div></div>').appendTo('body')
        .html('<div><h6>Are you really sure to delete this item?</h6></div>')
        .dialog({
            modal: true,
            title: 'You want to delete this item?',
            zIndex: 10000,
            autoOpen: true,
            width: '315px',
            resizable: false,
            buttons: {
                Yes: function () {
                    var data = Cloud.delete({id : id}, function(data) {
                        if (data.success) {
                            FlashMessage.setMessage(data);
                            Cloud.show({user:$scope.user.id}, function(data) {
                                $scope.forms = data.cloudData;
                                $scope.userStatus = 'all';
                                if (data.userStatus == 'user') {
                                    $scope.userStatus = 5;
                                }
                            });
                        }
                    });
                    $(this).dialog("close");
                },
                No: function () {
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });

    }

});

Qms.controller('DashboardCtrl',function($scope,$stateParams,$rootScope,$interval,$window,$anchorScroll,$location,$timeout,$http,Scopes,Form,Rates,FlashMessage,ngDialog,ScheduleGoods,FileUploader,ProposalActions,Dashboard,$state ) {


    $scope.dashboardData = {};
    $scope.dashboardData.userlist = "";
    $scope.dashboardData.chart_data = "";
    $scope.dashboardData.schAddUsers = [];

    //Parameter
    var pageURL = window.location.href;
    var lastURLSegment = pageURL.substr(pageURL.lastIndexOf('/') + 1);
    var arr = lastURLSegment;
    var param = arr.split(',');

    // auto check the checkbox
    param.forEach(function(item){
        setTimeout(function(){
        document.getElementById("userDropdownList-"+item).checked = true;
        }, 2000);
    });
   
    
     $scope.viewuserchart = function(id) {
        $scope.dashboardData.yr  = document.getElementById("year-dropdown").value;

        $scope.dashboardData.params = $('input:checkbox:checked').map(function() {
            return this.value;
        }).get();
        window.location = '#/dashboard/chart/'+$scope.dashboardData.yr +'/'+$scope.dashboardData.params;
    }

    $( "#year-dropdown" ).change(function() {
        $scope.dashboardData.yr  = document.getElementById("year-dropdown").value;
        $scope.dashboardData.params = $('input:checkbox:checked').map(function() {
            return this.value;
        }).get();
        window.location = '#/dashboard/chart/'+$scope.dashboardData.yr+'/'+$scope.dashboardData.params;
    });
    
    $scope.dashboardData.secondlastURLSegment = pageURL.split('/').reverse()[1];
    console.log($scope.dashboardData.secondlastURLSegment);

    if($scope.dashboardData.secondlastURLSegment != 'chart' ) {
        if ($scope.dashboardData.secondlastURLSegment != '#' ) {
            document.getElementById("opt-"+$scope.dashboardData.secondlastURLSegment).selected = "true";
        }
    }
    
    $scope.dashboardData.yr  = document.getElementById("year-dropdown").value;
    console.log($scope.dashboardData.yr);
    
    var chartProposalData = Dashboard.chartProposalData({
        user_id: param,
        year:  $scope.dashboardData.yr,
    }, function(data) {
        $scope.dashboardData.draftProposals = data.draftProposals;
        $scope.dashboardData.completedProposals = data.completedProposals;
        $scope.dashboardData.draftForms = data.draftForms;
        $scope.dashboardData.completedForms = data.completedForms;
        $scope.dashboardData.totalSales = '$'+ ' ' + data.totalSales;
    });

    var chartFormData = Dashboard.chartFormData({
        user_id: param,
        year: $scope.dashboardData.secondlastURLSegment,
    }, function(data) {
        $scope.dashboardData.chart = data.chart;
    var chart = 300000;
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Month on Month - SIGNED Year '+ $scope.dashboardData.yr
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com'
        // },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: [$scope.dashboardData.yr]
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: $scope.dashboardData.chart
    });
});

  var createPerMonthSales  = Dashboard.createPerMonthSales({
    }, function(data) {
    });

    // Display User list in dropdown
    var userList  = Dashboard.userList({
    }, function(data) {
        $scope.dashboardData.users = data.results;
    });

     // Get selected user
     var checkedValues = $('input:checkbox:checked').map(function() {
        return this.value;
    }).get();
  
    //Toggle dropdown user list
    $scope.show = function(id) {
        $(".btn-group").toggleClass("display");
    }

    $scope.userlist = function(id) {
        // Get selected user
        $scope.dashboardData.params = $('input:checkbox:checked').map(function() {
            return this.value;
        }).get();
        console.log($scope.dashboardData.params);
    }
});

Qms.controller('JobsheetsCtrl',function(Upload,$filter,$scope,$stateParams,$rootScope,$interval,$window,$anchorScroll,$location,$timeout,$http,Scopes,Form,Rates,FlashMessage,ngDialog,ScheduleGoods,FileUploader,ProposalActions,$state,Jobsheets ) {
//
$scope.htmlData = {};
$scope.schItemCount = 0;  
$scope.schAddItemCount = 0;
$scope.items = [];
// $scope.optionType ='2';
$scope.jobsheetData = {};
$scope.jobsheetData.search = [];
$scope.jobsheetData.workStatus = 'view';
$scope.htmlData = {};
$scope.jobsheetData.signedTimestamps = $window.signedTimeStamps;
$scope.jobsheetData.nightVmail = false;
$scope.jobsheetData.schAddItems = [];
$scope.jobsheetData.schAddItems[0] = "";
$scope.jobsheetData.schAddItems[1] = "";
$scope.jobsheetData.cctvDisplay = "";
$scope.jobsheetData.systemBrand = "";
$scope.jobsheetData.phoneLocalIp = "";

$scope.uploadFiles = function(files, errFiles) {
    $scope.orderNumberDate = function(){
        var currentDate = new Date();   
        return ("0" + (currentDate.getMonth() + 1)).slice(-2)+""+("0" + currentDate.getDate()).slice(-2);
    }
    $scope.orderNumberComp = function(){
      var name = $scope.jobsheetData.companyName.substring(0,3).toUpperCase();
      return name;
    }
    var rand = Math.floor(Math.random() * 99) ;
    var gen =  $scope.orderNumberDate() + $scope.orderNumberComp() + rand + "-";

    $scope.files = files;
    $scope.errFiles = errFiles;
    angular.forEach(files, function(file) {

        
        extension = file.name.split('.').pop();
        var origname = file.name.split('.').slice(0, -1).join('.')

        var filename = origname +  gen + "." + extension;

        Object.defineProperty(file, 'name', {
            writable: true,
            value: filename
          });

        file.upload = Upload.upload({
            url: '/jobsheets/uploads',
            data: {
                file: file,
                js_id: $state.params.id,
            }
        });
        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;

                setTimeout(function(){ 
                    var searchResults  = Jobsheets.getAttached({
                        js_id: $state.params.id,
                    }, function(data) {
                    $scope.jobsheetData.files = data.allAtached;
                });
                }, 2000);
            });
        }, function (response) {
            if (response.status > 0) 
                $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
            file.progress = Math.min(100, parseInt(100.0 * 
                                     evt.loaded / evt.total));
        });
    });
}

   
    
  
    console.log($scope.jobsheetData.schAddItems);

    var absUrl = $location.path();
 

    if(absUrl == '/search-customer') {
        $('#searchModal').modal('show');
    }

    $scope.edit = function(id) {
      if(id) {
        $('.sheet-data').prop('disabled', false);
        var userInfo =  Scopes.get('DefaultCtrl').profile;
        $scope.userSign = userInfo.sign;
        $scope.jobsheetData.technicianName = userInfo.name;
        $('.onoffswitch.tick').removeClass('disabled');
        $('.btn-edit').addClass('bg-green');
        $scope.jobsheetData.workStatus = 'editing';
      } else {
            $('#nojsModal').modal('show');
      }
    }

    $scope.cctvDisplay = function() {
        if (document.getElementById('cctvDisplay').checked == true) {
            $scope.jobsheetData.cctvDisplay = true;
        } else {
            $scope.jobsheetData.cctvDisplay = false;
        }
        console.log($scope.jobsheetData.cctvDisplay);

    }


    $scope.add = function(action, id) {
        var selectedType = document.querySelector('input[name="type"]:checked').value;
        $scope.orderNumberDate = function(){
            var currentDate = new Date();   
            return ("0" + (currentDate.getMonth() + 1)).slice(-2)+""+("0" + currentDate.getDate()).slice(-2);
        }
        $scope.orderNumberComp = function(){
          var name = $scope.jobsheetData.companyName.substring(0,3).toUpperCase();
          return name;
        }
        var rand = Math.floor(Math.random() * 99);
        $scope.js_num = "JS-"+ $scope.orderNumberDate() + $scope.orderNumberComp() + rand;
        $('.search-results').removeClass('d-none');
        var addJobSheet  = Jobsheets.add({
            action: action,
            id: id,
            js_num: $scope.js_num,
            type: selectedType,
            status: "draft"
       }, function(data) {
           console.log(data);
        $scope.jobsheetData.message = "Job sheet successfully added!";
        if (data.success == "success") {
            $('.alert-success').removeClass('fade');
            $('.alert-success').addClass('show');
        }
        setTimeout(function(){ 
            $(".alert-success").addClass('fade');
        }, 4500);
        $('#addModal').modal('hide');
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '0');
        $('.modal-backdrop').remove();
       
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        
        if(selectedType == "1") {
            setTimeout(function(){ 
                $state.go('form_install', {
                    id: data.editId,
                });
            }, 2000);
        } else {
            setTimeout(function(){ 
                $state.go('form_service', {
                    id: data.editId,
                });
            }, 2000);
        }
       });
    }

    $scope.addOtherEquip =  function(index) {
        $('.btn-add').prop('disabled', false);
        $scope.schItemCount++;
        $scope.jobsheetData.schAddItems.push({status:1});
        $scope.jobsheetData.otherEqpt.push();
    };

    $scope.oneAtATime = true;
     $scope.sign = function(id) {
        $("#testDi").dialog("open");
        $('#iAccept').attr('data-id', id);
        if (!$('#'+id).find('canvas').length) {
             $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
             $('#signatureBox').jSignature({color:"#000000",lineWidth:1,
                        width :490, height :98,
                        cssclass : "signature-canvas",
                    "decor-color": 'transparent'
                    });
             clicked = true;
         }
     }

    $scope.search = function(companyName) {
        console.log("pasok");
        $('.search-results').removeClass('d-none');
        var searchResults  = Jobsheets.searchCustomer({
            companyName: companyName,
       }, function(data) {
            $scope.jobsheetData.search.results = data.results;
            $scope.jobsheetData.search.counts = data.counts;
            $scope.jobsheetData.search.jobsheet_list = data.jobsheet_list;
            $scope.jobsheetData.search.message = data.message;
       });
    }

    $scope.view = function(id, type) {
        $state.params = id;
        $('#searchModal').modal('hide');
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '0');
        $('.modal-backdrop').remove();
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        if(type == "1") {
            setTimeout(function(){ 
                $state.go('form_install', {
                    id: id,
                });
            }, 2000);
        } else {
            setTimeout(function(){ 
                $state.go('form_service', {
                    id: id,
                });
            }, 2000);
        }
    }

    $scope.change = function(field, value) {
        console.log(field);
        var name = "input[name='"+field+"']";
        $(name).css('border', '1px solid white');
        $(name).css('background', 'none');

        console.log(value);
        $('.btn-edit').addClass('bg-green');
        if(field == 'companyName') {
            $('#notallowedModal').modal('show');
        }
        if(field == 'phoneRemoteAccess' && value == true) {
            console.log("passook");

            $('.contactTrigger').removeClass('hide');
        } else {
            $('.contactTrigger').addClass('hide');
        }
    }

    $scope.getCurrentDate = function(){
        var currentDate = new Date();   
        return (currentDate.getHours() < 10 ? "0" : "") + currentDate.getHours() + ":" + (currentDate.getMinutes() < 10 ? "0" : "") + currentDate.getMinutes() + ":" + (currentDate.getSeconds() < 10 ? "0" : "") + currentDate.getSeconds()+" "+currentDate.getDate()+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getFullYear();
    }
    $scope.getSignedTime = function(formName,userSign) {
        var time = '';
        if(!angular.isDefined($scope.jobsheetData.signedTimestamps)) {
            return 'Sign Here';
        }
        if( Object.prototype.toString.call( $scope.jobsheetData.signedTimestamps ) === '[object Array]' ) {
            $scope.jobsheetData.signedTimestamps.forEach(function(e) {
                if(e.form_name==formName) {
                    time = e.timestamp;
                }
            });
        } else {
            if($scope.jobsheetData.signedTimestamps.form_name==formName) {
                time = $scope.jobsheetData.signedTimestamps.timestamp;
            }
        }
        var currentDate = Date.parse(time);
        var formatted = $filter('date')(time, 'HH:mm:ss dd-MM-yyyy');
        return formatted;
    }

    $scope.isSigned = function(form) {
        if ($scope[form]) {
            return true;
        } else {
            if (form == false) {
                if ( angular.isDefined($scope.htmlData[form])) {
                    delete $scope.htmlData[form];
                }
                    $scope[form] = false;
                }
            return false;
        }
    }

 
    $scope.signDocument = function(templateName, formData) {

        $scope.fieldStatus = 'complete';
        console.log($scope.jobsheetData.cctvDisplay);

        angular.forEach( $scope.jobsheetData, function(value, key) {
            if($scope.type == 1) {
                if (key == 'userSign' || key == 'otherEqpt' || key == 'footerCount' || key == 'quot_num' || key == 'tradingName' || key == 'nightVmail' || key == 'signedTimestamps' || key == 'search' || key == 'files' || key == 'autoAttendant' || key == 'faxTested' || key == 'pbx' || key == 'licensesApp' || key == 'databaseSent' || key == 'photosSent' || key == 'adslTested' || key == 'routerRemoteAccess' || key == 'clientTrained' || key == 'dayVmail' || key == 'eftposTested' || key == 'alarmTested' || key == 'comments' || key == 'cctvDisplay' || key == 'remoteContactNo' || key == 'phoneRemoteAccess') {
                } else {
                    if(value == undefined || value == "") {
                        console.log(key);
                        var name = "input[name='"+key+"']";
                        $(name).css('border', '1px solid #fbc2c4');
                        $(name).css('background', 'rgb(251, 227, 228)');
                        $('.alert-danger').removeClass('fade');
                        $('.alert-danger').addClass('show');
                        setTimeout(function(){ 
                            $(".alert-danger").addClass('fade');
                        }, 4500);
                            $scope.fieldStatus = 'required';
                            $scope.loading = false;
                    }
                }
            } else {
               if(key == 'otherEqpt' || key == 'noOfHandsets' || key == 'typeOfHandset' || key == 'noOfCordless' || key == 'typeOfCordless' || key == 'exitNo1' || key == 'exitName1' || key == 'exitNo2' || key == 'exitName2' || key == 'exitNo3' || key == 'exitName3' || key == 'exitNo4' || key == 'exitName4' || key == 'exitNo5' || key == 'exitName5' || key == 'exitNo6' || key == 'exitName6' || key == 'exitNo7' || key == 'exitName7' || key == 'exitNo8' || key == 'exitName8' || key == 'exitNo9' || key == 'exitName9' || key == 'exitNo10' || key == 'exitName10' || key == 'exitNo11' || key == 'exitName11' || key == 'exitNo12' || key == 'exitName12' || key == 'exitNo13' || key == 'exitName13' || key == 'exitNo14' || key == 'exitName14' || key == 'exitNo15' || key == 'exitName15' || key == 'exitNo16' || key == 'exitName16' || key == 'exitNo17' || key == 'exitName17' || key == 'systemNumber1' || key == 'lineType1' || key == 'fax' || key == 'systemNumber2' || key == 'lineType2' || key == 'internet' || key == 'systemNumber3' || key == 'lineType3' || key == 'eftpo' || key == 'systemNumber4' || key == 'lineType4' || key == 'alarm' || key == 'systemNumber5' || key == 'lineType5' || key == 'other' || key == 'systemNumber6' || key == 'lineType6' || key == 'systemNumber7' || key == 'lineType7' || key == 'systemNumber8' || key == 'lineType8' || key == 'qty1' || key == 'partName1' || key == 'systemNumber9' || key == 'lineType9' || key == 'qty2' || key == 'partName2' || key == 'systemNumber10' || key == 'lineType10' || key == 'qty3' || key == 'partName3' || key == 'systemNumber11' || key == 'lineType11' || key == 'qty4' || key == 'partName4' || key == 'systemNumber12' || key == 'lineType12' || key == 'qty5' || key == 'partName5' || key == 'footerCount' || key == 'quot_num' || key == 'tradingName' || key == 'nightVmail' || key == 'signedTimestamps' || key == 'search' || key == 'files' || key == 'autoAttendant' || key == 'faxTested' || key == 'pbx' || key == 'licensesApp' || key == 'databaseSent' || key == 'photosSent' || key == 'adslTested' || key == 'routerRemoteAccess' || key == 'clientTrained' || key == 'dayVmail' || key == 'eftposTested' || key == 'alarmTested' || key == 'comments' || key == 'cctvDisplay' || key == 'remoteContactNo' || key == 'phoneRemoteAccess') {

               } else {
                    if(value == undefined || value == "") {
                        console.log(key);
                        var name = "input[name='"+key+"']";
                        $(name).css('border', '1px solid #fbc2c4');
                        $(name).css('background', 'rgb(251, 227, 228)');
                        $('.alert-danger').removeClass('fade');
                        $('.alert-danger').addClass('show');
                        setTimeout(function(){ 
                            $(".alert-danger").addClass('fade');
                        }, 4500);
                            $scope.fieldStatus = 'required';
                            $scope.loading = false;
                    }
               }

            }


            // if($scope.jobsheetData.cctvDisplay == true) {
            //     if( key == 'footerCount' || key == 'quot_num' || key == 'tradingName' || key == 'nightVmail' || key == 'signedTimestamps' || key == 'search' || key == 'files' || key == 'autoAttendant' || key == 'faxTested' || key == 'pbx' || key == 'licensesApp' || key == 'databaseSent' || key == 'photosSent' || key == 'adslTested' || key == 'routerRemoteAccess' || key == 'clientTrained' || key == 'dayVmail' || key == 'eftposTested' || key == 'alarmTested' || key == 'comments' || key == 'cctvDisplay' || key == 'remoteContactNo' || key == 'phoneRemoteAccess') {
            //     } else {
            //         if($scope.type == 2) {

            //         } else {
            //             if(value == undefined || value == "") {
            //                 console.log(key);
            //                 var name = "input[name='"+key+"']";
            //                 $(name).css('border', '1px solid #fbc2c4');
            //                 $(name).css('background', 'rgb(251, 227, 228)');
            //                 $('.alert-danger').removeClass('fade');
            //                 $('.alert-danger').addClass('show');
            //                 setTimeout(function(){ 
            //                     $(".alert-danger").addClass('fade');
            //                 }, 4500);
            //                     $scope.fieldStatus = 'required';
            //                     $scope.loading = false;
            //             }
            //         }
            //     }
            // } else  {
            //     if(key == 'cctvDisplay' || key == 'clientTrained' || key == 'timeAndDateSet' ||key == 'mot' || key == 'adminPass' || key == 'adminU' || key == 'tcpPort' || key == 'intIpAdd' || key == 'extIpAdd' || key == 'noOfChannels' || key == 'footerCount' || key == 'quot_num' || key == 'tradingName' || key == 'nightVmail' || key == 'signedTimestamps' || key == 'search' || key == 'files' || key == 'autoAttendant' || key == 'faxTested' || key == 'pbx' || key == 'licensesApp' || key == 'databaseSent' || key == 'photosSent' || key == 'adslTested' || key == 'routerRemoteAccess' || key == 'clientTrained' || key == 'dayVmail' || key == 'eftposTested' || key == 'alarmTested' || key == 'comments' || key == 'remoteContactNo' || key == 'phoneRemoteAccess') {
            //     } else {
            //         if($scope.type == 2) {

            //         } else {
            //             if(value == undefined || value == "") {
            //                 console.log(key);
            //                 var name = "input[name='"+key+"']";
            //                 $(name).css('border', '1px solid #fbc2c4');
            //                 $(name).css('background', 'rgb(251, 227, 228)');
            //                 $('.alert-danger').removeClass('fade');
            //                 $('.alert-danger').addClass('show');
            //                 setTimeout(function(){ 
            //                     $(".alert-danger").addClass('fade');
            //                 }, 4500);
            //                     $scope.fieldStatus = 'required';
            //                     $scope.loading = false;
            //             }
            //         }
                  
            //     }
            // }
       
        });

        if( $scope.fieldStatus == 'complete') {
            var userProfile =  Scopes.get('DefaultCtrl').profile;
            console.log(userProfile);
            Jobsheets.save({
                user_id: userProfile.id,
                editId: $state.params.id,
                type:"draft", 
                status:"draft",
                data:  $scope.jobsheetData,
             }, function(data) {
                console.log(formData);
                $scope.count = 0;
                angular.forEach( $scope.jobsheetData.type, function(value, key) {
                    if (value == true) {
                        $scope.count = $scope.count + 1;
                    }
                });
                $scope.jobsheetData.footerCount = $scope.count;
                console.log($scope.jobsheetData.footerCount);
                console.log(templateName);
                var userProfile =  Scopes.get('DefaultCtrl').profile;
                var datum = $scope.jobsheetData;
                datum.quot_num = $scope.quot_num;
                datum.tradingName = datum.tradingName;
                console.log($scope.jobsheetData.schOption);
                var datePrepared = new Date();
                datum.datePrepared = datePrepared.toDateString();
                datum.preparedBy = userProfile.name;
                datum.preparedEmail = userProfile.email;
                datum.preparePhone = userProfile.phone;
                if($scope.type == '1') {
                    $scope.template = "templates/jobsheets/form-install.html?t=" + _version;        
                } else {
                    $scope.template = "templates/jobsheets/form-service.html?t=" + _version;        
                }
                $rootScope.formData = formData;
                // console.log($rootScope.formData)
                console.log(datum);
                
                ngDialog.open({
                    data: angular.toJson({
                        jobsheetData: datum
                    }),
                    template: $scope.template,
                    plan: true,
                    controller : 'JobsheetsCtrl',
                    width: 900,
                    height: 600,
                    scope: $scope,
                    className: 'ngdialog-theme-default',
                    preCloseCallback : function(value) {
                        var allSigned = true;
        
                        $(".signatureImg").each(function() {
                            console.log($(this).attr('id'),$(this).children("img").length);
                            if ($(this).children("img").length == 0) {
                                allSigned = false;
                                return;
                            }
                        });
        
                        if (!allSigned) {
                           $('#isSigned').val('F');
                           if (confirm("You have not signed all the fields.Do you want to exit?")) {
                               return true;
                           }
                           return false;
                            $timeout(angular.noop());
                        } else {
                            $('#isSigned').val('T');
                                $scope.htmlData[templateName] = [];
                                    var clone = $('.ngdialog-content').clone();
                                    clone.find('[class*="preview"]').each(function(divIndex,data){
                                        //console.log(divIndex,data,index);  
                                        if (index!=divIndex)  {
                                            clone.find('.preview-' + divIndex).remove();
                                            // console.log(clone.html());
                                            console.log($scope.items[templateName]);
                                        }                        
                                    });
                                    //clone.find('.preview-' + index).remove();
                                    var tableHtml = clone.html();
                                    $scope.htmlData[templateName].push(tableHtml);
                        
                            $scope[templateName] = true;
                            $timeout(angular.noop());
                        }
                    }
                    });
                    console.log($scope.jobsheetData.type);
                 console.log($scope.htmlData);
            });
        }
      
    }

    $scope.downloadPdf = function(id) {
        $scope.editId = id;
        console.log($scope.htmlData);
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var formHtmlData = new Array();
        var fileNameArr = new Array();
        //$interval.cancel(saveDraft);
        // $("#loading").show();
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;

        angular.forEach($scope.htmlData, function(value,key) {
            var rand = Math.floor((Math.random()*1000)+10);
            console.log($scope.jobsheetData.number);
            var str = $scope.jobsheetData.number;
            str = str.replace(/\s+/g, '_');
            str = str.replace(/[\/\\#,+$~%.'":*?<>{}]/g, '_');
            var fileName = str+rand+".pdf";
            $scope.jobsheetData.rand_num = fileName;
            fileNameArr.push(fileName);
            $("#downloadPdfBn").prop('disabled',true);
            formHtmlData.push({html:value, fileName : fileName});
        });
        // if(angular.isUndefined($scope.quot_num)){
        //   $scope.quot_num = $scope.getQuotationNumber();
        // }
        console.log(formHtmlData);
     
        console.log($scope.fieldStatus);
        if($scope.fieldStatus != 'required') {
            var pdf = Jobsheets.createPdf({
                htmlData :formHtmlData,
                js_id :$scope.editId,
                account: {name: userProfile.name, email: userProfile.email}
              },
                function(data) {
                    console.log(data);
                    if ($scope.fieldStatus == "required") {
                        $scope.loading = false;
                    } else if (data.success == "failed") {
                        $scope.loading = false;
                        FlashMessage.setMessage({
                        success:false,
                        message:"Please sign the job sheet to generate pdf."}
                        );
                        alert("Please sign the job sheet to generate pdf.");
                    } else {
                        if($scope.editId) {
                            var editId = $scope.editId;
                            $rootScope.editId = null;
                        }
                        Jobsheets.save({ data : $scope.jobsheetData,
                            user : userProfile.id,
                            editId : editId,
                            fileName : fileNameArr,
                            status :'Completed',
                            note: "finish",
                            quot_num : $scope.quot_num,
                            type:"Completed"}, function(data) {
                            if (data.success) {
                                $scope.jobsheetData = {};
                                // $("#loading").hide();
                                $scope.loading = false;
                                FlashMessage.setMessage({
                                success:true,
                                message:"Job sheet successully completed! You will receive an email shortly"}
                                );
                                alert("Job sheet successully completed! You will receive an email shortly.");
                                $window.location.reload();
                            }
                        });
                    }
                });
        } else {
            $scope.loading = false;
            FlashMessage.setMessage({
            success:false,
            message:"Please sign the job sheet to generate pdf."}
            );
            alert("Please sign the job sheet to generate pdf.");
        }
    }

    $scope.save = function(id) {
        console.log("pasok");
        $('.btn-edit').removeClass('bg-green');
        $scope.jobsheetData.userSign = $scope.userSign;
        $scope.jobsheetData.startTime  = $("#startTime").val();
        $scope.jobsheetData.finishTime  = $("#finishTime").val();
        $scope.jobsheetData.timeAndDateSet  = $("#timeAndDateSet").val();
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        Jobsheets.save({
            user_id: userProfile.id,
            editId: id,
            status:"draft", 
            data:  $scope.jobsheetData,
         }, function(data) {
             console.log(data);
            FlashMessage.setMessage(data);
            $scope.jobsheetData.message = data.message;
            if (data.success == "success") {
                $('.alert-success').removeClass('fade');
                $('.alert-success').addClass('show');
            }
            setTimeout(function(){ 
                $(".alert-success").addClass('fade');
            }, 4500);
        });
   
    }

    // $scope.getCurrentDate = function(){
    //     var currentDate = new Date();   
    //     return (currentDate.getHours() < 10 ? "0" : "") + currentDate.getHours() + ":" + (currentDate.getMinutes() < 10 ? "0" : "") + currentDate.getMinutes() + ":" + (currentDate.getSeconds() < 10 ? "0" : "") + currentDate.getSeconds()+" "+currentDate.getDate()+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getFullYear();
    // }
    // $scope.getSignedTime = function(formName,userSign) {
    //     var time = '';
    //     if(!angular.isDefined($scope.jobsheetData.signedTimestamps)) {
    //         return 'Sign Here';
    //     }
    //     if( Object.prototype.toString.call( $scope.jobsheetData.signedTimestamps ) === '[object Array]' ) {
    //         $scope.jobsheetData.signedTimestamps.forEach(function(e) {
    //             if(e.form_name==formName) {
    //                 time = e.timestamp;
    //             }
    //         });
    //     } else {
    //         if($scope.jobsheetData.signedTimestamps.form_name==formName) {
    //             time = $scope.jobsheetData.signedTimestamps.timestamp;
    //         }
    //     }
    //     var currentDate = Date.parse(time);
    //     var formatted = $filter('date')(time, 'HH:mm:ss dd-MM-yyyy');
    //     return formatted;
    // }

    $scope.isSigned = function(form) {
        if ($scope[form]) {
            return true;
        } else {
            if (form == false) {
                if ( angular.isDefined($scope.htmlData[form])) {
                    delete $scope.htmlData[form];
                }
                    $scope[form] = false;
                }
            return false;
        }
    }
    if($state.params.id != null) {
        var jobsheetData  = Jobsheets.show({
            id: $state.params.id,
       }, function(data) {
            $rootScope.jobsheetData = data.jobsheetData.data;
            $scope.jobsheetData.number = data.jobsheetData.jobsheet_number;
            $scope.jobsheetData.id = data.jobsheetData.id;
            $scope.status = data.jobsheetStatus;
            $scope.updated_at = data.jobsheetUpdatedAt.date.replace('.000000','');
            $scope.jobsheetData.files = data.jobsheetData.files;

            console.log($scope.updated_at);
            console.log(data.jobsheetData);

            if (angular.isDefined($rootScope.jobsheetData)) {
                $scope.type = data.jobsheetType;
                if($rootScope.jobsheetData.userSign != null) {
                    $scope.userSign = $rootScope.jobsheetData.userSign;
                }
                $scope.jobsheetData.companyName = $rootScope.jobsheetData.companyName;
                $scope.jobsheetData.startTime = $rootScope.jobsheetData.startTime;
                if($rootScope.jobsheetData.state != null) {
                    $scope.jobsheetData.address = $rootScope.jobsheetData.address + " " + $rootScope.jobsheetData.state + " " + $rootScope.jobsheetData.suburb + " " + $rootScope.jobsheetData.postcode;
                } else {
                    $scope.jobsheetData.address = $rootScope.jobsheetData.address;
                }
                $scope.jobsheetData.otherEqpt = $rootScope.jobsheetData.otherEqpt;
                $scope.jobsheetData.cctvDisplay = $rootScope.jobsheetData.cctvDisplay;

                angular.forEach($scope.jobsheetData.otherEqpt, function(value, key){
                    console.log(key);
                $scope.jobsheetData.schAddItems[key] = [{status:1}];
               });
                $scope.jobsheetData.finishTime = $rootScope.jobsheetData.finishTime;
                $scope.jobsheetData.systemBrand = $rootScope.jobsheetData.systemBrand;
                $scope.jobsheetData.noOfHandsets = $rootScope.jobsheetData.noOfHandsets;
                $scope.jobsheetData.typeOfHandset = $rootScope.jobsheetData.typeOfHandset;
                $scope.jobsheetData.noOfCordless = $rootScope.jobsheetData.noOfCordless;
                $scope.jobsheetData.typeOfCordless = $rootScope.jobsheetData.typeOfCordless;
                // $scope.jobsheetData.otherEqpt1 = $rootScope.jobsheetData.otherEqpt1;
                // $scope.jobsheetData.otherEqpt2 = $rootScope.jobsheetData.otherEqpt2;
                $scope.jobsheetData.phoneLocalIp = $rootScope.jobsheetData.phoneLocalIp;
                $scope.jobsheetData.phoneUsername = $rootScope.jobsheetData.phoneUsername;
                $scope.jobsheetData.phonePassword = $rootScope.jobsheetData.phonePassword;
                $scope.jobsheetData.phoneSystemBrand = $rootScope.jobsheetData.phoneSystemBrand;
                $scope.jobsheetData.phoneWebProPort = $rootScope.jobsheetData.phoneWebProPort;
                $scope.jobsheetData.phonePcProPort = $rootScope.jobsheetData.phonePcProPort;
                $scope.jobsheetData.phoneTrunkType = $rootScope.jobsheetData.phoneTrunkType;
                $scope.jobsheetData.phonePabxGateway = $rootScope.jobsheetData.phonePabxGateway;
                $scope.jobsheetData.phoneRemoteAccess = $rootScope.jobsheetData.phoneRemoteAccess;
                $scope.jobsheetData.remoteContactNo = $rootScope.jobsheetData.remoteContactNo;
                $scope.jobsheetData.phoneCustomerIp = $rootScope.jobsheetData.phoneCustomerIp;
                $scope.jobsheetData.routerIpAddress = $rootScope.jobsheetData.routerIpAddress;
                $scope.jobsheetData.routerUsername = $rootScope.jobsheetData.routerUsername;
                $scope.jobsheetData.routerPassword = $rootScope.jobsheetData.routerPassword;
                $scope.jobsheetData.routerInternetTechnology = $rootScope.jobsheetData.routerInternetTechnology;
                $scope.jobsheetData.routerWirelessName = $rootScope.jobsheetData.routerWirelessName;
                $scope.jobsheetData.routerRemoteAccess = $rootScope.jobsheetData.routerRemoteAccess;
                $scope.jobsheetData.routerCustomerIpAdd = $rootScope.jobsheetData.routerCustomerIpAdd;
                $scope.jobsheetData.exitNo1 = $rootScope.jobsheetData.exitNo1;
                $scope.jobsheetData.exitName1 = $rootScope.jobsheetData.exitName1;
                $scope.jobsheetData.exitNo2 = $rootScope.jobsheetData.exitNo2;
                $scope.jobsheetData.exitName2 = $rootScope.jobsheetData.exitName2;
                $scope.jobsheetData.exitNo3 = $rootScope.jobsheetData.exitNo3;
                $scope.jobsheetData.exitName3 = $rootScope.jobsheetData.exitName3;
                $scope.jobsheetData.exitNo4 = $rootScope.jobsheetData.exitNo4;
                $scope.jobsheetData.exitName4 = $rootScope.jobsheetData.exitName4;
                $scope.jobsheetData.exitNo5 = $rootScope.jobsheetData.exitNo5;
                $scope.jobsheetData.exitName5 = $rootScope.jobsheetData.exitName5;
                $scope.jobsheetData.exitNo6 = $rootScope.jobsheetData.exitNo6;
                $scope.jobsheetData.exitName6 = $rootScope.jobsheetData.exitName6;
                $scope.jobsheetData.exitNo7 = $rootScope.jobsheetData.exitNo7;
                $scope.jobsheetData.exitName7 = $rootScope.jobsheetData.exitName7;
                $scope.jobsheetData.exitNo8 = $rootScope.jobsheetData.exitNo8;
                $scope.jobsheetData.exitName8 = $rootScope.jobsheetData.exitName8;
                $scope.jobsheetData.exitNo9 = $rootScope.jobsheetData.exitNo9;
                $scope.jobsheetData.exitName9 = $rootScope.jobsheetData.exitName9;
                $scope.jobsheetData.exitNo10 = $rootScope.jobsheetData.exitNo10;
                $scope.jobsheetData.exitName10 = $rootScope.jobsheetData.exitName10;
                $scope.jobsheetData.exitNo11 = $rootScope.jobsheetData.exitNo11;
                $scope.jobsheetData.exitName11 = $rootScope.jobsheetData.exitName11;
                $scope.jobsheetData.exitNo12 = $rootScope.jobsheetData.exitNo12;
                $scope.jobsheetData.exitName12 = $rootScope.jobsheetData.exitName12;
                $scope.jobsheetData.exitNo13 = $rootScope.jobsheetData.exitNo13;
                $scope.jobsheetData.exitName13 = $rootScope.jobsheetData.exitName13;
                $scope.jobsheetData.exitNo14 = $rootScope.jobsheetData.exitNo14;
                $scope.jobsheetData.exitName14 = $rootScope.jobsheetData.exitName14;
                $scope.jobsheetData.exitNo15 = $rootScope.jobsheetData.exitNo15;
                $scope.jobsheetData.exitName15 = $rootScope.jobsheetData.exitName15;
                $scope.jobsheetData.exitNo16 = $rootScope.jobsheetData.exitNo16;
                $scope.jobsheetData.exitName16 = $rootScope.jobsheetData.exitName16;
                $scope.jobsheetData.exitNo17 = $rootScope.jobsheetData.exitNo17;
                $scope.jobsheetData.exitName17 = $rootScope.jobsheetData.exitName17;
                $scope.jobsheetData.noOfChannels = $rootScope.jobsheetData.noOfChannels;
                $scope.jobsheetData.extIpAdd = $rootScope.jobsheetData.extIpAdd;
                $scope.jobsheetData.intIpAdd = $rootScope.jobsheetData.intIpAdd;
                $scope.jobsheetData.tcpPort = $rootScope.jobsheetData.tcpPort;
                $scope.jobsheetData.adminU = $rootScope.jobsheetData.adminU;
                $scope.jobsheetData.adminPass = $rootScope.jobsheetData.adminPass;
                $scope.jobsheetData.mot = $rootScope.jobsheetData.mot;
                $scope.jobsheetData.timeAndDateSet = $rootScope.jobsheetData.timeAndDateSet;
                $scope.jobsheetData.clientTrained = $rootScope.jobsheetData.clientTrained;
                $scope.jobsheetData.dayVmail = $rootScope.jobsheetData.dayVmail;
                $scope.jobsheetData.nightVmail = $rootScope.jobsheetData.nightVmail;
                $scope.jobsheetData.autoAttendant = $rootScope.jobsheetData.autoAttendant;
                $scope.jobsheetData.eftposTested = $rootScope.jobsheetData.eftposTested;
                $scope.jobsheetData.alarmTested = $rootScope.jobsheetData.alarmTested;
                $scope.jobsheetData.faxTested = $rootScope.jobsheetData.faxTested;
                $scope.jobsheetData.adslTested = $rootScope.jobsheetData.adslTested;
                $scope.jobsheetData.pbx = $rootScope.jobsheetData.pbx;
                $scope.jobsheetData.ver = $rootScope.jobsheetData.ver;
                $scope.jobsheetData.licensesApp = $rootScope.jobsheetData.licensesApp;
                $scope.jobsheetData.databaseSent = $rootScope.jobsheetData.databaseSent;
                $scope.jobsheetData.photosSent = $rootScope.jobsheetData.photosSent;
                $scope.jobsheetData.systemNumber1 = $rootScope.jobsheetData.systemNumber1;
                $scope.jobsheetData.lineType1 = $rootScope.jobsheetData.lineType1;
                $scope.jobsheetData.fax = $rootScope.jobsheetData.fax;
                $scope.jobsheetData.systemNumber2 = $rootScope.jobsheetData.systemNumber2;
                $scope.jobsheetData.lineType2 = $rootScope.jobsheetData.lineType2;
                $scope.jobsheetData.internet = $rootScope.jobsheetData.internet;
                $scope.jobsheetData.systemNumber3 = $rootScope.jobsheetData.systemNumber3;
                $scope.jobsheetData.lineType3 = $rootScope.jobsheetData.lineType3;
                $scope.jobsheetData.eftpo = $rootScope.jobsheetData.eftpo;
                $scope.jobsheetData.systemNumber4 = $rootScope.jobsheetData.systemNumber4;
                $scope.jobsheetData.lineType4 = $rootScope.jobsheetData.lineType4;
                $scope.jobsheetData.alarm = $rootScope.jobsheetData.alarm;
                $scope.jobsheetData.systemNumber5 = $rootScope.jobsheetData.systemNumber5;
                $scope.jobsheetData.lineType5 = $rootScope.jobsheetData.lineType5;
                $scope.jobsheetData.other = $rootScope.jobsheetData.other;
                $scope.jobsheetData.systemNumber6 = $rootScope.jobsheetData.systemNumber6;
                $scope.jobsheetData.lineType6 = $rootScope.jobsheetData.lineType6;
                $scope.jobsheetData.systemNumber7 = $rootScope.jobsheetData.systemNumber7;
                $scope.jobsheetData.lineType7 = $rootScope.jobsheetData.lineType7;
                $scope.jobsheetData.systemNumber8 = $rootScope.jobsheetData.systemNumber8;
                $scope.jobsheetData.lineType8 = $rootScope.jobsheetData.lineType8;
                $scope.jobsheetData.qty1 = $rootScope.jobsheetData.qty1;
                $scope.jobsheetData.partName1 = $rootScope.jobsheetData.partName1;
                $scope.jobsheetData.systemNumber9 = $rootScope.jobsheetData.systemNumber9;
                $scope.jobsheetData.lineType9 = $rootScope.jobsheetData.lineType9;
                $scope.jobsheetData.qty2 = $rootScope.jobsheetData.qty2;
                $scope.jobsheetData.partName2 = $rootScope.jobsheetData.partName2;
                $scope.jobsheetData.systemNumber10 = $rootScope.jobsheetData.systemNumber10;
                $scope.jobsheetData.lineType10 = $rootScope.jobsheetData.lineType10;
                $scope.jobsheetData.qty3 = $rootScope.jobsheetData.qty3;
                $scope.jobsheetData.partName3 = $rootScope.jobsheetData.partName3;
                $scope.jobsheetData.systemNumber11 = $rootScope.jobsheetData.systemNumber11;
                $scope.jobsheetData.lineType11 = $rootScope.jobsheetData.lineType11;
                $scope.jobsheetData.qty4 = $rootScope.jobsheetData.qty4;
                $scope.jobsheetData.partName4 = $rootScope.jobsheetData.partName4;
                $scope.jobsheetData.systemNumber12 = $rootScope.jobsheetData.systemNumber12;
                $scope.jobsheetData.lineType12 = $rootScope.jobsheetData.lineType12;
                $scope.jobsheetData.qty5 = $rootScope.jobsheetData.qty5;
                $scope.jobsheetData.partName5 = $rootScope.jobsheetData.partName5;
                $scope.jobsheetData.comments = $rootScope.jobsheetData.comments;
                $scope.jobsheetData.dName = $rootScope.jobsheetData.dName;
                $scope.jobsheetData.cPosition = $rootScope.jobsheetData.cPosition;
                $scope.jobsheetData.technicianName = $rootScope.jobsheetData.technicianName;
            }
       });
    }


    var dateObj = new Date();
    var dd = dateObj.getDate();
    var mm = dateObj.getMonth()+1;
    var yy = dateObj.getFullYear();
    var today = mm+"/"+dd+"/"+yy;
    var minYY = yy - 18; //(Applicant have to be atleast 18);
    var birthMinDate = mm+"/"+dd+"/"+minYY;


    $('.startTime').datetimepicker({
        format : "DD/MM/YYYY hh:mm A",
        defaultDate : today
    });

    $('.finishTime').datetimepicker({
        format : "DD/MM/YYYY hh:mm A",
        defaultDate : today
    });

    $('.timeAndDateSet').datetimepicker({
        format : "DD/MM/YYYY hh:mm A",
        // defaultDate : today
    });
    $scope.pickDate = function() {
        $('.datePayment').datetimepicker({
            format : "DD/MM/YYYY hh:mm:ss.ss",
            defaultDate : today
        });
    }
});



angular.module('ui.filters', []).filter('unique', function () {

    return function (items, filterOn) {
  
      if (filterOn === false) {
        return items;
      }
  
      if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
        var hashCheck = {}, newItems = [];
  
        var extractValueToCompare = function (item) {
          if (angular.isObject(item) && angular.isString(filterOn)) {
            return item[filterOn];
          } else {
            return item;
          }
        };
  
        angular.forEach(items, function (item) {
          var valueToCheck, isDuplicate = false;
  
          for (var i = 0; i < newItems.length; i++) {
            if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
              isDuplicate = true;
              break;
            }
          }
          if (!isDuplicate) {
            newItems.push(item);
          }
  
        });
        items = newItems;
      }
      return items;
    };
  });