var product = function($resource){
    return $resource('product/:product_id', {product_id:'@product_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}

var category = function($resource){
    return $resource('category/:category_id/:category_level', {category_id:'@category_id', category_level:'@category_level'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false},'getCatBylevel': {method: 'GET', isArray: true}});
}

var financetype = function($resource){
    return $resource('financetype/:financetype_id', {financetype_id:'@financetype_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}

var typet = function($resource){
    return $resource('typet/:typet_id', {typet_id:'@typet_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}

var company = function($resource){
    return $resource('company/:company_id', {company_id:'@company_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}

var salesrep = function($resource){
    return $resource('salesrep/:salesrep_id', {salesrep_id:'@salesrep_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}

var telemarketer = function($resource){
    return $resource('telemarketer/:telemarketer_id', {telemarketer_id:'@telemarketer_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}

var winback = function($resource){
    return $resource('winback/:winback_id', {winback_id:'@winback_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}

var leadsource = function($resource){
    return $resource('leadsource/:leadsource_id', {leadsource_id:'@leadsource_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}


var user = function($resource){
    return $resource('user/:user_id', {user_id:'@user_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}
var users = function($resource){
    return $resource('users/:action', {}, {
                                            'showUsers': { method: "POST", params : {action : 'your-staff'}},
                                            'pickstaff': { method: "POST", params : {action : 'pick-staff'}},
                                          }
                    );
  }

  var jobsheets = function($resource){
    return $resource('jobsheets/:action', {}, {
                                            'searchCustomer': { method: "POST", params : {action : 'search-customer'}},
                                            'pickstaff': { method: "POST", params : {action : 'pick-staff'}},
                                            'show': { method: "POST", params : {action : 'show'}},
                                            'save': { method:"POST", params: {action :'save-data'}},
                                            'add': { method:"POST", params: {action :'save-data'}},
                                            'createPdf': {method : "POST", params: {action : 'create-pdf'}},
                                            'downloadForms': {method : "POST", params: {action : 'download-forms'}},
                                            'getAttached': {method : "POST", params: {action : 'uploaded-files'}},
                                          }
                    );
  }

var config = function($resource){
    return $resource('config/:config_id',{config_id:'@config_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}


var sellprice = function($resource){
    return $resource('sellprice/:id',{id:'@id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}


var rentalprice = function($resource){
    return $resource('rentalprice/:id',{id:'@id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
}

var flexirent = function($resource){
    return $resource('flexirent/:id',{id:'@id'} , {'query': {method: 'GET', isArray: false},'update': {method: 'PUT', isArray: false}});
}


var proposal = function($resource){
    return $resource('proposal/:id',{id:'@id'} , {'query': {method: 'GET', isArray: true},
                                                  'update': {method: 'PUT', isArray: false}
                    });                    
}

var proposals = function($resource){
  return $resource('proposals/:action', {}, {'createPdf': { method:"POST", params: {action:'create-pdf'}},
                                            'save': { method:"POST", params: {action :'save-data'}},
                                            'show': { method: "POST", params : {action : 'show-data'}},
                                            'delete': {method: "POST", params : {action : 'delete-data'}},
                                            'downloadForms': {method : "POST", params: {action : 'download-forms'}},
                                            'showSalesRepProposals' :{ method: "POST", params : {action : 'show-sales-rep-proposal-data'}},

                                        }
                  );
}
var dashboard = function($resource){
    return $resource('dashboard/:action',{} , {'checkMove': {method: 'POST',params: {action :'check-move'}},
                                               'createPerMonthSales': { method:"POST", params: {action:'create-month-sales'}},
                                               'userList': { method:"POST", params: {action:'user-list'}},
                                               'chartFormData': { method:"POST", params: {action:'chart-formdata'}},
                                               'chartProposalData': { method:"POST", params: {action:'chart-proposaldata'}},
                                          }
                    );
  }
var proposalMove = function($resource){
    return $resource('proposal/:id',{} , {'movetoForm': {method: 'GET', params : {}},
                    });                    
}

var feedbacks = function($resource){
    return $resource('feedbacks/:id',{id:'@id'} , {'query': {method: 'GET', isArray: true},
                                                  'update': {method: 'PUT', isArray: false}
                    });
}

var scheduleGoods = function($resource){
    return $resource('schedule_goods/:id', {id:'@id'} , {'query': {method: 'GET', isArray: true},
                                                  'update': {method: 'PUT', isArray: false}
                    });                    
}

var products = function($resource) { 
    return $resource('proposal/view-products/:id', {id:'@id'} , {'query': { method: 'GET', isArray: false }});
}


var productRules = function($resource) { return $resource('proposal/get-product-rules/:id', {id:'@id'}, {'query':{method:'GET',isArray:true}});
}

var associatedProduct = function($resource){    
    return $resource('product/:product_id', {product_id:'@product_id'} , {'query': {method: 'GET', isArray: false},
                                                                          'update': {method: 'PUT', isArray: false}
                                                                         }
                    );
}


// var scheduleGoods = function($resource){
//     return $resource('schedule_goods/:schedule_goods_id', {schedule_goods_id:'@schedule_goods_id'} , {'query': {method: 'GET', isArray: true},'update': {method: 'PUT', isArray: false}});
// }



var rates = function($resource){
    return $resource('rates/:id', {id:'@id'} , {'query': {method: 'GET', isArray: true},
                                                  'update': {method: 'PUT', isArray: false}
                    });                    
}

var form = function($resource) {
    return $resource('form/:action', {}, {'update': { method:"POST", params: {action:'create-pdf'}},
                                          'save': { method:"POST", params: {action :'save-data'}},
                                          'show' :{ method: "POST", params : {action : 'show-data'}},
                                          'showSalesRepForms' :{ method: "POST", params : {action : 'show-sales-rep-form-data'}},
                                          'delete':{ method : "POST", params: {action : 'delete-data'}},
                                          'getScheduleGoods':{ method: "POST", params: {action : 'get-schedule-goods'}},
                                          'getRates':{ method: "POST", params: {action : 'get-rates'}},
                                          'downloadForms': {method : "POST", params: {action : 'download-forms'}},
                                          'view_referals' : {method : "POST", params: {action : 'view-referals'}},
                                          'to_csv' : {method : "POST", params: {action : 'to-csv'}},
                                          'view_all' : {method : "POST", params: {action : 'view-all'}},
                                          'send_referrals' : {method : "POST", params: {action : 'send-referrals'}},
                                          'showPaycorp' : {method : "POST", params: {action : 'show-form'}},
                                          'getToken' : {method : "POST", params: {action: 'get-paycorp-token'}},
                                          'sendCustomerEmail' : {method : "POST", params: {action: 'send-customer-email'}},
                                         }
                    );
}

var customerSign = function($resource) {
  return $resource('customersign/:action', {}, {'update': { method:"POST", params: {action:'create-pdf'}},
                                        'save': { method:"POST", params: {action :'save-data'}},
                                        'show' :{ method: "POST", params : {action : 'show-data'}},
                                        'complete' :{ method: "POST", params : {action : 'complete'}},
                                       }
                  );
}

var cloud = function($resource) {
    return $resource('cloud/:action', {}, {'update': { method:"POST", params: {action:'create-pdf'}},
                                          'save': { method:"POST", params: {action :'save-data'}},
                                          'show' :{ method: "POST", params : {action : 'show-data'}},
                                          'delete':{ method : "POST", params: {action : 'delete-data'}},
                                          'getScheduleGoods':{ method: "POST", params: {action : 'get-schedule-goods'}},
                                          'downloadForms': {method : "POST", params: {action : 'download-forms'}},
                                          
                                         }
                    );
}

var flashMessage = function($rootScope) {
var queue = [];
var currentMessage = "";


  return {
    message:'',
    icon_font:'',
    bg:'',
    header:'',
    setMessage: function(message) {
      if(message.success){
        this.icon_font = 'font-blue';
        this.bg = 'alert-success';
        this.header='Success!';
      } else {
        this.icon_font = 'font-red';
        this.bg        = 'alert-danger';
        this.header    = 'Oops!';
      }
      this.message = message.message;
    },
    getMessage: function() {
      var msg = this.message;
      this.message = '';
      return msg;
    },
    getBg:function(){
      return this.bg;
    },
    getIconFont:function(){
      return this.icon_font;
    },
    getHeaderMessage:function(){
      return this.header;
    }

  };
};

var frontierQualification = function($resource){
    return $resource('frontier/:action',{},{'get_addresses': { method:"POST", params: {action:'get-addresses'}},
                                            'get_advance': { method:"POST", params: {action:'get-advance'}},
                                            'get_fnn': { method:"POST", params: {action:'get-fnn'}},
                                            'qualify': { method:"POST", params: {action:'qualify'}},
                                            'sq_Csv' : {method:"POST",params:{action:'sqCsv'}}
                                           }
                    );
}

Qms.factory('Product', product);
Qms.factory('Rates', rates);
Qms.factory('Feedbacks', feedbacks);
Qms.factory('ScheduleGoods',scheduleGoods);
Qms.factory('Category', category);
Qms.factory('FinanceType', financetype);
Qms.factory('Typet', typet);
Qms.factory('Company', company);
Qms.factory('Salesrep', salesrep);
Qms.factory('Telemarketer', telemarketer);
Qms.factory('Winback', winback);
Qms.factory('Leadsource', leadsource);
Qms.factory('User', user);
Qms.factory('Users', users);
Qms.factory('Jobsheets', jobsheets);
Qms.factory('FlashMessage', flashMessage);
Qms.factory('Config', config);
Qms.factory('SellPrice', sellprice);
Qms.factory('RentalPrice', rentalprice);
Qms.factory('FlexiRent', flexirent);
Qms.factory('Proposals', proposal);
Qms.factory('Products', products);
Qms.factory('ProductRules',productRules);
Qms.factory('AssociatedProduct',associatedProduct);
Qms.factory('Form', form);
Qms.factory('FrontierQualification',frontierQualification);
Qms.factory('ProposalActions',proposals);
Qms.factory('Dashboard',dashboard);
Qms.factory('ProposalMove',proposalMove);
Qms.factory('Cloud',cloud);
Qms.factory('CustomerSign', customerSign);
